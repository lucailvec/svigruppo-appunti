\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\contentsline {part}{\@mypartnumtocformat {I}{Part One}}{7}{part.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {0.0.1}Introduzione}{9}{subsection.0.0.1}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Cap. 1 - The tar pit}{9}{section*.2}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Cap. 2 - MMM}{9}{section*.3}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Cap. 3 - *The Surgical Team}{10}{section*.4}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{No silver bullet}{10}{section*.5}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Second system effect}{10}{section*.6}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{The tendency towards irreducible number of errors}{10}{section*.7}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Cattedrale, bazaar, kibbutz}{11}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Modelli organizzativi}{12}{section.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1.1}Bazaar}{12}{subsection.1.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Come utilizzo al meglio bazaar?}{12}{section*.8}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Un problema pi\IeC {\`u} grande (ma vicino a noi)}{13}{section.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2.1}Distribuzioni}{13}{subsection.1.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2.2}Fissa sul Debian}{13}{subsection.1.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2.3}Package}{13}{subsection.1.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Quale \IeC {\`e} il modello che risponde alle nostre necessit\IeC {\`a}?}{13}{section.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}From Bazaar to Kibbutz: How Freedom Deals with Coherence in the Debian Project}{15}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Distribuzioni}{15}{section*.9}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{Il valore delle distribuzioni}{15}{section*.10}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{I pacchetti}{16}{section*.11}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{Particolarit\IeC {\`a} di Debian}{16}{section*.12}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{Kibbutz}{16}{section*.13}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Metodologie agili}{17}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Riassunto}{17}{section*.14}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Metodologie agili}{17}{section.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.1}Il manifesto agile}{18}{subsection.3.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Principi}{18}{section*.15}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Un manifesto politico}{18}{section*.16}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{I veri punti chiave}{18}{section*.17}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{team piccoli}{18}{section*.18}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{Decisioni upfront}{18}{section*.19}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{Misura del progresso}{18}{section*.20}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{Test:}{19}{section*.21}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{Committenza}{19}{section*.22}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Semplicit\IeC {\`a} e minimalismo}{19}{section*.23}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{User stories e test}{19}{section*.24}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{TDD}{19}{section*.25}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.2}SCRUM}{20}{subsection.3.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Tecniche di lavoro}{20}{section*.26}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Task board}{21}{section*.27}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Velocity tracking}{21}{section*.28}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Pair programming}{21}{section*.29}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Codice condiviso}{22}{section*.30}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Refactoring}{22}{section*.31}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Test 2-mock}{22}{section*.32}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Documentazione componenti}{23}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Dipendenze}{23}{section*.33}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.0.1}Librerie di supporto/dipendenze di sviluppo}{23}{subsection.4.0.1}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Dipendenze}{24}{section*.34}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Esempio: python}{24}{section*.35}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{Problemi}{24}{section*.36}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}configurazione ambiente di lavoro pt2 (Build)}{24}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Dependency hell}{24}{section*.37}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Build automation}{25}{section.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.1}Make}{25}{subsection.4.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Portabilit\IeC {\`a} della costruzione}{25}{section*.38}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{(Esempio) Non-portability del C}{26}{section*.39}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{Come risolvevamo questo problema}{26}{section*.40}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Automake}{26}{section*.41}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Configure}{26}{section*.42}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Autoconf}{26}{section*.43}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{configure process}{26}{section*.44}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.2}Build automation tool (quelli veri)}{27}{subsection.4.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Ant}{27}{section*.45}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{Come \IeC {\`e} fatto ant}{27}{section*.46}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Gradle}{27}{section*.47}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{Esempio gradle}{27}{section*.48}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.3}Continous integration}{28}{subsection.4.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{CI}{28}{section*.49}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}La documentazione}{29}{section.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.1}La suddivisione del lavoro sw e gli interface fault}{29}{subsection.4.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Correttezza e robustezza}{29}{section*.50}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{What and how}{29}{section*.51}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Specifiche nel lavoro di gruppo}{30}{section*.52}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{La suddivisione non vuol dire isolamento}{30}{section*.53}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.4}La soluzione ad interface fault}{30}{section.4.4}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Assertion}{30}{section*.54}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{Primo caso di asserzioni Rosenblum}{31}{section*.55}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{Assume}{31}{section*.56}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{Promise}{31}{section*.57}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{Return}{31}{section*.58}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Classificazione delle asserzioni}{31}{section*.59}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.4.1}Design by contract}{31}{subsection.4.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Hoare triple}{32}{section*.60}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Contratti}{32}{section*.61}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{Casi limite}{32}{section*.62}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Design by contract}{33}{chapter.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.0.1}Contratti}{33}{subsection.5.0.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Eiffel}{33}{section.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Esempio}{33}{section*.63}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Organizzazione delle asserzioni}{34}{section*.64}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{Invarianti di classe}{34}{section*.65}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Contratti ed ereditariet\IeC {\`a}}{34}{section*.66}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{Principio di sostituibilit\IeC {\`a}}{34}{section*.67}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{Quindi in pratica}{35}{section*.68}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Eccezioni}{35}{section*.69}
\defcounter {refsection}{0}\relax 
\contentsline {part}{\@mypartnumtocformat {II}{BELLETS}}{37}{part.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {6}Software Configuration Management}{39}{chapter.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.1}SCM}{39}{section.6.1}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Di cosa si occupano?}{39}{section*.70}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{Lavorer\IeC {\`a} su due livelli diversi.}{39}{section*.71}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Gli strumenti di SCM}{40}{section*.72}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1.1}Due decisioni importanti}{40}{subsection.6.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1.2}Il meccanismo base}{40}{subsection.6.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Perch\IeC {\`e} la riga?}{40}{section*.73}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Check-out}{40}{section*.74}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Check-in o commit}{40}{section*.75}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1.3}La regolazione del lavoro concorrente}{40}{subsection.6.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Merge}{41}{section*.76}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1.4}3-way merge}{41}{subsection.6.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {7}GIT 1}{43}{chapter.7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.1}Internals}{43}{section.7.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.1.1}Comandi base}{43}{subsection.7.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{git init}{43}{section*.77}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{git status}{44}{section*.78}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{git add \textit {file}}{44}{section*.79}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{Registrazione versione}{44}{section*.80}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{git commit}{44}{section*.81}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Aggiungo un file uguale}{44}{section*.82}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.1.2}Struttura file}{44}{subsection.7.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.2}Contenuto di un .git/objects/XX/file}{45}{section.7.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.3}Ricostruisco workingdirectory da .git}{45}{section.7.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.4}Bare}{46}{section.7.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.4.1}Elementi essenziali del repository}{46}{subsection.7.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.5}L'index}{46}{section.7.5}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {8}GIT 2}{47}{chapter.8}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Ripasso}{47}{section*.83}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.1}Branch}{47}{section.8.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {8.1.1}git commit --ammend}{47}{subsection.8.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {8.1.2}.git/logs/HEAD}{48}{subsection.8.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {8.1.3}git reflogs}{48}{subsection.8.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.2}come riferirsi a un commit}{48}{section.8.2}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Esercizio}{48}{section*.84}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.3}git diff}{48}{section.8.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {9}GIT 3}{49}{chapter.9}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {9.0.1}File non committati}{49}{subsection.9.0.1}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{git add -p}{49}{section*.85}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{git merge}{49}{section*.86}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{Caso particolare}{51}{section*.87}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{REBASE}{51}{section*.88}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{git rebase --onto}{51}{section*.89}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{bho}{51}{section*.90}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {10}GIT 4}{53}{chapter.10}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {10.0.1}git bisect}{53}{subsection.10.0.1}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Esercizi}{53}{section*.91}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {10.0.2}git blame}{53}{subsection.10.0.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {10.0.3}git stash}{53}{subsection.10.0.3}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Pulling into a dirty tree}{54}{section*.92}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{Metodo brutto}{54}{section*.93}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {10.0.4}hooks}{54}{subsection.10.0.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {10.0.5}git submodule}{54}{subsection.10.0.5}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {11}GIT ONLINE}{55}{chapter.11}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {11.1}Comandi plumbing}{55}{section.11.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {11.1.1}Git references}{56}{subsection.11.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{HEAD}{56}{section*.94}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{TAG}{56}{section*.95}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{remotes}{57}{section*.96}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {11.2}Figure}{57}{section.11.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\leavevmode {\color {ocre}Bibliography}}{59}{chapter*.97}
\defcounter {refsection}{0}\relax 
\contentsline {section}{Books}{59}{section*.98}
\defcounter {refsection}{0}\relax 
\contentsline {section}{Articles}{59}{section*.99}
\contentsfinish 
