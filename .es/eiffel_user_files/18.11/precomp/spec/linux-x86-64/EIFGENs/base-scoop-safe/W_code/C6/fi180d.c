/*
 * Class FIBONACCI
 */

#include "eif_macros.h"


#ifdef __cplusplus
extern "C" {
#endif

static const EIF_TYPE_INDEX egt_0_180 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX egt_1_180 [] = {0xFF01,245,179,0xFFFF};
static const EIF_TYPE_INDEX egt_2_180 [] = {0xFF01,179,0xFFFF};
static const EIF_TYPE_INDEX egt_3_180 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX egt_4_180 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX egt_5_180 [] = {0xFF01,179,0xFFFF};
static const EIF_TYPE_INDEX egt_6_180 [] = {0xFF01,179,0xFFFF};
static const EIF_TYPE_INDEX egt_7_180 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX egt_8_180 [] = {0xFF01,16,0xFFFF};
static const EIF_TYPE_INDEX egt_9_180 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX egt_10_180 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX egt_11_180 [] = {0xFF01,17,0xFFFF};
static const EIF_TYPE_INDEX egt_12_180 [] = {0xFF01,179,0xFFFF};
static const EIF_TYPE_INDEX egt_13_180 [] = {0xFF01,457,218,0xFFFF};
static const EIF_TYPE_INDEX egt_14_180 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX egt_15_180 [] = {0xFF01,817,203,0xFFFF};
static const EIF_TYPE_INDEX egt_16_180 [] = {0xFF01,179,0xFFFF};
static const EIF_TYPE_INDEX egt_17_180 [] = {218,0xFFFF};


static const struct desc_info desc_180[] = {
	{EIF_GENERIC(NULL), 0xFFFFFFFF, 0xFFFFFFFF},
	{EIF_GENERIC(egt_0_180), 0, 0xFFFFFFFF},
	{EIF_GENERIC(egt_1_180), 1, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x0197 /*203*/), 2, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x0197 /*203*/), 3, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x0197 /*203*/), 4, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x0197 /*203*/), 5, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x0197 /*203*/), 6, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x0197 /*203*/), 7, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x0197 /*203*/), 8, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x0197 /*203*/), 9, 0xFFFFFFFF},
	{EIF_GENERIC(egt_2_180), 10, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 11, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 12, 0xFFFFFFFF},
	{EIF_GENERIC(egt_3_180), 13, 0xFFFFFFFF},
	{EIF_GENERIC(egt_4_180), 14, 0xFFFFFFFF},
	{EIF_GENERIC(egt_5_180), 15, 0xFFFFFFFF},
	{EIF_GENERIC(egt_6_180), 16, 0xFFFFFFFF},
	{EIF_GENERIC(egt_7_180), 17, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 18, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 19, 0xFFFFFFFF},
	{EIF_GENERIC(egt_8_180), 20, 0xFFFFFFFF},
	{EIF_GENERIC(egt_9_180), 21, 0xFFFFFFFF},
	{EIF_GENERIC(egt_10_180), 22, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 23, 0xFFFFFFFF},
	{EIF_GENERIC(egt_11_180), 24, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 25, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 26, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 27, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x0167 /*179*/), 28, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x01C7 /*227*/), 29, 0xFFFFFFFF},
	{EIF_GENERIC(egt_12_180), 30, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x0197 /*203*/), 9056, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x0197 /*203*/), 3353, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x0197 /*203*/), 3205, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x0197 /*203*/), 3206, 0},
	{EIF_NON_GENERIC(0x0197 /*203*/), 3207, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 3208, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 3209, 0xFFFFFFFF},
	{EIF_GENERIC(egt_13_180), 3953, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x01B5 /*218*/), 3210, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x0197 /*203*/), 3354, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x01B5 /*218*/), 3960, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x0197 /*203*/), 3681, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 3950, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 3682, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 3683, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x0197 /*203*/), 3684, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x0197 /*203*/), 3685, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x01B5 /*218*/), 3689, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 3677, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x01B5 /*218*/), 3959, 4},
	{EIF_NON_GENERIC(0x01B5 /*218*/), 3678, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x01B5 /*218*/), 3679, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x0197 /*203*/), 3680, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x0197 /*203*/), 3961, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 3952, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 3949, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x01B5 /*218*/), 3959, 4},
	{EIF_NON_GENERIC(0x0197 /*203*/), 3944, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x0197 /*203*/), 3945, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x0197 /*203*/), 3277, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 3955, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 3951, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 3278, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 3954, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 3279, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 3958, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x01B5 /*218*/), 3960, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x0197 /*203*/), 3946, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x0197 /*203*/), 3947, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x0197 /*203*/), 3948, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 3957, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 3956, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x01B5 /*218*/), 3678, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x01B5 /*218*/), 3960, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x0197 /*203*/), 3961, 0xFFFFFFFF},
	{EIF_GENERIC(NULL), 3949, 0xFFFFFFFF},
	{EIF_GENERIC(egt_14_180), 0x00, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x01B5 /*218*/), 9051, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x01B5 /*218*/), 9052, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x01B5 /*218*/), 9053, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x01B5 /*218*/), 9054, 0xFFFFFFFF},
	{EIF_GENERIC(egt_15_180), 9055, 0xFFFFFFFF},
	{EIF_NON_GENERIC(0x01B5 /*218*/), 9057, 0xFFFFFFFF},
	{EIF_GENERIC(egt_16_180), 9058, 0xFFFFFFFF},
	{EIF_GENERIC(egt_17_180), 0x00, 0xFFFFFFFF},
};
void Init180(void)
{
	IDSC(desc_180, 0, 179);
	IDSC(desc_180 + 1, 1, 179);
	IDSC(desc_180 + 32, 78, 179);
	IDSC(desc_180 + 41, 36, 179);
	IDSC(desc_180 + 42, 30, 179);
	IDSC(desc_180 + 49, 165, 179);
	IDSC(desc_180 + 58, 153, 179);
	IDSC(desc_180 + 59, 297, 179);
	IDSC(desc_180 + 68, 257, 179);
	IDSC(desc_180 + 74, 240, 179);
	IDSC(desc_180 + 75, 102, 179);
	IDSC(desc_180 + 79, 207, 179);
	IDSC(desc_180 + 84, 223, 179);
	IDSC(desc_180 + 85, 124, 179);
}


#ifdef __cplusplus
}
#endif
