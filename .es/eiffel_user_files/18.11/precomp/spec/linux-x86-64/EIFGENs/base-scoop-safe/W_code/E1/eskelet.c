#include "eif_eiffel.h"

#ifdef __cplusplus
extern "C" {
#endif

extern const char *names7[];
static const uint32 types7 [] =
{
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags7 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype7_0 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype7_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype7_2 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype7_3 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes7 [] = {
g_atype7_0,
g_atype7_1,
g_atype7_2,
g_atype7_3,
};

static const int32 cn_attr7 [] =
{
53,
54,
55,
52,
};

extern const char *names11[];
static const uint32 types11 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
};

static const uint16 attr_flags11 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype11_0 [] = {444,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype11_1 [] = {644,188,0xFFFF};
static const EIF_TYPE_INDEX g_atype11_2 [] = {680,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype11_3 [] = {771,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype11_4 [] = {391,191,0xFFFF};
static const EIF_TYPE_INDEX g_atype11_5 [] = {608,206,0xFFFF};
static const EIF_TYPE_INDEX g_atype11_6 [] = {807,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype11_7 [] = {752,197,0xFFFF};
static const EIF_TYPE_INDEX g_atype11_8 [] = {553,227,0xFFFF};
static const EIF_TYPE_INDEX g_atype11_9 [] = {324,0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype11_10 [] = {427,209,0xFFFF};
static const EIF_TYPE_INDEX g_atype11_11 [] = {354,215,0xFFFF};
static const EIF_TYPE_INDEX g_atype11_12 [] = {716,212,0xFFFF};
static const EIF_TYPE_INDEX g_atype11_13 [] = {867,224,0xFFFF};

static const EIF_TYPE_INDEX *gtypes11 [] = {
g_atype11_0,
g_atype11_1,
g_atype11_2,
g_atype11_3,
g_atype11_4,
g_atype11_5,
g_atype11_6,
g_atype11_7,
g_atype11_8,
g_atype11_9,
g_atype11_10,
g_atype11_11,
g_atype11_12,
g_atype11_13,
};

static const int32 cn_attr11 [] =
{
246,
247,
248,
249,
250,
251,
252,
253,
254,
255,
256,
257,
258,
259,
};

extern const char *names12[];
static const uint32 types12 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags12 [] =
{0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype12_0 [] = {240,0xFFFF};
static const EIF_TYPE_INDEX g_atype12_1 [] = {240,0xFFFF};
static const EIF_TYPE_INDEX g_atype12_2 [] = {0xFF01,597,0xFF01,0xFFF9,9,186,0xFF01,232,0xFF01,232,218,218,218,218,203,203,203,0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype12_3 [] = {0xFF01,441,0xFF01,0xFFF9,9,186,0xFF01,232,0xFF01,232,218,218,218,218,203,203,203,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype12_4 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype12_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype12_6 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype12_7 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype12_8 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype12_9 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes12 [] = {
g_atype12_0,
g_atype12_1,
g_atype12_2,
g_atype12_3,
g_atype12_4,
g_atype12_5,
g_atype12_6,
g_atype12_7,
g_atype12_8,
g_atype12_9,
};

static const int32 cn_attr12 [] =
{
273,
274,
275,
276,
267,
268,
269,
270,
271,
272,
};

extern const char *names15[];
static const uint32 types15 [] =
{
SK_REF,
};

static const uint16 attr_flags15 [] =
{0,};

static const EIF_TYPE_INDEX g_atype15_0 [] = {0xFF01,238,0xFFFF};

static const EIF_TYPE_INDEX *gtypes15 [] = {
g_atype15_0,
};

static const int32 cn_attr15 [] =
{
298,
};

extern const char *names17[];
static const uint32 types17 [] =
{
SK_REF,
};

static const uint16 attr_flags17 [] =
{0,};

static const EIF_TYPE_INDEX g_atype17_0 [] = {175,0xFFFF};

static const EIF_TYPE_INDEX *gtypes17 [] = {
g_atype17_0,
};

static const int32 cn_attr17 [] =
{
428,
};

extern const char *names19[];
static const uint32 types19 [] =
{
SK_UINT32,
};

static const uint16 attr_flags19 [] =
{1,};

static const EIF_TYPE_INDEX g_atype19_0 [] = {212,0xFFFF};

static const EIF_TYPE_INDEX *gtypes19 [] = {
g_atype19_0,
};

static const int32 cn_attr19 [] =
{
507,
};

extern const char *names30[];
static const uint32 types30 [] =
{
SK_REF,
SK_CHAR8,
SK_CHAR8,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags30 [] =
{0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype30_0 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype30_1 [] = {197,0xFFFF};
static const EIF_TYPE_INDEX g_atype30_2 [] = {197,0xFFFF};
static const EIF_TYPE_INDEX g_atype30_3 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype30_4 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype30_5 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype30_6 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype30_7 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes30 [] = {
g_atype30_0,
g_atype30_1,
g_atype30_2,
g_atype30_3,
g_atype30_4,
g_atype30_5,
g_atype30_6,
g_atype30_7,
};

static const int32 cn_attr30 [] =
{
772,
766,
768,
770,
773,
767,
769,
771,
};

extern const char *names34[];
static const uint32 types34 [] =
{
SK_REF,
};

static const uint16 attr_flags34 [] =
{0,};

static const EIF_TYPE_INDEX g_atype34_0 [] = {0xFF01,262,0xFF01,0xFFF9,5,186,0xFF01,245,0,232,232,218,203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes34 [] = {
g_atype34_0,
};

static const int32 cn_attr34 [] =
{
871,
};

extern const char *names36[];
static const uint32 types36 [] =
{
SK_UINT32,
};

static const uint16 attr_flags36 [] =
{0,};

static const EIF_TYPE_INDEX g_atype36_0 [] = {212,0xFFFF};

static const EIF_TYPE_INDEX *gtypes36 [] = {
g_atype36_0,
};

static const int32 cn_attr36 [] =
{
958,
};

extern const char *names37[];
static const uint32 types37 [] =
{
SK_REF,
};

static const uint16 attr_flags37 [] =
{0,};

static const EIF_TYPE_INDEX g_atype37_0 [] = {320,0xFF01,14,0xFFFF};

static const EIF_TYPE_INDEX *gtypes37 [] = {
g_atype37_0,
};

static const int32 cn_attr37 [] =
{
967,
};

extern const char *names39[];
static const uint32 types39 [] =
{
SK_BOOL,
};

static const uint16 attr_flags39 [] =
{0,};

static const EIF_TYPE_INDEX g_atype39_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes39 [] = {
g_atype39_0,
};

static const int32 cn_attr39 [] =
{
995,
};

extern const char *names40[];
static const uint32 types40 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags40 [] =
{0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype40_0 [] = {0xFF01,163,0xFFFF};
static const EIF_TYPE_INDEX g_atype40_1 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX g_atype40_2 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype40_3 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype40_4 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype40_5 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype40_6 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype40_7 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes40 [] = {
g_atype40_0,
g_atype40_1,
g_atype40_2,
g_atype40_3,
g_atype40_4,
g_atype40_5,
g_atype40_6,
g_atype40_7,
};

static const int32 cn_attr40 [] =
{
1040,
1041,
995,
1037,
1045,
1042,
1043,
1046,
};

extern const char *names41[];
static const uint32 types41 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags41 [] =
{0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype41_0 [] = {0xFF01,163,0xFFFF};
static const EIF_TYPE_INDEX g_atype41_1 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX g_atype41_2 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype41_3 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype41_4 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype41_5 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype41_6 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype41_7 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes41 [] = {
g_atype41_0,
g_atype41_1,
g_atype41_2,
g_atype41_3,
g_atype41_4,
g_atype41_5,
g_atype41_6,
g_atype41_7,
};

static const int32 cn_attr41 [] =
{
1040,
1041,
995,
1037,
1045,
1042,
1043,
1046,
};

extern const char *names48[];
static const uint32 types48 [] =
{
SK_REF,
SK_BOOL,
SK_POINTER,
};

static const uint16 attr_flags48 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype48_0 [] = {163,0xFFFF};
static const EIF_TYPE_INDEX g_atype48_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype48_2 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes48 [] = {
g_atype48_0,
g_atype48_1,
g_atype48_2,
};

static const int32 cn_attr48 [] =
{
1105,
1100,
1104,
};

extern const char *names49[];
static const uint32 types49 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
};

static const uint16 attr_flags49 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype49_0 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX g_atype49_1 [] = {262,0xFF01,0xFFF9,1,186,0xFF01,129,0xFFFF};
static const EIF_TYPE_INDEX g_atype49_2 [] = {262,0xFF01,0xFFF9,2,186,0xFF01,129,0xFF01,129,0xFFFF};
static const EIF_TYPE_INDEX g_atype49_3 [] = {262,0xFF01,0xFFF9,1,186,0xFF05,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype49_4 [] = {320,0xFF05,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype49_5 [] = {525,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype49_6 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype49_7 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype49_8 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype49_9 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype49_10 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype49_11 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes49 [] = {
g_atype49_0,
g_atype49_1,
g_atype49_2,
g_atype49_3,
g_atype49_4,
g_atype49_5,
g_atype49_6,
g_atype49_7,
g_atype49_8,
g_atype49_9,
g_atype49_10,
g_atype49_11,
};

static const int32 cn_attr49 [] =
{
1106,
1107,
1108,
1109,
1110,
1111,
1112,
1115,
1116,
1117,
1118,
1119,
};

extern const char *names50[];
static const uint32 types50 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
};

static const uint16 attr_flags50 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype50_0 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX g_atype50_1 [] = {262,0xFF01,0xFFF9,1,186,0xFF01,129,0xFFFF};
static const EIF_TYPE_INDEX g_atype50_2 [] = {262,0xFF01,0xFFF9,2,186,0xFF01,129,0xFF01,129,0xFFFF};
static const EIF_TYPE_INDEX g_atype50_3 [] = {262,0xFF01,0xFFF9,1,186,0xFF05,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype50_4 [] = {320,0xFF05,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype50_5 [] = {525,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype50_6 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype50_7 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype50_8 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype50_9 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype50_10 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype50_11 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes50 [] = {
g_atype50_0,
g_atype50_1,
g_atype50_2,
g_atype50_3,
g_atype50_4,
g_atype50_5,
g_atype50_6,
g_atype50_7,
g_atype50_8,
g_atype50_9,
g_atype50_10,
g_atype50_11,
};

static const int32 cn_attr50 [] =
{
1106,
1107,
1108,
1109,
1110,
1111,
1112,
1115,
1116,
1117,
1118,
1119,
};

extern const char *names51[];
static const uint32 types51 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
};

static const uint16 attr_flags51 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype51_0 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX g_atype51_1 [] = {262,0xFF01,0xFFF9,1,186,0xFF01,129,0xFFFF};
static const EIF_TYPE_INDEX g_atype51_2 [] = {262,0xFF01,0xFFF9,2,186,0xFF01,129,0xFF01,129,0xFFFF};
static const EIF_TYPE_INDEX g_atype51_3 [] = {262,0xFF01,0xFFF9,1,186,0xFF05,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype51_4 [] = {320,0xFF05,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype51_5 [] = {525,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype51_6 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype51_7 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype51_8 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype51_9 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype51_10 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype51_11 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes51 [] = {
g_atype51_0,
g_atype51_1,
g_atype51_2,
g_atype51_3,
g_atype51_4,
g_atype51_5,
g_atype51_6,
g_atype51_7,
g_atype51_8,
g_atype51_9,
g_atype51_10,
g_atype51_11,
};

static const int32 cn_attr51 [] =
{
1106,
1107,
1108,
1109,
1110,
1111,
1112,
1115,
1116,
1117,
1118,
1119,
};

extern const char *names57[];
static const uint32 types57 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
};

static const uint16 attr_flags57 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype57_0 [] = {0xFF01,867,224,0xFFFF};
static const EIF_TYPE_INDEX g_atype57_1 [] = {0xFF01,867,224,0xFFFF};
static const EIF_TYPE_INDEX g_atype57_2 [] = {0xFF01,867,224,0xFFFF};
static const EIF_TYPE_INDEX g_atype57_3 [] = {0xFF01,867,224,0xFFFF};

static const EIF_TYPE_INDEX *gtypes57 [] = {
g_atype57_0,
g_atype57_1,
g_atype57_2,
g_atype57_3,
};

static const int32 cn_attr57 [] =
{
1198,
1199,
1200,
1201,
};

extern const char *names58[];
static const uint32 types58 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags58 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype58_0 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype58_1 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype58_2 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype58_3 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype58_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype58_5 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype58_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes58 [] = {
g_atype58_0,
g_atype58_1,
g_atype58_2,
g_atype58_3,
g_atype58_4,
g_atype58_5,
g_atype58_6,
};

static const int32 cn_attr58 [] =
{
1204,
1205,
1202,
1203,
1211,
1219,
1220,
};

extern const char *names59[];
static const uint32 types59 [] =
{
SK_REF,
SK_REF,
SK_CHAR8,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_UINT64,
SK_UINT64,
};

static const uint16 attr_flags59 [] =
{0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype59_0 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype59_1 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype59_2 [] = {197,0xFFFF};
static const EIF_TYPE_INDEX g_atype59_3 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype59_4 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype59_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype59_6 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype59_7 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype59_8 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype59_9 [] = {224,0xFFFF};
static const EIF_TYPE_INDEX g_atype59_10 [] = {224,0xFFFF};

static const EIF_TYPE_INDEX *gtypes59 [] = {
g_atype59_0,
g_atype59_1,
g_atype59_2,
g_atype59_3,
g_atype59_4,
g_atype59_5,
g_atype59_6,
g_atype59_7,
g_atype59_8,
g_atype59_9,
g_atype59_10,
};

static const int32 cn_attr59 [] =
{
1204,
1205,
1234,
1202,
1203,
1235,
1211,
1219,
1220,
1237,
1238,
};

extern const char *names60[];
static const uint32 types60 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_REAL64,
SK_REAL64,
SK_REAL64,
};

static const uint16 attr_flags60 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype60_0 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype60_1 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype60_2 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype60_3 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype60_4 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype60_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype60_6 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype60_7 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype60_8 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype60_9 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype60_10 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype60_11 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype60_12 [] = {206,0xFFFF};
static const EIF_TYPE_INDEX g_atype60_13 [] = {206,0xFFFF};
static const EIF_TYPE_INDEX g_atype60_14 [] = {206,0xFFFF};

static const EIF_TYPE_INDEX *gtypes60 [] = {
g_atype60_0,
g_atype60_1,
g_atype60_2,
g_atype60_3,
g_atype60_4,
g_atype60_5,
g_atype60_6,
g_atype60_7,
g_atype60_8,
g_atype60_9,
g_atype60_10,
g_atype60_11,
g_atype60_12,
g_atype60_13,
g_atype60_14,
};

static const int32 cn_attr60 [] =
{
1204,
1205,
1202,
1203,
1250,
1251,
1252,
1253,
1211,
1219,
1220,
1249,
1246,
1247,
1248,
};

extern const char *names61[];
static const uint32 types61 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_UINT64,
SK_UINT64,
};

static const uint16 attr_flags61 [] =
{0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype61_0 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype61_1 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype61_2 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype61_3 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype61_4 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype61_5 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype61_6 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype61_7 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype61_8 [] = {224,0xFFFF};
static const EIF_TYPE_INDEX g_atype61_9 [] = {224,0xFFFF};

static const EIF_TYPE_INDEX *gtypes61 [] = {
g_atype61_0,
g_atype61_1,
g_atype61_2,
g_atype61_3,
g_atype61_4,
g_atype61_5,
g_atype61_6,
g_atype61_7,
g_atype61_8,
g_atype61_9,
};

static const int32 cn_attr61 [] =
{
1204,
1205,
1202,
1203,
1270,
1211,
1219,
1220,
1268,
1269,
};

extern const char *names65[];
static const uint32 types65 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags65 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype65_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype65_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype65_2 [] = {64,0xFFFF};
static const EIF_TYPE_INDEX g_atype65_3 [] = {127,0xFFFF};
static const EIF_TYPE_INDEX g_atype65_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype65_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype65_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes65 [] = {
g_atype65_0,
g_atype65_1,
g_atype65_2,
g_atype65_3,
g_atype65_4,
g_atype65_5,
g_atype65_6,
};

static const int32 cn_attr65 [] =
{
1317,
1318,
1327,
1332,
1336,
1334,
1319,
};

extern const char *names66[];
static const uint32 types66 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags66 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype66_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype66_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype66_2 [] = {64,0xFFFF};
static const EIF_TYPE_INDEX g_atype66_3 [] = {127,0xFFFF};
static const EIF_TYPE_INDEX g_atype66_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype66_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype66_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes66 [] = {
g_atype66_0,
g_atype66_1,
g_atype66_2,
g_atype66_3,
g_atype66_4,
g_atype66_5,
g_atype66_6,
};

static const int32 cn_attr66 [] =
{
1317,
1318,
1327,
1332,
1336,
1334,
1319,
};

extern const char *names67[];
static const uint32 types67 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags67 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype67_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype67_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype67_2 [] = {64,0xFFFF};
static const EIF_TYPE_INDEX g_atype67_3 [] = {127,0xFFFF};
static const EIF_TYPE_INDEX g_atype67_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype67_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype67_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes67 [] = {
g_atype67_0,
g_atype67_1,
g_atype67_2,
g_atype67_3,
g_atype67_4,
g_atype67_5,
g_atype67_6,
};

static const int32 cn_attr67 [] =
{
1317,
1318,
1327,
1332,
1336,
1334,
1319,
};

extern const char *names68[];
static const uint32 types68 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags68 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype68_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype68_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype68_2 [] = {64,0xFFFF};
static const EIF_TYPE_INDEX g_atype68_3 [] = {127,0xFFFF};
static const EIF_TYPE_INDEX g_atype68_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype68_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype68_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes68 [] = {
g_atype68_0,
g_atype68_1,
g_atype68_2,
g_atype68_3,
g_atype68_4,
g_atype68_5,
g_atype68_6,
};

static const int32 cn_attr68 [] =
{
1317,
1318,
1327,
1332,
1336,
1334,
1319,
};

extern const char *names69[];
static const uint32 types69 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags69 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype69_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype69_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype69_2 [] = {64,0xFFFF};
static const EIF_TYPE_INDEX g_atype69_3 [] = {127,0xFFFF};
static const EIF_TYPE_INDEX g_atype69_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype69_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype69_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes69 [] = {
g_atype69_0,
g_atype69_1,
g_atype69_2,
g_atype69_3,
g_atype69_4,
g_atype69_5,
g_atype69_6,
};

static const int32 cn_attr69 [] =
{
1317,
1318,
1327,
1332,
1336,
1334,
1319,
};

extern const char *names70[];
static const uint32 types70 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags70 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype70_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype70_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype70_2 [] = {64,0xFFFF};
static const EIF_TYPE_INDEX g_atype70_3 [] = {127,0xFFFF};
static const EIF_TYPE_INDEX g_atype70_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype70_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype70_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes70 [] = {
g_atype70_0,
g_atype70_1,
g_atype70_2,
g_atype70_3,
g_atype70_4,
g_atype70_5,
g_atype70_6,
};

static const int32 cn_attr70 [] =
{
1317,
1318,
1327,
1332,
1336,
1334,
1319,
};

extern const char *names71[];
static const uint32 types71 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags71 [] =
{0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype71_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype71_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype71_2 [] = {64,0xFFFF};
static const EIF_TYPE_INDEX g_atype71_3 [] = {127,0xFFFF};
static const EIF_TYPE_INDEX g_atype71_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype71_5 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype71_6 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype71_7 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype71_8 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype71_9 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes71 [] = {
g_atype71_0,
g_atype71_1,
g_atype71_2,
g_atype71_3,
g_atype71_4,
g_atype71_5,
g_atype71_6,
g_atype71_7,
g_atype71_8,
g_atype71_9,
};

static const int32 cn_attr71 [] =
{
1317,
1318,
1327,
1332,
1336,
1343,
1334,
1319,
1337,
1338,
};

extern const char *names72[];
static const uint32 types72 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags72 [] =
{0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype72_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype72_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype72_2 [] = {64,0xFFFF};
static const EIF_TYPE_INDEX g_atype72_3 [] = {127,0xFFFF};
static const EIF_TYPE_INDEX g_atype72_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype72_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype72_6 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype72_7 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes72 [] = {
g_atype72_0,
g_atype72_1,
g_atype72_2,
g_atype72_3,
g_atype72_4,
g_atype72_5,
g_atype72_6,
g_atype72_7,
};

static const int32 cn_attr72 [] =
{
1317,
1318,
1327,
1332,
1336,
1334,
1319,
1354,
};

extern const char *names73[];
static const uint32 types73 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags73 [] =
{0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype73_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype73_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype73_2 [] = {64,0xFFFF};
static const EIF_TYPE_INDEX g_atype73_3 [] = {127,0xFFFF};
static const EIF_TYPE_INDEX g_atype73_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype73_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype73_6 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype73_7 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes73 [] = {
g_atype73_0,
g_atype73_1,
g_atype73_2,
g_atype73_3,
g_atype73_4,
g_atype73_5,
g_atype73_6,
g_atype73_7,
};

static const int32 cn_attr73 [] =
{
1317,
1318,
1327,
1332,
1336,
1334,
1319,
1356,
};

extern const char *names74[];
static const uint32 types74 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags74 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype74_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype74_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype74_2 [] = {64,0xFFFF};
static const EIF_TYPE_INDEX g_atype74_3 [] = {127,0xFFFF};
static const EIF_TYPE_INDEX g_atype74_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype74_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype74_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes74 [] = {
g_atype74_0,
g_atype74_1,
g_atype74_2,
g_atype74_3,
g_atype74_4,
g_atype74_5,
g_atype74_6,
};

static const int32 cn_attr74 [] =
{
1317,
1318,
1327,
1332,
1336,
1334,
1319,
};

extern const char *names75[];
static const uint32 types75 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags75 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype75_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype75_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype75_2 [] = {64,0xFFFF};
static const EIF_TYPE_INDEX g_atype75_3 [] = {127,0xFFFF};
static const EIF_TYPE_INDEX g_atype75_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype75_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype75_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes75 [] = {
g_atype75_0,
g_atype75_1,
g_atype75_2,
g_atype75_3,
g_atype75_4,
g_atype75_5,
g_atype75_6,
};

static const int32 cn_attr75 [] =
{
1317,
1318,
1327,
1332,
1336,
1334,
1319,
};

extern const char *names76[];
static const uint32 types76 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags76 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype76_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype76_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype76_2 [] = {64,0xFFFF};
static const EIF_TYPE_INDEX g_atype76_3 [] = {127,0xFFFF};
static const EIF_TYPE_INDEX g_atype76_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype76_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype76_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes76 [] = {
g_atype76_0,
g_atype76_1,
g_atype76_2,
g_atype76_3,
g_atype76_4,
g_atype76_5,
g_atype76_6,
};

static const int32 cn_attr76 [] =
{
1317,
1318,
1327,
1332,
1336,
1334,
1319,
};

extern const char *names77[];
static const uint32 types77 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags77 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype77_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype77_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype77_2 [] = {64,0xFFFF};
static const EIF_TYPE_INDEX g_atype77_3 [] = {127,0xFFFF};
static const EIF_TYPE_INDEX g_atype77_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype77_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype77_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes77 [] = {
g_atype77_0,
g_atype77_1,
g_atype77_2,
g_atype77_3,
g_atype77_4,
g_atype77_5,
g_atype77_6,
};

static const int32 cn_attr77 [] =
{
1317,
1318,
1327,
1332,
1336,
1334,
1319,
};

extern const char *names78[];
static const uint32 types78 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags78 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype78_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype78_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype78_2 [] = {64,0xFFFF};
static const EIF_TYPE_INDEX g_atype78_3 [] = {127,0xFFFF};
static const EIF_TYPE_INDEX g_atype78_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype78_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype78_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes78 [] = {
g_atype78_0,
g_atype78_1,
g_atype78_2,
g_atype78_3,
g_atype78_4,
g_atype78_5,
g_atype78_6,
};

static const int32 cn_attr78 [] =
{
1317,
1318,
1327,
1332,
1336,
1334,
1319,
};

extern const char *names79[];
static const uint32 types79 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags79 [] =
{0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype79_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype79_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype79_2 [] = {64,0xFFFF};
static const EIF_TYPE_INDEX g_atype79_3 [] = {127,0xFFFF};
static const EIF_TYPE_INDEX g_atype79_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype79_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype79_6 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype79_7 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes79 [] = {
g_atype79_0,
g_atype79_1,
g_atype79_2,
g_atype79_3,
g_atype79_4,
g_atype79_5,
g_atype79_6,
g_atype79_7,
};

static const int32 cn_attr79 [] =
{
1317,
1318,
1327,
1332,
1336,
1334,
1319,
1359,
};

extern const char *names80[];
static const uint32 types80 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags80 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype80_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype80_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype80_2 [] = {64,0xFFFF};
static const EIF_TYPE_INDEX g_atype80_3 [] = {127,0xFFFF};
static const EIF_TYPE_INDEX g_atype80_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype80_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype80_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes80 [] = {
g_atype80_0,
g_atype80_1,
g_atype80_2,
g_atype80_3,
g_atype80_4,
g_atype80_5,
g_atype80_6,
};

static const int32 cn_attr80 [] =
{
1317,
1318,
1327,
1332,
1336,
1334,
1319,
};

extern const char *names81[];
static const uint32 types81 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags81 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype81_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype81_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype81_2 [] = {64,0xFFFF};
static const EIF_TYPE_INDEX g_atype81_3 [] = {127,0xFFFF};
static const EIF_TYPE_INDEX g_atype81_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype81_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype81_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes81 [] = {
g_atype81_0,
g_atype81_1,
g_atype81_2,
g_atype81_3,
g_atype81_4,
g_atype81_5,
g_atype81_6,
};

static const int32 cn_attr81 [] =
{
1317,
1318,
1327,
1332,
1336,
1334,
1319,
};

extern const char *names82[];
static const uint32 types82 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags82 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype82_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype82_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype82_2 [] = {64,0xFFFF};
static const EIF_TYPE_INDEX g_atype82_3 [] = {127,0xFFFF};
static const EIF_TYPE_INDEX g_atype82_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype82_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype82_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes82 [] = {
g_atype82_0,
g_atype82_1,
g_atype82_2,
g_atype82_3,
g_atype82_4,
g_atype82_5,
g_atype82_6,
};

static const int32 cn_attr82 [] =
{
1317,
1318,
1327,
1332,
1336,
1334,
1319,
};

extern const char *names83[];
static const uint32 types83 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags83 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype83_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype83_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype83_2 [] = {64,0xFFFF};
static const EIF_TYPE_INDEX g_atype83_3 [] = {127,0xFFFF};
static const EIF_TYPE_INDEX g_atype83_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype83_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype83_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes83 [] = {
g_atype83_0,
g_atype83_1,
g_atype83_2,
g_atype83_3,
g_atype83_4,
g_atype83_5,
g_atype83_6,
};

static const int32 cn_attr83 [] =
{
1317,
1318,
1327,
1332,
1336,
1334,
1319,
};

extern const char *names84[];
static const uint32 types84 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags84 [] =
{0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype84_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype84_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype84_2 [] = {64,0xFFFF};
static const EIF_TYPE_INDEX g_atype84_3 [] = {127,0xFFFF};
static const EIF_TYPE_INDEX g_atype84_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype84_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype84_6 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype84_7 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes84 [] = {
g_atype84_0,
g_atype84_1,
g_atype84_2,
g_atype84_3,
g_atype84_4,
g_atype84_5,
g_atype84_6,
g_atype84_7,
};

static const int32 cn_attr84 [] =
{
1317,
1318,
1327,
1332,
1336,
1334,
1319,
1361,
};

extern const char *names85[];
static const uint32 types85 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags85 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype85_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype85_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype85_2 [] = {64,0xFFFF};
static const EIF_TYPE_INDEX g_atype85_3 [] = {127,0xFFFF};
static const EIF_TYPE_INDEX g_atype85_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype85_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype85_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes85 [] = {
g_atype85_0,
g_atype85_1,
g_atype85_2,
g_atype85_3,
g_atype85_4,
g_atype85_5,
g_atype85_6,
};

static const int32 cn_attr85 [] =
{
1317,
1318,
1327,
1332,
1336,
1334,
1319,
};

extern const char *names86[];
static const uint32 types86 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags86 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype86_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype86_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype86_2 [] = {64,0xFFFF};
static const EIF_TYPE_INDEX g_atype86_3 [] = {127,0xFFFF};
static const EIF_TYPE_INDEX g_atype86_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype86_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype86_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes86 [] = {
g_atype86_0,
g_atype86_1,
g_atype86_2,
g_atype86_3,
g_atype86_4,
g_atype86_5,
g_atype86_6,
};

static const int32 cn_attr86 [] =
{
1317,
1318,
1327,
1332,
1336,
1334,
1319,
};

extern const char *names87[];
static const uint32 types87 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags87 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype87_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype87_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype87_2 [] = {64,0xFFFF};
static const EIF_TYPE_INDEX g_atype87_3 [] = {127,0xFFFF};
static const EIF_TYPE_INDEX g_atype87_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype87_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype87_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes87 [] = {
g_atype87_0,
g_atype87_1,
g_atype87_2,
g_atype87_3,
g_atype87_4,
g_atype87_5,
g_atype87_6,
};

static const int32 cn_attr87 [] =
{
1317,
1318,
1327,
1332,
1336,
1334,
1319,
};

extern const char *names88[];
static const uint32 types88 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags88 [] =
{0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype88_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype88_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype88_2 [] = {64,0xFFFF};
static const EIF_TYPE_INDEX g_atype88_3 [] = {127,0xFFFF};
static const EIF_TYPE_INDEX g_atype88_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype88_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype88_6 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype88_7 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype88_8 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes88 [] = {
g_atype88_0,
g_atype88_1,
g_atype88_2,
g_atype88_3,
g_atype88_4,
g_atype88_5,
g_atype88_6,
g_atype88_7,
g_atype88_8,
};

static const int32 cn_attr88 [] =
{
1317,
1318,
1327,
1332,
1336,
1334,
1319,
1362,
1365,
};

extern const char *names89[];
static const uint32 types89 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags89 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype89_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype89_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype89_2 [] = {64,0xFFFF};
static const EIF_TYPE_INDEX g_atype89_3 [] = {127,0xFFFF};
static const EIF_TYPE_INDEX g_atype89_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype89_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype89_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes89 [] = {
g_atype89_0,
g_atype89_1,
g_atype89_2,
g_atype89_3,
g_atype89_4,
g_atype89_5,
g_atype89_6,
};

static const int32 cn_attr89 [] =
{
1317,
1318,
1327,
1332,
1336,
1334,
1319,
};

extern const char *names90[];
static const uint32 types90 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags90 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype90_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype90_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype90_2 [] = {64,0xFFFF};
static const EIF_TYPE_INDEX g_atype90_3 [] = {127,0xFFFF};
static const EIF_TYPE_INDEX g_atype90_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype90_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype90_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes90 [] = {
g_atype90_0,
g_atype90_1,
g_atype90_2,
g_atype90_3,
g_atype90_4,
g_atype90_5,
g_atype90_6,
};

static const int32 cn_attr90 [] =
{
1317,
1318,
1327,
1332,
1336,
1334,
1319,
};

extern const char *names91[];
static const uint32 types91 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags91 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype91_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype91_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype91_2 [] = {64,0xFFFF};
static const EIF_TYPE_INDEX g_atype91_3 [] = {127,0xFFFF};
static const EIF_TYPE_INDEX g_atype91_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype91_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype91_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes91 [] = {
g_atype91_0,
g_atype91_1,
g_atype91_2,
g_atype91_3,
g_atype91_4,
g_atype91_5,
g_atype91_6,
};

static const int32 cn_attr91 [] =
{
1317,
1318,
1327,
1332,
1336,
1334,
1319,
};

extern const char *names92[];
static const uint32 types92 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags92 [] =
{0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype92_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype92_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype92_2 [] = {64,0xFFFF};
static const EIF_TYPE_INDEX g_atype92_3 [] = {127,0xFFFF};
static const EIF_TYPE_INDEX g_atype92_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype92_5 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype92_6 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype92_7 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype92_8 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes92 [] = {
g_atype92_0,
g_atype92_1,
g_atype92_2,
g_atype92_3,
g_atype92_4,
g_atype92_5,
g_atype92_6,
g_atype92_7,
g_atype92_8,
};

static const int32 cn_attr92 [] =
{
1317,
1318,
1327,
1332,
1336,
1366,
1367,
1334,
1319,
};

extern const char *names93[];
static const uint32 types93 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags93 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype93_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype93_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype93_2 [] = {64,0xFFFF};
static const EIF_TYPE_INDEX g_atype93_3 [] = {127,0xFFFF};
static const EIF_TYPE_INDEX g_atype93_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype93_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype93_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes93 [] = {
g_atype93_0,
g_atype93_1,
g_atype93_2,
g_atype93_3,
g_atype93_4,
g_atype93_5,
g_atype93_6,
};

static const int32 cn_attr93 [] =
{
1317,
1318,
1327,
1332,
1336,
1334,
1319,
};

extern const char *names94[];
static const uint32 types94 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags94 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype94_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype94_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype94_2 [] = {64,0xFFFF};
static const EIF_TYPE_INDEX g_atype94_3 [] = {127,0xFFFF};
static const EIF_TYPE_INDEX g_atype94_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype94_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype94_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes94 [] = {
g_atype94_0,
g_atype94_1,
g_atype94_2,
g_atype94_3,
g_atype94_4,
g_atype94_5,
g_atype94_6,
};

static const int32 cn_attr94 [] =
{
1317,
1318,
1327,
1332,
1336,
1334,
1319,
};

extern const char *names95[];
static const uint32 types95 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags95 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype95_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype95_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype95_2 [] = {64,0xFFFF};
static const EIF_TYPE_INDEX g_atype95_3 [] = {127,0xFFFF};
static const EIF_TYPE_INDEX g_atype95_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype95_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype95_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes95 [] = {
g_atype95_0,
g_atype95_1,
g_atype95_2,
g_atype95_3,
g_atype95_4,
g_atype95_5,
g_atype95_6,
};

static const int32 cn_attr95 [] =
{
1317,
1318,
1327,
1332,
1336,
1334,
1319,
};

extern const char *names96[];
static const uint32 types96 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags96 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype96_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype96_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype96_2 [] = {64,0xFFFF};
static const EIF_TYPE_INDEX g_atype96_3 [] = {127,0xFFFF};
static const EIF_TYPE_INDEX g_atype96_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype96_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype96_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes96 [] = {
g_atype96_0,
g_atype96_1,
g_atype96_2,
g_atype96_3,
g_atype96_4,
g_atype96_5,
g_atype96_6,
};

static const int32 cn_attr96 [] =
{
1317,
1318,
1327,
1332,
1336,
1334,
1319,
};

extern const char *names97[];
static const uint32 types97 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags97 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype97_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype97_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype97_2 [] = {64,0xFFFF};
static const EIF_TYPE_INDEX g_atype97_3 [] = {127,0xFFFF};
static const EIF_TYPE_INDEX g_atype97_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype97_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype97_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes97 [] = {
g_atype97_0,
g_atype97_1,
g_atype97_2,
g_atype97_3,
g_atype97_4,
g_atype97_5,
g_atype97_6,
};

static const int32 cn_attr97 [] =
{
1317,
1318,
1327,
1332,
1336,
1334,
1319,
};

extern const char *names98[];
static const uint32 types98 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags98 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype98_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype98_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype98_2 [] = {64,0xFFFF};
static const EIF_TYPE_INDEX g_atype98_3 [] = {127,0xFFFF};
static const EIF_TYPE_INDEX g_atype98_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype98_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype98_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes98 [] = {
g_atype98_0,
g_atype98_1,
g_atype98_2,
g_atype98_3,
g_atype98_4,
g_atype98_5,
g_atype98_6,
};

static const int32 cn_attr98 [] =
{
1317,
1318,
1327,
1332,
1336,
1334,
1319,
};

extern const char *names99[];
static const uint32 types99 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags99 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype99_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype99_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype99_2 [] = {64,0xFFFF};
static const EIF_TYPE_INDEX g_atype99_3 [] = {127,0xFFFF};
static const EIF_TYPE_INDEX g_atype99_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype99_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype99_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes99 [] = {
g_atype99_0,
g_atype99_1,
g_atype99_2,
g_atype99_3,
g_atype99_4,
g_atype99_5,
g_atype99_6,
};

static const int32 cn_attr99 [] =
{
1317,
1318,
1327,
1332,
1336,
1334,
1319,
};

extern const char *names100[];
static const uint32 types100 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags100 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype100_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype100_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype100_2 [] = {64,0xFFFF};
static const EIF_TYPE_INDEX g_atype100_3 [] = {127,0xFFFF};
static const EIF_TYPE_INDEX g_atype100_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype100_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype100_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes100 [] = {
g_atype100_0,
g_atype100_1,
g_atype100_2,
g_atype100_3,
g_atype100_4,
g_atype100_5,
g_atype100_6,
};

static const int32 cn_attr100 [] =
{
1317,
1318,
1327,
1332,
1336,
1334,
1319,
};

extern const char *names101[];
static const uint32 types101 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags101 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype101_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype101_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype101_2 [] = {64,0xFFFF};
static const EIF_TYPE_INDEX g_atype101_3 [] = {127,0xFFFF};
static const EIF_TYPE_INDEX g_atype101_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype101_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype101_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes101 [] = {
g_atype101_0,
g_atype101_1,
g_atype101_2,
g_atype101_3,
g_atype101_4,
g_atype101_5,
g_atype101_6,
};

static const int32 cn_attr101 [] =
{
1317,
1318,
1327,
1332,
1336,
1334,
1319,
};

extern const char *names102[];
static const uint32 types102 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags102 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype102_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype102_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype102_2 [] = {64,0xFFFF};
static const EIF_TYPE_INDEX g_atype102_3 [] = {127,0xFFFF};
static const EIF_TYPE_INDEX g_atype102_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype102_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype102_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes102 [] = {
g_atype102_0,
g_atype102_1,
g_atype102_2,
g_atype102_3,
g_atype102_4,
g_atype102_5,
g_atype102_6,
};

static const int32 cn_attr102 [] =
{
1317,
1318,
1327,
1332,
1336,
1334,
1319,
};

extern const char *names103[];
static const uint32 types103 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags103 [] =
{0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype103_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype103_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype103_2 [] = {64,0xFFFF};
static const EIF_TYPE_INDEX g_atype103_3 [] = {127,0xFFFF};
static const EIF_TYPE_INDEX g_atype103_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype103_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype103_6 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype103_7 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes103 [] = {
g_atype103_0,
g_atype103_1,
g_atype103_2,
g_atype103_3,
g_atype103_4,
g_atype103_5,
g_atype103_6,
g_atype103_7,
};

static const int32 cn_attr103 [] =
{
1317,
1318,
1327,
1332,
1336,
1334,
1371,
1319,
};

extern const char *names104[];
static const uint32 types104 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags104 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype104_0 [] = {0xFF01,444,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype104_1 [] = {324,0xFF01,444,218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes104 [] = {
g_atype104_0,
g_atype104_1,
};

static const int32 cn_attr104 [] =
{
1380,
1381,
};

extern const char *names105[];
static const uint32 types105 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags105 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype105_0 [] = {0xFF01,444,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype105_1 [] = {324,0xFF01,444,218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes105 [] = {
g_atype105_0,
g_atype105_1,
};

static const int32 cn_attr105 [] =
{
1380,
1381,
};

extern const char *names106[];
static const uint32 types106 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags106 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype106_0 [] = {0xFF01,444,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype106_1 [] = {324,0xFF01,444,218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes106 [] = {
g_atype106_0,
g_atype106_1,
};

static const int32 cn_attr106 [] =
{
1380,
1381,
};

extern const char *names110[];
static const uint32 types110 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_UINT32,
};

static const uint16 attr_flags110 [] =
{0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype110_0 [] = {0xFF01,38,0xFFFF};
static const EIF_TYPE_INDEX g_atype110_1 [] = {0xFF01,130,0xFFFF};
static const EIF_TYPE_INDEX g_atype110_2 [] = {0xFF01,136,0xFFFF};
static const EIF_TYPE_INDEX g_atype110_3 [] = {0xFF01,48,0xFFFF};
static const EIF_TYPE_INDEX g_atype110_4 [] = {0xFF01,160,0xFFFF};
static const EIF_TYPE_INDEX g_atype110_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype110_6 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype110_7 [] = {212,0xFFFF};

static const EIF_TYPE_INDEX *gtypes110 [] = {
g_atype110_0,
g_atype110_1,
g_atype110_2,
g_atype110_3,
g_atype110_4,
g_atype110_5,
g_atype110_6,
g_atype110_7,
};

static const int32 cn_attr110 [] =
{
1402,
1410,
1411,
1412,
1413,
1404,
1419,
1416,
};

extern const char *names111[];
static const uint32 types111 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_UINT32,
};

static const uint16 attr_flags111 [] =
{0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype111_0 [] = {0xFF01,38,0xFFFF};
static const EIF_TYPE_INDEX g_atype111_1 [] = {0xFF01,130,0xFFFF};
static const EIF_TYPE_INDEX g_atype111_2 [] = {0xFF01,136,0xFFFF};
static const EIF_TYPE_INDEX g_atype111_3 [] = {0xFF01,48,0xFFFF};
static const EIF_TYPE_INDEX g_atype111_4 [] = {0xFF01,160,0xFFFF};
static const EIF_TYPE_INDEX g_atype111_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype111_6 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype111_7 [] = {212,0xFFFF};

static const EIF_TYPE_INDEX *gtypes111 [] = {
g_atype111_0,
g_atype111_1,
g_atype111_2,
g_atype111_3,
g_atype111_4,
g_atype111_5,
g_atype111_6,
g_atype111_7,
};

static const int32 cn_attr111 [] =
{
1402,
1410,
1411,
1412,
1413,
1404,
1419,
1416,
};

extern const char *names112[];
static const uint32 types112 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_UINT32,
};

static const uint16 attr_flags112 [] =
{0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype112_0 [] = {0xFF01,38,0xFFFF};
static const EIF_TYPE_INDEX g_atype112_1 [] = {0xFF01,130,0xFFFF};
static const EIF_TYPE_INDEX g_atype112_2 [] = {0xFF01,136,0xFFFF};
static const EIF_TYPE_INDEX g_atype112_3 [] = {0xFF01,48,0xFFFF};
static const EIF_TYPE_INDEX g_atype112_4 [] = {0xFF01,160,0xFFFF};
static const EIF_TYPE_INDEX g_atype112_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype112_6 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype112_7 [] = {212,0xFFFF};

static const EIF_TYPE_INDEX *gtypes112 [] = {
g_atype112_0,
g_atype112_1,
g_atype112_2,
g_atype112_3,
g_atype112_4,
g_atype112_5,
g_atype112_6,
g_atype112_7,
};

static const int32 cn_attr112 [] =
{
1402,
1410,
1411,
1412,
1413,
1404,
1419,
1416,
};

extern const char *names113[];
static const uint32 types113 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_UINT32,
};

static const uint16 attr_flags113 [] =
{0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype113_0 [] = {0xFF01,38,0xFFFF};
static const EIF_TYPE_INDEX g_atype113_1 [] = {0xFF01,130,0xFFFF};
static const EIF_TYPE_INDEX g_atype113_2 [] = {0xFF01,136,0xFFFF};
static const EIF_TYPE_INDEX g_atype113_3 [] = {0xFF01,48,0xFFFF};
static const EIF_TYPE_INDEX g_atype113_4 [] = {0xFF01,160,0xFFFF};
static const EIF_TYPE_INDEX g_atype113_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype113_6 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype113_7 [] = {212,0xFFFF};

static const EIF_TYPE_INDEX *gtypes113 [] = {
g_atype113_0,
g_atype113_1,
g_atype113_2,
g_atype113_3,
g_atype113_4,
g_atype113_5,
g_atype113_6,
g_atype113_7,
};

static const int32 cn_attr113 [] =
{
1402,
1410,
1411,
1412,
1413,
1404,
1419,
1416,
};

extern const char *names114[];
static const uint32 types114 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_UINT32,
};

static const uint16 attr_flags114 [] =
{0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype114_0 [] = {0xFF01,38,0xFFFF};
static const EIF_TYPE_INDEX g_atype114_1 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX g_atype114_2 [] = {320,0xFF01,14,0xFFFF};
static const EIF_TYPE_INDEX g_atype114_3 [] = {0xFF01,130,0xFFFF};
static const EIF_TYPE_INDEX g_atype114_4 [] = {0xFF01,136,0xFFFF};
static const EIF_TYPE_INDEX g_atype114_5 [] = {0xFF01,324,0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype114_6 [] = {444,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype114_7 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype114_8 [] = {212,0xFFFF};

static const EIF_TYPE_INDEX *gtypes114 [] = {
g_atype114_0,
g_atype114_1,
g_atype114_2,
g_atype114_3,
g_atype114_4,
g_atype114_5,
g_atype114_6,
g_atype114_7,
g_atype114_8,
};

static const int32 cn_attr114 [] =
{
1448,
1449,
1451,
1455,
1456,
1457,
1463,
1460,
1462,
};

extern const char *names115[];
static const uint32 types115 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_UINT32,
};

static const uint16 attr_flags115 [] =
{0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype115_0 [] = {0xFF01,38,0xFFFF};
static const EIF_TYPE_INDEX g_atype115_1 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX g_atype115_2 [] = {320,0xFF01,14,0xFFFF};
static const EIF_TYPE_INDEX g_atype115_3 [] = {0xFF01,130,0xFFFF};
static const EIF_TYPE_INDEX g_atype115_4 [] = {0xFF01,136,0xFFFF};
static const EIF_TYPE_INDEX g_atype115_5 [] = {0xFF01,324,0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype115_6 [] = {444,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype115_7 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype115_8 [] = {212,0xFFFF};

static const EIF_TYPE_INDEX *gtypes115 [] = {
g_atype115_0,
g_atype115_1,
g_atype115_2,
g_atype115_3,
g_atype115_4,
g_atype115_5,
g_atype115_6,
g_atype115_7,
g_atype115_8,
};

static const int32 cn_attr115 [] =
{
1448,
1449,
1451,
1455,
1456,
1457,
1463,
1460,
1462,
};

extern const char *names116[];
static const uint32 types116 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_UINT32,
};

static const uint16 attr_flags116 [] =
{0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype116_0 [] = {0xFF01,38,0xFFFF};
static const EIF_TYPE_INDEX g_atype116_1 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX g_atype116_2 [] = {320,0xFF01,14,0xFFFF};
static const EIF_TYPE_INDEX g_atype116_3 [] = {0xFF01,130,0xFFFF};
static const EIF_TYPE_INDEX g_atype116_4 [] = {0xFF01,136,0xFFFF};
static const EIF_TYPE_INDEX g_atype116_5 [] = {0xFF01,324,0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype116_6 [] = {444,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype116_7 [] = {324,444,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype116_8 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype116_9 [] = {212,0xFFFF};

static const EIF_TYPE_INDEX *gtypes116 [] = {
g_atype116_0,
g_atype116_1,
g_atype116_2,
g_atype116_3,
g_atype116_4,
g_atype116_5,
g_atype116_6,
g_atype116_7,
g_atype116_8,
g_atype116_9,
};

static const int32 cn_attr116 [] =
{
1448,
1449,
1451,
1455,
1456,
1457,
1463,
1499,
1460,
1462,
};

extern const char *names120[];
static const uint32 types120 [] =
{
SK_REF,
SK_CHAR8,
SK_CHAR8,
SK_CHAR8,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags120 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype120_0 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype120_1 [] = {197,0xFFFF};
static const EIF_TYPE_INDEX g_atype120_2 [] = {197,0xFFFF};
static const EIF_TYPE_INDEX g_atype120_3 [] = {197,0xFFFF};
static const EIF_TYPE_INDEX g_atype120_4 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype120_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype120_6 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype120_7 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype120_8 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype120_9 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype120_10 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype120_11 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype120_12 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes120 [] = {
g_atype120_0,
g_atype120_1,
g_atype120_2,
g_atype120_3,
g_atype120_4,
g_atype120_5,
g_atype120_6,
g_atype120_7,
g_atype120_8,
g_atype120_9,
g_atype120_10,
g_atype120_11,
g_atype120_12,
};

static const int32 cn_attr120 [] =
{
772,
766,
768,
1540,
770,
773,
1536,
1538,
1539,
767,
769,
771,
1537,
};

extern const char *names122[];
static const uint32 types122 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_POINTER,
};

static const uint16 attr_flags122 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype122_0 [] = {163,0xFFFF};
static const EIF_TYPE_INDEX g_atype122_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype122_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype122_3 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes122 [] = {
g_atype122_0,
g_atype122_1,
g_atype122_2,
g_atype122_3,
};

static const int32 cn_attr122 [] =
{
1105,
1100,
1560,
1104,
};

extern const char *names123[];
static const uint32 types123 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_POINTER,
};

static const uint16 attr_flags123 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype123_0 [] = {163,0xFFFF};
static const EIF_TYPE_INDEX g_atype123_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype123_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype123_3 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes123 [] = {
g_atype123_0,
g_atype123_1,
g_atype123_2,
g_atype123_3,
};

static const int32 cn_attr123 [] =
{
1105,
1100,
1578,
1104,
};

extern const char *names125[];
static const uint32 types125 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags125 [] =
{0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype125_0 [] = {0xFF01,163,0xFFFF};
static const EIF_TYPE_INDEX g_atype125_1 [] = {0xFF01,171,0xFFFF};
static const EIF_TYPE_INDEX g_atype125_2 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype125_3 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype125_4 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype125_5 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype125_6 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype125_7 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes125 [] = {
g_atype125_0,
g_atype125_1,
g_atype125_2,
g_atype125_3,
g_atype125_4,
g_atype125_5,
g_atype125_6,
g_atype125_7,
};

static const int32 cn_attr125 [] =
{
1040,
1041,
995,
1037,
1045,
1042,
1043,
1046,
};

extern const char *names126[];
static const uint32 types126 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags126 [] =
{0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype126_0 [] = {0xFF01,163,0xFFFF};
static const EIF_TYPE_INDEX g_atype126_1 [] = {0xFF01,171,0xFFFF};
static const EIF_TYPE_INDEX g_atype126_2 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype126_3 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype126_4 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype126_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype126_6 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype126_7 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype126_8 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes126 [] = {
g_atype126_0,
g_atype126_1,
g_atype126_2,
g_atype126_3,
g_atype126_4,
g_atype126_5,
g_atype126_6,
g_atype126_7,
g_atype126_8,
};

static const int32 cn_attr126 [] =
{
1040,
1041,
995,
1037,
1045,
1676,
1042,
1043,
1046,
};

extern const char *names128[];
static const uint32 types128 [] =
{
SK_REF,
SK_INT32,
};

static const uint16 attr_flags128 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype128_0 [] = {0xFF01,163,0xFFFF};
static const EIF_TYPE_INDEX g_atype128_1 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes128 [] = {
g_atype128_0,
g_atype128_1,
};

static const int32 cn_attr128 [] =
{
1704,
1707,
};

extern const char *names130[];
static const uint32 types130 [] =
{
SK_INT32,
};

static const uint16 attr_flags130 [] =
{0,};

static const EIF_TYPE_INDEX g_atype130_0 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes130 [] = {
g_atype130_0,
};

static const int32 cn_attr130 [] =
{
1760,
};

extern const char *names135[];
static const uint32 types135 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags135 [] =
{0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype135_0 [] = {138,0xFFFF};
static const EIF_TYPE_INDEX g_atype135_1 [] = {138,0xFFFF};
static const EIF_TYPE_INDEX g_atype135_2 [] = {138,0xFFFF};
static const EIF_TYPE_INDEX g_atype135_3 [] = {520,0xFF01,0xFFF9,2,186,0xFF01,138,320,0xFF01,0xFFF9,2,186,0xFF01,140,0xFF01,140,0xFFFF};
static const EIF_TYPE_INDEX g_atype135_4 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype135_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype135_6 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype135_7 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype135_8 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype135_9 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype135_10 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes135 [] = {
g_atype135_0,
g_atype135_1,
g_atype135_2,
g_atype135_3,
g_atype135_4,
g_atype135_5,
g_atype135_6,
g_atype135_7,
g_atype135_8,
g_atype135_9,
g_atype135_10,
};

static const int32 cn_attr135 [] =
{
1971,
1972,
1998,
2000,
1968,
1969,
1970,
1996,
1997,
1966,
1967,
};

extern const char *names136[];
static const uint32 types136 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags136 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype136_0 [] = {0xFF01,129,0xFFFF};
static const EIF_TYPE_INDEX g_atype136_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype136_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype136_3 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes136 [] = {
g_atype136_0,
g_atype136_1,
g_atype136_2,
g_atype136_3,
};

static const int32 cn_attr136 [] =
{
2013,
1760,
2012,
2014,
};

extern const char *names137[];
static const uint32 types137 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags137 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype137_0 [] = {0xFF05,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype137_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype137_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes137 [] = {
g_atype137_0,
g_atype137_1,
g_atype137_2,
};

static const int32 cn_attr137 [] =
{
2020,
1760,
2021,
};

extern const char *names139[];
static const uint32 types139 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags139 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype139_0 [] = {0xFF01,134,0xFFFF};
static const EIF_TYPE_INDEX g_atype139_1 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX g_atype139_2 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype139_3 [] = {138,0xFFFF};
static const EIF_TYPE_INDEX g_atype139_4 [] = {0xFF01,829,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype139_5 [] = {320,138,0xFFFF};
static const EIF_TYPE_INDEX g_atype139_6 [] = {320,0xFF01,140,0xFFFF};
static const EIF_TYPE_INDEX g_atype139_7 [] = {0xFF01,0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype139_8 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype139_9 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype139_10 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype139_11 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype139_12 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype139_13 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype139_14 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes139 [] = {
g_atype139_0,
g_atype139_1,
g_atype139_2,
g_atype139_3,
g_atype139_4,
g_atype139_5,
g_atype139_6,
g_atype139_7,
g_atype139_8,
g_atype139_9,
g_atype139_10,
g_atype139_11,
g_atype139_12,
g_atype139_13,
g_atype139_14,
};

static const int32 cn_attr139 [] =
{
2025,
2026,
2030,
2032,
2033,
2034,
2035,
2038,
2037,
2043,
2044,
2045,
2028,
2029,
2031,
};

extern const char *names141[];
static const uint32 types141 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags141 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype141_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype141_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype141_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes141 [] = {
g_atype141_0,
g_atype141_1,
g_atype141_2,
};

static const int32 cn_attr141 [] =
{
2084,
2082,
2083,
};

extern const char *names144[];
static const uint32 types144 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags144 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype144_0 [] = {0xFF01,142,0xFFFF};
static const EIF_TYPE_INDEX g_atype144_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype144_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes144 [] = {
g_atype144_0,
g_atype144_1,
g_atype144_2,
};

static const int32 cn_attr144 [] =
{
2135,
2136,
2137,
};

extern const char *names145[];
static const uint32 types145 [] =
{
SK_INT32,
};

static const uint16 attr_flags145 [] =
{0,};

static const EIF_TYPE_INDEX g_atype145_0 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes145 [] = {
g_atype145_0,
};

static const int32 cn_attr145 [] =
{
2139,
};

extern const char *names146[];
static const uint32 types146 [] =
{
SK_INT32,
};

static const uint16 attr_flags146 [] =
{0,};

static const EIF_TYPE_INDEX g_atype146_0 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes146 [] = {
g_atype146_0,
};

static const int32 cn_attr146 [] =
{
2148,
};

extern const char *names149[];
static const uint32 types149 [] =
{
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags149 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype149_0 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype149_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype149_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes149 [] = {
g_atype149_0,
g_atype149_1,
g_atype149_2,
};

static const int32 cn_attr149 [] =
{
2212,
2622,
2623,
};

extern const char *names150[];
static const uint32 types150 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags150 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype150_0 [] = {515,0xFF01,0xFFF9,0,186,0xFFFF};
static const EIF_TYPE_INDEX g_atype150_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype150_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype150_3 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes150 [] = {
g_atype150_0,
g_atype150_1,
g_atype150_2,
g_atype150_3,
};

static const int32 cn_attr150 [] =
{
2627,
2212,
2622,
2623,
};

extern const char *names152[];
static const uint32 types152 [] =
{
SK_REF,
SK_INT32,
};

static const uint16 attr_flags152 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype152_0 [] = {0xFF01,163,0xFFFF};
static const EIF_TYPE_INDEX g_atype152_1 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes152 [] = {
g_atype152_0,
g_atype152_1,
};

static const int32 cn_attr152 [] =
{
2828,
2832,
};

extern const char *names154[];
static const uint32 types154 [] =
{
SK_INT32,
};

static const uint16 attr_flags154 [] =
{0,};

static const EIF_TYPE_INDEX g_atype154_0 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes154 [] = {
g_atype154_0,
};

static const int32 cn_attr154 [] =
{
2856,
};

extern const char *names155[];
static const uint32 types155 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
};

static const uint16 attr_flags155 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype155_0 [] = {0xFF01,427,209,0xFFFF};
static const EIF_TYPE_INDEX g_atype155_1 [] = {229,0xFFFF};
static const EIF_TYPE_INDEX g_atype155_2 [] = {163,0xFFFF};
static const EIF_TYPE_INDEX g_atype155_3 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype155_4 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes155 [] = {
g_atype155_0,
g_atype155_1,
g_atype155_2,
g_atype155_3,
g_atype155_4,
};

static const int32 cn_attr155 [] =
{
2156,
2922,
2923,
2896,
2897,
};

extern const char *names156[];
static const uint32 types156 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
};

static const uint16 attr_flags156 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype156_0 [] = {0xFF01,427,209,0xFFFF};
static const EIF_TYPE_INDEX g_atype156_1 [] = {229,0xFFFF};
static const EIF_TYPE_INDEX g_atype156_2 [] = {163,0xFFFF};
static const EIF_TYPE_INDEX g_atype156_3 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype156_4 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes156 [] = {
g_atype156_0,
g_atype156_1,
g_atype156_2,
g_atype156_3,
g_atype156_4,
};

static const int32 cn_attr156 [] =
{
2156,
2922,
2923,
2896,
2897,
};

extern const char *names158[];
static const uint32 types158 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_UINT32,
};

static const uint16 attr_flags158 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype158_0 [] = {0xFF01,38,0xFFFF};
static const EIF_TYPE_INDEX g_atype158_1 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX g_atype158_2 [] = {320,0xFF01,14,0xFFFF};
static const EIF_TYPE_INDEX g_atype158_3 [] = {0xFF01,130,0xFFFF};
static const EIF_TYPE_INDEX g_atype158_4 [] = {0xFF01,136,0xFFFF};
static const EIF_TYPE_INDEX g_atype158_5 [] = {0xFF01,324,0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype158_6 [] = {444,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype158_7 [] = {324,444,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype158_8 [] = {368,0xFF01,0xFFF9,1,186,0xFF01,232,0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype158_9 [] = {368,0xFF01,0xFFF9,2,186,0xFF01,232,218,0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype158_10 [] = {0xFF01,441,0xFF01,11,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype158_11 [] = {0xFF01,320,0xFF01,0xFFF9,2,186,0xFF01,0,0xFF01,159,0xFFFF};
static const EIF_TYPE_INDEX g_atype158_12 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype158_13 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype158_14 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype158_15 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype158_16 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype158_17 [] = {212,0xFFFF};

static const EIF_TYPE_INDEX *gtypes158 [] = {
g_atype158_0,
g_atype158_1,
g_atype158_2,
g_atype158_3,
g_atype158_4,
g_atype158_5,
g_atype158_6,
g_atype158_7,
g_atype158_8,
g_atype158_9,
g_atype158_10,
g_atype158_11,
g_atype158_12,
g_atype158_13,
g_atype158_14,
g_atype158_15,
g_atype158_16,
g_atype158_17,
};

static const int32 cn_attr158 [] =
{
1448,
1449,
1451,
1455,
1456,
1457,
1463,
2944,
2945,
2946,
2947,
2948,
1460,
2949,
2950,
2951,
2952,
1462,
};

extern const char *names159[];
static const uint32 types159 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags159 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype159_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype159_1 [] = {0xFF01,324,0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype159_2 [] = {0xFF01,324,0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype159_3 [] = {0xFF01,444,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype159_4 [] = {0xFF01,807,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype159_5 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype159_6 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype159_7 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype159_8 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype159_9 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype159_10 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype159_11 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype159_12 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype159_13 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype159_14 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype159_15 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype159_16 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes159 [] = {
g_atype159_0,
g_atype159_1,
g_atype159_2,
g_atype159_3,
g_atype159_4,
g_atype159_5,
g_atype159_6,
g_atype159_7,
g_atype159_8,
g_atype159_9,
g_atype159_10,
g_atype159_11,
g_atype159_12,
g_atype159_13,
g_atype159_14,
g_atype159_15,
g_atype159_16,
};

static const int32 cn_attr159 [] =
{
2972,
3009,
3010,
3011,
3012,
3024,
3025,
2212,
3008,
3014,
2981,
3013,
3015,
3018,
3019,
3023,
3059,
};

extern const char *names160[];
static const uint32 types160 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags160 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype160_0 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX g_atype160_1 [] = {0xFF01,324,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype160_2 [] = {0xFF01,324,0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype160_3 [] = {0xFF01,444,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype160_4 [] = {0xFF01,807,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype160_5 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX g_atype160_6 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype160_7 [] = {240,0xFFFF};
static const EIF_TYPE_INDEX g_atype160_8 [] = {240,0xFFFF};
static const EIF_TYPE_INDEX g_atype160_9 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype160_10 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype160_11 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype160_12 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype160_13 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype160_14 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype160_15 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype160_16 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype160_17 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype160_18 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes160 [] = {
g_atype160_0,
g_atype160_1,
g_atype160_2,
g_atype160_3,
g_atype160_4,
g_atype160_5,
g_atype160_6,
g_atype160_7,
g_atype160_8,
g_atype160_9,
g_atype160_10,
g_atype160_11,
g_atype160_12,
g_atype160_13,
g_atype160_14,
g_atype160_15,
g_atype160_16,
g_atype160_17,
g_atype160_18,
};

static const int32 cn_attr160 [] =
{
2972,
3009,
3010,
3011,
3012,
3024,
3025,
3070,
3071,
2212,
3008,
3014,
2981,
3013,
3015,
3018,
3019,
3023,
3059,
};

extern const char *names161[];
static const uint32 types161 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_UINT32,
SK_UINT32,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_POINTER,
};

static const uint16 attr_flags161 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype161_0 [] = {0xFF01,716,212,0xFFFF};
static const EIF_TYPE_INDEX g_atype161_1 [] = {0xFF01,553,227,0xFFFF};
static const EIF_TYPE_INDEX g_atype161_2 [] = {0xFF01,444,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype161_3 [] = {0xFF01,807,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype161_4 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype161_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype161_6 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype161_7 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype161_8 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype161_9 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype161_10 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype161_11 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype161_12 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype161_13 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype161_14 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype161_15 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype161_16 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype161_17 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes161 [] = {
g_atype161_0,
g_atype161_1,
g_atype161_2,
g_atype161_3,
g_atype161_4,
g_atype161_5,
g_atype161_6,
g_atype161_7,
g_atype161_8,
g_atype161_9,
g_atype161_10,
g_atype161_11,
g_atype161_12,
g_atype161_13,
g_atype161_14,
g_atype161_15,
g_atype161_16,
g_atype161_17,
};

static const int32 cn_attr161 [] =
{
3009,
3010,
3011,
3012,
2212,
3008,
3014,
958,
2972,
3024,
2981,
3013,
3015,
3018,
3019,
3023,
3059,
3025,
};

extern const char *names163[];
static const uint32 types163 [] =
{
SK_POINTER,
};

static const uint16 attr_flags163 [] =
{0,};

static const EIF_TYPE_INDEX g_atype163_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes163 [] = {
g_atype163_0,
};

static const int32 cn_attr163 [] =
{
3163,
};

extern const char *names164[];
static const uint32 types164 [] =
{
SK_BOOL,
SK_INT32,
SK_POINTER,
SK_UINT64,
};

static const uint16 attr_flags164 [] =
{0,0,1,1,};

static const EIF_TYPE_INDEX g_atype164_0 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype164_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype164_2 [] = {227,0xFFFF};
static const EIF_TYPE_INDEX g_atype164_3 [] = {224,0xFFFF};

static const EIF_TYPE_INDEX *gtypes164 [] = {
g_atype164_0,
g_atype164_1,
g_atype164_2,
g_atype164_3,
};

static const int32 cn_attr164 [] =
{
3177,
3176,
3175,
3259,
};

extern const char *names165[];
static const uint32 types165 [] =
{
SK_REF,
SK_BOOL,
};

static const uint16 attr_flags165 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype165_0 [] = {0xFF01,163,0xFFFF};
static const EIF_TYPE_INDEX g_atype165_1 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes165 [] = {
g_atype165_0,
g_atype165_1,
};

static const int32 cn_attr165 [] =
{
3275,
3269,
};

extern const char *names167[];
static const uint32 types167 [] =
{
SK_POINTER,
};

static const uint16 attr_flags167 [] =
{0,};

static const EIF_TYPE_INDEX g_atype167_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes167 [] = {
g_atype167_0,
};

static const int32 cn_attr167 [] =
{
3321,
};

extern const char *names168[];
static const uint32 types168 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_INT32,
SK_POINTER,
SK_POINTER,
};

static const uint16 attr_flags168 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype168_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype168_1 [] = {0xFF01,229,0xFFFF};
static const EIF_TYPE_INDEX g_atype168_2 [] = {163,0xFFFF};
static const EIF_TYPE_INDEX g_atype168_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype168_4 [] = {227,0xFFFF};
static const EIF_TYPE_INDEX g_atype168_5 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes168 [] = {
g_atype168_0,
g_atype168_1,
g_atype168_2,
g_atype168_3,
g_atype168_4,
g_atype168_5,
};

static const int32 cn_attr168 [] =
{
3350,
3365,
3367,
3368,
3362,
3363,
};

extern const char *names169[];
static const uint32 types169 [] =
{
SK_POINTER,
};

static const uint16 attr_flags169 [] =
{0,};

static const EIF_TYPE_INDEX g_atype169_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes169 [] = {
g_atype169_0,
};

static const int32 cn_attr169 [] =
{
3393,
};

extern const char *names170[];
static const uint32 types170 [] =
{
SK_POINTER,
SK_POINTER,
};

static const uint16 attr_flags170 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype170_0 [] = {227,0xFFFF};
static const EIF_TYPE_INDEX g_atype170_1 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes170 [] = {
g_atype170_0,
g_atype170_1,
};

static const int32 cn_attr170 [] =
{
3400,
3408,
};

extern const char *names171[];
static const uint32 types171 [] =
{
SK_INT32,
};

static const uint16 attr_flags171 [] =
{0,};

static const EIF_TYPE_INDEX g_atype171_0 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes171 [] = {
g_atype171_0,
};

static const int32 cn_attr171 [] =
{
3418,
};

extern const char *names172[];
static const uint32 types172 [] =
{
SK_REF,
SK_CHAR8,
SK_UINT8,
SK_INT8,
SK_UINT16,
SK_INT16,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_REAL32,
SK_UINT64,
SK_INT64,
SK_REAL64,
};

static const uint16 attr_flags172 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype172_0 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype172_1 [] = {197,0xFFFF};
static const EIF_TYPE_INDEX g_atype172_2 [] = {209,0xFFFF};
static const EIF_TYPE_INDEX g_atype172_3 [] = {188,0xFFFF};
static const EIF_TYPE_INDEX g_atype172_4 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype172_5 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype172_6 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype172_7 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype172_8 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype172_9 [] = {191,0xFFFF};
static const EIF_TYPE_INDEX g_atype172_10 [] = {224,0xFFFF};
static const EIF_TYPE_INDEX g_atype172_11 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype172_12 [] = {206,0xFFFF};

static const EIF_TYPE_INDEX *gtypes172 [] = {
g_atype172_0,
g_atype172_1,
g_atype172_2,
g_atype172_3,
g_atype172_4,
g_atype172_5,
g_atype172_6,
g_atype172_7,
g_atype172_8,
g_atype172_9,
g_atype172_10,
g_atype172_11,
g_atype172_12,
};

static const int32 cn_attr172 [] =
{
3428,
3427,
3438,
3433,
3437,
3432,
3435,
3429,
3441,
3439,
3434,
3431,
3440,
};

extern const char *names173[];
static const uint32 types173 [] =
{
SK_REF,
SK_CHAR8,
SK_BOOL,
SK_UINT8,
SK_INT8,
SK_UINT16,
SK_INT16,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_REAL32,
SK_POINTER,
SK_UINT64,
SK_INT64,
SK_REAL64,
};

static const uint16 attr_flags173 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype173_0 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype173_1 [] = {197,0xFFFF};
static const EIF_TYPE_INDEX g_atype173_2 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype173_3 [] = {209,0xFFFF};
static const EIF_TYPE_INDEX g_atype173_4 [] = {188,0xFFFF};
static const EIF_TYPE_INDEX g_atype173_5 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype173_6 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype173_7 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype173_8 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype173_9 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype173_10 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype173_11 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype173_12 [] = {191,0xFFFF};
static const EIF_TYPE_INDEX g_atype173_13 [] = {227,0xFFFF};
static const EIF_TYPE_INDEX g_atype173_14 [] = {224,0xFFFF};
static const EIF_TYPE_INDEX g_atype173_15 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype173_16 [] = {206,0xFFFF};

static const EIF_TYPE_INDEX *gtypes173 [] = {
g_atype173_0,
g_atype173_1,
g_atype173_2,
g_atype173_3,
g_atype173_4,
g_atype173_5,
g_atype173_6,
g_atype173_7,
g_atype173_8,
g_atype173_9,
g_atype173_10,
g_atype173_11,
g_atype173_12,
g_atype173_13,
g_atype173_14,
g_atype173_15,
g_atype173_16,
};

static const int32 cn_attr173 [] =
{
3428,
3427,
3522,
3438,
3433,
3437,
3432,
3435,
3429,
3441,
3510,
3511,
3439,
3514,
3434,
3431,
3440,
};

extern const char *names174[];
static const uint32 types174 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_CHAR8,
SK_CHAR8,
SK_BOOL,
SK_BOOL,
SK_UINT8,
SK_INT8,
SK_UINT16,
SK_INT16,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_REAL32,
SK_POINTER,
SK_UINT64,
SK_INT64,
SK_REAL64,
};

static const uint16 attr_flags174 [] =
{0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype174_0 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_1 [] = {0xFF01,229,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_2 [] = {163,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_3 [] = {197,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_4 [] = {197,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_6 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_7 [] = {209,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_8 [] = {188,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_9 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_10 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_11 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_12 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_13 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_14 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_15 [] = {191,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_16 [] = {227,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_17 [] = {224,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_18 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_19 [] = {206,0xFFFF};

static const EIF_TYPE_INDEX *gtypes174 [] = {
g_atype174_0,
g_atype174_1,
g_atype174_2,
g_atype174_3,
g_atype174_4,
g_atype174_5,
g_atype174_6,
g_atype174_7,
g_atype174_8,
g_atype174_9,
g_atype174_10,
g_atype174_11,
g_atype174_12,
g_atype174_13,
g_atype174_14,
g_atype174_15,
g_atype174_16,
g_atype174_17,
g_atype174_18,
g_atype174_19,
};

static const int32 cn_attr174 [] =
{
3428,
3616,
3618,
3427,
3534,
2212,
3680,
3438,
3433,
3437,
3432,
3435,
3429,
3441,
3671,
3439,
3535,
3434,
3431,
3440,
};

extern const char *names175[];
static const uint32 types175 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_CHAR8,
SK_CHAR8,
SK_BOOL,
SK_BOOL,
SK_UINT8,
SK_INT8,
SK_UINT16,
SK_INT16,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_REAL32,
SK_POINTER,
SK_UINT64,
SK_INT64,
SK_REAL64,
};

static const uint16 attr_flags175 [] =
{0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype175_0 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_1 [] = {0xFF01,229,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_2 [] = {163,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_3 [] = {163,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_4 [] = {197,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_5 [] = {197,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_6 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_7 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_8 [] = {209,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_9 [] = {188,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_10 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_11 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_12 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_13 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_14 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_15 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_16 [] = {191,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_17 [] = {227,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_18 [] = {224,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_19 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_20 [] = {206,0xFFFF};

static const EIF_TYPE_INDEX *gtypes175 [] = {
g_atype175_0,
g_atype175_1,
g_atype175_2,
g_atype175_3,
g_atype175_4,
g_atype175_5,
g_atype175_6,
g_atype175_7,
g_atype175_8,
g_atype175_9,
g_atype175_10,
g_atype175_11,
g_atype175_12,
g_atype175_13,
g_atype175_14,
g_atype175_15,
g_atype175_16,
g_atype175_17,
g_atype175_18,
g_atype175_19,
g_atype175_20,
};

static const int32 cn_attr175 [] =
{
3428,
3616,
3618,
3685,
3427,
3534,
2212,
3680,
3438,
3433,
3437,
3432,
3435,
3429,
3441,
3671,
3439,
3535,
3434,
3431,
3440,
};

extern const char *names176[];
static const uint32 types176 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_CHAR8,
SK_CHAR8,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_UINT8,
SK_INT8,
SK_UINT16,
SK_INT16,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_REAL32,
SK_POINTER,
SK_UINT64,
SK_INT64,
SK_REAL64,
};

static const uint16 attr_flags176 [] =
{0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype176_0 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_1 [] = {0xFF01,229,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_2 [] = {163,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_3 [] = {197,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_4 [] = {197,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_6 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_7 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_8 [] = {209,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_9 [] = {188,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_10 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_11 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_12 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_13 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_14 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_15 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_16 [] = {191,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_17 [] = {227,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_18 [] = {224,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_19 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_20 [] = {206,0xFFFF};

static const EIF_TYPE_INDEX *gtypes176 [] = {
g_atype176_0,
g_atype176_1,
g_atype176_2,
g_atype176_3,
g_atype176_4,
g_atype176_5,
g_atype176_6,
g_atype176_7,
g_atype176_8,
g_atype176_9,
g_atype176_10,
g_atype176_11,
g_atype176_12,
g_atype176_13,
g_atype176_14,
g_atype176_15,
g_atype176_16,
g_atype176_17,
g_atype176_18,
g_atype176_19,
g_atype176_20,
};

static const int32 cn_attr176 [] =
{
3428,
3616,
3618,
3427,
3534,
2212,
3680,
3694,
3438,
3433,
3437,
3432,
3435,
3429,
3441,
3671,
3439,
3535,
3434,
3431,
3440,
};

extern const char *names177[];
static const uint32 types177 [] =
{
SK_UINT8,
SK_POINTER,
};

static const uint16 attr_flags177 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype177_0 [] = {209,0xFFFF};
static const EIF_TYPE_INDEX g_atype177_1 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes177 [] = {
g_atype177_0,
g_atype177_1,
};

static const int32 cn_attr177 [] =
{
3711,
3712,
};

extern const char *names178[];
static const uint32 types178 [] =
{
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags178 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype178_0 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype178_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype178_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype178_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype178_4 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes178 [] = {
g_atype178_0,
g_atype178_1,
g_atype178_2,
g_atype178_3,
g_atype178_4,
};

static const int32 cn_attr178 [] =
{
2212,
2376,
3719,
3727,
3728,
};

extern const char *names179[];
static const uint32 types179 [] =
{
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags179 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype179_0 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype179_1 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes179 [] = {
g_atype179_0,
g_atype179_1,
};

static const int32 cn_attr179 [] =
{
2212,
2376,
};

extern const char *names180[];
static const uint32 types180 [] =
{
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags180 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype180_0 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype180_1 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes180 [] = {
g_atype180_0,
g_atype180_1,
};

static const int32 cn_attr180 [] =
{
2212,
2376,
};

extern const char *names182[];
static const uint32 types182 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags182 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype182_0 [] = {0xFF01,229,0xFFFF};
static const EIF_TYPE_INDEX g_atype182_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype182_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype182_3 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes182 [] = {
g_atype182_0,
g_atype182_1,
g_atype182_2,
g_atype182_3,
};

static const int32 cn_attr182 [] =
{
3759,
3757,
3760,
3761,
};

extern const char *names183[];
static const uint32 types183 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags183 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype183_0 [] = {0xFF01,495,194,0xFFFF};
static const EIF_TYPE_INDEX g_atype183_1 [] = {0xFF01,237,0xFFFF};
static const EIF_TYPE_INDEX g_atype183_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype183_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype183_4 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes183 [] = {
g_atype183_0,
g_atype183_1,
g_atype183_2,
g_atype183_3,
g_atype183_4,
};

static const int32 cn_attr183 [] =
{
3789,
3797,
3790,
3792,
3796,
};

extern const char *names184[];
static const uint32 types184 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags184 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype184_0 [] = {0xFF01,752,197,0xFFFF};
static const EIF_TYPE_INDEX g_atype184_1 [] = {0xFF01,230,0xFFFF};
static const EIF_TYPE_INDEX g_atype184_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype184_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype184_4 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes184 [] = {
g_atype184_0,
g_atype184_1,
g_atype184_2,
g_atype184_3,
g_atype184_4,
};

static const int32 cn_attr184 [] =
{
3789,
3800,
3790,
3792,
3799,
};

extern const char *names186[];
static const uint32 types186 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
};

static const uint16 attr_flags186 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype186_0 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype186_1 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype186_2 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes186 [] = {
g_atype186_0,
g_atype186_1,
g_atype186_2,
};

static const int32 cn_attr186 [] =
{
3851,
3855,
3850,
};

extern const char *names188[];
static const uint32 types188 [] =
{
SK_INT8,
};

static const uint16 attr_flags188 [] =
{0,};

static const EIF_TYPE_INDEX g_atype188_0 [] = {188,0xFFFF};

static const EIF_TYPE_INDEX *gtypes188 [] = {
g_atype188_0,
};

static const int32 cn_attr188 [] =
{
4014,
};

extern const char *names189[];
static const uint32 types189 [] =
{
SK_INT8,
};

static const uint16 attr_flags189 [] =
{0,};

static const EIF_TYPE_INDEX g_atype189_0 [] = {188,0xFFFF};

static const EIF_TYPE_INDEX *gtypes189 [] = {
g_atype189_0,
};

static const int32 cn_attr189 [] =
{
4014,
};

extern const char *names190[];
static const uint32 types190 [] =
{
SK_INT8,
};

static const uint16 attr_flags190 [] =
{0,};

static const EIF_TYPE_INDEX g_atype190_0 [] = {188,0xFFFF};

static const EIF_TYPE_INDEX *gtypes190 [] = {
g_atype190_0,
};

static const int32 cn_attr190 [] =
{
4014,
};

extern const char *names191[];
static const uint32 types191 [] =
{
SK_REAL32,
};

static const uint16 attr_flags191 [] =
{0,};

static const EIF_TYPE_INDEX g_atype191_0 [] = {191,0xFFFF};

static const EIF_TYPE_INDEX *gtypes191 [] = {
g_atype191_0,
};

static const int32 cn_attr191 [] =
{
4066,
};

extern const char *names192[];
static const uint32 types192 [] =
{
SK_REAL32,
};

static const uint16 attr_flags192 [] =
{0,};

static const EIF_TYPE_INDEX g_atype192_0 [] = {191,0xFFFF};

static const EIF_TYPE_INDEX *gtypes192 [] = {
g_atype192_0,
};

static const int32 cn_attr192 [] =
{
4066,
};

extern const char *names193[];
static const uint32 types193 [] =
{
SK_REAL32,
};

static const uint16 attr_flags193 [] =
{0,};

static const EIF_TYPE_INDEX g_atype193_0 [] = {191,0xFFFF};

static const EIF_TYPE_INDEX *gtypes193 [] = {
g_atype193_0,
};

static const int32 cn_attr193 [] =
{
4066,
};

extern const char *names194[];
static const uint32 types194 [] =
{
SK_CHAR32,
};

static const uint16 attr_flags194 [] =
{0,};

static const EIF_TYPE_INDEX g_atype194_0 [] = {194,0xFFFF};

static const EIF_TYPE_INDEX *gtypes194 [] = {
g_atype194_0,
};

static const int32 cn_attr194 [] =
{
4093,
};

extern const char *names195[];
static const uint32 types195 [] =
{
SK_CHAR32,
};

static const uint16 attr_flags195 [] =
{0,};

static const EIF_TYPE_INDEX g_atype195_0 [] = {194,0xFFFF};

static const EIF_TYPE_INDEX *gtypes195 [] = {
g_atype195_0,
};

static const int32 cn_attr195 [] =
{
4093,
};

extern const char *names196[];
static const uint32 types196 [] =
{
SK_CHAR32,
};

static const uint16 attr_flags196 [] =
{0,};

static const EIF_TYPE_INDEX g_atype196_0 [] = {194,0xFFFF};

static const EIF_TYPE_INDEX *gtypes196 [] = {
g_atype196_0,
};

static const int32 cn_attr196 [] =
{
4093,
};

extern const char *names197[];
static const uint32 types197 [] =
{
SK_CHAR8,
};

static const uint16 attr_flags197 [] =
{0,};

static const EIF_TYPE_INDEX g_atype197_0 [] = {197,0xFFFF};

static const EIF_TYPE_INDEX *gtypes197 [] = {
g_atype197_0,
};

static const int32 cn_attr197 [] =
{
4125,
};

extern const char *names198[];
static const uint32 types198 [] =
{
SK_CHAR8,
};

static const uint16 attr_flags198 [] =
{0,};

static const EIF_TYPE_INDEX g_atype198_0 [] = {197,0xFFFF};

static const EIF_TYPE_INDEX *gtypes198 [] = {
g_atype198_0,
};

static const int32 cn_attr198 [] =
{
4125,
};

extern const char *names199[];
static const uint32 types199 [] =
{
SK_CHAR8,
};

static const uint16 attr_flags199 [] =
{0,};

static const EIF_TYPE_INDEX g_atype199_0 [] = {197,0xFFFF};

static const EIF_TYPE_INDEX *gtypes199 [] = {
g_atype199_0,
};

static const int32 cn_attr199 [] =
{
4125,
};

extern const char *names200[];
static const uint32 types200 [] =
{
SK_INT64,
};

static const uint16 attr_flags200 [] =
{0,};

static const EIF_TYPE_INDEX g_atype200_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes200 [] = {
g_atype200_0,
};

static const int32 cn_attr200 [] =
{
4166,
};

extern const char *names201[];
static const uint32 types201 [] =
{
SK_INT64,
};

static const uint16 attr_flags201 [] =
{0,};

static const EIF_TYPE_INDEX g_atype201_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes201 [] = {
g_atype201_0,
};

static const int32 cn_attr201 [] =
{
4166,
};

extern const char *names202[];
static const uint32 types202 [] =
{
SK_INT64,
};

static const uint16 attr_flags202 [] =
{0,};

static const EIF_TYPE_INDEX g_atype202_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes202 [] = {
g_atype202_0,
};

static const int32 cn_attr202 [] =
{
4166,
};

extern const char *names203[];
static const uint32 types203 [] =
{
SK_BOOL,
};

static const uint16 attr_flags203 [] =
{0,};

static const EIF_TYPE_INDEX g_atype203_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes203 [] = {
g_atype203_0,
};

static const int32 cn_attr203 [] =
{
4217,
};

extern const char *names204[];
static const uint32 types204 [] =
{
SK_BOOL,
};

static const uint16 attr_flags204 [] =
{0,};

static const EIF_TYPE_INDEX g_atype204_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes204 [] = {
g_atype204_0,
};

static const int32 cn_attr204 [] =
{
4217,
};

extern const char *names205[];
static const uint32 types205 [] =
{
SK_BOOL,
};

static const uint16 attr_flags205 [] =
{0,};

static const EIF_TYPE_INDEX g_atype205_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes205 [] = {
g_atype205_0,
};

static const int32 cn_attr205 [] =
{
4217,
};

extern const char *names206[];
static const uint32 types206 [] =
{
SK_REAL64,
};

static const uint16 attr_flags206 [] =
{0,};

static const EIF_TYPE_INDEX g_atype206_0 [] = {206,0xFFFF};

static const EIF_TYPE_INDEX *gtypes206 [] = {
g_atype206_0,
};

static const int32 cn_attr206 [] =
{
4229,
};

extern const char *names207[];
static const uint32 types207 [] =
{
SK_REAL64,
};

static const uint16 attr_flags207 [] =
{0,};

static const EIF_TYPE_INDEX g_atype207_0 [] = {206,0xFFFF};

static const EIF_TYPE_INDEX *gtypes207 [] = {
g_atype207_0,
};

static const int32 cn_attr207 [] =
{
4229,
};

extern const char *names208[];
static const uint32 types208 [] =
{
SK_REAL64,
};

static const uint16 attr_flags208 [] =
{0,};

static const EIF_TYPE_INDEX g_atype208_0 [] = {206,0xFFFF};

static const EIF_TYPE_INDEX *gtypes208 [] = {
g_atype208_0,
};

static const int32 cn_attr208 [] =
{
4229,
};

extern const char *names209[];
static const uint32 types209 [] =
{
SK_UINT8,
};

static const uint16 attr_flags209 [] =
{0,};

static const EIF_TYPE_INDEX g_atype209_0 [] = {209,0xFFFF};

static const EIF_TYPE_INDEX *gtypes209 [] = {
g_atype209_0,
};

static const int32 cn_attr209 [] =
{
4256,
};

extern const char *names210[];
static const uint32 types210 [] =
{
SK_UINT8,
};

static const uint16 attr_flags210 [] =
{0,};

static const EIF_TYPE_INDEX g_atype210_0 [] = {209,0xFFFF};

static const EIF_TYPE_INDEX *gtypes210 [] = {
g_atype210_0,
};

static const int32 cn_attr210 [] =
{
4256,
};

extern const char *names211[];
static const uint32 types211 [] =
{
SK_UINT8,
};

static const uint16 attr_flags211 [] =
{0,};

static const EIF_TYPE_INDEX g_atype211_0 [] = {209,0xFFFF};

static const EIF_TYPE_INDEX *gtypes211 [] = {
g_atype211_0,
};

static const int32 cn_attr211 [] =
{
4256,
};

extern const char *names212[];
static const uint32 types212 [] =
{
SK_UINT32,
};

static const uint16 attr_flags212 [] =
{0,};

static const EIF_TYPE_INDEX g_atype212_0 [] = {212,0xFFFF};

static const EIF_TYPE_INDEX *gtypes212 [] = {
g_atype212_0,
};

static const int32 cn_attr212 [] =
{
4305,
};

extern const char *names213[];
static const uint32 types213 [] =
{
SK_UINT32,
};

static const uint16 attr_flags213 [] =
{0,};

static const EIF_TYPE_INDEX g_atype213_0 [] = {212,0xFFFF};

static const EIF_TYPE_INDEX *gtypes213 [] = {
g_atype213_0,
};

static const int32 cn_attr213 [] =
{
4305,
};

extern const char *names214[];
static const uint32 types214 [] =
{
SK_UINT32,
};

static const uint16 attr_flags214 [] =
{0,};

static const EIF_TYPE_INDEX g_atype214_0 [] = {212,0xFFFF};

static const EIF_TYPE_INDEX *gtypes214 [] = {
g_atype214_0,
};

static const int32 cn_attr214 [] =
{
4305,
};

extern const char *names215[];
static const uint32 types215 [] =
{
SK_UINT16,
};

static const uint16 attr_flags215 [] =
{0,};

static const EIF_TYPE_INDEX g_atype215_0 [] = {215,0xFFFF};

static const EIF_TYPE_INDEX *gtypes215 [] = {
g_atype215_0,
};

static const int32 cn_attr215 [] =
{
4353,
};

extern const char *names216[];
static const uint32 types216 [] =
{
SK_UINT16,
};

static const uint16 attr_flags216 [] =
{0,};

static const EIF_TYPE_INDEX g_atype216_0 [] = {215,0xFFFF};

static const EIF_TYPE_INDEX *gtypes216 [] = {
g_atype216_0,
};

static const int32 cn_attr216 [] =
{
4353,
};

extern const char *names217[];
static const uint32 types217 [] =
{
SK_UINT16,
};

static const uint16 attr_flags217 [] =
{0,};

static const EIF_TYPE_INDEX g_atype217_0 [] = {215,0xFFFF};

static const EIF_TYPE_INDEX *gtypes217 [] = {
g_atype217_0,
};

static const int32 cn_attr217 [] =
{
4353,
};

extern const char *names218[];
static const uint32 types218 [] =
{
SK_INT32,
};

static const uint16 attr_flags218 [] =
{0,};

static const EIF_TYPE_INDEX g_atype218_0 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes218 [] = {
g_atype218_0,
};

static const int32 cn_attr218 [] =
{
4402,
};

extern const char *names219[];
static const uint32 types219 [] =
{
SK_INT32,
};

static const uint16 attr_flags219 [] =
{0,};

static const EIF_TYPE_INDEX g_atype219_0 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes219 [] = {
g_atype219_0,
};

static const int32 cn_attr219 [] =
{
4402,
};

extern const char *names220[];
static const uint32 types220 [] =
{
SK_INT32,
};

static const uint16 attr_flags220 [] =
{0,};

static const EIF_TYPE_INDEX g_atype220_0 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes220 [] = {
g_atype220_0,
};

static const int32 cn_attr220 [] =
{
4402,
};

extern const char *names221[];
static const uint32 types221 [] =
{
SK_INT16,
};

static const uint16 attr_flags221 [] =
{0,};

static const EIF_TYPE_INDEX g_atype221_0 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes221 [] = {
g_atype221_0,
};

static const int32 cn_attr221 [] =
{
4454,
};

extern const char *names222[];
static const uint32 types222 [] =
{
SK_INT16,
};

static const uint16 attr_flags222 [] =
{0,};

static const EIF_TYPE_INDEX g_atype222_0 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes222 [] = {
g_atype222_0,
};

static const int32 cn_attr222 [] =
{
4454,
};

extern const char *names223[];
static const uint32 types223 [] =
{
SK_INT16,
};

static const uint16 attr_flags223 [] =
{0,};

static const EIF_TYPE_INDEX g_atype223_0 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes223 [] = {
g_atype223_0,
};

static const int32 cn_attr223 [] =
{
4454,
};

extern const char *names224[];
static const uint32 types224 [] =
{
SK_UINT64,
};

static const uint16 attr_flags224 [] =
{0,};

static const EIF_TYPE_INDEX g_atype224_0 [] = {224,0xFFFF};

static const EIF_TYPE_INDEX *gtypes224 [] = {
g_atype224_0,
};

static const int32 cn_attr224 [] =
{
4506,
};

extern const char *names225[];
static const uint32 types225 [] =
{
SK_UINT64,
};

static const uint16 attr_flags225 [] =
{0,};

static const EIF_TYPE_INDEX g_atype225_0 [] = {224,0xFFFF};

static const EIF_TYPE_INDEX *gtypes225 [] = {
g_atype225_0,
};

static const int32 cn_attr225 [] =
{
4506,
};

extern const char *names226[];
static const uint32 types226 [] =
{
SK_UINT64,
};

static const uint16 attr_flags226 [] =
{0,};

static const EIF_TYPE_INDEX g_atype226_0 [] = {224,0xFFFF};

static const EIF_TYPE_INDEX *gtypes226 [] = {
g_atype226_0,
};

static const int32 cn_attr226 [] =
{
4506,
};

extern const char *names227[];
static const uint32 types227 [] =
{
SK_POINTER,
};

static const uint16 attr_flags227 [] =
{0,};

static const EIF_TYPE_INDEX g_atype227_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes227 [] = {
g_atype227_0,
};

static const int32 cn_attr227 [] =
{
4554,
};

extern const char *names228[];
static const uint32 types228 [] =
{
SK_POINTER,
};

static const uint16 attr_flags228 [] =
{0,};

static const EIF_TYPE_INDEX g_atype228_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes228 [] = {
g_atype228_0,
};

static const int32 cn_attr228 [] =
{
4554,
};

extern const char *names229[];
static const uint32 types229 [] =
{
SK_POINTER,
};

static const uint16 attr_flags229 [] =
{0,};

static const EIF_TYPE_INDEX g_atype229_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes229 [] = {
g_atype229_0,
};

static const int32 cn_attr229 [] =
{
4554,
};

extern const char *names230[];
static const uint32 types230 [] =
{
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags230 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype230_0 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype230_1 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes230 [] = {
g_atype230_0,
g_atype230_1,
};

static const int32 cn_attr230 [] =
{
4714,
4715,
};

extern const char *names231[];
static const uint32 types231 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags231 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype231_0 [] = {0xFF01,752,197,0xFFFF};
static const EIF_TYPE_INDEX g_atype231_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype231_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype231_3 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes231 [] = {
g_atype231_0,
g_atype231_1,
g_atype231_2,
g_atype231_3,
};

static const int32 cn_attr231 [] =
{
4745,
4714,
4715,
4748,
};

extern const char *names232[];
static const uint32 types232 [] =
{
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags232 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype232_0 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype232_1 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes232 [] = {
g_atype232_0,
g_atype232_1,
};

static const int32 cn_attr232 [] =
{
4714,
4715,
};

extern const char *names233[];
static const uint32 types233 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags233 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype233_0 [] = {0xFF01,752,197,0xFFFF};
static const EIF_TYPE_INDEX g_atype233_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype233_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype233_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype233_4 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes233 [] = {
g_atype233_0,
g_atype233_1,
g_atype233_2,
g_atype233_3,
g_atype233_4,
};

static const int32 cn_attr233 [] =
{
4745,
2212,
4714,
4715,
4748,
};

extern const char *names234[];
static const uint32 types234 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags234 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype234_0 [] = {0xFF01,752,197,0xFFFF};
static const EIF_TYPE_INDEX g_atype234_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype234_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype234_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype234_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype234_5 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes234 [] = {
g_atype234_0,
g_atype234_1,
g_atype234_2,
g_atype234_3,
g_atype234_4,
g_atype234_5,
};

static const int32 cn_attr234 [] =
{
4745,
2212,
4714,
4715,
4748,
4833,
};

extern const char *names235[];
static const uint32 types235 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags235 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype235_0 [] = {0xFF01,752,197,0xFFFF};
static const EIF_TYPE_INDEX g_atype235_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype235_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype235_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype235_4 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes235 [] = {
g_atype235_0,
g_atype235_1,
g_atype235_2,
g_atype235_3,
g_atype235_4,
};

static const int32 cn_attr235 [] =
{
4745,
2212,
4714,
4715,
4748,
};

extern const char *names236[];
static const uint32 types236 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags236 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype236_0 [] = {0xFF01,752,197,0xFFFF};
static const EIF_TYPE_INDEX g_atype236_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype236_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype236_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype236_4 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes236 [] = {
g_atype236_0,
g_atype236_1,
g_atype236_2,
g_atype236_3,
g_atype236_4,
};

static const int32 cn_attr236 [] =
{
4745,
2212,
4714,
4715,
4748,
};

extern const char *names237[];
static const uint32 types237 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags237 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype237_0 [] = {0xFF01,752,197,0xFFFF};
static const EIF_TYPE_INDEX g_atype237_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype237_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype237_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype237_4 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes237 [] = {
g_atype237_0,
g_atype237_1,
g_atype237_2,
g_atype237_3,
g_atype237_4,
};

static const int32 cn_attr237 [] =
{
4745,
2212,
4714,
4715,
4748,
};

extern const char *names238[];
static const uint32 types238 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags238 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype238_0 [] = {0xFF01,495,194,0xFFFF};
static const EIF_TYPE_INDEX g_atype238_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype238_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype238_3 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes238 [] = {
g_atype238_0,
g_atype238_1,
g_atype238_2,
g_atype238_3,
};

static const int32 cn_attr238 [] =
{
4888,
4714,
4715,
4891,
};

extern const char *names239[];
static const uint32 types239 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags239 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype239_0 [] = {0xFF01,495,194,0xFFFF};
static const EIF_TYPE_INDEX g_atype239_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype239_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype239_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype239_4 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes239 [] = {
g_atype239_0,
g_atype239_1,
g_atype239_2,
g_atype239_3,
g_atype239_4,
};

static const int32 cn_attr239 [] =
{
4888,
2212,
4714,
4715,
4891,
};

extern const char *names240[];
static const uint32 types240 [] =
{
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags240 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype240_0 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype240_1 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes240 [] = {
g_atype240_0,
g_atype240_1,
};

static const int32 cn_attr240 [] =
{
4714,
4715,
};

extern const char *names241[];
static const uint32 types241 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags241 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype241_0 [] = {0xFF01,752,197,0xFFFF};
static const EIF_TYPE_INDEX g_atype241_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype241_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype241_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype241_4 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes241 [] = {
g_atype241_0,
g_atype241_1,
g_atype241_2,
g_atype241_3,
g_atype241_4,
};

static const int32 cn_attr241 [] =
{
4745,
4714,
4715,
4748,
4935,
};

extern const char *names242[];
static const uint32 types242 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags242 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype242_0 [] = {0xFF01,495,194,0xFFFF};
static const EIF_TYPE_INDEX g_atype242_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype242_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype242_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype242_4 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes242 [] = {
g_atype242_0,
g_atype242_1,
g_atype242_2,
g_atype242_3,
g_atype242_4,
};

static const int32 cn_attr242 [] =
{
4888,
4714,
4715,
4891,
4942,
};

extern const char *names243[];
static const uint32 types243 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_CHAR8,
SK_CHAR8,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_UINT8,
SK_INT8,
SK_UINT16,
SK_INT16,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_REAL32,
SK_POINTER,
SK_UINT64,
SK_INT64,
SK_REAL64,
};

static const uint16 attr_flags243 [] =
{0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype243_0 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_1 [] = {0xFF01,229,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_2 [] = {163,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_3 [] = {197,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_4 [] = {197,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_6 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_7 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_8 [] = {209,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_9 [] = {188,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_10 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_11 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_12 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_13 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_14 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_15 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_16 [] = {191,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_17 [] = {227,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_18 [] = {224,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_19 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_20 [] = {206,0xFFFF};

static const EIF_TYPE_INDEX *gtypes243 [] = {
g_atype243_0,
g_atype243_1,
g_atype243_2,
g_atype243_3,
g_atype243_4,
g_atype243_5,
g_atype243_6,
g_atype243_7,
g_atype243_8,
g_atype243_9,
g_atype243_10,
g_atype243_11,
g_atype243_12,
g_atype243_13,
g_atype243_14,
g_atype243_15,
g_atype243_16,
g_atype243_17,
g_atype243_18,
g_atype243_19,
g_atype243_20,
};

static const int32 cn_attr243 [] =
{
3428,
3616,
3618,
3427,
3534,
2212,
3680,
3694,
3438,
3433,
3437,
3432,
3435,
3429,
3441,
3671,
3439,
3535,
3434,
3431,
3440,
};

extern const char *names244[];
static const uint32 types244 [] =
{
SK_REF,
};

static const uint16 attr_flags244 [] =
{0,};

static const EIF_TYPE_INDEX g_atype244_0 [] = {0xFF01,807,203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes244 [] = {
g_atype244_0,
};

static const int32 cn_attr244 [] =
{
2156,
};

extern const char *names245[];
static const uint32 types245 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags245 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype245_0 [] = {320,0xFF01,14,0xFFFF};
static const EIF_TYPE_INDEX g_atype245_1 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX g_atype245_2 [] = {0xFF01,238,0xFFFF};
static const EIF_TYPE_INDEX g_atype245_3 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype245_4 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes245 [] = {
g_atype245_0,
g_atype245_1,
g_atype245_2,
g_atype245_3,
g_atype245_4,
};

static const int32 cn_attr245 [] =
{
967,
4972,
4974,
4975,
4973,
};

extern const char *names246[];
static const uint32 types246 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags246 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype246_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype246_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes246 [] = {
g_atype246_0,
g_atype246_1,
};

static const int32 cn_attr246 [] =
{
3889,
3890,
};

extern const char *names247[];
static const uint32 types247 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags247 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype247_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype247_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes247 [] = {
g_atype247_0,
g_atype247_1,
};

static const int32 cn_attr247 [] =
{
3889,
3890,
};

extern const char *names248[];
static const uint32 types248 [] =
{
SK_BOOL,
};

static const uint16 attr_flags248 [] =
{0,};

static const EIF_TYPE_INDEX g_atype248_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes248 [] = {
g_atype248_0,
};

static const int32 cn_attr248 [] =
{
2212,
};

extern const char *names251[];
static const uint32 types251 [] =
{
SK_BOOL,
};

static const uint16 attr_flags251 [] =
{0,};

static const EIF_TYPE_INDEX g_atype251_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes251 [] = {
g_atype251_0,
};

static const int32 cn_attr251 [] =
{
2212,
};

extern const char *names252[];
static const uint32 types252 [] =
{
SK_BOOL,
};

static const uint16 attr_flags252 [] =
{0,};

static const EIF_TYPE_INDEX g_atype252_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes252 [] = {
g_atype252_0,
};

static const int32 cn_attr252 [] =
{
2212,
};

extern const char *names254[];
static const uint32 types254 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags254 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype254_0 [] = {0xFF01,252,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype254_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype254_2 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype254_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype254_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype254_5 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype254_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes254 [] = {
g_atype254_0,
g_atype254_1,
g_atype254_2,
g_atype254_3,
g_atype254_4,
g_atype254_5,
g_atype254_6,
};

static const int32 cn_attr254 [] =
{
3782,
3783,
3779,
3784,
3785,
3786,
3787,
};

extern const char *names257[];
static const uint32 types257 [] =
{
SK_BOOL,
};

static const uint16 attr_flags257 [] =
{0,};

static const EIF_TYPE_INDEX g_atype257_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes257 [] = {
g_atype257_0,
};

static const int32 cn_attr257 [] =
{
2212,
};

extern const char *names258[];
static const uint32 types258 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_POINTER,
SK_POINTER,
SK_POINTER,
};

static const uint16 attr_flags258 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype258_0 [] = {0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype258_1 [] = {0xFFF9,0,186,0xFFFF};
static const EIF_TYPE_INDEX g_atype258_2 [] = {455,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype258_3 [] = {455,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype258_4 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype258_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype258_6 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype258_7 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype258_8 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype258_9 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype258_10 [] = {227,0xFFFF};
static const EIF_TYPE_INDEX g_atype258_11 [] = {227,0xFFFF};
static const EIF_TYPE_INDEX g_atype258_12 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes258 [] = {
g_atype258_0,
g_atype258_1,
g_atype258_2,
g_atype258_3,
g_atype258_4,
g_atype258_5,
g_atype258_6,
g_atype258_7,
g_atype258_8,
g_atype258_9,
g_atype258_10,
g_atype258_11,
g_atype258_12,
};

static const int32 cn_attr258 [] =
{
4579,
4594,
4598,
4606,
4587,
4601,
4615,
4588,
4600,
4602,
4596,
4597,
4599,
};

extern const char *names259[];
static const uint32 types259 [] =
{
SK_POINTER,
};

static const uint16 attr_flags259 [] =
{0,};

static const EIF_TYPE_INDEX g_atype259_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes259 [] = {
g_atype259_0,
};

static const int32 cn_attr259 [] =
{
4554,
};

extern const char *names260[];
static const uint32 types260 [] =
{
SK_POINTER,
};

static const uint16 attr_flags260 [] =
{0,};

static const EIF_TYPE_INDEX g_atype260_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes260 [] = {
g_atype260_0,
};

static const int32 cn_attr260 [] =
{
4554,
};

extern const char *names261[];
static const uint32 types261 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags261 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype261_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype261_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes261 [] = {
g_atype261_0,
g_atype261_1,
};

static const int32 cn_attr261 [] =
{
3889,
3890,
};

extern const char *names262[];
static const uint32 types262 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_POINTER,
SK_POINTER,
SK_POINTER,
};

static const uint16 attr_flags262 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype262_0 [] = {0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype262_1 [] = {0xFFF9,0,186,0xFFFF};
static const EIF_TYPE_INDEX g_atype262_2 [] = {455,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype262_3 [] = {455,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype262_4 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype262_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype262_6 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype262_7 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype262_8 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype262_9 [] = {227,0xFFFF};
static const EIF_TYPE_INDEX g_atype262_10 [] = {227,0xFFFF};
static const EIF_TYPE_INDEX g_atype262_11 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes262 [] = {
g_atype262_0,
g_atype262_1,
g_atype262_2,
g_atype262_3,
g_atype262_4,
g_atype262_5,
g_atype262_6,
g_atype262_7,
g_atype262_8,
g_atype262_9,
g_atype262_10,
g_atype262_11,
};

static const int32 cn_attr262 [] =
{
4579,
4594,
4598,
4606,
4587,
4601,
4588,
4600,
4602,
4596,
4597,
4599,
};

extern const char *names263[];
static const uint32 types263 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_POINTER,
SK_POINTER,
SK_POINTER,
};

static const uint16 attr_flags263 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype263_0 [] = {0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype263_1 [] = {0xFFF9,0,186,0xFFFF};
static const EIF_TYPE_INDEX g_atype263_2 [] = {455,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype263_3 [] = {455,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype263_4 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype263_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype263_6 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype263_7 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype263_8 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype263_9 [] = {227,0xFFFF};
static const EIF_TYPE_INDEX g_atype263_10 [] = {227,0xFFFF};
static const EIF_TYPE_INDEX g_atype263_11 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes263 [] = {
g_atype263_0,
g_atype263_1,
g_atype263_2,
g_atype263_3,
g_atype263_4,
g_atype263_5,
g_atype263_6,
g_atype263_7,
g_atype263_8,
g_atype263_9,
g_atype263_10,
g_atype263_11,
};

static const int32 cn_attr263 [] =
{
4579,
4594,
4598,
4606,
4587,
4601,
4588,
4600,
4602,
4596,
4597,
4599,
};

extern const char *names264[];
static const uint32 types264 [] =
{
SK_BOOL,
};

static const uint16 attr_flags264 [] =
{0,};

static const EIF_TYPE_INDEX g_atype264_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes264 [] = {
g_atype264_0,
};

static const int32 cn_attr264 [] =
{
2212,
};

extern const char *names265[];
static const uint32 types265 [] =
{
SK_BOOL,
};

static const uint16 attr_flags265 [] =
{0,};

static const EIF_TYPE_INDEX g_atype265_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes265 [] = {
g_atype265_0,
};

static const int32 cn_attr265 [] =
{
2212,
};

extern const char *names266[];
static const uint32 types266 [] =
{
SK_BOOL,
};

static const uint16 attr_flags266 [] =
{0,};

static const EIF_TYPE_INDEX g_atype266_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes266 [] = {
g_atype266_0,
};

static const int32 cn_attr266 [] =
{
2212,
};

extern const char *names267[];
static const uint32 types267 [] =
{
SK_BOOL,
};

static const uint16 attr_flags267 [] =
{0,};

static const EIF_TYPE_INDEX g_atype267_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes267 [] = {
g_atype267_0,
};

static const int32 cn_attr267 [] =
{
2212,
};

extern const char *names268[];
static const uint32 types268 [] =
{
SK_BOOL,
};

static const uint16 attr_flags268 [] =
{0,};

static const EIF_TYPE_INDEX g_atype268_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes268 [] = {
g_atype268_0,
};

static const int32 cn_attr268 [] =
{
2212,
};

extern const char *names269[];
static const uint32 types269 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags269 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype269_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype269_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype269_2 [] = {0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype269_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype269_4 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes269 [] = {
g_atype269_0,
g_atype269_1,
g_atype269_2,
g_atype269_3,
g_atype269_4,
};

static const int32 cn_attr269 [] =
{
2084,
2105,
2106,
2082,
2083,
};

extern const char *names270[];
static const uint32 types270 [] =
{
SK_BOOL,
};

static const uint16 attr_flags270 [] =
{0,};

static const EIF_TYPE_INDEX g_atype270_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes270 [] = {
g_atype270_0,
};

static const int32 cn_attr270 [] =
{
2212,
};

extern const char *names271[];
static const uint32 types271 [] =
{
SK_BOOL,
};

static const uint16 attr_flags271 [] =
{0,};

static const EIF_TYPE_INDEX g_atype271_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes271 [] = {
g_atype271_0,
};

static const int32 cn_attr271 [] =
{
2212,
};

extern const char *names272[];
static const uint32 types272 [] =
{
SK_BOOL,
};

static const uint16 attr_flags272 [] =
{0,};

static const EIF_TYPE_INDEX g_atype272_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes272 [] = {
g_atype272_0,
};

static const int32 cn_attr272 [] =
{
2212,
};

extern const char *names273[];
static const uint32 types273 [] =
{
SK_BOOL,
};

static const uint16 attr_flags273 [] =
{0,};

static const EIF_TYPE_INDEX g_atype273_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes273 [] = {
g_atype273_0,
};

static const int32 cn_attr273 [] =
{
2212,
};

extern const char *names274[];
static const uint32 types274 [] =
{
SK_BOOL,
};

static const uint16 attr_flags274 [] =
{0,};

static const EIF_TYPE_INDEX g_atype274_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes274 [] = {
g_atype274_0,
};

static const int32 cn_attr274 [] =
{
2212,
};

extern const char *names275[];
static const uint32 types275 [] =
{
SK_BOOL,
};

static const uint16 attr_flags275 [] =
{0,};

static const EIF_TYPE_INDEX g_atype275_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes275 [] = {
g_atype275_0,
};

static const int32 cn_attr275 [] =
{
2212,
};

extern const char *names276[];
static const uint32 types276 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags276 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype276_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype276_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes276 [] = {
g_atype276_0,
g_atype276_1,
};

static const int32 cn_attr276 [] =
{
3889,
3890,
};

extern const char *names277[];
static const uint32 types277 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags277 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype277_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype277_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes277 [] = {
g_atype277_0,
g_atype277_1,
};

static const int32 cn_attr277 [] =
{
3889,
3890,
};

extern const char *names278[];
static const uint32 types278 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags278 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype278_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype278_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes278 [] = {
g_atype278_0,
g_atype278_1,
};

static const int32 cn_attr278 [] =
{
3889,
3890,
};

extern const char *names279[];
static const uint32 types279 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags279 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype279_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype279_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes279 [] = {
g_atype279_0,
g_atype279_1,
};

static const int32 cn_attr279 [] =
{
3889,
3890,
};

extern const char *names280[];
static const uint32 types280 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags280 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype280_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype280_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes280 [] = {
g_atype280_0,
g_atype280_1,
};

static const int32 cn_attr280 [] =
{
3889,
3890,
};

extern const char *names281[];
static const uint32 types281 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags281 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype281_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype281_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes281 [] = {
g_atype281_0,
g_atype281_1,
};

static const int32 cn_attr281 [] =
{
3889,
3890,
};

extern const char *names282[];
static const uint32 types282 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags282 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype282_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype282_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes282 [] = {
g_atype282_0,
g_atype282_1,
};

static const int32 cn_attr282 [] =
{
3889,
3890,
};

extern const char *names283[];
static const uint32 types283 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags283 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype283_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype283_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes283 [] = {
g_atype283_0,
g_atype283_1,
};

static const int32 cn_attr283 [] =
{
3889,
3890,
};

extern const char *names284[];
static const uint32 types284 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags284 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype284_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype284_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes284 [] = {
g_atype284_0,
g_atype284_1,
};

static const int32 cn_attr284 [] =
{
3889,
3890,
};

extern const char *names285[];
static const uint32 types285 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags285 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype285_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype285_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes285 [] = {
g_atype285_0,
g_atype285_1,
};

static const int32 cn_attr285 [] =
{
3889,
3890,
};

extern const char *names286[];
static const uint32 types286 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags286 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype286_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype286_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes286 [] = {
g_atype286_0,
g_atype286_1,
};

static const int32 cn_attr286 [] =
{
3889,
3890,
};

extern const char *names287[];
static const uint32 types287 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags287 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype287_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype287_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes287 [] = {
g_atype287_0,
g_atype287_1,
};

static const int32 cn_attr287 [] =
{
3889,
3890,
};

extern const char *names288[];
static const uint32 types288 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags288 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype288_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype288_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes288 [] = {
g_atype288_0,
g_atype288_1,
};

static const int32 cn_attr288 [] =
{
3889,
3890,
};

extern const char *names289[];
static const uint32 types289 [] =
{
SK_BOOL,
};

static const uint16 attr_flags289 [] =
{0,};

static const EIF_TYPE_INDEX g_atype289_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes289 [] = {
g_atype289_0,
};

static const int32 cn_attr289 [] =
{
2212,
};

extern const char *names292[];
static const uint32 types292 [] =
{
SK_BOOL,
};

static const uint16 attr_flags292 [] =
{0,};

static const EIF_TYPE_INDEX g_atype292_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes292 [] = {
g_atype292_0,
};

static const int32 cn_attr292 [] =
{
2212,
};

extern const char *names293[];
static const uint32 types293 [] =
{
SK_BOOL,
};

static const uint16 attr_flags293 [] =
{0,};

static const EIF_TYPE_INDEX g_atype293_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes293 [] = {
g_atype293_0,
};

static const int32 cn_attr293 [] =
{
2212,
};

extern const char *names294[];
static const uint32 types294 [] =
{
SK_BOOL,
};

static const uint16 attr_flags294 [] =
{0,};

static const EIF_TYPE_INDEX g_atype294_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes294 [] = {
g_atype294_0,
};

static const int32 cn_attr294 [] =
{
2212,
};

extern const char *names295[];
static const uint32 types295 [] =
{
SK_BOOL,
};

static const uint16 attr_flags295 [] =
{0,};

static const EIF_TYPE_INDEX g_atype295_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes295 [] = {
g_atype295_0,
};

static const int32 cn_attr295 [] =
{
2212,
};

extern const char *names296[];
static const uint32 types296 [] =
{
SK_BOOL,
};

static const uint16 attr_flags296 [] =
{0,};

static const EIF_TYPE_INDEX g_atype296_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes296 [] = {
g_atype296_0,
};

static const int32 cn_attr296 [] =
{
2212,
};

extern const char *names297[];
static const uint32 types297 [] =
{
SK_BOOL,
};

static const uint16 attr_flags297 [] =
{0,};

static const EIF_TYPE_INDEX g_atype297_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes297 [] = {
g_atype297_0,
};

static const int32 cn_attr297 [] =
{
2212,
};

extern const char *names298[];
static const uint32 types298 [] =
{
SK_BOOL,
};

static const uint16 attr_flags298 [] =
{0,};

static const EIF_TYPE_INDEX g_atype298_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes298 [] = {
g_atype298_0,
};

static const int32 cn_attr298 [] =
{
2212,
};

extern const char *names299[];
static const uint32 types299 [] =
{
SK_BOOL,
};

static const uint16 attr_flags299 [] =
{0,};

static const EIF_TYPE_INDEX g_atype299_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes299 [] = {
g_atype299_0,
};

static const int32 cn_attr299 [] =
{
2212,
};

extern const char *names301[];
static const uint32 types301 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags301 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype301_0 [] = {0xFF01,299,194,0xFFFF};
static const EIF_TYPE_INDEX g_atype301_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype301_2 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype301_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype301_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype301_5 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype301_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes301 [] = {
g_atype301_0,
g_atype301_1,
g_atype301_2,
g_atype301_3,
g_atype301_4,
g_atype301_5,
g_atype301_6,
};

static const int32 cn_attr301 [] =
{
3782,
3783,
3779,
3784,
3785,
3786,
3787,
};

extern const char *names304[];
static const uint32 types304 [] =
{
SK_BOOL,
};

static const uint16 attr_flags304 [] =
{0,};

static const EIF_TYPE_INDEX g_atype304_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes304 [] = {
g_atype304_0,
};

static const int32 cn_attr304 [] =
{
2212,
};

extern const char *names307[];
static const uint32 types307 [] =
{
SK_BOOL,
};

static const uint16 attr_flags307 [] =
{0,};

static const EIF_TYPE_INDEX g_atype307_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes307 [] = {
g_atype307_0,
};

static const int32 cn_attr307 [] =
{
2212,
};

extern const char *names308[];
static const uint32 types308 [] =
{
SK_BOOL,
};

static const uint16 attr_flags308 [] =
{0,};

static const EIF_TYPE_INDEX g_atype308_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes308 [] = {
g_atype308_0,
};

static const int32 cn_attr308 [] =
{
2212,
};

extern const char *names309[];
static const uint32 types309 [] =
{
SK_BOOL,
};

static const uint16 attr_flags309 [] =
{0,};

static const EIF_TYPE_INDEX g_atype309_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes309 [] = {
g_atype309_0,
};

static const int32 cn_attr309 [] =
{
2212,
};

extern const char *names310[];
static const uint32 types310 [] =
{
SK_BOOL,
};

static const uint16 attr_flags310 [] =
{0,};

static const EIF_TYPE_INDEX g_atype310_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes310 [] = {
g_atype310_0,
};

static const int32 cn_attr310 [] =
{
2212,
};

extern const char *names311[];
static const uint32 types311 [] =
{
SK_BOOL,
};

static const uint16 attr_flags311 [] =
{0,};

static const EIF_TYPE_INDEX g_atype311_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes311 [] = {
g_atype311_0,
};

static const int32 cn_attr311 [] =
{
2212,
};

extern const char *names312[];
static const uint32 types312 [] =
{
SK_BOOL,
};

static const uint16 attr_flags312 [] =
{0,};

static const EIF_TYPE_INDEX g_atype312_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes312 [] = {
g_atype312_0,
};

static const int32 cn_attr312 [] =
{
2212,
};

extern const char *names313[];
static const uint32 types313 [] =
{
SK_BOOL,
};

static const uint16 attr_flags313 [] =
{0,};

static const EIF_TYPE_INDEX g_atype313_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes313 [] = {
g_atype313_0,
};

static const int32 cn_attr313 [] =
{
2212,
};

extern const char *names314[];
static const uint32 types314 [] =
{
SK_BOOL,
};

static const uint16 attr_flags314 [] =
{0,};

static const EIF_TYPE_INDEX g_atype314_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes314 [] = {
g_atype314_0,
};

static const int32 cn_attr314 [] =
{
2212,
};

extern const char *names316[];
static const uint32 types316 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags316 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype316_0 [] = {0xFF01,314,197,0xFFFF};
static const EIF_TYPE_INDEX g_atype316_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype316_2 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype316_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype316_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype316_5 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype316_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes316 [] = {
g_atype316_0,
g_atype316_1,
g_atype316_2,
g_atype316_3,
g_atype316_4,
g_atype316_5,
g_atype316_6,
};

static const int32 cn_attr316 [] =
{
3782,
3783,
3779,
3784,
3785,
3786,
3787,
};

extern const char *names319[];
static const uint32 types319 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags319 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype319_0 [] = {0xFF01,324,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype319_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype319_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype319_3 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes319 [] = {
g_atype319_0,
g_atype319_1,
g_atype319_2,
g_atype319_3,
};

static const int32 cn_attr319 [] =
{
2156,
2212,
2664,
2665,
};

extern const char *names321[];
static const uint32 types321 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags321 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype321_0 [] = {0xFF01,324,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype321_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype321_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes321 [] = {
g_atype321_0,
g_atype321_1,
g_atype321_2,
};

static const int32 cn_attr321 [] =
{
2156,
2212,
3094,
};

extern const char *names322[];
static const uint32 types322 [] =
{
SK_BOOL,
};

static const uint16 attr_flags322 [] =
{0,};

static const EIF_TYPE_INDEX g_atype322_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes322 [] = {
g_atype322_0,
};

static const int32 cn_attr322 [] =
{
2212,
};

extern const char *names323[];
static const uint32 types323 [] =
{
SK_BOOL,
};

static const uint16 attr_flags323 [] =
{0,};

static const EIF_TYPE_INDEX g_atype323_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes323 [] = {
g_atype323_0,
};

static const int32 cn_attr323 [] =
{
2212,
};

extern const char *names324[];
static const uint32 types324 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags324 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype324_0 [] = {0xFF01,324,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype324_1 [] = {0xFF01,318,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype324_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype324_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype324_4 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes324 [] = {
g_atype324_0,
g_atype324_1,
g_atype324_2,
g_atype324_3,
g_atype324_4,
};

static const int32 cn_attr324 [] =
{
3789,
3802,
3790,
3792,
3803,
};

extern const char *names326[];
static const uint32 types326 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags326 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype326_0 [] = {0xFF01,324,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype326_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype326_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes326 [] = {
g_atype326_0,
g_atype326_1,
g_atype326_2,
};

static const int32 cn_attr326 [] =
{
3789,
3790,
3792,
};

extern const char *names327[];
static const uint32 types327 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags327 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype327_0 [] = {0xFF01,324,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype327_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype327_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes327 [] = {
g_atype327_0,
g_atype327_1,
g_atype327_2,
};

static const int32 cn_attr327 [] =
{
3789,
3790,
3792,
};

extern const char *names328[];
static const uint32 types328 [] =
{
SK_REF,
};

static const uint16 attr_flags328 [] =
{0,};

static const EIF_TYPE_INDEX g_atype328_0 [] = {0xFF01,324,0xFFF8,1,0xFFFF};

static const EIF_TYPE_INDEX *gtypes328 [] = {
g_atype328_0,
};

static const int32 cn_attr328 [] =
{
2156,
};

extern const char *names329[];
static const uint32 types329 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags329 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype329_0 [] = {0xFF01,324,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype329_1 [] = {0xFF01,320,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype329_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype329_3 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes329 [] = {
g_atype329_0,
g_atype329_1,
g_atype329_2,
g_atype329_3,
};

static const int32 cn_attr329 [] =
{
3789,
3794,
3790,
3792,
};

extern const char *names330[];
static const uint32 types330 [] =
{
SK_BOOL,
};

static const uint16 attr_flags330 [] =
{0,};

static const EIF_TYPE_INDEX g_atype330_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes330 [] = {
g_atype330_0,
};

static const int32 cn_attr330 [] =
{
2212,
};

extern const char *names331[];
static const uint32 types331 [] =
{
SK_BOOL,
};

static const uint16 attr_flags331 [] =
{0,};

static const EIF_TYPE_INDEX g_atype331_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes331 [] = {
g_atype331_0,
};

static const int32 cn_attr331 [] =
{
2212,
};

extern const char *names332[];
static const uint32 types332 [] =
{
SK_BOOL,
};

static const uint16 attr_flags332 [] =
{0,};

static const EIF_TYPE_INDEX g_atype332_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes332 [] = {
g_atype332_0,
};

static const int32 cn_attr332 [] =
{
2212,
};

extern const char *names333[];
static const uint32 types333 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags333 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype333_0 [] = {0xFF01,354,215,0xFFFF};
static const EIF_TYPE_INDEX g_atype333_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype333_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype333_3 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes333 [] = {
g_atype333_0,
g_atype333_1,
g_atype333_2,
g_atype333_3,
};

static const int32 cn_attr333 [] =
{
2156,
2212,
2664,
2665,
};

extern const char *names336[];
static const uint32 types336 [] =
{
SK_BOOL,
};

static const uint16 attr_flags336 [] =
{0,};

static const EIF_TYPE_INDEX g_atype336_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes336 [] = {
g_atype336_0,
};

static const int32 cn_attr336 [] =
{
2212,
};

extern const char *names337[];
static const uint32 types337 [] =
{
SK_BOOL,
};

static const uint16 attr_flags337 [] =
{0,};

static const EIF_TYPE_INDEX g_atype337_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes337 [] = {
g_atype337_0,
};

static const int32 cn_attr337 [] =
{
2212,
};

extern const char *names338[];
static const uint32 types338 [] =
{
SK_BOOL,
};

static const uint16 attr_flags338 [] =
{0,};

static const EIF_TYPE_INDEX g_atype338_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes338 [] = {
g_atype338_0,
};

static const int32 cn_attr338 [] =
{
2212,
};

extern const char *names339[];
static const uint32 types339 [] =
{
SK_BOOL,
};

static const uint16 attr_flags339 [] =
{0,};

static const EIF_TYPE_INDEX g_atype339_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes339 [] = {
g_atype339_0,
};

static const int32 cn_attr339 [] =
{
2212,
};

extern const char *names340[];
static const uint32 types340 [] =
{
SK_BOOL,
};

static const uint16 attr_flags340 [] =
{0,};

static const EIF_TYPE_INDEX g_atype340_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes340 [] = {
g_atype340_0,
};

static const int32 cn_attr340 [] =
{
2212,
};

extern const char *names341[];
static const uint32 types341 [] =
{
SK_BOOL,
};

static const uint16 attr_flags341 [] =
{0,};

static const EIF_TYPE_INDEX g_atype341_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes341 [] = {
g_atype341_0,
};

static const int32 cn_attr341 [] =
{
2212,
};

extern const char *names342[];
static const uint32 types342 [] =
{
SK_BOOL,
};

static const uint16 attr_flags342 [] =
{0,};

static const EIF_TYPE_INDEX g_atype342_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes342 [] = {
g_atype342_0,
};

static const int32 cn_attr342 [] =
{
2212,
};

extern const char *names344[];
static const uint32 types344 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags344 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype344_0 [] = {0xFF01,342,215,0xFFFF};
static const EIF_TYPE_INDEX g_atype344_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype344_2 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype344_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype344_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype344_5 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype344_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes344 [] = {
g_atype344_0,
g_atype344_1,
g_atype344_2,
g_atype344_3,
g_atype344_4,
g_atype344_5,
g_atype344_6,
};

static const int32 cn_attr344 [] =
{
3782,
3783,
3779,
3784,
3785,
3786,
3787,
};

extern const char *names347[];
static const uint32 types347 [] =
{
SK_BOOL,
};

static const uint16 attr_flags347 [] =
{0,};

static const EIF_TYPE_INDEX g_atype347_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes347 [] = {
g_atype347_0,
};

static const int32 cn_attr347 [] =
{
2212,
};

extern const char *names348[];
static const uint32 types348 [] =
{
SK_BOOL,
};

static const uint16 attr_flags348 [] =
{0,};

static const EIF_TYPE_INDEX g_atype348_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes348 [] = {
g_atype348_0,
};

static const int32 cn_attr348 [] =
{
2212,
};

extern const char *names349[];
static const uint32 types349 [] =
{
SK_BOOL,
};

static const uint16 attr_flags349 [] =
{0,};

static const EIF_TYPE_INDEX g_atype349_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes349 [] = {
g_atype349_0,
};

static const int32 cn_attr349 [] =
{
2212,
};

extern const char *names350[];
static const uint32 types350 [] =
{
SK_BOOL,
};

static const uint16 attr_flags350 [] =
{0,};

static const EIF_TYPE_INDEX g_atype350_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes350 [] = {
g_atype350_0,
};

static const int32 cn_attr350 [] =
{
2212,
};

extern const char *names352[];
static const uint32 types352 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags352 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype352_0 [] = {0xFF01,354,215,0xFFFF};
static const EIF_TYPE_INDEX g_atype352_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype352_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes352 [] = {
g_atype352_0,
g_atype352_1,
g_atype352_2,
};

static const int32 cn_attr352 [] =
{
2156,
2212,
3094,
};

extern const char *names353[];
static const uint32 types353 [] =
{
SK_BOOL,
};

static const uint16 attr_flags353 [] =
{0,};

static const EIF_TYPE_INDEX g_atype353_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes353 [] = {
g_atype353_0,
};

static const int32 cn_attr353 [] =
{
2212,
};

extern const char *names354[];
static const uint32 types354 [] =
{
SK_BOOL,
};

static const uint16 attr_flags354 [] =
{0,};

static const EIF_TYPE_INDEX g_atype354_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes354 [] = {
g_atype354_0,
};

static const int32 cn_attr354 [] =
{
2212,
};

extern const char *names356[];
static const uint32 types356 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags356 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype356_0 [] = {0xFF01,354,215,0xFFFF};
static const EIF_TYPE_INDEX g_atype356_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype356_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes356 [] = {
g_atype356_0,
g_atype356_1,
g_atype356_2,
};

static const int32 cn_attr356 [] =
{
3789,
3790,
3792,
};

extern const char *names357[];
static const uint32 types357 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags357 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype357_0 [] = {0xFF01,354,215,0xFFFF};
static const EIF_TYPE_INDEX g_atype357_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype357_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes357 [] = {
g_atype357_0,
g_atype357_1,
g_atype357_2,
};

static const int32 cn_attr357 [] =
{
3789,
3790,
3792,
};

extern const char *names358[];
static const uint32 types358 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags358 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype358_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype358_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes358 [] = {
g_atype358_0,
g_atype358_1,
};

static const int32 cn_attr358 [] =
{
3889,
3890,
};

extern const char *names359[];
static const uint32 types359 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags359 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype359_0 [] = {0xFF01,354,215,0xFFFF};
static const EIF_TYPE_INDEX g_atype359_1 [] = {0xFF01,351,215,0xFFFF};
static const EIF_TYPE_INDEX g_atype359_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype359_3 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes359 [] = {
g_atype359_0,
g_atype359_1,
g_atype359_2,
g_atype359_3,
};

static const int32 cn_attr359 [] =
{
3789,
3794,
3790,
3792,
};

extern const char *names360[];
static const uint32 types360 [] =
{
SK_BOOL,
};

static const uint16 attr_flags360 [] =
{0,};

static const EIF_TYPE_INDEX g_atype360_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes360 [] = {
g_atype360_0,
};

static const int32 cn_attr360 [] =
{
2212,
};

extern const char *names361[];
static const uint32 types361 [] =
{
SK_BOOL,
};

static const uint16 attr_flags361 [] =
{0,};

static const EIF_TYPE_INDEX g_atype361_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes361 [] = {
g_atype361_0,
};

static const int32 cn_attr361 [] =
{
2212,
};

extern const char *names362[];
static const uint32 types362 [] =
{
SK_BOOL,
};

static const uint16 attr_flags362 [] =
{0,};

static const EIF_TYPE_INDEX g_atype362_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes362 [] = {
g_atype362_0,
};

static const int32 cn_attr362 [] =
{
2212,
};

extern const char *names363[];
static const uint32 types363 [] =
{
SK_BOOL,
};

static const uint16 attr_flags363 [] =
{0,};

static const EIF_TYPE_INDEX g_atype363_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes363 [] = {
g_atype363_0,
};

static const int32 cn_attr363 [] =
{
2212,
};

extern const char *names364[];
static const uint32 types364 [] =
{
SK_BOOL,
};

static const uint16 attr_flags364 [] =
{0,};

static const EIF_TYPE_INDEX g_atype364_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes364 [] = {
g_atype364_0,
};

static const int32 cn_attr364 [] =
{
2212,
};

extern const char *names365[];
static const uint32 types365 [] =
{
SK_BOOL,
};

static const uint16 attr_flags365 [] =
{0,};

static const EIF_TYPE_INDEX g_atype365_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes365 [] = {
g_atype365_0,
};

static const int32 cn_attr365 [] =
{
2212,
};

extern const char *names366[];
static const uint32 types366 [] =
{
SK_BOOL,
};

static const uint16 attr_flags366 [] =
{0,};

static const EIF_TYPE_INDEX g_atype366_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes366 [] = {
g_atype366_0,
};

static const int32 cn_attr366 [] =
{
2212,
};

extern const char *names367[];
static const uint32 types367 [] =
{
SK_REF,
};

static const uint16 attr_flags367 [] =
{0,};

static const EIF_TYPE_INDEX g_atype367_0 [] = {0xFF01,354,215,0xFFFF};

static const EIF_TYPE_INDEX *gtypes367 [] = {
g_atype367_0,
};

static const int32 cn_attr367 [] =
{
2156,
};

extern const char *names368[];
static const uint32 types368 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags368 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype368_0 [] = {0xFF01,354,215,0xFFFF};
static const EIF_TYPE_INDEX g_atype368_1 [] = {0xFF01,332,215,0xFFFF};
static const EIF_TYPE_INDEX g_atype368_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype368_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype368_4 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes368 [] = {
g_atype368_0,
g_atype368_1,
g_atype368_2,
g_atype368_3,
g_atype368_4,
};

static const int32 cn_attr368 [] =
{
3789,
3802,
3790,
3792,
3803,
};

extern const char *names369[];
static const uint32 types369 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_POINTER,
SK_POINTER,
SK_POINTER,
};

static const uint16 attr_flags369 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype369_0 [] = {0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype369_1 [] = {0xFFF9,0,186,0xFFFF};
static const EIF_TYPE_INDEX g_atype369_2 [] = {455,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype369_3 [] = {455,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype369_4 [] = {0xFF02,0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX g_atype369_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype369_6 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype369_7 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype369_8 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype369_9 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype369_10 [] = {227,0xFFFF};
static const EIF_TYPE_INDEX g_atype369_11 [] = {227,0xFFFF};
static const EIF_TYPE_INDEX g_atype369_12 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes369 [] = {
g_atype369_0,
g_atype369_1,
g_atype369_2,
g_atype369_3,
g_atype369_4,
g_atype369_5,
g_atype369_6,
g_atype369_7,
g_atype369_8,
g_atype369_9,
g_atype369_10,
g_atype369_11,
g_atype369_12,
};

static const int32 cn_attr369 [] =
{
4579,
4594,
4598,
4606,
4615,
4587,
4601,
4588,
4600,
4602,
4596,
4597,
4599,
};

extern const char *names370[];
static const uint32 types370 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags370 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype370_0 [] = {0xFF01,391,191,0xFFFF};
static const EIF_TYPE_INDEX g_atype370_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype370_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype370_3 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes370 [] = {
g_atype370_0,
g_atype370_1,
g_atype370_2,
g_atype370_3,
};

static const int32 cn_attr370 [] =
{
2156,
2212,
2664,
2665,
};

extern const char *names373[];
static const uint32 types373 [] =
{
SK_BOOL,
};

static const uint16 attr_flags373 [] =
{0,};

static const EIF_TYPE_INDEX g_atype373_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes373 [] = {
g_atype373_0,
};

static const int32 cn_attr373 [] =
{
2212,
};

extern const char *names374[];
static const uint32 types374 [] =
{
SK_BOOL,
};

static const uint16 attr_flags374 [] =
{0,};

static const EIF_TYPE_INDEX g_atype374_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes374 [] = {
g_atype374_0,
};

static const int32 cn_attr374 [] =
{
2212,
};

extern const char *names375[];
static const uint32 types375 [] =
{
SK_BOOL,
};

static const uint16 attr_flags375 [] =
{0,};

static const EIF_TYPE_INDEX g_atype375_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes375 [] = {
g_atype375_0,
};

static const int32 cn_attr375 [] =
{
2212,
};

extern const char *names376[];
static const uint32 types376 [] =
{
SK_BOOL,
};

static const uint16 attr_flags376 [] =
{0,};

static const EIF_TYPE_INDEX g_atype376_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes376 [] = {
g_atype376_0,
};

static const int32 cn_attr376 [] =
{
2212,
};

extern const char *names377[];
static const uint32 types377 [] =
{
SK_BOOL,
};

static const uint16 attr_flags377 [] =
{0,};

static const EIF_TYPE_INDEX g_atype377_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes377 [] = {
g_atype377_0,
};

static const int32 cn_attr377 [] =
{
2212,
};

extern const char *names378[];
static const uint32 types378 [] =
{
SK_BOOL,
};

static const uint16 attr_flags378 [] =
{0,};

static const EIF_TYPE_INDEX g_atype378_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes378 [] = {
g_atype378_0,
};

static const int32 cn_attr378 [] =
{
2212,
};

extern const char *names379[];
static const uint32 types379 [] =
{
SK_BOOL,
};

static const uint16 attr_flags379 [] =
{0,};

static const EIF_TYPE_INDEX g_atype379_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes379 [] = {
g_atype379_0,
};

static const int32 cn_attr379 [] =
{
2212,
};

extern const char *names381[];
static const uint32 types381 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags381 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype381_0 [] = {0xFF01,379,191,0xFFFF};
static const EIF_TYPE_INDEX g_atype381_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype381_2 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype381_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype381_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype381_5 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype381_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes381 [] = {
g_atype381_0,
g_atype381_1,
g_atype381_2,
g_atype381_3,
g_atype381_4,
g_atype381_5,
g_atype381_6,
};

static const int32 cn_attr381 [] =
{
3782,
3783,
3779,
3784,
3785,
3786,
3787,
};

extern const char *names384[];
static const uint32 types384 [] =
{
SK_BOOL,
};

static const uint16 attr_flags384 [] =
{0,};

static const EIF_TYPE_INDEX g_atype384_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes384 [] = {
g_atype384_0,
};

static const int32 cn_attr384 [] =
{
2212,
};

extern const char *names385[];
static const uint32 types385 [] =
{
SK_BOOL,
};

static const uint16 attr_flags385 [] =
{0,};

static const EIF_TYPE_INDEX g_atype385_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes385 [] = {
g_atype385_0,
};

static const int32 cn_attr385 [] =
{
2212,
};

extern const char *names386[];
static const uint32 types386 [] =
{
SK_BOOL,
};

static const uint16 attr_flags386 [] =
{0,};

static const EIF_TYPE_INDEX g_atype386_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes386 [] = {
g_atype386_0,
};

static const int32 cn_attr386 [] =
{
2212,
};

extern const char *names387[];
static const uint32 types387 [] =
{
SK_BOOL,
};

static const uint16 attr_flags387 [] =
{0,};

static const EIF_TYPE_INDEX g_atype387_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes387 [] = {
g_atype387_0,
};

static const int32 cn_attr387 [] =
{
2212,
};

extern const char *names389[];
static const uint32 types389 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags389 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype389_0 [] = {0xFF01,391,191,0xFFFF};
static const EIF_TYPE_INDEX g_atype389_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype389_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes389 [] = {
g_atype389_0,
g_atype389_1,
g_atype389_2,
};

static const int32 cn_attr389 [] =
{
2156,
2212,
3094,
};

extern const char *names390[];
static const uint32 types390 [] =
{
SK_BOOL,
};

static const uint16 attr_flags390 [] =
{0,};

static const EIF_TYPE_INDEX g_atype390_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes390 [] = {
g_atype390_0,
};

static const int32 cn_attr390 [] =
{
2212,
};

extern const char *names391[];
static const uint32 types391 [] =
{
SK_BOOL,
};

static const uint16 attr_flags391 [] =
{0,};

static const EIF_TYPE_INDEX g_atype391_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes391 [] = {
g_atype391_0,
};

static const int32 cn_attr391 [] =
{
2212,
};

extern const char *names393[];
static const uint32 types393 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags393 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype393_0 [] = {0xFF01,391,191,0xFFFF};
static const EIF_TYPE_INDEX g_atype393_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype393_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes393 [] = {
g_atype393_0,
g_atype393_1,
g_atype393_2,
};

static const int32 cn_attr393 [] =
{
3789,
3790,
3792,
};

extern const char *names394[];
static const uint32 types394 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags394 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype394_0 [] = {0xFF01,391,191,0xFFFF};
static const EIF_TYPE_INDEX g_atype394_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype394_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes394 [] = {
g_atype394_0,
g_atype394_1,
g_atype394_2,
};

static const int32 cn_attr394 [] =
{
3789,
3790,
3792,
};

extern const char *names395[];
static const uint32 types395 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags395 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype395_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype395_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes395 [] = {
g_atype395_0,
g_atype395_1,
};

static const int32 cn_attr395 [] =
{
3889,
3890,
};

extern const char *names396[];
static const uint32 types396 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags396 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype396_0 [] = {0xFF01,391,191,0xFFFF};
static const EIF_TYPE_INDEX g_atype396_1 [] = {0xFF01,388,191,0xFFFF};
static const EIF_TYPE_INDEX g_atype396_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype396_3 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes396 [] = {
g_atype396_0,
g_atype396_1,
g_atype396_2,
g_atype396_3,
};

static const int32 cn_attr396 [] =
{
3789,
3794,
3790,
3792,
};

extern const char *names397[];
static const uint32 types397 [] =
{
SK_BOOL,
};

static const uint16 attr_flags397 [] =
{0,};

static const EIF_TYPE_INDEX g_atype397_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes397 [] = {
g_atype397_0,
};

static const int32 cn_attr397 [] =
{
2212,
};

extern const char *names398[];
static const uint32 types398 [] =
{
SK_BOOL,
};

static const uint16 attr_flags398 [] =
{0,};

static const EIF_TYPE_INDEX g_atype398_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes398 [] = {
g_atype398_0,
};

static const int32 cn_attr398 [] =
{
2212,
};

extern const char *names399[];
static const uint32 types399 [] =
{
SK_BOOL,
};

static const uint16 attr_flags399 [] =
{0,};

static const EIF_TYPE_INDEX g_atype399_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes399 [] = {
g_atype399_0,
};

static const int32 cn_attr399 [] =
{
2212,
};

extern const char *names400[];
static const uint32 types400 [] =
{
SK_BOOL,
};

static const uint16 attr_flags400 [] =
{0,};

static const EIF_TYPE_INDEX g_atype400_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes400 [] = {
g_atype400_0,
};

static const int32 cn_attr400 [] =
{
2212,
};

extern const char *names401[];
static const uint32 types401 [] =
{
SK_BOOL,
};

static const uint16 attr_flags401 [] =
{0,};

static const EIF_TYPE_INDEX g_atype401_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes401 [] = {
g_atype401_0,
};

static const int32 cn_attr401 [] =
{
2212,
};

extern const char *names402[];
static const uint32 types402 [] =
{
SK_BOOL,
};

static const uint16 attr_flags402 [] =
{0,};

static const EIF_TYPE_INDEX g_atype402_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes402 [] = {
g_atype402_0,
};

static const int32 cn_attr402 [] =
{
2212,
};

extern const char *names403[];
static const uint32 types403 [] =
{
SK_BOOL,
};

static const uint16 attr_flags403 [] =
{0,};

static const EIF_TYPE_INDEX g_atype403_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes403 [] = {
g_atype403_0,
};

static const int32 cn_attr403 [] =
{
2212,
};

extern const char *names404[];
static const uint32 types404 [] =
{
SK_REF,
};

static const uint16 attr_flags404 [] =
{0,};

static const EIF_TYPE_INDEX g_atype404_0 [] = {0xFF01,391,191,0xFFFF};

static const EIF_TYPE_INDEX *gtypes404 [] = {
g_atype404_0,
};

static const int32 cn_attr404 [] =
{
2156,
};

extern const char *names405[];
static const uint32 types405 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags405 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype405_0 [] = {0xFF01,391,191,0xFFFF};
static const EIF_TYPE_INDEX g_atype405_1 [] = {0xFF01,369,191,0xFFFF};
static const EIF_TYPE_INDEX g_atype405_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype405_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype405_4 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes405 [] = {
g_atype405_0,
g_atype405_1,
g_atype405_2,
g_atype405_3,
g_atype405_4,
};

static const int32 cn_attr405 [] =
{
3789,
3802,
3790,
3792,
3803,
};

extern const char *names406[];
static const uint32 types406 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags406 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype406_0 [] = {0xFF01,427,209,0xFFFF};
static const EIF_TYPE_INDEX g_atype406_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype406_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype406_3 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes406 [] = {
g_atype406_0,
g_atype406_1,
g_atype406_2,
g_atype406_3,
};

static const int32 cn_attr406 [] =
{
2156,
2212,
2664,
2665,
};

extern const char *names409[];
static const uint32 types409 [] =
{
SK_BOOL,
};

static const uint16 attr_flags409 [] =
{0,};

static const EIF_TYPE_INDEX g_atype409_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes409 [] = {
g_atype409_0,
};

static const int32 cn_attr409 [] =
{
2212,
};

extern const char *names410[];
static const uint32 types410 [] =
{
SK_BOOL,
};

static const uint16 attr_flags410 [] =
{0,};

static const EIF_TYPE_INDEX g_atype410_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes410 [] = {
g_atype410_0,
};

static const int32 cn_attr410 [] =
{
2212,
};

extern const char *names411[];
static const uint32 types411 [] =
{
SK_BOOL,
};

static const uint16 attr_flags411 [] =
{0,};

static const EIF_TYPE_INDEX g_atype411_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes411 [] = {
g_atype411_0,
};

static const int32 cn_attr411 [] =
{
2212,
};

extern const char *names412[];
static const uint32 types412 [] =
{
SK_BOOL,
};

static const uint16 attr_flags412 [] =
{0,};

static const EIF_TYPE_INDEX g_atype412_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes412 [] = {
g_atype412_0,
};

static const int32 cn_attr412 [] =
{
2212,
};

extern const char *names413[];
static const uint32 types413 [] =
{
SK_BOOL,
};

static const uint16 attr_flags413 [] =
{0,};

static const EIF_TYPE_INDEX g_atype413_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes413 [] = {
g_atype413_0,
};

static const int32 cn_attr413 [] =
{
2212,
};

extern const char *names414[];
static const uint32 types414 [] =
{
SK_BOOL,
};

static const uint16 attr_flags414 [] =
{0,};

static const EIF_TYPE_INDEX g_atype414_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes414 [] = {
g_atype414_0,
};

static const int32 cn_attr414 [] =
{
2212,
};

extern const char *names415[];
static const uint32 types415 [] =
{
SK_BOOL,
};

static const uint16 attr_flags415 [] =
{0,};

static const EIF_TYPE_INDEX g_atype415_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes415 [] = {
g_atype415_0,
};

static const int32 cn_attr415 [] =
{
2212,
};

extern const char *names417[];
static const uint32 types417 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags417 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype417_0 [] = {0xFF01,415,209,0xFFFF};
static const EIF_TYPE_INDEX g_atype417_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype417_2 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype417_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype417_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype417_5 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype417_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes417 [] = {
g_atype417_0,
g_atype417_1,
g_atype417_2,
g_atype417_3,
g_atype417_4,
g_atype417_5,
g_atype417_6,
};

static const int32 cn_attr417 [] =
{
3782,
3783,
3779,
3784,
3785,
3786,
3787,
};

extern const char *names420[];
static const uint32 types420 [] =
{
SK_BOOL,
};

static const uint16 attr_flags420 [] =
{0,};

static const EIF_TYPE_INDEX g_atype420_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes420 [] = {
g_atype420_0,
};

static const int32 cn_attr420 [] =
{
2212,
};

extern const char *names421[];
static const uint32 types421 [] =
{
SK_BOOL,
};

static const uint16 attr_flags421 [] =
{0,};

static const EIF_TYPE_INDEX g_atype421_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes421 [] = {
g_atype421_0,
};

static const int32 cn_attr421 [] =
{
2212,
};

extern const char *names422[];
static const uint32 types422 [] =
{
SK_BOOL,
};

static const uint16 attr_flags422 [] =
{0,};

static const EIF_TYPE_INDEX g_atype422_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes422 [] = {
g_atype422_0,
};

static const int32 cn_attr422 [] =
{
2212,
};

extern const char *names423[];
static const uint32 types423 [] =
{
SK_BOOL,
};

static const uint16 attr_flags423 [] =
{0,};

static const EIF_TYPE_INDEX g_atype423_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes423 [] = {
g_atype423_0,
};

static const int32 cn_attr423 [] =
{
2212,
};

extern const char *names425[];
static const uint32 types425 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags425 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype425_0 [] = {0xFF01,427,209,0xFFFF};
static const EIF_TYPE_INDEX g_atype425_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype425_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes425 [] = {
g_atype425_0,
g_atype425_1,
g_atype425_2,
};

static const int32 cn_attr425 [] =
{
2156,
2212,
3094,
};

extern const char *names426[];
static const uint32 types426 [] =
{
SK_BOOL,
};

static const uint16 attr_flags426 [] =
{0,};

static const EIF_TYPE_INDEX g_atype426_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes426 [] = {
g_atype426_0,
};

static const int32 cn_attr426 [] =
{
2212,
};

extern const char *names427[];
static const uint32 types427 [] =
{
SK_BOOL,
};

static const uint16 attr_flags427 [] =
{0,};

static const EIF_TYPE_INDEX g_atype427_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes427 [] = {
g_atype427_0,
};

static const int32 cn_attr427 [] =
{
2212,
};

extern const char *names429[];
static const uint32 types429 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags429 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype429_0 [] = {0xFF01,427,209,0xFFFF};
static const EIF_TYPE_INDEX g_atype429_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype429_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes429 [] = {
g_atype429_0,
g_atype429_1,
g_atype429_2,
};

static const int32 cn_attr429 [] =
{
3789,
3790,
3792,
};

extern const char *names430[];
static const uint32 types430 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags430 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype430_0 [] = {0xFF01,427,209,0xFFFF};
static const EIF_TYPE_INDEX g_atype430_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype430_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes430 [] = {
g_atype430_0,
g_atype430_1,
g_atype430_2,
};

static const int32 cn_attr430 [] =
{
3789,
3790,
3792,
};

extern const char *names431[];
static const uint32 types431 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags431 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype431_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype431_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes431 [] = {
g_atype431_0,
g_atype431_1,
};

static const int32 cn_attr431 [] =
{
3889,
3890,
};

extern const char *names432[];
static const uint32 types432 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags432 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype432_0 [] = {0xFF01,427,209,0xFFFF};
static const EIF_TYPE_INDEX g_atype432_1 [] = {0xFF01,424,209,0xFFFF};
static const EIF_TYPE_INDEX g_atype432_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype432_3 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes432 [] = {
g_atype432_0,
g_atype432_1,
g_atype432_2,
g_atype432_3,
};

static const int32 cn_attr432 [] =
{
3789,
3794,
3790,
3792,
};

extern const char *names433[];
static const uint32 types433 [] =
{
SK_BOOL,
};

static const uint16 attr_flags433 [] =
{0,};

static const EIF_TYPE_INDEX g_atype433_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes433 [] = {
g_atype433_0,
};

static const int32 cn_attr433 [] =
{
2212,
};

extern const char *names434[];
static const uint32 types434 [] =
{
SK_BOOL,
};

static const uint16 attr_flags434 [] =
{0,};

static const EIF_TYPE_INDEX g_atype434_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes434 [] = {
g_atype434_0,
};

static const int32 cn_attr434 [] =
{
2212,
};

extern const char *names435[];
static const uint32 types435 [] =
{
SK_BOOL,
};

static const uint16 attr_flags435 [] =
{0,};

static const EIF_TYPE_INDEX g_atype435_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes435 [] = {
g_atype435_0,
};

static const int32 cn_attr435 [] =
{
2212,
};

extern const char *names436[];
static const uint32 types436 [] =
{
SK_BOOL,
};

static const uint16 attr_flags436 [] =
{0,};

static const EIF_TYPE_INDEX g_atype436_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes436 [] = {
g_atype436_0,
};

static const int32 cn_attr436 [] =
{
2212,
};

extern const char *names437[];
static const uint32 types437 [] =
{
SK_BOOL,
};

static const uint16 attr_flags437 [] =
{0,};

static const EIF_TYPE_INDEX g_atype437_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes437 [] = {
g_atype437_0,
};

static const int32 cn_attr437 [] =
{
2212,
};

extern const char *names438[];
static const uint32 types438 [] =
{
SK_BOOL,
};

static const uint16 attr_flags438 [] =
{0,};

static const EIF_TYPE_INDEX g_atype438_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes438 [] = {
g_atype438_0,
};

static const int32 cn_attr438 [] =
{
2212,
};

extern const char *names439[];
static const uint32 types439 [] =
{
SK_BOOL,
};

static const uint16 attr_flags439 [] =
{0,};

static const EIF_TYPE_INDEX g_atype439_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes439 [] = {
g_atype439_0,
};

static const int32 cn_attr439 [] =
{
2212,
};

extern const char *names440[];
static const uint32 types440 [] =
{
SK_REF,
};

static const uint16 attr_flags440 [] =
{0,};

static const EIF_TYPE_INDEX g_atype440_0 [] = {0xFF01,427,209,0xFFFF};

static const EIF_TYPE_INDEX *gtypes440 [] = {
g_atype440_0,
};

static const int32 cn_attr440 [] =
{
2156,
};

extern const char *names441[];
static const uint32 types441 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags441 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype441_0 [] = {0xFF01,427,209,0xFFFF};
static const EIF_TYPE_INDEX g_atype441_1 [] = {0xFF01,405,209,0xFFFF};
static const EIF_TYPE_INDEX g_atype441_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype441_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype441_4 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes441 [] = {
g_atype441_0,
g_atype441_1,
g_atype441_2,
g_atype441_3,
g_atype441_4,
};

static const int32 cn_attr441 [] =
{
3789,
3802,
3790,
3792,
3803,
};

extern const char *names442[];
static const uint32 types442 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags442 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype442_0 [] = {0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype442_1 [] = {0xFF01,324,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype442_2 [] = {0xFF01,444,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype442_3 [] = {0xFF01,444,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype442_4 [] = {0xFF01,807,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype442_5 [] = {0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype442_6 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype442_7 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype442_8 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype442_9 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype442_10 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype442_11 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype442_12 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype442_13 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype442_14 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype442_15 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype442_16 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes442 [] = {
g_atype442_0,
g_atype442_1,
g_atype442_2,
g_atype442_3,
g_atype442_4,
g_atype442_5,
g_atype442_6,
g_atype442_7,
g_atype442_8,
g_atype442_9,
g_atype442_10,
g_atype442_11,
g_atype442_12,
g_atype442_13,
g_atype442_14,
g_atype442_15,
g_atype442_16,
};

static const int32 cn_attr442 [] =
{
2972,
3009,
3010,
3011,
3012,
3024,
2212,
3008,
3014,
2981,
3013,
3015,
3018,
3019,
3023,
3025,
3059,
};

extern const char *names443[];
static const uint32 types443 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags443 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype443_0 [] = {0xFF01,441,0xFFF8,1,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype443_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype443_2 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype443_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype443_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype443_5 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype443_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes443 [] = {
g_atype443_0,
g_atype443_1,
g_atype443_2,
g_atype443_3,
g_atype443_4,
g_atype443_5,
g_atype443_6,
};

static const int32 cn_attr443 [] =
{
3782,
3783,
3779,
3784,
3785,
3786,
3787,
};

extern const char *names448[];
static const uint32 types448 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags448 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype448_0 [] = {0xFF01,446,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype448_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype448_2 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype448_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype448_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype448_5 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype448_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes448 [] = {
g_atype448_0,
g_atype448_1,
g_atype448_2,
g_atype448_3,
g_atype448_4,
g_atype448_5,
g_atype448_6,
};

static const int32 cn_attr448 [] =
{
3782,
3783,
3779,
3784,
3785,
3786,
3787,
};

extern const char *names453[];
static const uint32 types453 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags453 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype453_0 [] = {0xFF01,444,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype453_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype453_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes453 [] = {
g_atype453_0,
g_atype453_1,
g_atype453_2,
};

static const int32 cn_attr453 [] =
{
3789,
3790,
3792,
};

extern const char *names454[];
static const uint32 types454 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags454 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype454_0 [] = {0xFF01,444,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype454_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype454_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes454 [] = {
g_atype454_0,
g_atype454_1,
g_atype454_2,
};

static const int32 cn_attr454 [] =
{
3789,
3790,
3792,
};

extern const char *names455[];
static const uint32 types455 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags455 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype455_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype455_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes455 [] = {
g_atype455_0,
g_atype455_1,
};

static const int32 cn_attr455 [] =
{
3889,
3890,
};

extern const char *names456[];
static const uint32 types456 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags456 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype456_0 [] = {0xFF01,444,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype456_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype456_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype456_3 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes456 [] = {
g_atype456_0,
g_atype456_1,
g_atype456_2,
g_atype456_3,
};

static const int32 cn_attr456 [] =
{
2156,
2212,
2664,
2665,
};

extern const char *names457[];
static const uint32 types457 [] =
{
SK_BOOL,
};

static const uint16 attr_flags457 [] =
{0,};

static const EIF_TYPE_INDEX g_atype457_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes457 [] = {
g_atype457_0,
};

static const int32 cn_attr457 [] =
{
2212,
};

extern const char *names458[];
static const uint32 types458 [] =
{
SK_BOOL,
};

static const uint16 attr_flags458 [] =
{0,};

static const EIF_TYPE_INDEX g_atype458_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes458 [] = {
g_atype458_0,
};

static const int32 cn_attr458 [] =
{
2212,
};

extern const char *names459[];
static const uint32 types459 [] =
{
SK_BOOL,
};

static const uint16 attr_flags459 [] =
{0,};

static const EIF_TYPE_INDEX g_atype459_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes459 [] = {
g_atype459_0,
};

static const int32 cn_attr459 [] =
{
2212,
};

extern const char *names460[];
static const uint32 types460 [] =
{
SK_BOOL,
};

static const uint16 attr_flags460 [] =
{0,};

static const EIF_TYPE_INDEX g_atype460_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes460 [] = {
g_atype460_0,
};

static const int32 cn_attr460 [] =
{
2212,
};

extern const char *names461[];
static const uint32 types461 [] =
{
SK_BOOL,
};

static const uint16 attr_flags461 [] =
{0,};

static const EIF_TYPE_INDEX g_atype461_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes461 [] = {
g_atype461_0,
};

static const int32 cn_attr461 [] =
{
2212,
};

extern const char *names462[];
static const uint32 types462 [] =
{
SK_BOOL,
};

static const uint16 attr_flags462 [] =
{0,};

static const EIF_TYPE_INDEX g_atype462_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes462 [] = {
g_atype462_0,
};

static const int32 cn_attr462 [] =
{
2212,
};

extern const char *names463[];
static const uint32 types463 [] =
{
SK_BOOL,
};

static const uint16 attr_flags463 [] =
{0,};

static const EIF_TYPE_INDEX g_atype463_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes463 [] = {
g_atype463_0,
};

static const int32 cn_attr463 [] =
{
2212,
};

extern const char *names464[];
static const uint32 types464 [] =
{
SK_BOOL,
};

static const uint16 attr_flags464 [] =
{0,};

static const EIF_TYPE_INDEX g_atype464_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes464 [] = {
g_atype464_0,
};

static const int32 cn_attr464 [] =
{
2212,
};

extern const char *names465[];
static const uint32 types465 [] =
{
SK_BOOL,
};

static const uint16 attr_flags465 [] =
{0,};

static const EIF_TYPE_INDEX g_atype465_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes465 [] = {
g_atype465_0,
};

static const int32 cn_attr465 [] =
{
2212,
};

extern const char *names466[];
static const uint32 types466 [] =
{
SK_BOOL,
};

static const uint16 attr_flags466 [] =
{0,};

static const EIF_TYPE_INDEX g_atype466_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes466 [] = {
g_atype466_0,
};

static const int32 cn_attr466 [] =
{
2212,
};

extern const char *names467[];
static const uint32 types467 [] =
{
SK_BOOL,
};

static const uint16 attr_flags467 [] =
{0,};

static const EIF_TYPE_INDEX g_atype467_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes467 [] = {
g_atype467_0,
};

static const int32 cn_attr467 [] =
{
2212,
};

extern const char *names468[];
static const uint32 types468 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags468 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype468_0 [] = {0xFF01,444,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype468_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype468_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes468 [] = {
g_atype468_0,
g_atype468_1,
g_atype468_2,
};

static const int32 cn_attr468 [] =
{
2156,
2212,
3094,
};

extern const char *names469[];
static const uint32 types469 [] =
{
SK_BOOL,
};

static const uint16 attr_flags469 [] =
{0,};

static const EIF_TYPE_INDEX g_atype469_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes469 [] = {
g_atype469_0,
};

static const int32 cn_attr469 [] =
{
2212,
};

extern const char *names470[];
static const uint32 types470 [] =
{
SK_BOOL,
};

static const uint16 attr_flags470 [] =
{0,};

static const EIF_TYPE_INDEX g_atype470_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes470 [] = {
g_atype470_0,
};

static const int32 cn_attr470 [] =
{
2212,
};

extern const char *names471[];
static const uint32 types471 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags471 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype471_0 [] = {0xFF01,444,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype471_1 [] = {0xFF01,467,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype471_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype471_3 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes471 [] = {
g_atype471_0,
g_atype471_1,
g_atype471_2,
g_atype471_3,
};

static const int32 cn_attr471 [] =
{
3789,
3794,
3790,
3792,
};

extern const char *names472[];
static const uint32 types472 [] =
{
SK_BOOL,
};

static const uint16 attr_flags472 [] =
{0,};

static const EIF_TYPE_INDEX g_atype472_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes472 [] = {
g_atype472_0,
};

static const int32 cn_attr472 [] =
{
2212,
};

extern const char *names473[];
static const uint32 types473 [] =
{
SK_BOOL,
};

static const uint16 attr_flags473 [] =
{0,};

static const EIF_TYPE_INDEX g_atype473_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes473 [] = {
g_atype473_0,
};

static const int32 cn_attr473 [] =
{
2212,
};

extern const char *names474[];
static const uint32 types474 [] =
{
SK_BOOL,
};

static const uint16 attr_flags474 [] =
{0,};

static const EIF_TYPE_INDEX g_atype474_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes474 [] = {
g_atype474_0,
};

static const int32 cn_attr474 [] =
{
2212,
};

extern const char *names475[];
static const uint32 types475 [] =
{
SK_BOOL,
};

static const uint16 attr_flags475 [] =
{0,};

static const EIF_TYPE_INDEX g_atype475_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes475 [] = {
g_atype475_0,
};

static const int32 cn_attr475 [] =
{
2212,
};

extern const char *names476[];
static const uint32 types476 [] =
{
SK_BOOL,
};

static const uint16 attr_flags476 [] =
{0,};

static const EIF_TYPE_INDEX g_atype476_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes476 [] = {
g_atype476_0,
};

static const int32 cn_attr476 [] =
{
2212,
};

extern const char *names477[];
static const uint32 types477 [] =
{
SK_BOOL,
};

static const uint16 attr_flags477 [] =
{0,};

static const EIF_TYPE_INDEX g_atype477_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes477 [] = {
g_atype477_0,
};

static const int32 cn_attr477 [] =
{
2212,
};

extern const char *names478[];
static const uint32 types478 [] =
{
SK_BOOL,
};

static const uint16 attr_flags478 [] =
{0,};

static const EIF_TYPE_INDEX g_atype478_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes478 [] = {
g_atype478_0,
};

static const int32 cn_attr478 [] =
{
2212,
};

extern const char *names479[];
static const uint32 types479 [] =
{
SK_REF,
};

static const uint16 attr_flags479 [] =
{0,};

static const EIF_TYPE_INDEX g_atype479_0 [] = {0xFF01,444,218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes479 [] = {
g_atype479_0,
};

static const int32 cn_attr479 [] =
{
2156,
};

extern const char *names480[];
static const uint32 types480 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags480 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype480_0 [] = {0xFF01,444,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype480_1 [] = {0xFF01,455,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype480_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype480_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype480_4 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes480 [] = {
g_atype480_0,
g_atype480_1,
g_atype480_2,
g_atype480_3,
g_atype480_4,
};

static const int32 cn_attr480 [] =
{
3789,
3802,
3790,
3792,
3803,
};

extern const char *names482[];
static const uint32 types482 [] =
{
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags482 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype482_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype482_1 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype482_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype482_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype482_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype482_5 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes482 [] = {
g_atype482_0,
g_atype482_1,
g_atype482_2,
g_atype482_3,
g_atype482_4,
g_atype482_5,
};

static const int32 cn_attr482 [] =
{
2084,
2099,
2082,
2083,
2097,
2098,
};

extern const char *names483[];
static const uint32 types483 [] =
{
SK_POINTER,
};

static const uint16 attr_flags483 [] =
{0,};

static const EIF_TYPE_INDEX g_atype483_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes483 [] = {
g_atype483_0,
};

static const int32 cn_attr483 [] =
{
4554,
};

extern const char *names484[];
static const uint32 types484 [] =
{
SK_POINTER,
};

static const uint16 attr_flags484 [] =
{0,};

static const EIF_TYPE_INDEX g_atype484_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes484 [] = {
g_atype484_0,
};

static const int32 cn_attr484 [] =
{
4554,
};

extern const char *names485[];
static const uint32 types485 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags485 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype485_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype485_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes485 [] = {
g_atype485_0,
g_atype485_1,
};

static const int32 cn_attr485 [] =
{
3889,
3890,
};

extern const char *names486[];
static const uint32 types486 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags486 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype486_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype486_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes486 [] = {
g_atype486_0,
g_atype486_1,
};

static const int32 cn_attr486 [] =
{
3889,
3890,
};

extern const char *names487[];
static const uint32 types487 [] =
{
SK_REF,
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_POINTER,
};

static const uint16 attr_flags487 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype487_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype487_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype487_2 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype487_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype487_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype487_5 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes487 [] = {
g_atype487_0,
g_atype487_1,
g_atype487_2,
g_atype487_3,
g_atype487_4,
g_atype487_5,
};

static const int32 cn_attr487 [] =
{
2084,
2112,
2114,
2082,
2083,
2113,
};

extern const char *names488[];
static const uint32 types488 [] =
{
SK_POINTER,
};

static const uint16 attr_flags488 [] =
{0,};

static const EIF_TYPE_INDEX g_atype488_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes488 [] = {
g_atype488_0,
};

static const int32 cn_attr488 [] =
{
4554,
};

extern const char *names489[];
static const uint32 types489 [] =
{
SK_POINTER,
};

static const uint16 attr_flags489 [] =
{0,};

static const EIF_TYPE_INDEX g_atype489_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes489 [] = {
g_atype489_0,
};

static const int32 cn_attr489 [] =
{
4554,
};

extern const char *names490[];
static const uint32 types490 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags490 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype490_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype490_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes490 [] = {
g_atype490_0,
g_atype490_1,
};

static const int32 cn_attr490 [] =
{
3889,
3890,
};

extern const char *names491[];
static const uint32 types491 [] =
{
SK_REF,
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_REAL64,
};

static const uint16 attr_flags491 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype491_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype491_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype491_2 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype491_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype491_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype491_5 [] = {206,0xFFFF};

static const EIF_TYPE_INDEX *gtypes491 [] = {
g_atype491_0,
g_atype491_1,
g_atype491_2,
g_atype491_3,
g_atype491_4,
g_atype491_5,
};

static const int32 cn_attr491 [] =
{
2084,
2112,
2114,
2082,
2083,
2113,
};

extern const char *names492[];
static const uint32 types492 [] =
{
SK_POINTER,
};

static const uint16 attr_flags492 [] =
{0,};

static const EIF_TYPE_INDEX g_atype492_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes492 [] = {
g_atype492_0,
};

static const int32 cn_attr492 [] =
{
4554,
};

extern const char *names493[];
static const uint32 types493 [] =
{
SK_POINTER,
};

static const uint16 attr_flags493 [] =
{0,};

static const EIF_TYPE_INDEX g_atype493_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes493 [] = {
g_atype493_0,
};

static const int32 cn_attr493 [] =
{
4554,
};

extern const char *names494[];
static const uint32 types494 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags494 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype494_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype494_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes494 [] = {
g_atype494_0,
g_atype494_1,
};

static const int32 cn_attr494 [] =
{
3889,
3890,
};

extern const char *names495[];
static const uint32 types495 [] =
{
SK_REF,
};

static const uint16 attr_flags495 [] =
{0,};

static const EIF_TYPE_INDEX g_atype495_0 [] = {0xFF01,495,194,0xFFFF};

static const EIF_TYPE_INDEX *gtypes495 [] = {
g_atype495_0,
};

static const int32 cn_attr495 [] =
{
2156,
};

extern const char *names498[];
static const uint32 types498 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags498 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype498_0 [] = {0xFF01,495,194,0xFFFF};
static const EIF_TYPE_INDEX g_atype498_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype498_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes498 [] = {
g_atype498_0,
g_atype498_1,
g_atype498_2,
};

static const int32 cn_attr498 [] =
{
3789,
3790,
3792,
};

extern const char *names499[];
static const uint32 types499 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags499 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype499_0 [] = {0xFF01,495,194,0xFFFF};
static const EIF_TYPE_INDEX g_atype499_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype499_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes499 [] = {
g_atype499_0,
g_atype499_1,
g_atype499_2,
};

static const int32 cn_attr499 [] =
{
3789,
3790,
3792,
};

extern const char *names500[];
static const uint32 types500 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags500 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype500_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype500_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes500 [] = {
g_atype500_0,
g_atype500_1,
};

static const int32 cn_attr500 [] =
{
3889,
3890,
};

extern const char *names501[];
static const uint32 types501 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags501 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype501_0 [] = {0xFF01,495,194,0xFFFF};
static const EIF_TYPE_INDEX g_atype501_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype501_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype501_3 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes501 [] = {
g_atype501_0,
g_atype501_1,
g_atype501_2,
g_atype501_3,
};

static const int32 cn_attr501 [] =
{
2156,
2212,
2664,
2665,
};

extern const char *names502[];
static const uint32 types502 [] =
{
SK_BOOL,
};

static const uint16 attr_flags502 [] =
{0,};

static const EIF_TYPE_INDEX g_atype502_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes502 [] = {
g_atype502_0,
};

static const int32 cn_attr502 [] =
{
2212,
};

extern const char *names503[];
static const uint32 types503 [] =
{
SK_BOOL,
};

static const uint16 attr_flags503 [] =
{0,};

static const EIF_TYPE_INDEX g_atype503_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes503 [] = {
g_atype503_0,
};

static const int32 cn_attr503 [] =
{
2212,
};

extern const char *names504[];
static const uint32 types504 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags504 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype504_0 [] = {0xFF01,495,194,0xFFFF};
static const EIF_TYPE_INDEX g_atype504_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype504_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes504 [] = {
g_atype504_0,
g_atype504_1,
g_atype504_2,
};

static const int32 cn_attr504 [] =
{
2156,
2212,
3094,
};

extern const char *names505[];
static const uint32 types505 [] =
{
SK_BOOL,
};

static const uint16 attr_flags505 [] =
{0,};

static const EIF_TYPE_INDEX g_atype505_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes505 [] = {
g_atype505_0,
};

static const int32 cn_attr505 [] =
{
2212,
};

extern const char *names506[];
static const uint32 types506 [] =
{
SK_BOOL,
};

static const uint16 attr_flags506 [] =
{0,};

static const EIF_TYPE_INDEX g_atype506_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes506 [] = {
g_atype506_0,
};

static const int32 cn_attr506 [] =
{
2212,
};

extern const char *names507[];
static const uint32 types507 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags507 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype507_0 [] = {0xFF01,495,194,0xFFFF};
static const EIF_TYPE_INDEX g_atype507_1 [] = {0xFF01,503,194,0xFFFF};
static const EIF_TYPE_INDEX g_atype507_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype507_3 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes507 [] = {
g_atype507_0,
g_atype507_1,
g_atype507_2,
g_atype507_3,
};

static const int32 cn_attr507 [] =
{
3789,
3794,
3790,
3792,
};

extern const char *names508[];
static const uint32 types508 [] =
{
SK_BOOL,
};

static const uint16 attr_flags508 [] =
{0,};

static const EIF_TYPE_INDEX g_atype508_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes508 [] = {
g_atype508_0,
};

static const int32 cn_attr508 [] =
{
2212,
};

extern const char *names509[];
static const uint32 types509 [] =
{
SK_BOOL,
};

static const uint16 attr_flags509 [] =
{0,};

static const EIF_TYPE_INDEX g_atype509_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes509 [] = {
g_atype509_0,
};

static const int32 cn_attr509 [] =
{
2212,
};

extern const char *names510[];
static const uint32 types510 [] =
{
SK_BOOL,
};

static const uint16 attr_flags510 [] =
{0,};

static const EIF_TYPE_INDEX g_atype510_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes510 [] = {
g_atype510_0,
};

static const int32 cn_attr510 [] =
{
2212,
};

extern const char *names511[];
static const uint32 types511 [] =
{
SK_BOOL,
};

static const uint16 attr_flags511 [] =
{0,};

static const EIF_TYPE_INDEX g_atype511_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes511 [] = {
g_atype511_0,
};

static const int32 cn_attr511 [] =
{
2212,
};

extern const char *names512[];
static const uint32 types512 [] =
{
SK_BOOL,
};

static const uint16 attr_flags512 [] =
{0,};

static const EIF_TYPE_INDEX g_atype512_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes512 [] = {
g_atype512_0,
};

static const int32 cn_attr512 [] =
{
2212,
};

extern const char *names513[];
static const uint32 types513 [] =
{
SK_BOOL,
};

static const uint16 attr_flags513 [] =
{0,};

static const EIF_TYPE_INDEX g_atype513_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes513 [] = {
g_atype513_0,
};

static const int32 cn_attr513 [] =
{
2212,
};

extern const char *names514[];
static const uint32 types514 [] =
{
SK_BOOL,
};

static const uint16 attr_flags514 [] =
{0,};

static const EIF_TYPE_INDEX g_atype514_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes514 [] = {
g_atype514_0,
};

static const int32 cn_attr514 [] =
{
2212,
};

extern const char *names515[];
static const uint32 types515 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags515 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype515_0 [] = {0xFF01,495,194,0xFFFF};
static const EIF_TYPE_INDEX g_atype515_1 [] = {0xFF01,500,194,0xFFFF};
static const EIF_TYPE_INDEX g_atype515_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype515_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype515_4 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes515 [] = {
g_atype515_0,
g_atype515_1,
g_atype515_2,
g_atype515_3,
g_atype515_4,
};

static const int32 cn_attr515 [] =
{
3789,
3802,
3790,
3792,
3803,
};

extern const char *names516[];
static const uint32 types516 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags516 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype516_0 [] = {0xFF01,324,0xFF01,262,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype516_1 [] = {904,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype516_2 [] = {516,0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype516_3 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype516_4 [] = {318,0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype516_5 [] = {0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype516_6 [] = {320,0xFF01,262,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype516_7 [] = {320,0xFF01,262,0xFF01,0xFFF9,0,186,0xFFFF};
static const EIF_TYPE_INDEX g_atype516_8 [] = {320,0xFF01,262,0xFF01,0xFFF9,0,186,0xFFFF};
static const EIF_TYPE_INDEX g_atype516_9 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype516_10 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype516_11 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype516_12 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes516 [] = {
g_atype516_0,
g_atype516_1,
g_atype516_2,
g_atype516_3,
g_atype516_4,
g_atype516_5,
g_atype516_6,
g_atype516_7,
g_atype516_8,
g_atype516_9,
g_atype516_10,
g_atype516_11,
g_atype516_12,
};

static const int32 cn_attr516 [] =
{
2156,
3133,
3135,
3136,
3137,
3138,
3140,
3141,
3142,
2212,
3106,
3094,
3120,
};

extern const char *names517[];
static const uint32 types517 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags517 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype517_0 [] = {518,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype517_1 [] = {518,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype517_2 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype517_3 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype517_4 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype517_5 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes517 [] = {
g_atype517_0,
g_atype517_1,
g_atype517_2,
g_atype517_3,
g_atype517_4,
g_atype517_5,
};

static const int32 cn_attr517 [] =
{
2766,
2770,
2212,
2774,
2775,
2776,
};

extern const char *names518[];
static const uint32 types518 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags518 [] =
{0,0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype518_0 [] = {0xFF01,520,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype518_1 [] = {518,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype518_2 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype518_3 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype518_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype518_5 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype518_6 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype518_7 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes518 [] = {
g_atype518_0,
g_atype518_1,
g_atype518_2,
g_atype518_3,
g_atype518_4,
g_atype518_5,
g_atype518_6,
g_atype518_7,
};

static const int32 cn_attr518 [] =
{
3782,
3788,
3783,
3779,
3784,
3785,
3786,
3787,
};

extern const char *names519[];
static const uint32 types519 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags519 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype519_0 [] = {0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype519_1 [] = {518,0xFFF8,1,0xFFFF};

static const EIF_TYPE_INDEX *gtypes519 [] = {
g_atype519_0,
g_atype519_1,
};

static const int32 cn_attr519 [] =
{
1718,
1722,
};

extern const char *names520[];
static const uint32 types520 [] =
{
SK_REF,
};

static const uint16 attr_flags520 [] =
{0,};

static const EIF_TYPE_INDEX g_atype520_0 [] = {0xFFF8,1,0xFFFF};

static const EIF_TYPE_INDEX *gtypes520 [] = {
g_atype520_0,
};

static const int32 cn_attr520 [] =
{
1718,
};

extern const char *names521[];
static const uint32 types521 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags521 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype521_0 [] = {518,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype521_1 [] = {518,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype521_2 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype521_3 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype521_4 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype521_5 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes521 [] = {
g_atype521_0,
g_atype521_1,
g_atype521_2,
g_atype521_3,
g_atype521_4,
g_atype521_5,
};

static const int32 cn_attr521 [] =
{
2766,
2770,
2212,
2774,
2775,
2776,
};

extern const char *names522[];
static const uint32 types522 [] =
{
SK_REF,
SK_BOOL,
SK_BOOL,
};

static const uint16 attr_flags522 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype522_0 [] = {518,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype522_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype522_2 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes522 [] = {
g_atype522_0,
g_atype522_1,
g_atype522_2,
};

static const int32 cn_attr522 [] =
{
2150,
2151,
2152,
};

extern const char *names523[];
static const uint32 types523 [] =
{
SK_BOOL,
};

static const uint16 attr_flags523 [] =
{0,};

static const EIF_TYPE_INDEX g_atype523_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes523 [] = {
g_atype523_0,
};

static const int32 cn_attr523 [] =
{
2212,
};

extern const char *names524[];
static const uint32 types524 [] =
{
SK_BOOL,
};

static const uint16 attr_flags524 [] =
{0,};

static const EIF_TYPE_INDEX g_atype524_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes524 [] = {
g_atype524_0,
};

static const int32 cn_attr524 [] =
{
2212,
};

extern const char *names525[];
static const uint32 types525 [] =
{
SK_REF,
SK_BOOL,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags525 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype525_0 [] = {0xFF01,324,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype525_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype525_2 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype525_3 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes525 [] = {
g_atype525_0,
g_atype525_1,
g_atype525_2,
g_atype525_3,
};

static const int32 cn_attr525 [] =
{
2156,
2212,
3106,
3094,
};

extern const char *names526[];
static const uint32 types526 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags526 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype526_0 [] = {0xFF01,444,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype526_1 [] = {0xFF01,444,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype526_2 [] = {0xFF01,444,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype526_3 [] = {0xFF01,807,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype526_4 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype526_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype526_6 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype526_7 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype526_8 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype526_9 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype526_10 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype526_11 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype526_12 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype526_13 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype526_14 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype526_15 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype526_16 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes526 [] = {
g_atype526_0,
g_atype526_1,
g_atype526_2,
g_atype526_3,
g_atype526_4,
g_atype526_5,
g_atype526_6,
g_atype526_7,
g_atype526_8,
g_atype526_9,
g_atype526_10,
g_atype526_11,
g_atype526_12,
g_atype526_13,
g_atype526_14,
g_atype526_15,
g_atype526_16,
};

static const int32 cn_attr526 [] =
{
3009,
3010,
3011,
3012,
2212,
3008,
3014,
2972,
2981,
3013,
3015,
3018,
3019,
3023,
3024,
3025,
3059,
};

extern const char *names527[];
static const uint32 types527 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags527 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype527_0 [] = {0xFF01,525,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype527_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype527_2 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype527_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype527_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype527_5 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype527_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes527 [] = {
g_atype527_0,
g_atype527_1,
g_atype527_2,
g_atype527_3,
g_atype527_4,
g_atype527_5,
g_atype527_6,
};

static const int32 cn_attr527 [] =
{
3782,
3783,
3779,
3784,
3785,
3786,
3787,
};

extern const char *names530[];
static const uint32 types530 [] =
{
SK_BOOL,
};

static const uint16 attr_flags530 [] =
{0,};

static const EIF_TYPE_INDEX g_atype530_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes530 [] = {
g_atype530_0,
};

static const int32 cn_attr530 [] =
{
2212,
};

extern const char *names531[];
static const uint32 types531 [] =
{
SK_BOOL,
};

static const uint16 attr_flags531 [] =
{0,};

static const EIF_TYPE_INDEX g_atype531_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes531 [] = {
g_atype531_0,
};

static const int32 cn_attr531 [] =
{
2212,
};

extern const char *names532[];
static const uint32 types532 [] =
{
SK_REF,
SK_REF,
SK_CHAR8,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags532 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype532_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype532_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype532_2 [] = {197,0xFFFF};
static const EIF_TYPE_INDEX g_atype532_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype532_4 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes532 [] = {
g_atype532_0,
g_atype532_1,
g_atype532_2,
g_atype532_3,
g_atype532_4,
};

static const int32 cn_attr532 [] =
{
2084,
2105,
2106,
2082,
2083,
};

extern const char *names533[];
static const uint32 types533 [] =
{
SK_POINTER,
};

static const uint16 attr_flags533 [] =
{0,};

static const EIF_TYPE_INDEX g_atype533_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes533 [] = {
g_atype533_0,
};

static const int32 cn_attr533 [] =
{
4554,
};

extern const char *names534[];
static const uint32 types534 [] =
{
SK_POINTER,
};

static const uint16 attr_flags534 [] =
{0,};

static const EIF_TYPE_INDEX g_atype534_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes534 [] = {
g_atype534_0,
};

static const int32 cn_attr534 [] =
{
4554,
};

extern const char *names535[];
static const uint32 types535 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags535 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype535_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype535_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes535 [] = {
g_atype535_0,
g_atype535_1,
};

static const int32 cn_attr535 [] =
{
3889,
3890,
};

extern const char *names536[];
static const uint32 types536 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT64,
};

static const uint16 attr_flags536 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype536_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype536_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype536_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype536_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype536_4 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes536 [] = {
g_atype536_0,
g_atype536_1,
g_atype536_2,
g_atype536_3,
g_atype536_4,
};

static const int32 cn_attr536 [] =
{
2084,
2105,
2082,
2083,
2106,
};

extern const char *names537[];
static const uint32 types537 [] =
{
SK_POINTER,
};

static const uint16 attr_flags537 [] =
{0,};

static const EIF_TYPE_INDEX g_atype537_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes537 [] = {
g_atype537_0,
};

static const int32 cn_attr537 [] =
{
4554,
};

extern const char *names538[];
static const uint32 types538 [] =
{
SK_POINTER,
};

static const uint16 attr_flags538 [] =
{0,};

static const EIF_TYPE_INDEX g_atype538_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes538 [] = {
g_atype538_0,
};

static const int32 cn_attr538 [] =
{
4554,
};

extern const char *names539[];
static const uint32 types539 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags539 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype539_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype539_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes539 [] = {
g_atype539_0,
g_atype539_1,
};

static const int32 cn_attr539 [] =
{
3889,
3890,
};

extern const char *names540[];
static const uint32 types540 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags540 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype540_0 [] = {541,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype540_1 [] = {541,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype540_2 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype540_3 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype540_4 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype540_5 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes540 [] = {
g_atype540_0,
g_atype540_1,
g_atype540_2,
g_atype540_3,
g_atype540_4,
g_atype540_5,
};

static const int32 cn_attr540 [] =
{
2766,
2770,
2212,
2774,
2775,
2776,
};

extern const char *names541[];
static const uint32 types541 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags541 [] =
{0,0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype541_0 [] = {0xFF01,539,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype541_1 [] = {541,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype541_2 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype541_3 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype541_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype541_5 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype541_6 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype541_7 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes541 [] = {
g_atype541_0,
g_atype541_1,
g_atype541_2,
g_atype541_3,
g_atype541_4,
g_atype541_5,
g_atype541_6,
g_atype541_7,
};

static const int32 cn_attr541 [] =
{
3782,
3788,
3783,
3779,
3784,
3785,
3786,
3787,
};

extern const char *names542[];
static const uint32 types542 [] =
{
SK_REF,
SK_INT32,
};

static const uint16 attr_flags542 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype542_0 [] = {541,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype542_1 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes542 [] = {
g_atype542_0,
g_atype542_1,
};

static const int32 cn_attr542 [] =
{
1722,
1718,
};

extern const char *names543[];
static const uint32 types543 [] =
{
SK_INT32,
};

static const uint16 attr_flags543 [] =
{0,};

static const EIF_TYPE_INDEX g_atype543_0 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes543 [] = {
g_atype543_0,
};

static const int32 cn_attr543 [] =
{
1718,
};

extern const char *names544[];
static const uint32 types544 [] =
{
SK_REF,
SK_BOOL,
SK_BOOL,
};

static const uint16 attr_flags544 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype544_0 [] = {541,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype544_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype544_2 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes544 [] = {
g_atype544_0,
g_atype544_1,
g_atype544_2,
};

static const int32 cn_attr544 [] =
{
2150,
2151,
2152,
};

extern const char *names545[];
static const uint32 types545 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_REAL32,
};

static const uint16 attr_flags545 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype545_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype545_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype545_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype545_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype545_4 [] = {191,0xFFFF};

static const EIF_TYPE_INDEX *gtypes545 [] = {
g_atype545_0,
g_atype545_1,
g_atype545_2,
g_atype545_3,
g_atype545_4,
};

static const int32 cn_attr545 [] =
{
2084,
2105,
2082,
2083,
2106,
};

extern const char *names546[];
static const uint32 types546 [] =
{
SK_POINTER,
};

static const uint16 attr_flags546 [] =
{0,};

static const EIF_TYPE_INDEX g_atype546_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes546 [] = {
g_atype546_0,
};

static const int32 cn_attr546 [] =
{
4554,
};

extern const char *names547[];
static const uint32 types547 [] =
{
SK_POINTER,
};

static const uint16 attr_flags547 [] =
{0,};

static const EIF_TYPE_INDEX g_atype547_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes547 [] = {
g_atype547_0,
};

static const int32 cn_attr547 [] =
{
4554,
};

extern const char *names548[];
static const uint32 types548 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags548 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype548_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype548_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes548 [] = {
g_atype548_0,
g_atype548_1,
};

static const int32 cn_attr548 [] =
{
3889,
3890,
};

extern const char *names549[];
static const uint32 types549 [] =
{
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_POINTER,
};

static const uint16 attr_flags549 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype549_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype549_1 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype549_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype549_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype549_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype549_5 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes549 [] = {
g_atype549_0,
g_atype549_1,
g_atype549_2,
g_atype549_3,
g_atype549_4,
g_atype549_5,
};

static const int32 cn_attr549 [] =
{
2084,
2099,
2082,
2083,
2098,
2097,
};

extern const char *names550[];
static const uint32 types550 [] =
{
SK_REF,
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags550 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype550_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype550_1 [] = {0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype550_2 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype550_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype550_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype550_5 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes550 [] = {
g_atype550_0,
g_atype550_1,
g_atype550_2,
g_atype550_3,
g_atype550_4,
g_atype550_5,
};

static const int32 cn_attr550 [] =
{
2084,
2097,
2099,
2082,
2083,
2098,
};

extern const char *names551[];
static const uint32 types551 [] =
{
SK_REF,
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags551 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype551_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype551_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype551_2 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype551_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype551_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype551_5 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes551 [] = {
g_atype551_0,
g_atype551_1,
g_atype551_2,
g_atype551_3,
g_atype551_4,
g_atype551_5,
};

static const int32 cn_attr551 [] =
{
2084,
2112,
2114,
2082,
2083,
2113,
};

extern const char *names552[];
static const uint32 types552 [] =
{
SK_BOOL,
};

static const uint16 attr_flags552 [] =
{0,};

static const EIF_TYPE_INDEX g_atype552_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes552 [] = {
g_atype552_0,
};

static const int32 cn_attr552 [] =
{
2212,
};

extern const char *names553[];
static const uint32 types553 [] =
{
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT64,
};

static const uint16 attr_flags553 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype553_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype553_1 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype553_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype553_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype553_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype553_5 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes553 [] = {
g_atype553_0,
g_atype553_1,
g_atype553_2,
g_atype553_3,
g_atype553_4,
g_atype553_5,
};

static const int32 cn_attr553 [] =
{
2084,
2099,
2082,
2083,
2098,
2097,
};

extern const char *names557[];
static const uint32 types557 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags557 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype557_0 [] = {0xFF01,555,227,0xFFFF};
static const EIF_TYPE_INDEX g_atype557_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype557_2 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype557_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype557_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype557_5 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype557_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes557 [] = {
g_atype557_0,
g_atype557_1,
g_atype557_2,
g_atype557_3,
g_atype557_4,
g_atype557_5,
g_atype557_6,
};

static const int32 cn_attr557 [] =
{
3782,
3783,
3779,
3784,
3785,
3786,
3787,
};

extern const char *names562[];
static const uint32 types562 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags562 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype562_0 [] = {0xFF01,553,227,0xFFFF};
static const EIF_TYPE_INDEX g_atype562_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype562_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes562 [] = {
g_atype562_0,
g_atype562_1,
g_atype562_2,
};

static const int32 cn_attr562 [] =
{
3789,
3790,
3792,
};

extern const char *names563[];
static const uint32 types563 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags563 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype563_0 [] = {0xFF01,553,227,0xFFFF};
static const EIF_TYPE_INDEX g_atype563_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype563_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes563 [] = {
g_atype563_0,
g_atype563_1,
g_atype563_2,
};

static const int32 cn_attr563 [] =
{
3789,
3790,
3792,
};

extern const char *names564[];
static const uint32 types564 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags564 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype564_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype564_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes564 [] = {
g_atype564_0,
g_atype564_1,
};

static const int32 cn_attr564 [] =
{
3889,
3890,
};

extern const char *names565[];
static const uint32 types565 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags565 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype565_0 [] = {0xFF01,553,227,0xFFFF};
static const EIF_TYPE_INDEX g_atype565_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype565_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype565_3 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes565 [] = {
g_atype565_0,
g_atype565_1,
g_atype565_2,
g_atype565_3,
};

static const int32 cn_attr565 [] =
{
2156,
2212,
2664,
2665,
};

extern const char *names566[];
static const uint32 types566 [] =
{
SK_BOOL,
};

static const uint16 attr_flags566 [] =
{0,};

static const EIF_TYPE_INDEX g_atype566_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes566 [] = {
g_atype566_0,
};

static const int32 cn_attr566 [] =
{
2212,
};

extern const char *names567[];
static const uint32 types567 [] =
{
SK_BOOL,
};

static const uint16 attr_flags567 [] =
{0,};

static const EIF_TYPE_INDEX g_atype567_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes567 [] = {
g_atype567_0,
};

static const int32 cn_attr567 [] =
{
2212,
};

extern const char *names568[];
static const uint32 types568 [] =
{
SK_BOOL,
};

static const uint16 attr_flags568 [] =
{0,};

static const EIF_TYPE_INDEX g_atype568_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes568 [] = {
g_atype568_0,
};

static const int32 cn_attr568 [] =
{
2212,
};

extern const char *names569[];
static const uint32 types569 [] =
{
SK_BOOL,
};

static const uint16 attr_flags569 [] =
{0,};

static const EIF_TYPE_INDEX g_atype569_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes569 [] = {
g_atype569_0,
};

static const int32 cn_attr569 [] =
{
2212,
};

extern const char *names570[];
static const uint32 types570 [] =
{
SK_BOOL,
};

static const uint16 attr_flags570 [] =
{0,};

static const EIF_TYPE_INDEX g_atype570_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes570 [] = {
g_atype570_0,
};

static const int32 cn_attr570 [] =
{
2212,
};

extern const char *names571[];
static const uint32 types571 [] =
{
SK_BOOL,
};

static const uint16 attr_flags571 [] =
{0,};

static const EIF_TYPE_INDEX g_atype571_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes571 [] = {
g_atype571_0,
};

static const int32 cn_attr571 [] =
{
2212,
};

extern const char *names572[];
static const uint32 types572 [] =
{
SK_BOOL,
};

static const uint16 attr_flags572 [] =
{0,};

static const EIF_TYPE_INDEX g_atype572_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes572 [] = {
g_atype572_0,
};

static const int32 cn_attr572 [] =
{
2212,
};

extern const char *names573[];
static const uint32 types573 [] =
{
SK_BOOL,
};

static const uint16 attr_flags573 [] =
{0,};

static const EIF_TYPE_INDEX g_atype573_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes573 [] = {
g_atype573_0,
};

static const int32 cn_attr573 [] =
{
2212,
};

extern const char *names574[];
static const uint32 types574 [] =
{
SK_BOOL,
};

static const uint16 attr_flags574 [] =
{0,};

static const EIF_TYPE_INDEX g_atype574_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes574 [] = {
g_atype574_0,
};

static const int32 cn_attr574 [] =
{
2212,
};

extern const char *names575[];
static const uint32 types575 [] =
{
SK_BOOL,
};

static const uint16 attr_flags575 [] =
{0,};

static const EIF_TYPE_INDEX g_atype575_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes575 [] = {
g_atype575_0,
};

static const int32 cn_attr575 [] =
{
2212,
};

extern const char *names576[];
static const uint32 types576 [] =
{
SK_BOOL,
};

static const uint16 attr_flags576 [] =
{0,};

static const EIF_TYPE_INDEX g_atype576_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes576 [] = {
g_atype576_0,
};

static const int32 cn_attr576 [] =
{
2212,
};

extern const char *names577[];
static const uint32 types577 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags577 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype577_0 [] = {0xFF01,553,227,0xFFFF};
static const EIF_TYPE_INDEX g_atype577_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype577_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes577 [] = {
g_atype577_0,
g_atype577_1,
g_atype577_2,
};

static const int32 cn_attr577 [] =
{
2156,
2212,
3094,
};

extern const char *names578[];
static const uint32 types578 [] =
{
SK_BOOL,
};

static const uint16 attr_flags578 [] =
{0,};

static const EIF_TYPE_INDEX g_atype578_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes578 [] = {
g_atype578_0,
};

static const int32 cn_attr578 [] =
{
2212,
};

extern const char *names579[];
static const uint32 types579 [] =
{
SK_BOOL,
};

static const uint16 attr_flags579 [] =
{0,};

static const EIF_TYPE_INDEX g_atype579_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes579 [] = {
g_atype579_0,
};

static const int32 cn_attr579 [] =
{
2212,
};

extern const char *names580[];
static const uint32 types580 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags580 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype580_0 [] = {0xFF01,553,227,0xFFFF};
static const EIF_TYPE_INDEX g_atype580_1 [] = {0xFF01,576,227,0xFFFF};
static const EIF_TYPE_INDEX g_atype580_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype580_3 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes580 [] = {
g_atype580_0,
g_atype580_1,
g_atype580_2,
g_atype580_3,
};

static const int32 cn_attr580 [] =
{
3789,
3794,
3790,
3792,
};

extern const char *names581[];
static const uint32 types581 [] =
{
SK_BOOL,
};

static const uint16 attr_flags581 [] =
{0,};

static const EIF_TYPE_INDEX g_atype581_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes581 [] = {
g_atype581_0,
};

static const int32 cn_attr581 [] =
{
2212,
};

extern const char *names582[];
static const uint32 types582 [] =
{
SK_BOOL,
};

static const uint16 attr_flags582 [] =
{0,};

static const EIF_TYPE_INDEX g_atype582_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes582 [] = {
g_atype582_0,
};

static const int32 cn_attr582 [] =
{
2212,
};

extern const char *names583[];
static const uint32 types583 [] =
{
SK_BOOL,
};

static const uint16 attr_flags583 [] =
{0,};

static const EIF_TYPE_INDEX g_atype583_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes583 [] = {
g_atype583_0,
};

static const int32 cn_attr583 [] =
{
2212,
};

extern const char *names584[];
static const uint32 types584 [] =
{
SK_BOOL,
};

static const uint16 attr_flags584 [] =
{0,};

static const EIF_TYPE_INDEX g_atype584_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes584 [] = {
g_atype584_0,
};

static const int32 cn_attr584 [] =
{
2212,
};

extern const char *names585[];
static const uint32 types585 [] =
{
SK_BOOL,
};

static const uint16 attr_flags585 [] =
{0,};

static const EIF_TYPE_INDEX g_atype585_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes585 [] = {
g_atype585_0,
};

static const int32 cn_attr585 [] =
{
2212,
};

extern const char *names586[];
static const uint32 types586 [] =
{
SK_BOOL,
};

static const uint16 attr_flags586 [] =
{0,};

static const EIF_TYPE_INDEX g_atype586_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes586 [] = {
g_atype586_0,
};

static const int32 cn_attr586 [] =
{
2212,
};

extern const char *names587[];
static const uint32 types587 [] =
{
SK_BOOL,
};

static const uint16 attr_flags587 [] =
{0,};

static const EIF_TYPE_INDEX g_atype587_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes587 [] = {
g_atype587_0,
};

static const int32 cn_attr587 [] =
{
2212,
};

extern const char *names588[];
static const uint32 types588 [] =
{
SK_REF,
};

static const uint16 attr_flags588 [] =
{0,};

static const EIF_TYPE_INDEX g_atype588_0 [] = {0xFF01,553,227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes588 [] = {
g_atype588_0,
};

static const int32 cn_attr588 [] =
{
2156,
};

extern const char *names589[];
static const uint32 types589 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags589 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype589_0 [] = {0xFF01,553,227,0xFFFF};
static const EIF_TYPE_INDEX g_atype589_1 [] = {0xFF01,564,227,0xFFFF};
static const EIF_TYPE_INDEX g_atype589_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype589_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype589_4 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes589 [] = {
g_atype589_0,
g_atype589_1,
g_atype589_2,
g_atype589_3,
g_atype589_4,
};

static const int32 cn_attr589 [] =
{
3789,
3802,
3790,
3792,
3803,
};

extern const char *names590[];
static const uint32 types590 [] =
{
SK_REF,
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_UINT64,
};

static const uint16 attr_flags590 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype590_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype590_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype590_2 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype590_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype590_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype590_5 [] = {224,0xFFFF};

static const EIF_TYPE_INDEX *gtypes590 [] = {
g_atype590_0,
g_atype590_1,
g_atype590_2,
g_atype590_3,
g_atype590_4,
g_atype590_5,
};

static const int32 cn_attr590 [] =
{
2084,
2112,
2114,
2082,
2083,
2113,
};

extern const char *names591[];
static const uint32 types591 [] =
{
SK_POINTER,
};

static const uint16 attr_flags591 [] =
{0,};

static const EIF_TYPE_INDEX g_atype591_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes591 [] = {
g_atype591_0,
};

static const int32 cn_attr591 [] =
{
4554,
};

extern const char *names592[];
static const uint32 types592 [] =
{
SK_POINTER,
};

static const uint16 attr_flags592 [] =
{0,};

static const EIF_TYPE_INDEX g_atype592_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes592 [] = {
g_atype592_0,
};

static const int32 cn_attr592 [] =
{
4554,
};

extern const char *names593[];
static const uint32 types593 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags593 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype593_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype593_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes593 [] = {
g_atype593_0,
g_atype593_1,
};

static const int32 cn_attr593 [] =
{
3889,
3890,
};

extern const char *names594[];
static const uint32 types594 [] =
{
SK_REF,
SK_REF,
SK_INT16,
SK_UINT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags594 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype594_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype594_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype594_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype594_3 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype594_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype594_5 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes594 [] = {
g_atype594_0,
g_atype594_1,
g_atype594_2,
g_atype594_3,
g_atype594_4,
g_atype594_5,
};

static const int32 cn_attr594 [] =
{
2084,
2112,
2113,
2114,
2082,
2083,
};

extern const char *names595[];
static const uint32 types595 [] =
{
SK_POINTER,
};

static const uint16 attr_flags595 [] =
{0,};

static const EIF_TYPE_INDEX g_atype595_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes595 [] = {
g_atype595_0,
};

static const int32 cn_attr595 [] =
{
4554,
};

extern const char *names596[];
static const uint32 types596 [] =
{
SK_POINTER,
};

static const uint16 attr_flags596 [] =
{0,};

static const EIF_TYPE_INDEX g_atype596_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes596 [] = {
g_atype596_0,
};

static const int32 cn_attr596 [] =
{
4554,
};

extern const char *names597[];
static const uint32 types597 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags597 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype597_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype597_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes597 [] = {
g_atype597_0,
g_atype597_1,
};

static const int32 cn_attr597 [] =
{
3889,
3890,
};

extern const char *names598[];
static const uint32 types598 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags598 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype598_0 [] = {0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype598_1 [] = {0xFF01,324,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype598_2 [] = {0xFF01,324,0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX g_atype598_3 [] = {0xFF01,444,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype598_4 [] = {0xFF01,807,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype598_5 [] = {0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype598_6 [] = {0xFF02,0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX g_atype598_7 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype598_8 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype598_9 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype598_10 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype598_11 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype598_12 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype598_13 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype598_14 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype598_15 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype598_16 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes598 [] = {
g_atype598_0,
g_atype598_1,
g_atype598_2,
g_atype598_3,
g_atype598_4,
g_atype598_5,
g_atype598_6,
g_atype598_7,
g_atype598_8,
g_atype598_9,
g_atype598_10,
g_atype598_11,
g_atype598_12,
g_atype598_13,
g_atype598_14,
g_atype598_15,
g_atype598_16,
};

static const int32 cn_attr598 [] =
{
2972,
3009,
3010,
3011,
3012,
3024,
3025,
2212,
3008,
3014,
2981,
3013,
3015,
3018,
3019,
3023,
3059,
};

extern const char *names599[];
static const uint32 types599 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags599 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype599_0 [] = {0xFF01,597,0xFFF8,1,0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX g_atype599_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype599_2 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype599_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype599_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype599_5 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype599_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes599 [] = {
g_atype599_0,
g_atype599_1,
g_atype599_2,
g_atype599_3,
g_atype599_4,
g_atype599_5,
g_atype599_6,
};

static const int32 cn_attr599 [] =
{
3782,
3783,
3779,
3784,
3785,
3786,
3787,
};

extern const char *names601[];
static const uint32 types601 [] =
{
SK_BOOL,
};

static const uint16 attr_flags601 [] =
{0,};

static const EIF_TYPE_INDEX g_atype601_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes601 [] = {
g_atype601_0,
};

static const int32 cn_attr601 [] =
{
2212,
};

extern const char *names603[];
static const uint32 types603 [] =
{
SK_REF,
SK_REF,
SK_CHAR8,
SK_UINT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags603 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype603_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype603_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype603_2 [] = {197,0xFFFF};
static const EIF_TYPE_INDEX g_atype603_3 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype603_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype603_5 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes603 [] = {
g_atype603_0,
g_atype603_1,
g_atype603_2,
g_atype603_3,
g_atype603_4,
g_atype603_5,
};

static const int32 cn_attr603 [] =
{
2084,
2112,
2113,
2114,
2082,
2083,
};

extern const char *names604[];
static const uint32 types604 [] =
{
SK_BOOL,
};

static const uint16 attr_flags604 [] =
{0,};

static const EIF_TYPE_INDEX g_atype604_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes604 [] = {
g_atype604_0,
};

static const int32 cn_attr604 [] =
{
2212,
};

extern const char *names605[];
static const uint32 types605 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags605 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype605_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype605_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype605_2 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype605_3 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype605_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype605_5 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes605 [] = {
g_atype605_0,
g_atype605_1,
g_atype605_2,
g_atype605_3,
g_atype605_4,
g_atype605_5,
};

static const int32 cn_attr605 [] =
{
2084,
2112,
2113,
2114,
2082,
2083,
};

extern const char *names606[];
static const uint32 types606 [] =
{
SK_POINTER,
};

static const uint16 attr_flags606 [] =
{0,};

static const EIF_TYPE_INDEX g_atype606_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes606 [] = {
g_atype606_0,
};

static const int32 cn_attr606 [] =
{
4554,
};

extern const char *names607[];
static const uint32 types607 [] =
{
SK_POINTER,
};

static const uint16 attr_flags607 [] =
{0,};

static const EIF_TYPE_INDEX g_atype607_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes607 [] = {
g_atype607_0,
};

static const int32 cn_attr607 [] =
{
4554,
};

extern const char *names608[];
static const uint32 types608 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags608 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype608_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype608_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes608 [] = {
g_atype608_0,
g_atype608_1,
};

static const int32 cn_attr608 [] =
{
3889,
3890,
};

extern const char *names612[];
static const uint32 types612 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags612 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype612_0 [] = {0xFF01,610,206,0xFFFF};
static const EIF_TYPE_INDEX g_atype612_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype612_2 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype612_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype612_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype612_5 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype612_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes612 [] = {
g_atype612_0,
g_atype612_1,
g_atype612_2,
g_atype612_3,
g_atype612_4,
g_atype612_5,
g_atype612_6,
};

static const int32 cn_attr612 [] =
{
3782,
3783,
3779,
3784,
3785,
3786,
3787,
};

extern const char *names617[];
static const uint32 types617 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags617 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype617_0 [] = {0xFF01,608,206,0xFFFF};
static const EIF_TYPE_INDEX g_atype617_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype617_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes617 [] = {
g_atype617_0,
g_atype617_1,
g_atype617_2,
};

static const int32 cn_attr617 [] =
{
3789,
3790,
3792,
};

extern const char *names618[];
static const uint32 types618 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags618 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype618_0 [] = {0xFF01,608,206,0xFFFF};
static const EIF_TYPE_INDEX g_atype618_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype618_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes618 [] = {
g_atype618_0,
g_atype618_1,
g_atype618_2,
};

static const int32 cn_attr618 [] =
{
3789,
3790,
3792,
};

extern const char *names619[];
static const uint32 types619 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags619 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype619_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype619_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes619 [] = {
g_atype619_0,
g_atype619_1,
};

static const int32 cn_attr619 [] =
{
3889,
3890,
};

extern const char *names620[];
static const uint32 types620 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags620 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype620_0 [] = {0xFF01,608,206,0xFFFF};
static const EIF_TYPE_INDEX g_atype620_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype620_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype620_3 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes620 [] = {
g_atype620_0,
g_atype620_1,
g_atype620_2,
g_atype620_3,
};

static const int32 cn_attr620 [] =
{
2156,
2212,
2664,
2665,
};

extern const char *names621[];
static const uint32 types621 [] =
{
SK_BOOL,
};

static const uint16 attr_flags621 [] =
{0,};

static const EIF_TYPE_INDEX g_atype621_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes621 [] = {
g_atype621_0,
};

static const int32 cn_attr621 [] =
{
2212,
};

extern const char *names622[];
static const uint32 types622 [] =
{
SK_BOOL,
};

static const uint16 attr_flags622 [] =
{0,};

static const EIF_TYPE_INDEX g_atype622_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes622 [] = {
g_atype622_0,
};

static const int32 cn_attr622 [] =
{
2212,
};

extern const char *names623[];
static const uint32 types623 [] =
{
SK_BOOL,
};

static const uint16 attr_flags623 [] =
{0,};

static const EIF_TYPE_INDEX g_atype623_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes623 [] = {
g_atype623_0,
};

static const int32 cn_attr623 [] =
{
2212,
};

extern const char *names624[];
static const uint32 types624 [] =
{
SK_BOOL,
};

static const uint16 attr_flags624 [] =
{0,};

static const EIF_TYPE_INDEX g_atype624_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes624 [] = {
g_atype624_0,
};

static const int32 cn_attr624 [] =
{
2212,
};

extern const char *names625[];
static const uint32 types625 [] =
{
SK_BOOL,
};

static const uint16 attr_flags625 [] =
{0,};

static const EIF_TYPE_INDEX g_atype625_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes625 [] = {
g_atype625_0,
};

static const int32 cn_attr625 [] =
{
2212,
};

extern const char *names626[];
static const uint32 types626 [] =
{
SK_BOOL,
};

static const uint16 attr_flags626 [] =
{0,};

static const EIF_TYPE_INDEX g_atype626_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes626 [] = {
g_atype626_0,
};

static const int32 cn_attr626 [] =
{
2212,
};

extern const char *names627[];
static const uint32 types627 [] =
{
SK_BOOL,
};

static const uint16 attr_flags627 [] =
{0,};

static const EIF_TYPE_INDEX g_atype627_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes627 [] = {
g_atype627_0,
};

static const int32 cn_attr627 [] =
{
2212,
};

extern const char *names628[];
static const uint32 types628 [] =
{
SK_BOOL,
};

static const uint16 attr_flags628 [] =
{0,};

static const EIF_TYPE_INDEX g_atype628_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes628 [] = {
g_atype628_0,
};

static const int32 cn_attr628 [] =
{
2212,
};

extern const char *names629[];
static const uint32 types629 [] =
{
SK_BOOL,
};

static const uint16 attr_flags629 [] =
{0,};

static const EIF_TYPE_INDEX g_atype629_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes629 [] = {
g_atype629_0,
};

static const int32 cn_attr629 [] =
{
2212,
};

extern const char *names630[];
static const uint32 types630 [] =
{
SK_BOOL,
};

static const uint16 attr_flags630 [] =
{0,};

static const EIF_TYPE_INDEX g_atype630_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes630 [] = {
g_atype630_0,
};

static const int32 cn_attr630 [] =
{
2212,
};

extern const char *names631[];
static const uint32 types631 [] =
{
SK_BOOL,
};

static const uint16 attr_flags631 [] =
{0,};

static const EIF_TYPE_INDEX g_atype631_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes631 [] = {
g_atype631_0,
};

static const int32 cn_attr631 [] =
{
2212,
};

extern const char *names632[];
static const uint32 types632 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags632 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype632_0 [] = {0xFF01,608,206,0xFFFF};
static const EIF_TYPE_INDEX g_atype632_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype632_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes632 [] = {
g_atype632_0,
g_atype632_1,
g_atype632_2,
};

static const int32 cn_attr632 [] =
{
2156,
2212,
3094,
};

extern const char *names633[];
static const uint32 types633 [] =
{
SK_BOOL,
};

static const uint16 attr_flags633 [] =
{0,};

static const EIF_TYPE_INDEX g_atype633_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes633 [] = {
g_atype633_0,
};

static const int32 cn_attr633 [] =
{
2212,
};

extern const char *names634[];
static const uint32 types634 [] =
{
SK_BOOL,
};

static const uint16 attr_flags634 [] =
{0,};

static const EIF_TYPE_INDEX g_atype634_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes634 [] = {
g_atype634_0,
};

static const int32 cn_attr634 [] =
{
2212,
};

extern const char *names635[];
static const uint32 types635 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags635 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype635_0 [] = {0xFF01,608,206,0xFFFF};
static const EIF_TYPE_INDEX g_atype635_1 [] = {0xFF01,631,206,0xFFFF};
static const EIF_TYPE_INDEX g_atype635_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype635_3 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes635 [] = {
g_atype635_0,
g_atype635_1,
g_atype635_2,
g_atype635_3,
};

static const int32 cn_attr635 [] =
{
3789,
3794,
3790,
3792,
};

extern const char *names636[];
static const uint32 types636 [] =
{
SK_BOOL,
};

static const uint16 attr_flags636 [] =
{0,};

static const EIF_TYPE_INDEX g_atype636_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes636 [] = {
g_atype636_0,
};

static const int32 cn_attr636 [] =
{
2212,
};

extern const char *names637[];
static const uint32 types637 [] =
{
SK_BOOL,
};

static const uint16 attr_flags637 [] =
{0,};

static const EIF_TYPE_INDEX g_atype637_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes637 [] = {
g_atype637_0,
};

static const int32 cn_attr637 [] =
{
2212,
};

extern const char *names638[];
static const uint32 types638 [] =
{
SK_BOOL,
};

static const uint16 attr_flags638 [] =
{0,};

static const EIF_TYPE_INDEX g_atype638_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes638 [] = {
g_atype638_0,
};

static const int32 cn_attr638 [] =
{
2212,
};

extern const char *names639[];
static const uint32 types639 [] =
{
SK_BOOL,
};

static const uint16 attr_flags639 [] =
{0,};

static const EIF_TYPE_INDEX g_atype639_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes639 [] = {
g_atype639_0,
};

static const int32 cn_attr639 [] =
{
2212,
};

extern const char *names640[];
static const uint32 types640 [] =
{
SK_BOOL,
};

static const uint16 attr_flags640 [] =
{0,};

static const EIF_TYPE_INDEX g_atype640_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes640 [] = {
g_atype640_0,
};

static const int32 cn_attr640 [] =
{
2212,
};

extern const char *names641[];
static const uint32 types641 [] =
{
SK_BOOL,
};

static const uint16 attr_flags641 [] =
{0,};

static const EIF_TYPE_INDEX g_atype641_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes641 [] = {
g_atype641_0,
};

static const int32 cn_attr641 [] =
{
2212,
};

extern const char *names642[];
static const uint32 types642 [] =
{
SK_BOOL,
};

static const uint16 attr_flags642 [] =
{0,};

static const EIF_TYPE_INDEX g_atype642_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes642 [] = {
g_atype642_0,
};

static const int32 cn_attr642 [] =
{
2212,
};

extern const char *names643[];
static const uint32 types643 [] =
{
SK_REF,
};

static const uint16 attr_flags643 [] =
{0,};

static const EIF_TYPE_INDEX g_atype643_0 [] = {0xFF01,608,206,0xFFFF};

static const EIF_TYPE_INDEX *gtypes643 [] = {
g_atype643_0,
};

static const int32 cn_attr643 [] =
{
2156,
};

extern const char *names644[];
static const uint32 types644 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags644 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype644_0 [] = {0xFF01,608,206,0xFFFF};
static const EIF_TYPE_INDEX g_atype644_1 [] = {0xFF01,619,206,0xFFFF};
static const EIF_TYPE_INDEX g_atype644_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype644_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype644_4 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes644 [] = {
g_atype644_0,
g_atype644_1,
g_atype644_2,
g_atype644_3,
g_atype644_4,
};

static const int32 cn_attr644 [] =
{
3789,
3802,
3790,
3792,
3803,
};

extern const char *names648[];
static const uint32 types648 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags648 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype648_0 [] = {0xFF01,646,188,0xFFFF};
static const EIF_TYPE_INDEX g_atype648_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype648_2 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype648_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype648_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype648_5 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype648_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes648 [] = {
g_atype648_0,
g_atype648_1,
g_atype648_2,
g_atype648_3,
g_atype648_4,
g_atype648_5,
g_atype648_6,
};

static const int32 cn_attr648 [] =
{
3782,
3783,
3779,
3784,
3785,
3786,
3787,
};

extern const char *names653[];
static const uint32 types653 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags653 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype653_0 [] = {0xFF01,644,188,0xFFFF};
static const EIF_TYPE_INDEX g_atype653_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype653_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes653 [] = {
g_atype653_0,
g_atype653_1,
g_atype653_2,
};

static const int32 cn_attr653 [] =
{
3789,
3790,
3792,
};

extern const char *names654[];
static const uint32 types654 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags654 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype654_0 [] = {0xFF01,644,188,0xFFFF};
static const EIF_TYPE_INDEX g_atype654_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype654_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes654 [] = {
g_atype654_0,
g_atype654_1,
g_atype654_2,
};

static const int32 cn_attr654 [] =
{
3789,
3790,
3792,
};

extern const char *names655[];
static const uint32 types655 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags655 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype655_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype655_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes655 [] = {
g_atype655_0,
g_atype655_1,
};

static const int32 cn_attr655 [] =
{
3889,
3890,
};

extern const char *names656[];
static const uint32 types656 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags656 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype656_0 [] = {0xFF01,644,188,0xFFFF};
static const EIF_TYPE_INDEX g_atype656_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype656_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype656_3 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes656 [] = {
g_atype656_0,
g_atype656_1,
g_atype656_2,
g_atype656_3,
};

static const int32 cn_attr656 [] =
{
2156,
2212,
2664,
2665,
};

extern const char *names657[];
static const uint32 types657 [] =
{
SK_BOOL,
};

static const uint16 attr_flags657 [] =
{0,};

static const EIF_TYPE_INDEX g_atype657_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes657 [] = {
g_atype657_0,
};

static const int32 cn_attr657 [] =
{
2212,
};

extern const char *names658[];
static const uint32 types658 [] =
{
SK_BOOL,
};

static const uint16 attr_flags658 [] =
{0,};

static const EIF_TYPE_INDEX g_atype658_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes658 [] = {
g_atype658_0,
};

static const int32 cn_attr658 [] =
{
2212,
};

extern const char *names659[];
static const uint32 types659 [] =
{
SK_BOOL,
};

static const uint16 attr_flags659 [] =
{0,};

static const EIF_TYPE_INDEX g_atype659_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes659 [] = {
g_atype659_0,
};

static const int32 cn_attr659 [] =
{
2212,
};

extern const char *names660[];
static const uint32 types660 [] =
{
SK_BOOL,
};

static const uint16 attr_flags660 [] =
{0,};

static const EIF_TYPE_INDEX g_atype660_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes660 [] = {
g_atype660_0,
};

static const int32 cn_attr660 [] =
{
2212,
};

extern const char *names661[];
static const uint32 types661 [] =
{
SK_BOOL,
};

static const uint16 attr_flags661 [] =
{0,};

static const EIF_TYPE_INDEX g_atype661_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes661 [] = {
g_atype661_0,
};

static const int32 cn_attr661 [] =
{
2212,
};

extern const char *names662[];
static const uint32 types662 [] =
{
SK_BOOL,
};

static const uint16 attr_flags662 [] =
{0,};

static const EIF_TYPE_INDEX g_atype662_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes662 [] = {
g_atype662_0,
};

static const int32 cn_attr662 [] =
{
2212,
};

extern const char *names663[];
static const uint32 types663 [] =
{
SK_BOOL,
};

static const uint16 attr_flags663 [] =
{0,};

static const EIF_TYPE_INDEX g_atype663_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes663 [] = {
g_atype663_0,
};

static const int32 cn_attr663 [] =
{
2212,
};

extern const char *names664[];
static const uint32 types664 [] =
{
SK_BOOL,
};

static const uint16 attr_flags664 [] =
{0,};

static const EIF_TYPE_INDEX g_atype664_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes664 [] = {
g_atype664_0,
};

static const int32 cn_attr664 [] =
{
2212,
};

extern const char *names665[];
static const uint32 types665 [] =
{
SK_BOOL,
};

static const uint16 attr_flags665 [] =
{0,};

static const EIF_TYPE_INDEX g_atype665_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes665 [] = {
g_atype665_0,
};

static const int32 cn_attr665 [] =
{
2212,
};

extern const char *names666[];
static const uint32 types666 [] =
{
SK_BOOL,
};

static const uint16 attr_flags666 [] =
{0,};

static const EIF_TYPE_INDEX g_atype666_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes666 [] = {
g_atype666_0,
};

static const int32 cn_attr666 [] =
{
2212,
};

extern const char *names667[];
static const uint32 types667 [] =
{
SK_BOOL,
};

static const uint16 attr_flags667 [] =
{0,};

static const EIF_TYPE_INDEX g_atype667_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes667 [] = {
g_atype667_0,
};

static const int32 cn_attr667 [] =
{
2212,
};

extern const char *names668[];
static const uint32 types668 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags668 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype668_0 [] = {0xFF01,644,188,0xFFFF};
static const EIF_TYPE_INDEX g_atype668_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype668_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes668 [] = {
g_atype668_0,
g_atype668_1,
g_atype668_2,
};

static const int32 cn_attr668 [] =
{
2156,
2212,
3094,
};

extern const char *names669[];
static const uint32 types669 [] =
{
SK_BOOL,
};

static const uint16 attr_flags669 [] =
{0,};

static const EIF_TYPE_INDEX g_atype669_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes669 [] = {
g_atype669_0,
};

static const int32 cn_attr669 [] =
{
2212,
};

extern const char *names670[];
static const uint32 types670 [] =
{
SK_BOOL,
};

static const uint16 attr_flags670 [] =
{0,};

static const EIF_TYPE_INDEX g_atype670_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes670 [] = {
g_atype670_0,
};

static const int32 cn_attr670 [] =
{
2212,
};

extern const char *names671[];
static const uint32 types671 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags671 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype671_0 [] = {0xFF01,644,188,0xFFFF};
static const EIF_TYPE_INDEX g_atype671_1 [] = {0xFF01,667,188,0xFFFF};
static const EIF_TYPE_INDEX g_atype671_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype671_3 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes671 [] = {
g_atype671_0,
g_atype671_1,
g_atype671_2,
g_atype671_3,
};

static const int32 cn_attr671 [] =
{
3789,
3794,
3790,
3792,
};

extern const char *names672[];
static const uint32 types672 [] =
{
SK_BOOL,
};

static const uint16 attr_flags672 [] =
{0,};

static const EIF_TYPE_INDEX g_atype672_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes672 [] = {
g_atype672_0,
};

static const int32 cn_attr672 [] =
{
2212,
};

extern const char *names673[];
static const uint32 types673 [] =
{
SK_BOOL,
};

static const uint16 attr_flags673 [] =
{0,};

static const EIF_TYPE_INDEX g_atype673_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes673 [] = {
g_atype673_0,
};

static const int32 cn_attr673 [] =
{
2212,
};

extern const char *names674[];
static const uint32 types674 [] =
{
SK_BOOL,
};

static const uint16 attr_flags674 [] =
{0,};

static const EIF_TYPE_INDEX g_atype674_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes674 [] = {
g_atype674_0,
};

static const int32 cn_attr674 [] =
{
2212,
};

extern const char *names675[];
static const uint32 types675 [] =
{
SK_BOOL,
};

static const uint16 attr_flags675 [] =
{0,};

static const EIF_TYPE_INDEX g_atype675_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes675 [] = {
g_atype675_0,
};

static const int32 cn_attr675 [] =
{
2212,
};

extern const char *names676[];
static const uint32 types676 [] =
{
SK_BOOL,
};

static const uint16 attr_flags676 [] =
{0,};

static const EIF_TYPE_INDEX g_atype676_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes676 [] = {
g_atype676_0,
};

static const int32 cn_attr676 [] =
{
2212,
};

extern const char *names677[];
static const uint32 types677 [] =
{
SK_BOOL,
};

static const uint16 attr_flags677 [] =
{0,};

static const EIF_TYPE_INDEX g_atype677_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes677 [] = {
g_atype677_0,
};

static const int32 cn_attr677 [] =
{
2212,
};

extern const char *names678[];
static const uint32 types678 [] =
{
SK_BOOL,
};

static const uint16 attr_flags678 [] =
{0,};

static const EIF_TYPE_INDEX g_atype678_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes678 [] = {
g_atype678_0,
};

static const int32 cn_attr678 [] =
{
2212,
};

extern const char *names679[];
static const uint32 types679 [] =
{
SK_REF,
};

static const uint16 attr_flags679 [] =
{0,};

static const EIF_TYPE_INDEX g_atype679_0 [] = {0xFF01,644,188,0xFFFF};

static const EIF_TYPE_INDEX *gtypes679 [] = {
g_atype679_0,
};

static const int32 cn_attr679 [] =
{
2156,
};

extern const char *names680[];
static const uint32 types680 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags680 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype680_0 [] = {0xFF01,644,188,0xFFFF};
static const EIF_TYPE_INDEX g_atype680_1 [] = {0xFF01,655,188,0xFFFF};
static const EIF_TYPE_INDEX g_atype680_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype680_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype680_4 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes680 [] = {
g_atype680_0,
g_atype680_1,
g_atype680_2,
g_atype680_3,
g_atype680_4,
};

static const int32 cn_attr680 [] =
{
3789,
3802,
3790,
3792,
3803,
};

extern const char *names684[];
static const uint32 types684 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags684 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype684_0 [] = {0xFF01,682,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype684_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype684_2 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype684_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype684_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype684_5 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype684_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes684 [] = {
g_atype684_0,
g_atype684_1,
g_atype684_2,
g_atype684_3,
g_atype684_4,
g_atype684_5,
g_atype684_6,
};

static const int32 cn_attr684 [] =
{
3782,
3783,
3779,
3784,
3785,
3786,
3787,
};

extern const char *names689[];
static const uint32 types689 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags689 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype689_0 [] = {0xFF01,680,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype689_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype689_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes689 [] = {
g_atype689_0,
g_atype689_1,
g_atype689_2,
};

static const int32 cn_attr689 [] =
{
3789,
3790,
3792,
};

extern const char *names690[];
static const uint32 types690 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags690 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype690_0 [] = {0xFF01,680,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype690_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype690_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes690 [] = {
g_atype690_0,
g_atype690_1,
g_atype690_2,
};

static const int32 cn_attr690 [] =
{
3789,
3790,
3792,
};

extern const char *names691[];
static const uint32 types691 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags691 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype691_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype691_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes691 [] = {
g_atype691_0,
g_atype691_1,
};

static const int32 cn_attr691 [] =
{
3889,
3890,
};

extern const char *names692[];
static const uint32 types692 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags692 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype692_0 [] = {0xFF01,680,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype692_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype692_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype692_3 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes692 [] = {
g_atype692_0,
g_atype692_1,
g_atype692_2,
g_atype692_3,
};

static const int32 cn_attr692 [] =
{
2156,
2212,
2664,
2665,
};

extern const char *names693[];
static const uint32 types693 [] =
{
SK_BOOL,
};

static const uint16 attr_flags693 [] =
{0,};

static const EIF_TYPE_INDEX g_atype693_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes693 [] = {
g_atype693_0,
};

static const int32 cn_attr693 [] =
{
2212,
};

extern const char *names694[];
static const uint32 types694 [] =
{
SK_BOOL,
};

static const uint16 attr_flags694 [] =
{0,};

static const EIF_TYPE_INDEX g_atype694_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes694 [] = {
g_atype694_0,
};

static const int32 cn_attr694 [] =
{
2212,
};

extern const char *names695[];
static const uint32 types695 [] =
{
SK_BOOL,
};

static const uint16 attr_flags695 [] =
{0,};

static const EIF_TYPE_INDEX g_atype695_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes695 [] = {
g_atype695_0,
};

static const int32 cn_attr695 [] =
{
2212,
};

extern const char *names696[];
static const uint32 types696 [] =
{
SK_BOOL,
};

static const uint16 attr_flags696 [] =
{0,};

static const EIF_TYPE_INDEX g_atype696_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes696 [] = {
g_atype696_0,
};

static const int32 cn_attr696 [] =
{
2212,
};

extern const char *names697[];
static const uint32 types697 [] =
{
SK_BOOL,
};

static const uint16 attr_flags697 [] =
{0,};

static const EIF_TYPE_INDEX g_atype697_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes697 [] = {
g_atype697_0,
};

static const int32 cn_attr697 [] =
{
2212,
};

extern const char *names698[];
static const uint32 types698 [] =
{
SK_BOOL,
};

static const uint16 attr_flags698 [] =
{0,};

static const EIF_TYPE_INDEX g_atype698_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes698 [] = {
g_atype698_0,
};

static const int32 cn_attr698 [] =
{
2212,
};

extern const char *names699[];
static const uint32 types699 [] =
{
SK_BOOL,
};

static const uint16 attr_flags699 [] =
{0,};

static const EIF_TYPE_INDEX g_atype699_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes699 [] = {
g_atype699_0,
};

static const int32 cn_attr699 [] =
{
2212,
};

extern const char *names700[];
static const uint32 types700 [] =
{
SK_BOOL,
};

static const uint16 attr_flags700 [] =
{0,};

static const EIF_TYPE_INDEX g_atype700_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes700 [] = {
g_atype700_0,
};

static const int32 cn_attr700 [] =
{
2212,
};

extern const char *names701[];
static const uint32 types701 [] =
{
SK_BOOL,
};

static const uint16 attr_flags701 [] =
{0,};

static const EIF_TYPE_INDEX g_atype701_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes701 [] = {
g_atype701_0,
};

static const int32 cn_attr701 [] =
{
2212,
};

extern const char *names702[];
static const uint32 types702 [] =
{
SK_BOOL,
};

static const uint16 attr_flags702 [] =
{0,};

static const EIF_TYPE_INDEX g_atype702_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes702 [] = {
g_atype702_0,
};

static const int32 cn_attr702 [] =
{
2212,
};

extern const char *names703[];
static const uint32 types703 [] =
{
SK_BOOL,
};

static const uint16 attr_flags703 [] =
{0,};

static const EIF_TYPE_INDEX g_atype703_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes703 [] = {
g_atype703_0,
};

static const int32 cn_attr703 [] =
{
2212,
};

extern const char *names704[];
static const uint32 types704 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags704 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype704_0 [] = {0xFF01,680,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype704_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype704_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes704 [] = {
g_atype704_0,
g_atype704_1,
g_atype704_2,
};

static const int32 cn_attr704 [] =
{
2156,
2212,
3094,
};

extern const char *names705[];
static const uint32 types705 [] =
{
SK_BOOL,
};

static const uint16 attr_flags705 [] =
{0,};

static const EIF_TYPE_INDEX g_atype705_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes705 [] = {
g_atype705_0,
};

static const int32 cn_attr705 [] =
{
2212,
};

extern const char *names706[];
static const uint32 types706 [] =
{
SK_BOOL,
};

static const uint16 attr_flags706 [] =
{0,};

static const EIF_TYPE_INDEX g_atype706_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes706 [] = {
g_atype706_0,
};

static const int32 cn_attr706 [] =
{
2212,
};

extern const char *names707[];
static const uint32 types707 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags707 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype707_0 [] = {0xFF01,680,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype707_1 [] = {0xFF01,703,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype707_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype707_3 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes707 [] = {
g_atype707_0,
g_atype707_1,
g_atype707_2,
g_atype707_3,
};

static const int32 cn_attr707 [] =
{
3789,
3794,
3790,
3792,
};

extern const char *names708[];
static const uint32 types708 [] =
{
SK_BOOL,
};

static const uint16 attr_flags708 [] =
{0,};

static const EIF_TYPE_INDEX g_atype708_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes708 [] = {
g_atype708_0,
};

static const int32 cn_attr708 [] =
{
2212,
};

extern const char *names709[];
static const uint32 types709 [] =
{
SK_BOOL,
};

static const uint16 attr_flags709 [] =
{0,};

static const EIF_TYPE_INDEX g_atype709_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes709 [] = {
g_atype709_0,
};

static const int32 cn_attr709 [] =
{
2212,
};

extern const char *names710[];
static const uint32 types710 [] =
{
SK_BOOL,
};

static const uint16 attr_flags710 [] =
{0,};

static const EIF_TYPE_INDEX g_atype710_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes710 [] = {
g_atype710_0,
};

static const int32 cn_attr710 [] =
{
2212,
};

extern const char *names711[];
static const uint32 types711 [] =
{
SK_BOOL,
};

static const uint16 attr_flags711 [] =
{0,};

static const EIF_TYPE_INDEX g_atype711_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes711 [] = {
g_atype711_0,
};

static const int32 cn_attr711 [] =
{
2212,
};

extern const char *names712[];
static const uint32 types712 [] =
{
SK_BOOL,
};

static const uint16 attr_flags712 [] =
{0,};

static const EIF_TYPE_INDEX g_atype712_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes712 [] = {
g_atype712_0,
};

static const int32 cn_attr712 [] =
{
2212,
};

extern const char *names713[];
static const uint32 types713 [] =
{
SK_BOOL,
};

static const uint16 attr_flags713 [] =
{0,};

static const EIF_TYPE_INDEX g_atype713_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes713 [] = {
g_atype713_0,
};

static const int32 cn_attr713 [] =
{
2212,
};

extern const char *names714[];
static const uint32 types714 [] =
{
SK_BOOL,
};

static const uint16 attr_flags714 [] =
{0,};

static const EIF_TYPE_INDEX g_atype714_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes714 [] = {
g_atype714_0,
};

static const int32 cn_attr714 [] =
{
2212,
};

extern const char *names715[];
static const uint32 types715 [] =
{
SK_REF,
};

static const uint16 attr_flags715 [] =
{0,};

static const EIF_TYPE_INDEX g_atype715_0 [] = {0xFF01,680,221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes715 [] = {
g_atype715_0,
};

static const int32 cn_attr715 [] =
{
2156,
};

extern const char *names716[];
static const uint32 types716 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags716 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype716_0 [] = {0xFF01,680,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype716_1 [] = {0xFF01,691,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype716_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype716_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype716_4 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes716 [] = {
g_atype716_0,
g_atype716_1,
g_atype716_2,
g_atype716_3,
g_atype716_4,
};

static const int32 cn_attr716 [] =
{
3789,
3802,
3790,
3792,
3803,
};

extern const char *names720[];
static const uint32 types720 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags720 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype720_0 [] = {0xFF01,718,212,0xFFFF};
static const EIF_TYPE_INDEX g_atype720_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype720_2 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype720_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype720_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype720_5 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype720_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes720 [] = {
g_atype720_0,
g_atype720_1,
g_atype720_2,
g_atype720_3,
g_atype720_4,
g_atype720_5,
g_atype720_6,
};

static const int32 cn_attr720 [] =
{
3782,
3783,
3779,
3784,
3785,
3786,
3787,
};

extern const char *names725[];
static const uint32 types725 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags725 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype725_0 [] = {0xFF01,716,212,0xFFFF};
static const EIF_TYPE_INDEX g_atype725_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype725_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes725 [] = {
g_atype725_0,
g_atype725_1,
g_atype725_2,
};

static const int32 cn_attr725 [] =
{
3789,
3790,
3792,
};

extern const char *names726[];
static const uint32 types726 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags726 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype726_0 [] = {0xFF01,716,212,0xFFFF};
static const EIF_TYPE_INDEX g_atype726_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype726_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes726 [] = {
g_atype726_0,
g_atype726_1,
g_atype726_2,
};

static const int32 cn_attr726 [] =
{
3789,
3790,
3792,
};

extern const char *names727[];
static const uint32 types727 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags727 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype727_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype727_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes727 [] = {
g_atype727_0,
g_atype727_1,
};

static const int32 cn_attr727 [] =
{
3889,
3890,
};

extern const char *names728[];
static const uint32 types728 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags728 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype728_0 [] = {0xFF01,716,212,0xFFFF};
static const EIF_TYPE_INDEX g_atype728_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype728_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype728_3 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes728 [] = {
g_atype728_0,
g_atype728_1,
g_atype728_2,
g_atype728_3,
};

static const int32 cn_attr728 [] =
{
2156,
2212,
2664,
2665,
};

extern const char *names729[];
static const uint32 types729 [] =
{
SK_BOOL,
};

static const uint16 attr_flags729 [] =
{0,};

static const EIF_TYPE_INDEX g_atype729_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes729 [] = {
g_atype729_0,
};

static const int32 cn_attr729 [] =
{
2212,
};

extern const char *names730[];
static const uint32 types730 [] =
{
SK_BOOL,
};

static const uint16 attr_flags730 [] =
{0,};

static const EIF_TYPE_INDEX g_atype730_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes730 [] = {
g_atype730_0,
};

static const int32 cn_attr730 [] =
{
2212,
};

extern const char *names731[];
static const uint32 types731 [] =
{
SK_BOOL,
};

static const uint16 attr_flags731 [] =
{0,};

static const EIF_TYPE_INDEX g_atype731_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes731 [] = {
g_atype731_0,
};

static const int32 cn_attr731 [] =
{
2212,
};

extern const char *names732[];
static const uint32 types732 [] =
{
SK_BOOL,
};

static const uint16 attr_flags732 [] =
{0,};

static const EIF_TYPE_INDEX g_atype732_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes732 [] = {
g_atype732_0,
};

static const int32 cn_attr732 [] =
{
2212,
};

extern const char *names733[];
static const uint32 types733 [] =
{
SK_BOOL,
};

static const uint16 attr_flags733 [] =
{0,};

static const EIF_TYPE_INDEX g_atype733_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes733 [] = {
g_atype733_0,
};

static const int32 cn_attr733 [] =
{
2212,
};

extern const char *names734[];
static const uint32 types734 [] =
{
SK_BOOL,
};

static const uint16 attr_flags734 [] =
{0,};

static const EIF_TYPE_INDEX g_atype734_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes734 [] = {
g_atype734_0,
};

static const int32 cn_attr734 [] =
{
2212,
};

extern const char *names735[];
static const uint32 types735 [] =
{
SK_BOOL,
};

static const uint16 attr_flags735 [] =
{0,};

static const EIF_TYPE_INDEX g_atype735_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes735 [] = {
g_atype735_0,
};

static const int32 cn_attr735 [] =
{
2212,
};

extern const char *names736[];
static const uint32 types736 [] =
{
SK_BOOL,
};

static const uint16 attr_flags736 [] =
{0,};

static const EIF_TYPE_INDEX g_atype736_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes736 [] = {
g_atype736_0,
};

static const int32 cn_attr736 [] =
{
2212,
};

extern const char *names737[];
static const uint32 types737 [] =
{
SK_BOOL,
};

static const uint16 attr_flags737 [] =
{0,};

static const EIF_TYPE_INDEX g_atype737_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes737 [] = {
g_atype737_0,
};

static const int32 cn_attr737 [] =
{
2212,
};

extern const char *names738[];
static const uint32 types738 [] =
{
SK_BOOL,
};

static const uint16 attr_flags738 [] =
{0,};

static const EIF_TYPE_INDEX g_atype738_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes738 [] = {
g_atype738_0,
};

static const int32 cn_attr738 [] =
{
2212,
};

extern const char *names739[];
static const uint32 types739 [] =
{
SK_BOOL,
};

static const uint16 attr_flags739 [] =
{0,};

static const EIF_TYPE_INDEX g_atype739_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes739 [] = {
g_atype739_0,
};

static const int32 cn_attr739 [] =
{
2212,
};

extern const char *names740[];
static const uint32 types740 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags740 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype740_0 [] = {0xFF01,716,212,0xFFFF};
static const EIF_TYPE_INDEX g_atype740_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype740_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes740 [] = {
g_atype740_0,
g_atype740_1,
g_atype740_2,
};

static const int32 cn_attr740 [] =
{
2156,
2212,
3094,
};

extern const char *names741[];
static const uint32 types741 [] =
{
SK_BOOL,
};

static const uint16 attr_flags741 [] =
{0,};

static const EIF_TYPE_INDEX g_atype741_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes741 [] = {
g_atype741_0,
};

static const int32 cn_attr741 [] =
{
2212,
};

extern const char *names742[];
static const uint32 types742 [] =
{
SK_BOOL,
};

static const uint16 attr_flags742 [] =
{0,};

static const EIF_TYPE_INDEX g_atype742_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes742 [] = {
g_atype742_0,
};

static const int32 cn_attr742 [] =
{
2212,
};

extern const char *names743[];
static const uint32 types743 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags743 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype743_0 [] = {0xFF01,716,212,0xFFFF};
static const EIF_TYPE_INDEX g_atype743_1 [] = {0xFF01,739,212,0xFFFF};
static const EIF_TYPE_INDEX g_atype743_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype743_3 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes743 [] = {
g_atype743_0,
g_atype743_1,
g_atype743_2,
g_atype743_3,
};

static const int32 cn_attr743 [] =
{
3789,
3794,
3790,
3792,
};

extern const char *names744[];
static const uint32 types744 [] =
{
SK_BOOL,
};

static const uint16 attr_flags744 [] =
{0,};

static const EIF_TYPE_INDEX g_atype744_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes744 [] = {
g_atype744_0,
};

static const int32 cn_attr744 [] =
{
2212,
};

extern const char *names745[];
static const uint32 types745 [] =
{
SK_BOOL,
};

static const uint16 attr_flags745 [] =
{0,};

static const EIF_TYPE_INDEX g_atype745_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes745 [] = {
g_atype745_0,
};

static const int32 cn_attr745 [] =
{
2212,
};

extern const char *names746[];
static const uint32 types746 [] =
{
SK_BOOL,
};

static const uint16 attr_flags746 [] =
{0,};

static const EIF_TYPE_INDEX g_atype746_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes746 [] = {
g_atype746_0,
};

static const int32 cn_attr746 [] =
{
2212,
};

extern const char *names747[];
static const uint32 types747 [] =
{
SK_BOOL,
};

static const uint16 attr_flags747 [] =
{0,};

static const EIF_TYPE_INDEX g_atype747_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes747 [] = {
g_atype747_0,
};

static const int32 cn_attr747 [] =
{
2212,
};

extern const char *names748[];
static const uint32 types748 [] =
{
SK_BOOL,
};

static const uint16 attr_flags748 [] =
{0,};

static const EIF_TYPE_INDEX g_atype748_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes748 [] = {
g_atype748_0,
};

static const int32 cn_attr748 [] =
{
2212,
};

extern const char *names749[];
static const uint32 types749 [] =
{
SK_BOOL,
};

static const uint16 attr_flags749 [] =
{0,};

static const EIF_TYPE_INDEX g_atype749_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes749 [] = {
g_atype749_0,
};

static const int32 cn_attr749 [] =
{
2212,
};

extern const char *names750[];
static const uint32 types750 [] =
{
SK_BOOL,
};

static const uint16 attr_flags750 [] =
{0,};

static const EIF_TYPE_INDEX g_atype750_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes750 [] = {
g_atype750_0,
};

static const int32 cn_attr750 [] =
{
2212,
};

extern const char *names751[];
static const uint32 types751 [] =
{
SK_REF,
};

static const uint16 attr_flags751 [] =
{0,};

static const EIF_TYPE_INDEX g_atype751_0 [] = {0xFF01,716,212,0xFFFF};

static const EIF_TYPE_INDEX *gtypes751 [] = {
g_atype751_0,
};

static const int32 cn_attr751 [] =
{
2156,
};

extern const char *names752[];
static const uint32 types752 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags752 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype752_0 [] = {0xFF01,716,212,0xFFFF};
static const EIF_TYPE_INDEX g_atype752_1 [] = {0xFF01,727,212,0xFFFF};
static const EIF_TYPE_INDEX g_atype752_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype752_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype752_4 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes752 [] = {
g_atype752_0,
g_atype752_1,
g_atype752_2,
g_atype752_3,
g_atype752_4,
};

static const int32 cn_attr752 [] =
{
3789,
3802,
3790,
3792,
3803,
};

extern const char *names755[];
static const uint32 types755 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags755 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype755_0 [] = {0xFF01,752,197,0xFFFF};
static const EIF_TYPE_INDEX g_atype755_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype755_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes755 [] = {
g_atype755_0,
g_atype755_1,
g_atype755_2,
};

static const int32 cn_attr755 [] =
{
3789,
3790,
3792,
};

extern const char *names756[];
static const uint32 types756 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags756 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype756_0 [] = {0xFF01,752,197,0xFFFF};
static const EIF_TYPE_INDEX g_atype756_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype756_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes756 [] = {
g_atype756_0,
g_atype756_1,
g_atype756_2,
};

static const int32 cn_attr756 [] =
{
3789,
3790,
3792,
};

extern const char *names757[];
static const uint32 types757 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags757 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype757_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype757_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes757 [] = {
g_atype757_0,
g_atype757_1,
};

static const int32 cn_attr757 [] =
{
3889,
3890,
};

extern const char *names758[];
static const uint32 types758 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags758 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype758_0 [] = {0xFF01,752,197,0xFFFF};
static const EIF_TYPE_INDEX g_atype758_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype758_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype758_3 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes758 [] = {
g_atype758_0,
g_atype758_1,
g_atype758_2,
g_atype758_3,
};

static const int32 cn_attr758 [] =
{
2156,
2212,
2664,
2665,
};

extern const char *names759[];
static const uint32 types759 [] =
{
SK_BOOL,
};

static const uint16 attr_flags759 [] =
{0,};

static const EIF_TYPE_INDEX g_atype759_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes759 [] = {
g_atype759_0,
};

static const int32 cn_attr759 [] =
{
2212,
};

extern const char *names760[];
static const uint32 types760 [] =
{
SK_BOOL,
};

static const uint16 attr_flags760 [] =
{0,};

static const EIF_TYPE_INDEX g_atype760_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes760 [] = {
g_atype760_0,
};

static const int32 cn_attr760 [] =
{
2212,
};

extern const char *names761[];
static const uint32 types761 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags761 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype761_0 [] = {0xFF01,752,197,0xFFFF};
static const EIF_TYPE_INDEX g_atype761_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype761_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes761 [] = {
g_atype761_0,
g_atype761_1,
g_atype761_2,
};

static const int32 cn_attr761 [] =
{
2156,
2212,
3094,
};

extern const char *names762[];
static const uint32 types762 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags762 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype762_0 [] = {0xFF01,752,197,0xFFFF};
static const EIF_TYPE_INDEX g_atype762_1 [] = {0xFF01,760,197,0xFFFF};
static const EIF_TYPE_INDEX g_atype762_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype762_3 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes762 [] = {
g_atype762_0,
g_atype762_1,
g_atype762_2,
g_atype762_3,
};

static const int32 cn_attr762 [] =
{
3789,
3794,
3790,
3792,
};

extern const char *names763[];
static const uint32 types763 [] =
{
SK_BOOL,
};

static const uint16 attr_flags763 [] =
{0,};

static const EIF_TYPE_INDEX g_atype763_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes763 [] = {
g_atype763_0,
};

static const int32 cn_attr763 [] =
{
2212,
};

extern const char *names764[];
static const uint32 types764 [] =
{
SK_BOOL,
};

static const uint16 attr_flags764 [] =
{0,};

static const EIF_TYPE_INDEX g_atype764_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes764 [] = {
g_atype764_0,
};

static const int32 cn_attr764 [] =
{
2212,
};

extern const char *names765[];
static const uint32 types765 [] =
{
SK_BOOL,
};

static const uint16 attr_flags765 [] =
{0,};

static const EIF_TYPE_INDEX g_atype765_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes765 [] = {
g_atype765_0,
};

static const int32 cn_attr765 [] =
{
2212,
};

extern const char *names766[];
static const uint32 types766 [] =
{
SK_BOOL,
};

static const uint16 attr_flags766 [] =
{0,};

static const EIF_TYPE_INDEX g_atype766_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes766 [] = {
g_atype766_0,
};

static const int32 cn_attr766 [] =
{
2212,
};

extern const char *names767[];
static const uint32 types767 [] =
{
SK_BOOL,
};

static const uint16 attr_flags767 [] =
{0,};

static const EIF_TYPE_INDEX g_atype767_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes767 [] = {
g_atype767_0,
};

static const int32 cn_attr767 [] =
{
2212,
};

extern const char *names768[];
static const uint32 types768 [] =
{
SK_BOOL,
};

static const uint16 attr_flags768 [] =
{0,};

static const EIF_TYPE_INDEX g_atype768_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes768 [] = {
g_atype768_0,
};

static const int32 cn_attr768 [] =
{
2212,
};

extern const char *names769[];
static const uint32 types769 [] =
{
SK_REF,
};

static const uint16 attr_flags769 [] =
{0,};

static const EIF_TYPE_INDEX g_atype769_0 [] = {0xFF01,752,197,0xFFFF};

static const EIF_TYPE_INDEX *gtypes769 [] = {
g_atype769_0,
};

static const int32 cn_attr769 [] =
{
2156,
};

extern const char *names770[];
static const uint32 types770 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags770 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype770_0 [] = {0xFF01,752,197,0xFFFF};
static const EIF_TYPE_INDEX g_atype770_1 [] = {0xFF01,757,197,0xFFFF};
static const EIF_TYPE_INDEX g_atype770_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype770_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype770_4 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes770 [] = {
g_atype770_0,
g_atype770_1,
g_atype770_2,
g_atype770_3,
g_atype770_4,
};

static const int32 cn_attr770 [] =
{
3789,
3802,
3790,
3792,
3803,
};

extern const char *names771[];
static const uint32 types771 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags771 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype771_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype771_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes771 [] = {
g_atype771_0,
g_atype771_1,
};

static const int32 cn_attr771 [] =
{
3889,
3890,
};

extern const char *names775[];
static const uint32 types775 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags775 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype775_0 [] = {0xFF01,773,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype775_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype775_2 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype775_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype775_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype775_5 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype775_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes775 [] = {
g_atype775_0,
g_atype775_1,
g_atype775_2,
g_atype775_3,
g_atype775_4,
g_atype775_5,
g_atype775_6,
};

static const int32 cn_attr775 [] =
{
3782,
3783,
3779,
3784,
3785,
3786,
3787,
};

extern const char *names780[];
static const uint32 types780 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags780 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype780_0 [] = {0xFF01,771,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype780_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype780_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes780 [] = {
g_atype780_0,
g_atype780_1,
g_atype780_2,
};

static const int32 cn_attr780 [] =
{
3789,
3790,
3792,
};

extern const char *names781[];
static const uint32 types781 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags781 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype781_0 [] = {0xFF01,771,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype781_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype781_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes781 [] = {
g_atype781_0,
g_atype781_1,
g_atype781_2,
};

static const int32 cn_attr781 [] =
{
3789,
3790,
3792,
};

extern const char *names782[];
static const uint32 types782 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags782 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype782_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype782_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes782 [] = {
g_atype782_0,
g_atype782_1,
};

static const int32 cn_attr782 [] =
{
3889,
3890,
};

extern const char *names783[];
static const uint32 types783 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags783 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype783_0 [] = {0xFF01,771,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype783_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype783_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype783_3 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes783 [] = {
g_atype783_0,
g_atype783_1,
g_atype783_2,
g_atype783_3,
};

static const int32 cn_attr783 [] =
{
2156,
2212,
2664,
2665,
};

extern const char *names784[];
static const uint32 types784 [] =
{
SK_BOOL,
};

static const uint16 attr_flags784 [] =
{0,};

static const EIF_TYPE_INDEX g_atype784_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes784 [] = {
g_atype784_0,
};

static const int32 cn_attr784 [] =
{
2212,
};

extern const char *names785[];
static const uint32 types785 [] =
{
SK_BOOL,
};

static const uint16 attr_flags785 [] =
{0,};

static const EIF_TYPE_INDEX g_atype785_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes785 [] = {
g_atype785_0,
};

static const int32 cn_attr785 [] =
{
2212,
};

extern const char *names786[];
static const uint32 types786 [] =
{
SK_BOOL,
};

static const uint16 attr_flags786 [] =
{0,};

static const EIF_TYPE_INDEX g_atype786_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes786 [] = {
g_atype786_0,
};

static const int32 cn_attr786 [] =
{
2212,
};

extern const char *names787[];
static const uint32 types787 [] =
{
SK_BOOL,
};

static const uint16 attr_flags787 [] =
{0,};

static const EIF_TYPE_INDEX g_atype787_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes787 [] = {
g_atype787_0,
};

static const int32 cn_attr787 [] =
{
2212,
};

extern const char *names788[];
static const uint32 types788 [] =
{
SK_BOOL,
};

static const uint16 attr_flags788 [] =
{0,};

static const EIF_TYPE_INDEX g_atype788_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes788 [] = {
g_atype788_0,
};

static const int32 cn_attr788 [] =
{
2212,
};

extern const char *names789[];
static const uint32 types789 [] =
{
SK_BOOL,
};

static const uint16 attr_flags789 [] =
{0,};

static const EIF_TYPE_INDEX g_atype789_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes789 [] = {
g_atype789_0,
};

static const int32 cn_attr789 [] =
{
2212,
};

extern const char *names790[];
static const uint32 types790 [] =
{
SK_BOOL,
};

static const uint16 attr_flags790 [] =
{0,};

static const EIF_TYPE_INDEX g_atype790_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes790 [] = {
g_atype790_0,
};

static const int32 cn_attr790 [] =
{
2212,
};

extern const char *names791[];
static const uint32 types791 [] =
{
SK_BOOL,
};

static const uint16 attr_flags791 [] =
{0,};

static const EIF_TYPE_INDEX g_atype791_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes791 [] = {
g_atype791_0,
};

static const int32 cn_attr791 [] =
{
2212,
};

extern const char *names792[];
static const uint32 types792 [] =
{
SK_BOOL,
};

static const uint16 attr_flags792 [] =
{0,};

static const EIF_TYPE_INDEX g_atype792_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes792 [] = {
g_atype792_0,
};

static const int32 cn_attr792 [] =
{
2212,
};

extern const char *names793[];
static const uint32 types793 [] =
{
SK_BOOL,
};

static const uint16 attr_flags793 [] =
{0,};

static const EIF_TYPE_INDEX g_atype793_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes793 [] = {
g_atype793_0,
};

static const int32 cn_attr793 [] =
{
2212,
};

extern const char *names794[];
static const uint32 types794 [] =
{
SK_BOOL,
};

static const uint16 attr_flags794 [] =
{0,};

static const EIF_TYPE_INDEX g_atype794_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes794 [] = {
g_atype794_0,
};

static const int32 cn_attr794 [] =
{
2212,
};

extern const char *names795[];
static const uint32 types795 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags795 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype795_0 [] = {0xFF01,771,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype795_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype795_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes795 [] = {
g_atype795_0,
g_atype795_1,
g_atype795_2,
};

static const int32 cn_attr795 [] =
{
2156,
2212,
3094,
};

extern const char *names796[];
static const uint32 types796 [] =
{
SK_BOOL,
};

static const uint16 attr_flags796 [] =
{0,};

static const EIF_TYPE_INDEX g_atype796_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes796 [] = {
g_atype796_0,
};

static const int32 cn_attr796 [] =
{
2212,
};

extern const char *names797[];
static const uint32 types797 [] =
{
SK_BOOL,
};

static const uint16 attr_flags797 [] =
{0,};

static const EIF_TYPE_INDEX g_atype797_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes797 [] = {
g_atype797_0,
};

static const int32 cn_attr797 [] =
{
2212,
};

extern const char *names798[];
static const uint32 types798 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags798 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype798_0 [] = {0xFF01,771,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype798_1 [] = {0xFF01,794,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype798_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype798_3 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes798 [] = {
g_atype798_0,
g_atype798_1,
g_atype798_2,
g_atype798_3,
};

static const int32 cn_attr798 [] =
{
3789,
3794,
3790,
3792,
};

extern const char *names799[];
static const uint32 types799 [] =
{
SK_BOOL,
};

static const uint16 attr_flags799 [] =
{0,};

static const EIF_TYPE_INDEX g_atype799_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes799 [] = {
g_atype799_0,
};

static const int32 cn_attr799 [] =
{
2212,
};

extern const char *names800[];
static const uint32 types800 [] =
{
SK_BOOL,
};

static const uint16 attr_flags800 [] =
{0,};

static const EIF_TYPE_INDEX g_atype800_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes800 [] = {
g_atype800_0,
};

static const int32 cn_attr800 [] =
{
2212,
};

extern const char *names801[];
static const uint32 types801 [] =
{
SK_BOOL,
};

static const uint16 attr_flags801 [] =
{0,};

static const EIF_TYPE_INDEX g_atype801_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes801 [] = {
g_atype801_0,
};

static const int32 cn_attr801 [] =
{
2212,
};

extern const char *names802[];
static const uint32 types802 [] =
{
SK_BOOL,
};

static const uint16 attr_flags802 [] =
{0,};

static const EIF_TYPE_INDEX g_atype802_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes802 [] = {
g_atype802_0,
};

static const int32 cn_attr802 [] =
{
2212,
};

extern const char *names803[];
static const uint32 types803 [] =
{
SK_BOOL,
};

static const uint16 attr_flags803 [] =
{0,};

static const EIF_TYPE_INDEX g_atype803_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes803 [] = {
g_atype803_0,
};

static const int32 cn_attr803 [] =
{
2212,
};

extern const char *names804[];
static const uint32 types804 [] =
{
SK_BOOL,
};

static const uint16 attr_flags804 [] =
{0,};

static const EIF_TYPE_INDEX g_atype804_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes804 [] = {
g_atype804_0,
};

static const int32 cn_attr804 [] =
{
2212,
};

extern const char *names805[];
static const uint32 types805 [] =
{
SK_BOOL,
};

static const uint16 attr_flags805 [] =
{0,};

static const EIF_TYPE_INDEX g_atype805_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes805 [] = {
g_atype805_0,
};

static const int32 cn_attr805 [] =
{
2212,
};

extern const char *names806[];
static const uint32 types806 [] =
{
SK_REF,
};

static const uint16 attr_flags806 [] =
{0,};

static const EIF_TYPE_INDEX g_atype806_0 [] = {0xFF01,771,200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes806 [] = {
g_atype806_0,
};

static const int32 cn_attr806 [] =
{
2156,
};

extern const char *names807[];
static const uint32 types807 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags807 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype807_0 [] = {0xFF01,771,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype807_1 [] = {0xFF01,782,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype807_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype807_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype807_4 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes807 [] = {
g_atype807_0,
g_atype807_1,
g_atype807_2,
g_atype807_3,
g_atype807_4,
};

static const int32 cn_attr807 [] =
{
3789,
3802,
3790,
3792,
3803,
};

extern const char *names811[];
static const uint32 types811 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags811 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype811_0 [] = {0xFF01,809,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype811_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype811_2 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype811_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype811_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype811_5 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype811_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes811 [] = {
g_atype811_0,
g_atype811_1,
g_atype811_2,
g_atype811_3,
g_atype811_4,
g_atype811_5,
g_atype811_6,
};

static const int32 cn_attr811 [] =
{
3782,
3783,
3779,
3784,
3785,
3786,
3787,
};

extern const char *names816[];
static const uint32 types816 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags816 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype816_0 [] = {0xFF01,807,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype816_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype816_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes816 [] = {
g_atype816_0,
g_atype816_1,
g_atype816_2,
};

static const int32 cn_attr816 [] =
{
3789,
3790,
3792,
};

extern const char *names817[];
static const uint32 types817 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags817 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype817_0 [] = {0xFF01,807,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype817_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype817_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes817 [] = {
g_atype817_0,
g_atype817_1,
g_atype817_2,
};

static const int32 cn_attr817 [] =
{
3789,
3790,
3792,
};

extern const char *names818[];
static const uint32 types818 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags818 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype818_0 [] = {0xFF01,807,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype818_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype818_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype818_3 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes818 [] = {
g_atype818_0,
g_atype818_1,
g_atype818_2,
g_atype818_3,
};

static const int32 cn_attr818 [] =
{
2156,
2212,
2664,
2665,
};

extern const char *names819[];
static const uint32 types819 [] =
{
SK_BOOL,
};

static const uint16 attr_flags819 [] =
{0,};

static const EIF_TYPE_INDEX g_atype819_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes819 [] = {
g_atype819_0,
};

static const int32 cn_attr819 [] =
{
2212,
};

extern const char *names820[];
static const uint32 types820 [] =
{
SK_BOOL,
};

static const uint16 attr_flags820 [] =
{0,};

static const EIF_TYPE_INDEX g_atype820_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes820 [] = {
g_atype820_0,
};

static const int32 cn_attr820 [] =
{
2212,
};

extern const char *names821[];
static const uint32 types821 [] =
{
SK_BOOL,
};

static const uint16 attr_flags821 [] =
{0,};

static const EIF_TYPE_INDEX g_atype821_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes821 [] = {
g_atype821_0,
};

static const int32 cn_attr821 [] =
{
2212,
};

extern const char *names822[];
static const uint32 types822 [] =
{
SK_BOOL,
};

static const uint16 attr_flags822 [] =
{0,};

static const EIF_TYPE_INDEX g_atype822_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes822 [] = {
g_atype822_0,
};

static const int32 cn_attr822 [] =
{
2212,
};

extern const char *names823[];
static const uint32 types823 [] =
{
SK_BOOL,
};

static const uint16 attr_flags823 [] =
{0,};

static const EIF_TYPE_INDEX g_atype823_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes823 [] = {
g_atype823_0,
};

static const int32 cn_attr823 [] =
{
2212,
};

extern const char *names824[];
static const uint32 types824 [] =
{
SK_BOOL,
};

static const uint16 attr_flags824 [] =
{0,};

static const EIF_TYPE_INDEX g_atype824_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes824 [] = {
g_atype824_0,
};

static const int32 cn_attr824 [] =
{
2212,
};

extern const char *names825[];
static const uint32 types825 [] =
{
SK_BOOL,
};

static const uint16 attr_flags825 [] =
{0,};

static const EIF_TYPE_INDEX g_atype825_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes825 [] = {
g_atype825_0,
};

static const int32 cn_attr825 [] =
{
2212,
};

extern const char *names826[];
static const uint32 types826 [] =
{
SK_BOOL,
};

static const uint16 attr_flags826 [] =
{0,};

static const EIF_TYPE_INDEX g_atype826_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes826 [] = {
g_atype826_0,
};

static const int32 cn_attr826 [] =
{
2212,
};

extern const char *names827[];
static const uint32 types827 [] =
{
SK_BOOL,
};

static const uint16 attr_flags827 [] =
{0,};

static const EIF_TYPE_INDEX g_atype827_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes827 [] = {
g_atype827_0,
};

static const int32 cn_attr827 [] =
{
2212,
};

extern const char *names828[];
static const uint32 types828 [] =
{
SK_BOOL,
};

static const uint16 attr_flags828 [] =
{0,};

static const EIF_TYPE_INDEX g_atype828_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes828 [] = {
g_atype828_0,
};

static const int32 cn_attr828 [] =
{
2212,
};

extern const char *names829[];
static const uint32 types829 [] =
{
SK_BOOL,
};

static const uint16 attr_flags829 [] =
{0,};

static const EIF_TYPE_INDEX g_atype829_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes829 [] = {
g_atype829_0,
};

static const int32 cn_attr829 [] =
{
2212,
};

extern const char *names830[];
static const uint32 types830 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags830 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype830_0 [] = {0xFF01,807,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype830_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype830_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes830 [] = {
g_atype830_0,
g_atype830_1,
g_atype830_2,
};

static const int32 cn_attr830 [] =
{
2156,
2212,
3094,
};

extern const char *names831[];
static const uint32 types831 [] =
{
SK_BOOL,
};

static const uint16 attr_flags831 [] =
{0,};

static const EIF_TYPE_INDEX g_atype831_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes831 [] = {
g_atype831_0,
};

static const int32 cn_attr831 [] =
{
2212,
};

extern const char *names832[];
static const uint32 types832 [] =
{
SK_BOOL,
};

static const uint16 attr_flags832 [] =
{0,};

static const EIF_TYPE_INDEX g_atype832_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes832 [] = {
g_atype832_0,
};

static const int32 cn_attr832 [] =
{
2212,
};

extern const char *names833[];
static const uint32 types833 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags833 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype833_0 [] = {0xFF01,807,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype833_1 [] = {0xFF01,829,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype833_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype833_3 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes833 [] = {
g_atype833_0,
g_atype833_1,
g_atype833_2,
g_atype833_3,
};

static const int32 cn_attr833 [] =
{
3789,
3794,
3790,
3792,
};

extern const char *names834[];
static const uint32 types834 [] =
{
SK_BOOL,
};

static const uint16 attr_flags834 [] =
{0,};

static const EIF_TYPE_INDEX g_atype834_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes834 [] = {
g_atype834_0,
};

static const int32 cn_attr834 [] =
{
2212,
};

extern const char *names835[];
static const uint32 types835 [] =
{
SK_BOOL,
};

static const uint16 attr_flags835 [] =
{0,};

static const EIF_TYPE_INDEX g_atype835_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes835 [] = {
g_atype835_0,
};

static const int32 cn_attr835 [] =
{
2212,
};

extern const char *names836[];
static const uint32 types836 [] =
{
SK_BOOL,
};

static const uint16 attr_flags836 [] =
{0,};

static const EIF_TYPE_INDEX g_atype836_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes836 [] = {
g_atype836_0,
};

static const int32 cn_attr836 [] =
{
2212,
};

extern const char *names837[];
static const uint32 types837 [] =
{
SK_BOOL,
};

static const uint16 attr_flags837 [] =
{0,};

static const EIF_TYPE_INDEX g_atype837_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes837 [] = {
g_atype837_0,
};

static const int32 cn_attr837 [] =
{
2212,
};

extern const char *names838[];
static const uint32 types838 [] =
{
SK_BOOL,
};

static const uint16 attr_flags838 [] =
{0,};

static const EIF_TYPE_INDEX g_atype838_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes838 [] = {
g_atype838_0,
};

static const int32 cn_attr838 [] =
{
2212,
};

extern const char *names839[];
static const uint32 types839 [] =
{
SK_BOOL,
};

static const uint16 attr_flags839 [] =
{0,};

static const EIF_TYPE_INDEX g_atype839_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes839 [] = {
g_atype839_0,
};

static const int32 cn_attr839 [] =
{
2212,
};

extern const char *names840[];
static const uint32 types840 [] =
{
SK_BOOL,
};

static const uint16 attr_flags840 [] =
{0,};

static const EIF_TYPE_INDEX g_atype840_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes840 [] = {
g_atype840_0,
};

static const int32 cn_attr840 [] =
{
2212,
};

extern const char *names841[];
static const uint32 types841 [] =
{
SK_REF,
};

static const uint16 attr_flags841 [] =
{0,};

static const EIF_TYPE_INDEX g_atype841_0 [] = {0xFF01,807,203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes841 [] = {
g_atype841_0,
};

static const int32 cn_attr841 [] =
{
2156,
};

extern const char *names842[];
static const uint32 types842 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags842 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype842_0 [] = {0xFF01,807,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype842_1 [] = {0xFF01,817,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype842_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype842_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype842_4 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes842 [] = {
g_atype842_0,
g_atype842_1,
g_atype842_2,
g_atype842_3,
g_atype842_4,
};

static const int32 cn_attr842 [] =
{
3789,
3802,
3790,
3792,
3803,
};

extern const char *names843[];
static const uint32 types843 [] =
{
SK_REF,
SK_REF,
SK_UINT16,
SK_UINT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags843 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype843_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype843_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype843_2 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype843_3 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype843_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype843_5 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes843 [] = {
g_atype843_0,
g_atype843_1,
g_atype843_2,
g_atype843_3,
g_atype843_4,
g_atype843_5,
};

static const int32 cn_attr843 [] =
{
2084,
2112,
2113,
2114,
2082,
2083,
};

extern const char *names844[];
static const uint32 types844 [] =
{
SK_POINTER,
};

static const uint16 attr_flags844 [] =
{0,};

static const EIF_TYPE_INDEX g_atype844_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes844 [] = {
g_atype844_0,
};

static const int32 cn_attr844 [] =
{
4554,
};

extern const char *names845[];
static const uint32 types845 [] =
{
SK_POINTER,
};

static const uint16 attr_flags845 [] =
{0,};

static const EIF_TYPE_INDEX g_atype845_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes845 [] = {
g_atype845_0,
};

static const int32 cn_attr845 [] =
{
4554,
};

extern const char *names846[];
static const uint32 types846 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags846 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype846_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype846_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes846 [] = {
g_atype846_0,
g_atype846_1,
};

static const int32 cn_attr846 [] =
{
3889,
3890,
};

extern const char *names847[];
static const uint32 types847 [] =
{
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags847 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype847_0 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype847_1 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes847 [] = {
g_atype847_0,
g_atype847_1,
};

static const int32 cn_attr847 [] =
{
2212,
2376,
};

extern const char *names848[];
static const uint32 types848 [] =
{
SK_BOOL,
};

static const uint16 attr_flags848 [] =
{0,};

static const EIF_TYPE_INDEX g_atype848_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes848 [] = {
g_atype848_0,
};

static const int32 cn_attr848 [] =
{
2212,
};

extern const char *names849[];
static const uint32 types849 [] =
{
SK_BOOL,
};

static const uint16 attr_flags849 [] =
{0,};

static const EIF_TYPE_INDEX g_atype849_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes849 [] = {
g_atype849_0,
};

static const int32 cn_attr849 [] =
{
2212,
};

extern const char *names850[];
static const uint32 types850 [] =
{
SK_REF,
SK_REF,
SK_UINT32,
SK_UINT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags850 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype850_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype850_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype850_2 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype850_3 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype850_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype850_5 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes850 [] = {
g_atype850_0,
g_atype850_1,
g_atype850_2,
g_atype850_3,
g_atype850_4,
g_atype850_5,
};

static const int32 cn_attr850 [] =
{
2084,
2112,
2113,
2114,
2082,
2083,
};

extern const char *names851[];
static const uint32 types851 [] =
{
SK_POINTER,
};

static const uint16 attr_flags851 [] =
{0,};

static const EIF_TYPE_INDEX g_atype851_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes851 [] = {
g_atype851_0,
};

static const int32 cn_attr851 [] =
{
4554,
};

extern const char *names852[];
static const uint32 types852 [] =
{
SK_POINTER,
};

static const uint16 attr_flags852 [] =
{0,};

static const EIF_TYPE_INDEX g_atype852_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes852 [] = {
g_atype852_0,
};

static const int32 cn_attr852 [] =
{
4554,
};

extern const char *names853[];
static const uint32 types853 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags853 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype853_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype853_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes853 [] = {
g_atype853_0,
g_atype853_1,
};

static const int32 cn_attr853 [] =
{
3889,
3890,
};

extern const char *names854[];
static const uint32 types854 [] =
{
SK_REF,
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT64,
};

static const uint16 attr_flags854 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype854_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype854_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype854_2 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype854_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype854_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype854_5 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes854 [] = {
g_atype854_0,
g_atype854_1,
g_atype854_2,
g_atype854_3,
g_atype854_4,
g_atype854_5,
};

static const int32 cn_attr854 [] =
{
2084,
2112,
2114,
2082,
2083,
2113,
};

extern const char *names855[];
static const uint32 types855 [] =
{
SK_REF,
SK_REF,
SK_INT8,
SK_UINT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags855 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype855_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype855_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype855_2 [] = {188,0xFFFF};
static const EIF_TYPE_INDEX g_atype855_3 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype855_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype855_5 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes855 [] = {
g_atype855_0,
g_atype855_1,
g_atype855_2,
g_atype855_3,
g_atype855_4,
g_atype855_5,
};

static const int32 cn_attr855 [] =
{
2084,
2112,
2113,
2114,
2082,
2083,
};

extern const char *names856[];
static const uint32 types856 [] =
{
SK_POINTER,
};

static const uint16 attr_flags856 [] =
{0,};

static const EIF_TYPE_INDEX g_atype856_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes856 [] = {
g_atype856_0,
};

static const int32 cn_attr856 [] =
{
4554,
};

extern const char *names857[];
static const uint32 types857 [] =
{
SK_POINTER,
};

static const uint16 attr_flags857 [] =
{0,};

static const EIF_TYPE_INDEX g_atype857_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes857 [] = {
g_atype857_0,
};

static const int32 cn_attr857 [] =
{
4554,
};

extern const char *names858[];
static const uint32 types858 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags858 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype858_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype858_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes858 [] = {
g_atype858_0,
g_atype858_1,
};

static const int32 cn_attr858 [] =
{
3889,
3890,
};

extern const char *names859[];
static const uint32 types859 [] =
{
SK_REF,
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_REAL32,
};

static const uint16 attr_flags859 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype859_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype859_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype859_2 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype859_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype859_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype859_5 [] = {191,0xFFFF};

static const EIF_TYPE_INDEX *gtypes859 [] = {
g_atype859_0,
g_atype859_1,
g_atype859_2,
g_atype859_3,
g_atype859_4,
g_atype859_5,
};

static const int32 cn_attr859 [] =
{
2084,
2112,
2114,
2082,
2083,
2113,
};

extern const char *names860[];
static const uint32 types860 [] =
{
SK_REF,
SK_REF,
SK_CHAR32,
SK_UINT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags860 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype860_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype860_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype860_2 [] = {194,0xFFFF};
static const EIF_TYPE_INDEX g_atype860_3 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype860_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype860_5 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes860 [] = {
g_atype860_0,
g_atype860_1,
g_atype860_2,
g_atype860_3,
g_atype860_4,
g_atype860_5,
};

static const int32 cn_attr860 [] =
{
2084,
2112,
2113,
2114,
2082,
2083,
};

extern const char *names861[];
static const uint32 types861 [] =
{
SK_POINTER,
};

static const uint16 attr_flags861 [] =
{0,};

static const EIF_TYPE_INDEX g_atype861_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes861 [] = {
g_atype861_0,
};

static const int32 cn_attr861 [] =
{
4554,
};

extern const char *names862[];
static const uint32 types862 [] =
{
SK_POINTER,
};

static const uint16 attr_flags862 [] =
{0,};

static const EIF_TYPE_INDEX g_atype862_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes862 [] = {
g_atype862_0,
};

static const int32 cn_attr862 [] =
{
4554,
};

extern const char *names863[];
static const uint32 types863 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags863 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype863_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype863_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes863 [] = {
g_atype863_0,
g_atype863_1,
};

static const int32 cn_attr863 [] =
{
3889,
3890,
};

extern const char *names864[];
static const uint32 types864 [] =
{
SK_REF,
SK_REF,
SK_UINT8,
SK_UINT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags864 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype864_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype864_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype864_2 [] = {209,0xFFFF};
static const EIF_TYPE_INDEX g_atype864_3 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype864_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype864_5 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes864 [] = {
g_atype864_0,
g_atype864_1,
g_atype864_2,
g_atype864_3,
g_atype864_4,
g_atype864_5,
};

static const int32 cn_attr864 [] =
{
2084,
2112,
2113,
2114,
2082,
2083,
};

extern const char *names865[];
static const uint32 types865 [] =
{
SK_POINTER,
};

static const uint16 attr_flags865 [] =
{0,};

static const EIF_TYPE_INDEX g_atype865_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes865 [] = {
g_atype865_0,
};

static const int32 cn_attr865 [] =
{
4554,
};

extern const char *names866[];
static const uint32 types866 [] =
{
SK_POINTER,
};

static const uint16 attr_flags866 [] =
{0,};

static const EIF_TYPE_INDEX g_atype866_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes866 [] = {
g_atype866_0,
};

static const int32 cn_attr866 [] =
{
4554,
};

extern const char *names867[];
static const uint32 types867 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags867 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype867_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype867_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes867 [] = {
g_atype867_0,
g_atype867_1,
};

static const int32 cn_attr867 [] =
{
3889,
3890,
};

extern const char *names871[];
static const uint32 types871 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags871 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype871_0 [] = {0xFF01,869,224,0xFFFF};
static const EIF_TYPE_INDEX g_atype871_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype871_2 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype871_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype871_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype871_5 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype871_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes871 [] = {
g_atype871_0,
g_atype871_1,
g_atype871_2,
g_atype871_3,
g_atype871_4,
g_atype871_5,
g_atype871_6,
};

static const int32 cn_attr871 [] =
{
3782,
3783,
3779,
3784,
3785,
3786,
3787,
};

extern const char *names876[];
static const uint32 types876 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags876 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype876_0 [] = {0xFF01,867,224,0xFFFF};
static const EIF_TYPE_INDEX g_atype876_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype876_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes876 [] = {
g_atype876_0,
g_atype876_1,
g_atype876_2,
};

static const int32 cn_attr876 [] =
{
3789,
3790,
3792,
};

extern const char *names877[];
static const uint32 types877 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags877 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype877_0 [] = {0xFF01,867,224,0xFFFF};
static const EIF_TYPE_INDEX g_atype877_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype877_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes877 [] = {
g_atype877_0,
g_atype877_1,
g_atype877_2,
};

static const int32 cn_attr877 [] =
{
3789,
3790,
3792,
};

extern const char *names878[];
static const uint32 types878 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags878 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype878_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype878_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes878 [] = {
g_atype878_0,
g_atype878_1,
};

static const int32 cn_attr878 [] =
{
3889,
3890,
};

extern const char *names879[];
static const uint32 types879 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags879 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype879_0 [] = {0xFF01,867,224,0xFFFF};
static const EIF_TYPE_INDEX g_atype879_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype879_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype879_3 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes879 [] = {
g_atype879_0,
g_atype879_1,
g_atype879_2,
g_atype879_3,
};

static const int32 cn_attr879 [] =
{
2156,
2212,
2664,
2665,
};

extern const char *names880[];
static const uint32 types880 [] =
{
SK_BOOL,
};

static const uint16 attr_flags880 [] =
{0,};

static const EIF_TYPE_INDEX g_atype880_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes880 [] = {
g_atype880_0,
};

static const int32 cn_attr880 [] =
{
2212,
};

extern const char *names881[];
static const uint32 types881 [] =
{
SK_BOOL,
};

static const uint16 attr_flags881 [] =
{0,};

static const EIF_TYPE_INDEX g_atype881_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes881 [] = {
g_atype881_0,
};

static const int32 cn_attr881 [] =
{
2212,
};

extern const char *names882[];
static const uint32 types882 [] =
{
SK_BOOL,
};

static const uint16 attr_flags882 [] =
{0,};

static const EIF_TYPE_INDEX g_atype882_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes882 [] = {
g_atype882_0,
};

static const int32 cn_attr882 [] =
{
2212,
};

extern const char *names883[];
static const uint32 types883 [] =
{
SK_BOOL,
};

static const uint16 attr_flags883 [] =
{0,};

static const EIF_TYPE_INDEX g_atype883_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes883 [] = {
g_atype883_0,
};

static const int32 cn_attr883 [] =
{
2212,
};

extern const char *names884[];
static const uint32 types884 [] =
{
SK_BOOL,
};

static const uint16 attr_flags884 [] =
{0,};

static const EIF_TYPE_INDEX g_atype884_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes884 [] = {
g_atype884_0,
};

static const int32 cn_attr884 [] =
{
2212,
};

extern const char *names885[];
static const uint32 types885 [] =
{
SK_BOOL,
};

static const uint16 attr_flags885 [] =
{0,};

static const EIF_TYPE_INDEX g_atype885_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes885 [] = {
g_atype885_0,
};

static const int32 cn_attr885 [] =
{
2212,
};

extern const char *names886[];
static const uint32 types886 [] =
{
SK_BOOL,
};

static const uint16 attr_flags886 [] =
{0,};

static const EIF_TYPE_INDEX g_atype886_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes886 [] = {
g_atype886_0,
};

static const int32 cn_attr886 [] =
{
2212,
};

extern const char *names887[];
static const uint32 types887 [] =
{
SK_BOOL,
};

static const uint16 attr_flags887 [] =
{0,};

static const EIF_TYPE_INDEX g_atype887_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes887 [] = {
g_atype887_0,
};

static const int32 cn_attr887 [] =
{
2212,
};

extern const char *names888[];
static const uint32 types888 [] =
{
SK_BOOL,
};

static const uint16 attr_flags888 [] =
{0,};

static const EIF_TYPE_INDEX g_atype888_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes888 [] = {
g_atype888_0,
};

static const int32 cn_attr888 [] =
{
2212,
};

extern const char *names889[];
static const uint32 types889 [] =
{
SK_BOOL,
};

static const uint16 attr_flags889 [] =
{0,};

static const EIF_TYPE_INDEX g_atype889_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes889 [] = {
g_atype889_0,
};

static const int32 cn_attr889 [] =
{
2212,
};

extern const char *names890[];
static const uint32 types890 [] =
{
SK_BOOL,
};

static const uint16 attr_flags890 [] =
{0,};

static const EIF_TYPE_INDEX g_atype890_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes890 [] = {
g_atype890_0,
};

static const int32 cn_attr890 [] =
{
2212,
};

extern const char *names891[];
static const uint32 types891 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags891 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype891_0 [] = {0xFF01,867,224,0xFFFF};
static const EIF_TYPE_INDEX g_atype891_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype891_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes891 [] = {
g_atype891_0,
g_atype891_1,
g_atype891_2,
};

static const int32 cn_attr891 [] =
{
2156,
2212,
3094,
};

extern const char *names892[];
static const uint32 types892 [] =
{
SK_BOOL,
};

static const uint16 attr_flags892 [] =
{0,};

static const EIF_TYPE_INDEX g_atype892_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes892 [] = {
g_atype892_0,
};

static const int32 cn_attr892 [] =
{
2212,
};

extern const char *names893[];
static const uint32 types893 [] =
{
SK_BOOL,
};

static const uint16 attr_flags893 [] =
{0,};

static const EIF_TYPE_INDEX g_atype893_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes893 [] = {
g_atype893_0,
};

static const int32 cn_attr893 [] =
{
2212,
};

extern const char *names894[];
static const uint32 types894 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags894 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype894_0 [] = {0xFF01,867,224,0xFFFF};
static const EIF_TYPE_INDEX g_atype894_1 [] = {0xFF01,890,224,0xFFFF};
static const EIF_TYPE_INDEX g_atype894_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype894_3 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes894 [] = {
g_atype894_0,
g_atype894_1,
g_atype894_2,
g_atype894_3,
};

static const int32 cn_attr894 [] =
{
3789,
3794,
3790,
3792,
};

extern const char *names895[];
static const uint32 types895 [] =
{
SK_BOOL,
};

static const uint16 attr_flags895 [] =
{0,};

static const EIF_TYPE_INDEX g_atype895_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes895 [] = {
g_atype895_0,
};

static const int32 cn_attr895 [] =
{
2212,
};

extern const char *names896[];
static const uint32 types896 [] =
{
SK_BOOL,
};

static const uint16 attr_flags896 [] =
{0,};

static const EIF_TYPE_INDEX g_atype896_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes896 [] = {
g_atype896_0,
};

static const int32 cn_attr896 [] =
{
2212,
};

extern const char *names897[];
static const uint32 types897 [] =
{
SK_BOOL,
};

static const uint16 attr_flags897 [] =
{0,};

static const EIF_TYPE_INDEX g_atype897_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes897 [] = {
g_atype897_0,
};

static const int32 cn_attr897 [] =
{
2212,
};

extern const char *names898[];
static const uint32 types898 [] =
{
SK_BOOL,
};

static const uint16 attr_flags898 [] =
{0,};

static const EIF_TYPE_INDEX g_atype898_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes898 [] = {
g_atype898_0,
};

static const int32 cn_attr898 [] =
{
2212,
};

extern const char *names899[];
static const uint32 types899 [] =
{
SK_BOOL,
};

static const uint16 attr_flags899 [] =
{0,};

static const EIF_TYPE_INDEX g_atype899_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes899 [] = {
g_atype899_0,
};

static const int32 cn_attr899 [] =
{
2212,
};

extern const char *names900[];
static const uint32 types900 [] =
{
SK_BOOL,
};

static const uint16 attr_flags900 [] =
{0,};

static const EIF_TYPE_INDEX g_atype900_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes900 [] = {
g_atype900_0,
};

static const int32 cn_attr900 [] =
{
2212,
};

extern const char *names901[];
static const uint32 types901 [] =
{
SK_BOOL,
};

static const uint16 attr_flags901 [] =
{0,};

static const EIF_TYPE_INDEX g_atype901_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes901 [] = {
g_atype901_0,
};

static const int32 cn_attr901 [] =
{
2212,
};

extern const char *names902[];
static const uint32 types902 [] =
{
SK_REF,
};

static const uint16 attr_flags902 [] =
{0,};

static const EIF_TYPE_INDEX g_atype902_0 [] = {0xFF01,867,224,0xFFFF};

static const EIF_TYPE_INDEX *gtypes902 [] = {
g_atype902_0,
};

static const int32 cn_attr902 [] =
{
2156,
};

extern const char *names903[];
static const uint32 types903 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags903 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype903_0 [] = {0xFF01,867,224,0xFFFF};
static const EIF_TYPE_INDEX g_atype903_1 [] = {0xFF01,878,224,0xFFFF};
static const EIF_TYPE_INDEX g_atype903_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype903_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype903_4 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes903 [] = {
g_atype903_0,
g_atype903_1,
g_atype903_2,
g_atype903_3,
g_atype903_4,
};

static const int32 cn_attr903 [] =
{
3789,
3802,
3790,
3792,
3803,
};

extern const char *names904[];
static const uint32 types904 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_POINTER,
};

static const uint16 attr_flags904 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype904_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype904_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype904_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype904_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype904_4 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes904 [] = {
g_atype904_0,
g_atype904_1,
g_atype904_2,
g_atype904_3,
g_atype904_4,
};

static const int32 cn_attr904 [] =
{
2084,
2105,
2082,
2083,
2106,
};

extern const char *names905[];
static const uint32 types905 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags905 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype905_0 [] = {906,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype905_1 [] = {906,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype905_2 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype905_3 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype905_4 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype905_5 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes905 [] = {
g_atype905_0,
g_atype905_1,
g_atype905_2,
g_atype905_3,
g_atype905_4,
g_atype905_5,
};

static const int32 cn_attr905 [] =
{
2766,
2770,
2212,
2774,
2775,
2776,
};

extern const char *names906[];
static const uint32 types906 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags906 [] =
{0,0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype906_0 [] = {0xFF01,908,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype906_1 [] = {906,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype906_2 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype906_3 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype906_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype906_5 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype906_6 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype906_7 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes906 [] = {
g_atype906_0,
g_atype906_1,
g_atype906_2,
g_atype906_3,
g_atype906_4,
g_atype906_5,
g_atype906_6,
g_atype906_7,
};

static const int32 cn_attr906 [] =
{
3782,
3788,
3783,
3779,
3784,
3785,
3786,
3787,
};

extern const char *names907[];
static const uint32 types907 [] =
{
SK_REF,
SK_BOOL,
};

static const uint16 attr_flags907 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype907_0 [] = {906,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype907_1 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes907 [] = {
g_atype907_0,
g_atype907_1,
};

static const int32 cn_attr907 [] =
{
1722,
1718,
};

extern const char *names908[];
static const uint32 types908 [] =
{
SK_BOOL,
};

static const uint16 attr_flags908 [] =
{0,};

static const EIF_TYPE_INDEX g_atype908_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes908 [] = {
g_atype908_0,
};

static const int32 cn_attr908 [] =
{
1718,
};

extern const char *names909[];
static const uint32 types909 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags909 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype909_0 [] = {906,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype909_1 [] = {906,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype909_2 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype909_3 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype909_4 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype909_5 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes909 [] = {
g_atype909_0,
g_atype909_1,
g_atype909_2,
g_atype909_3,
g_atype909_4,
g_atype909_5,
};

static const int32 cn_attr909 [] =
{
2766,
2770,
2212,
2774,
2775,
2776,
};

extern const char *names910[];
static const uint32 types910 [] =
{
SK_REF,
SK_BOOL,
SK_BOOL,
};

static const uint16 attr_flags910 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype910_0 [] = {906,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype910_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype910_2 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes910 [] = {
g_atype910_0,
g_atype910_1,
g_atype910_2,
};

static const int32 cn_attr910 [] =
{
2150,
2151,
2152,
};

extern const char *names911[];
static const uint32 types911 [] =
{
SK_BOOL,
};

static const uint16 attr_flags911 [] =
{0,};

static const EIF_TYPE_INDEX g_atype911_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes911 [] = {
g_atype911_0,
};

static const int32 cn_attr911 [] =
{
2212,
};

extern const char *names912[];
static const uint32 types912 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags912 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype912_0 [] = {0xFF01,807,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype912_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype912_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes912 [] = {
g_atype912_0,
g_atype912_1,
g_atype912_2,
};

static const int32 cn_attr912 [] =
{
2156,
2212,
3094,
};

extern const char *names913[];
static const uint32 types913 [] =
{
SK_BOOL,
};

static const uint16 attr_flags913 [] =
{0,};

static const EIF_TYPE_INDEX g_atype913_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes913 [] = {
g_atype913_0,
};

static const int32 cn_attr913 [] =
{
2212,
};

extern const char *names914[];
static const uint32 types914 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags914 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype914_0 [] = {541,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype914_1 [] = {541,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype914_2 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype914_3 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype914_4 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype914_5 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes914 [] = {
g_atype914_0,
g_atype914_1,
g_atype914_2,
g_atype914_3,
g_atype914_4,
g_atype914_5,
};

static const int32 cn_attr914 [] =
{
2766,
2770,
2212,
2774,
2775,
2776,
};

extern const char *names915[];
static const uint32 types915 [] =
{
SK_BOOL,
};

static const uint16 attr_flags915 [] =
{0,};

static const EIF_TYPE_INDEX g_atype915_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes915 [] = {
g_atype915_0,
};

static const int32 cn_attr915 [] =
{
2212,
};

extern const char *names916[];
static const uint32 types916 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags916 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype916_0 [] = {0xFF01,444,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype916_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype916_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes916 [] = {
g_atype916_0,
g_atype916_1,
g_atype916_2,
};

static const int32 cn_attr916 [] =
{
2156,
2212,
3094,
};

extern const char *names917[];
static const uint32 types917 [] =
{
SK_BOOL,
};

static const uint16 attr_flags917 [] =
{0,};

static const EIF_TYPE_INDEX g_atype917_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes917 [] = {
g_atype917_0,
};

static const int32 cn_attr917 [] =
{
2212,
};

extern const char *names918[];
static const uint32 types918 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_UINT32,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_POINTER,
};

static const uint16 attr_flags918 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype918_0 [] = {0xFF01,716,212,0xFFFF};
static const EIF_TYPE_INDEX g_atype918_1 [] = {0xFF01,553,227,0xFFFF};
static const EIF_TYPE_INDEX g_atype918_2 [] = {0xFF01,444,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype918_3 [] = {0xFF01,807,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype918_4 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype918_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype918_6 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype918_7 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype918_8 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype918_9 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype918_10 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype918_11 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype918_12 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype918_13 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype918_14 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype918_15 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype918_16 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes918 [] = {
g_atype918_0,
g_atype918_1,
g_atype918_2,
g_atype918_3,
g_atype918_4,
g_atype918_5,
g_atype918_6,
g_atype918_7,
g_atype918_8,
g_atype918_9,
g_atype918_10,
g_atype918_11,
g_atype918_12,
g_atype918_13,
g_atype918_14,
g_atype918_15,
g_atype918_16,
};

static const int32 cn_attr918 [] =
{
3009,
3010,
3011,
3012,
2212,
3008,
3014,
2972,
3024,
2981,
3013,
3015,
3018,
3019,
3023,
3059,
3025,
};

extern const char *names919[];
static const uint32 types919 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags919 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype919_0 [] = {0xFF01,917,212,227,0xFFFF};
static const EIF_TYPE_INDEX g_atype919_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype919_2 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype919_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype919_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype919_5 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype919_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes919 [] = {
g_atype919_0,
g_atype919_1,
g_atype919_2,
g_atype919_3,
g_atype919_4,
g_atype919_5,
g_atype919_6,
};

static const int32 cn_attr919 [] =
{
3782,
3783,
3779,
3784,
3785,
3786,
3787,
};

extern const char *names921[];
static const uint32 types921 [] =
{
SK_BOOL,
};

static const uint16 attr_flags921 [] =
{0,};

static const EIF_TYPE_INDEX g_atype921_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes921 [] = {
g_atype921_0,
};

static const int32 cn_attr921 [] =
{
2212,
};

extern const char *names923[];
static const uint32 types923 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags923 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype923_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype923_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype923_2 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype923_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype923_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype923_5 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes923 [] = {
g_atype923_0,
g_atype923_1,
g_atype923_2,
g_atype923_3,
g_atype923_4,
g_atype923_5,
};

static const int32 cn_attr923 [] =
{
2084,
2097,
2099,
2082,
2083,
2098,
};

extern const char *names924[];
static const uint32 types924 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_POINTER,
SK_POINTER,
SK_POINTER,
};

static const uint16 attr_flags924 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype924_0 [] = {0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype924_1 [] = {0xFFF9,0,186,0xFFFF};
static const EIF_TYPE_INDEX g_atype924_2 [] = {455,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype924_3 [] = {455,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype924_4 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype924_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype924_6 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype924_7 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype924_8 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype924_9 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype924_10 [] = {227,0xFFFF};
static const EIF_TYPE_INDEX g_atype924_11 [] = {227,0xFFFF};
static const EIF_TYPE_INDEX g_atype924_12 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes924 [] = {
g_atype924_0,
g_atype924_1,
g_atype924_2,
g_atype924_3,
g_atype924_4,
g_atype924_5,
g_atype924_6,
g_atype924_7,
g_atype924_8,
g_atype924_9,
g_atype924_10,
g_atype924_11,
g_atype924_12,
};

static const int32 cn_attr924 [] =
{
4579,
4594,
4598,
4606,
4587,
4601,
4615,
4588,
4600,
4602,
4596,
4597,
4599,
};

extern const char *names925[];
static const uint32 types925 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags925 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype925_0 [] = {0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype925_1 [] = {0xFF01,324,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype925_2 [] = {0xFF01,324,0xFF01,229,0xFFFF};
static const EIF_TYPE_INDEX g_atype925_3 [] = {0xFF01,444,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype925_4 [] = {0xFF01,807,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype925_5 [] = {0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype925_6 [] = {229,0xFFFF};
static const EIF_TYPE_INDEX g_atype925_7 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype925_8 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype925_9 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype925_10 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype925_11 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype925_12 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype925_13 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype925_14 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype925_15 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype925_16 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype925_17 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes925 [] = {
g_atype925_0,
g_atype925_1,
g_atype925_2,
g_atype925_3,
g_atype925_4,
g_atype925_5,
g_atype925_6,
g_atype925_7,
g_atype925_8,
g_atype925_9,
g_atype925_10,
g_atype925_11,
g_atype925_12,
g_atype925_13,
g_atype925_14,
g_atype925_15,
g_atype925_16,
g_atype925_17,
};

static const int32 cn_attr925 [] =
{
2972,
3009,
3010,
3011,
3012,
3024,
3025,
2212,
3008,
3014,
3062,
2981,
3013,
3015,
3018,
3019,
3023,
3059,
};

extern const char *names926[];
static const uint32 types926 [] =
{
SK_UINT64,
};

static const uint16 attr_flags926 [] =
{0,};

static const EIF_TYPE_INDEX g_atype926_0 [] = {224,0xFFFF};

static const EIF_TYPE_INDEX *gtypes926 [] = {
g_atype926_0,
};

static const int32 cn_attr926 [] =
{
1718,
};

extern const char *names927[];
static const uint32 types927 [] =
{
SK_REF,
SK_UINT16,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags927 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype927_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype927_1 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype927_2 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype927_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype927_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype927_5 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes927 [] = {
g_atype927_0,
g_atype927_1,
g_atype927_2,
g_atype927_3,
g_atype927_4,
g_atype927_5,
};

static const int32 cn_attr927 [] =
{
2084,
2097,
2099,
2082,
2083,
2098,
};

extern const char *names928[];
static const uint32 types928 [] =
{
SK_REF,
SK_UINT32,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags928 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype928_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype928_1 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype928_2 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype928_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype928_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype928_5 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes928 [] = {
g_atype928_0,
g_atype928_1,
g_atype928_2,
g_atype928_3,
g_atype928_4,
g_atype928_5,
};

static const int32 cn_attr928 [] =
{
2084,
2097,
2099,
2082,
2083,
2098,
};

extern const char *names929[];
static const uint32 types929 [] =
{
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_UINT64,
};

static const uint16 attr_flags929 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype929_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype929_1 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype929_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype929_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype929_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype929_5 [] = {224,0xFFFF};

static const EIF_TYPE_INDEX *gtypes929 [] = {
g_atype929_0,
g_atype929_1,
g_atype929_2,
g_atype929_3,
g_atype929_4,
g_atype929_5,
};

static const int32 cn_attr929 [] =
{
2084,
2099,
2082,
2083,
2098,
2097,
};

extern const char *names930[];
static const uint32 types930 [] =
{
SK_REF,
SK_INT8,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags930 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype930_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype930_1 [] = {188,0xFFFF};
static const EIF_TYPE_INDEX g_atype930_2 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype930_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype930_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype930_5 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes930 [] = {
g_atype930_0,
g_atype930_1,
g_atype930_2,
g_atype930_3,
g_atype930_4,
g_atype930_5,
};

static const int32 cn_attr930 [] =
{
2084,
2097,
2099,
2082,
2083,
2098,
};

extern const char *names931[];
static const uint32 types931 [] =
{
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_REAL32,
};

static const uint16 attr_flags931 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype931_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype931_1 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype931_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype931_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype931_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype931_5 [] = {191,0xFFFF};

static const EIF_TYPE_INDEX *gtypes931 [] = {
g_atype931_0,
g_atype931_1,
g_atype931_2,
g_atype931_3,
g_atype931_4,
g_atype931_5,
};

static const int32 cn_attr931 [] =
{
2084,
2099,
2082,
2083,
2098,
2097,
};

extern const char *names932[];
static const uint32 types932 [] =
{
SK_REF,
SK_CHAR32,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags932 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype932_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype932_1 [] = {194,0xFFFF};
static const EIF_TYPE_INDEX g_atype932_2 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype932_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype932_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype932_5 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes932 [] = {
g_atype932_0,
g_atype932_1,
g_atype932_2,
g_atype932_3,
g_atype932_4,
g_atype932_5,
};

static const int32 cn_attr932 [] =
{
2084,
2097,
2099,
2082,
2083,
2098,
};

extern const char *names933[];
static const uint32 types933 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags933 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype933_0 [] = {0xFF01,324,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype933_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype933_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype933_3 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes933 [] = {
g_atype933_0,
g_atype933_1,
g_atype933_2,
g_atype933_3,
};

static const int32 cn_attr933 [] =
{
2962,
2212,
2963,
2968,
};

extern const char *names934[];
static const uint32 types934 [] =
{
SK_REF,
SK_REF,
SK_REF,
};

static const uint16 attr_flags934 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype934_0 [] = {0xFF01,324,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype934_1 [] = {0xFFF5,1,0xFF01,324,0xFFF8,1,2558,0xFFFF};
static const EIF_TYPE_INDEX g_atype934_2 [] = {0xFFF5,1,0xFF01,932,0xFFF8,1,2968,0xFFFF};

static const EIF_TYPE_INDEX *gtypes934 [] = {
g_atype934_0,
g_atype934_1,
g_atype934_2,
};

static const int32 cn_attr934 [] =
{
3741,
3742,
3743,
};

extern const char *names935[];
static const uint32 types935 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags935 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype935_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype935_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype935_2 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype935_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype935_4 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes935 [] = {
g_atype935_0,
g_atype935_1,
g_atype935_2,
g_atype935_3,
g_atype935_4,
};

static const int32 cn_attr935 [] =
{
2084,
2105,
2106,
2082,
2083,
};

extern const char *names936[];
static const uint32 types936 [] =
{
SK_REF,
SK_REF,
SK_CHAR32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags936 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype936_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype936_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype936_2 [] = {194,0xFFFF};
static const EIF_TYPE_INDEX g_atype936_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype936_4 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes936 [] = {
g_atype936_0,
g_atype936_1,
g_atype936_2,
g_atype936_3,
g_atype936_4,
};

static const int32 cn_attr936 [] =
{
2084,
2105,
2106,
2082,
2083,
};

extern const char *names937[];
static const uint32 types937 [] =
{
SK_REF,
SK_REF,
SK_INT8,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags937 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype937_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype937_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype937_2 [] = {188,0xFFFF};
static const EIF_TYPE_INDEX g_atype937_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype937_4 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes937 [] = {
g_atype937_0,
g_atype937_1,
g_atype937_2,
g_atype937_3,
g_atype937_4,
};

static const int32 cn_attr937 [] =
{
2084,
2105,
2106,
2082,
2083,
};

extern const char *names938[];
static const uint32 types938 [] =
{
SK_REF,
SK_REF,
SK_INT16,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags938 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype938_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype938_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype938_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype938_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype938_4 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes938 [] = {
g_atype938_0,
g_atype938_1,
g_atype938_2,
g_atype938_3,
g_atype938_4,
};

static const int32 cn_attr938 [] =
{
2084,
2105,
2106,
2082,
2083,
};

extern const char *names939[];
static const uint32 types939 [] =
{
SK_REF,
SK_REF,
SK_UINT8,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags939 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype939_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype939_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype939_2 [] = {209,0xFFFF};
static const EIF_TYPE_INDEX g_atype939_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype939_4 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes939 [] = {
g_atype939_0,
g_atype939_1,
g_atype939_2,
g_atype939_3,
g_atype939_4,
};

static const int32 cn_attr939 [] =
{
2084,
2105,
2106,
2082,
2083,
};

extern const char *names940[];
static const uint32 types940 [] =
{
SK_REF,
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags940 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype940_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype940_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype940_2 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype940_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype940_4 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes940 [] = {
g_atype940_0,
g_atype940_1,
g_atype940_2,
g_atype940_3,
g_atype940_4,
};

static const int32 cn_attr940 [] =
{
2084,
2105,
2106,
2082,
2083,
};

extern const char *names941[];
static const uint32 types941 [] =
{
SK_REF,
SK_REF,
SK_UINT16,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags941 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype941_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype941_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype941_2 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype941_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype941_4 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes941 [] = {
g_atype941_0,
g_atype941_1,
g_atype941_2,
g_atype941_3,
g_atype941_4,
};

static const int32 cn_attr941 [] =
{
2084,
2105,
2106,
2082,
2083,
};

extern const char *names942[];
static const uint32 types942 [] =
{
SK_CHAR32,
};

static const uint16 attr_flags942 [] =
{0,};

static const EIF_TYPE_INDEX g_atype942_0 [] = {194,0xFFFF};

static const EIF_TYPE_INDEX *gtypes942 [] = {
g_atype942_0,
};

static const int32 cn_attr942 [] =
{
1718,
};

extern const char *names943[];
static const uint32 types943 [] =
{
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_REAL64,
};

static const uint16 attr_flags943 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype943_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype943_1 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype943_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype943_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype943_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype943_5 [] = {206,0xFFFF};

static const EIF_TYPE_INDEX *gtypes943 [] = {
g_atype943_0,
g_atype943_1,
g_atype943_2,
g_atype943_3,
g_atype943_4,
g_atype943_5,
};

static const int32 cn_attr943 [] =
{
2084,
2099,
2082,
2083,
2098,
2097,
};

extern const char *names944[];
static const uint32 types944 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags944 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype944_0 [] = {0xFF01,324,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype944_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype944_2 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes944 [] = {
g_atype944_0,
g_atype944_1,
g_atype944_2,
};

static const int32 cn_attr944 [] =
{
2156,
2212,
3094,
};

extern const char *names945[];
static const uint32 types945 [] =
{
SK_BOOL,
};

static const uint16 attr_flags945 [] =
{0,};

static const EIF_TYPE_INDEX g_atype945_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes945 [] = {
g_atype945_0,
};

static const int32 cn_attr945 [] =
{
2212,
};

extern const char *names946[];
static const uint32 types946 [] =
{
SK_REF,
SK_UINT8,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags946 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype946_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype946_1 [] = {209,0xFFFF};
static const EIF_TYPE_INDEX g_atype946_2 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype946_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype946_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype946_5 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes946 [] = {
g_atype946_0,
g_atype946_1,
g_atype946_2,
g_atype946_3,
g_atype946_4,
g_atype946_5,
};

static const int32 cn_attr946 [] =
{
2084,
2097,
2099,
2082,
2083,
2098,
};

extern const char *names947[];
static const uint32 types947 [] =
{
SK_REF,
SK_INT16,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags947 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype947_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype947_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype947_2 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype947_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype947_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype947_5 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes947 [] = {
g_atype947_0,
g_atype947_1,
g_atype947_2,
g_atype947_3,
g_atype947_4,
g_atype947_5,
};

static const int32 cn_attr947 [] =
{
2084,
2097,
2099,
2082,
2083,
2098,
};

extern const char *names948[];
static const uint32 types948 [] =
{
SK_REF,
SK_CHAR8,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags948 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype948_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype948_1 [] = {197,0xFFFF};
static const EIF_TYPE_INDEX g_atype948_2 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype948_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype948_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype948_5 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes948 [] = {
g_atype948_0,
g_atype948_1,
g_atype948_2,
g_atype948_3,
g_atype948_4,
g_atype948_5,
};

static const int32 cn_attr948 [] =
{
2084,
2097,
2099,
2082,
2083,
2098,
};

extern const char *names949[];
static const uint32 types949 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags949 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype949_0 [] = {0xFF01,444,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype949_1 [] = {0xFF01,324,0xFF01,229,0xFFFF};
static const EIF_TYPE_INDEX g_atype949_2 [] = {0xFF01,444,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype949_3 [] = {0xFF01,807,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype949_4 [] = {229,0xFFFF};
static const EIF_TYPE_INDEX g_atype949_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype949_6 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype949_7 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype949_8 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype949_9 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype949_10 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype949_11 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype949_12 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype949_13 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype949_14 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype949_15 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype949_16 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype949_17 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes949 [] = {
g_atype949_0,
g_atype949_1,
g_atype949_2,
g_atype949_3,
g_atype949_4,
g_atype949_5,
g_atype949_6,
g_atype949_7,
g_atype949_8,
g_atype949_9,
g_atype949_10,
g_atype949_11,
g_atype949_12,
g_atype949_13,
g_atype949_14,
g_atype949_15,
g_atype949_16,
g_atype949_17,
};

static const int32 cn_attr949 [] =
{
3009,
3010,
3011,
3012,
3025,
2212,
3008,
3014,
3062,
2972,
2981,
3013,
3015,
3018,
3019,
3023,
3024,
3059,
};

extern const char *names950[];
static const uint32 types950 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags950 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype950_0 [] = {0xFF01,951,218,0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX g_atype950_1 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype950_2 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype950_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype950_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype950_5 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype950_6 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes950 [] = {
g_atype950_0,
g_atype950_1,
g_atype950_2,
g_atype950_3,
g_atype950_4,
g_atype950_5,
g_atype950_6,
};

static const int32 cn_attr950 [] =
{
3782,
3783,
3779,
3784,
3785,
3786,
3787,
};

extern const char *names952[];
static const uint32 types952 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags952 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype952_0 [] = {0xFF01,444,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype952_1 [] = {0xFF01,324,0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX g_atype952_2 [] = {0xFF01,444,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype952_3 [] = {0xFF01,807,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype952_4 [] = {0xFF02,0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX g_atype952_5 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype952_6 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype952_7 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype952_8 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype952_9 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype952_10 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype952_11 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype952_12 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype952_13 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype952_14 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype952_15 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype952_16 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes952 [] = {
g_atype952_0,
g_atype952_1,
g_atype952_2,
g_atype952_3,
g_atype952_4,
g_atype952_5,
g_atype952_6,
g_atype952_7,
g_atype952_8,
g_atype952_9,
g_atype952_10,
g_atype952_11,
g_atype952_12,
g_atype952_13,
g_atype952_14,
g_atype952_15,
g_atype952_16,
};

static const int32 cn_attr952 [] =
{
3009,
3010,
3011,
3012,
3025,
2212,
3008,
3014,
2972,
2981,
3013,
3015,
3018,
3019,
3023,
3024,
3059,
};

extern const char *names953[];
static const uint32 types953 [] =
{
SK_BOOL,
};

static const uint16 attr_flags953 [] =
{0,};

static const EIF_TYPE_INDEX g_atype953_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes953 [] = {
g_atype953_0,
};

static const int32 cn_attr953 [] =
{
2212,
};

extern const char *names955[];
static const uint32 types955 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags955 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype955_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype955_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes955 [] = {
g_atype955_0,
g_atype955_1,
};

static const int32 cn_attr955 [] =
{
3889,
3890,
};

extern const char *names956[];
static const uint32 types956 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_UINT64,
};

static const uint16 attr_flags956 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype956_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype956_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype956_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype956_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype956_4 [] = {224,0xFFFF};

static const EIF_TYPE_INDEX *gtypes956 [] = {
g_atype956_0,
g_atype956_1,
g_atype956_2,
g_atype956_3,
g_atype956_4,
};

static const int32 cn_attr956 [] =
{
2084,
2105,
2082,
2083,
2106,
};

extern const char *names957[];
static const uint32 types957 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags957 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype957_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype957_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype957_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype957_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype957_4 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes957 [] = {
g_atype957_0,
g_atype957_1,
g_atype957_2,
g_atype957_3,
g_atype957_4,
};

static const int32 cn_attr957 [] =
{
2084,
2105,
2082,
2083,
2106,
};

extern const char *names958[];
static const uint32 types958 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags958 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype958_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype958_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype958_2 [] = {0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype958_3 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype958_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype958_5 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes958 [] = {
g_atype958_0,
g_atype958_1,
g_atype958_2,
g_atype958_3,
g_atype958_4,
g_atype958_5,
};

static const int32 cn_attr958 [] =
{
2084,
2112,
2113,
2114,
2082,
2083,
};

extern const char *names959[];
static const uint32 types959 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_REAL64,
};

static const uint16 attr_flags959 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype959_0 [] = {0xFFF9,2,186,218,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype959_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype959_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype959_3 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype959_4 [] = {206,0xFFFF};

static const EIF_TYPE_INDEX *gtypes959 [] = {
g_atype959_0,
g_atype959_1,
g_atype959_2,
g_atype959_3,
g_atype959_4,
};

static const int32 cn_attr959 [] =
{
2084,
2105,
2082,
2083,
2106,
};

const struct cnode egc_fsystem_init[] = {
{
	(long) 0,
	(long) 0,
	"ANY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TEST_CASE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SYSTEM_STRING_FACTORY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SHARED_EXECUTION_ENVIRONMENT",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ISE_SCOOP_RUNTIME",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SED_META_MODEL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"RT_DBG_EXECUTION_PARAMETERS",
	names7,
	types7,
	attr_flags7,
	gtypes7,
	(uint16) 0,
	cn_attr7,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"IDENTIFIED_CONTROLLER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"RT_DEBUGGER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ASCII",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 14,
	(long) 14,
	"DECLARATOR",
	names11,
	types11,
	attr_flags11,
	gtypes11,
	(uint16) 0,
	cn_attr11,
	112,
	14L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 10,
	(long) 10,
	"SED_TYPE_MISMATCH",
	names12,
	types12,
	attr_flags12,
	gtypes12,
	(uint16) 0,
	cn_attr12,
	48,
	4L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"FILE_UTILITIES",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 768,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"FILE_UTILITIES",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 256,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SED_ERROR",
	names15,
	types15,
	attr_flags15,
	gtypes15,
	(uint16) 0,
	cn_attr15,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"CHARACTER_PROPERTY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"STD_FILES",
	names17,
	types17,
	attr_flags17,
	gtypes17,
	(uint16) 0,
	cn_attr17,
	8,
	1L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"OPERATING_ENVIRONMENT",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 0,
	"VERSIONABLE",
	names19,
	types19,
	attr_flags19,
	gtypes19,
	(uint16) 4096,
	cn_attr19,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SED_ERROR_FACTORY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SED_VERSIONS",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ISE_RUNTIME",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"UTF_CONVERTER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 768,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"UTF_CONVERTER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 256,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"PROFILING_SETTING",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TRACING_SETTING",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"UNIX_SIGNALS",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SYSTEM_STRING",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"BASIC_ROUTINES",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 8,
	(long) 8,
	"FORMAT_INTEGER",
	names30,
	types30,
	attr_flags30,
	gtypes30,
	(uint16) 0,
	cn_attr30,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"EXCEP_CONST",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TRACING_HANDLER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"STRING_TRACING_HANDLER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"AGENT_TRACING_HANDLER",
	names34,
	types34,
	attr_flags34,
	gtypes34,
	(uint16) 0,
	cn_attr34,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"IDENTIFIED_ROUTINES",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SED_ABSTRACT_OBJECTS_TABLE",
	names36,
	types36,
	attr_flags36,
	gtypes36,
	(uint16) 4096,
	cn_attr36,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SED_STORABLE_FACILITIES",
	names37,
	types37,
	attr_flags37,
	gtypes37,
	(uint16) 0,
	cn_attr37,
	8,
	1L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"OBJECT_GRAPH_MARKER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SED_READER_WRITER",
	names39,
	types39,
	attr_flags39,
	gtypes39,
	(uint16) 4096,
	cn_attr39,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 8,
	(long) 8,
	"SED_BINARY_READER_WRITER",
	names40,
	types40,
	attr_flags40,
	gtypes40,
	(uint16) 4096,
	cn_attr40,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 8,
	(long) 8,
	"SED_MEMORY_READER_WRITER",
	names41,
	types41,
	attr_flags41,
	gtypes41,
	(uint16) 0,
	cn_attr41,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"REFACTORING_HELPER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"THREAD_ENVIRONMENT",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"RT_EXTENSION_COMMON",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"RT_EXTENSION_GENERAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"RT_EXTENSION",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"RT_DBG_COMMON",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"MEMORY_STRUCTURE",
	names48,
	types48,
	attr_flags48,
	gtypes48,
	(uint16) 4096,
	cn_attr48,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 12,
	(long) 12,
	"OBJECT_GRAPH_TRAVERSABLE",
	names49,
	types49,
	attr_flags49,
	gtypes49,
	(uint16) 4096,
	cn_attr49,
	56,
	6L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 12,
	(long) 12,
	"OBJECT_GRAPH_DEPTH_FIRST_TRAVERSABLE",
	names50,
	types50,
	attr_flags50,
	gtypes50,
	(uint16) 0,
	cn_attr50,
	56,
	6L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 12,
	(long) 12,
	"OBJECT_GRAPH_BREADTH_FIRST_TRAVERSABLE",
	names51,
	types51,
	attr_flags51,
	gtypes51,
	(uint16) 0,
	cn_attr51,
	56,
	6L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"EXCEPTION_MANAGER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ISE_EXCEPTION_MANAGER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"REFLECTOR_HELPER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"INTERNAL_HELPER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"NUMERIC_INFORMATION",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"INTEGER_OVERFLOW_CHECKER",
	names57,
	types57,
	attr_flags57,
	gtypes57,
	(uint16) 0,
	cn_attr57,
	32,
	4L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"STRING_TO_NUMERIC_CONVERTOR",
	names58,
	types58,
	attr_flags58,
	gtypes58,
	(uint16) 4096,
	cn_attr58,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 11,
	(long) 11,
	"HEXADECIMAL_STRING_TO_INTEGER_CONVERTER",
	names59,
	types59,
	attr_flags59,
	gtypes59,
	(uint16) 8192,
	cn_attr59,
	48,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 15,
	(long) 15,
	"STRING_TO_REAL_CONVERTOR",
	names60,
	types60,
	attr_flags60,
	gtypes60,
	(uint16) 8192,
	cn_attr60,
	64,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 10,
	(long) 10,
	"STRING_TO_INTEGER_CONVERTOR",
	names61,
	types61,
	attr_flags61,
	gtypes61,
	(uint16) 8192,
	cn_attr61,
	48,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"EXCEPTION_MANAGER_FACTORY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"EXCEPTIONS",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"STORABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"EXCEPTION",
	names65,
	types65,
	attr_flags65,
	gtypes65,
	(uint16) 0,
	cn_attr65,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"DEVELOPER_EXCEPTION",
	names66,
	types66,
	attr_flags66,
	gtypes66,
	(uint16) 0,
	cn_attr66,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"MACHINE_EXCEPTION",
	names67,
	types67,
	attr_flags67,
	gtypes67,
	(uint16) 4096,
	cn_attr67,
	48,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"HARDWARE_EXCEPTION",
	names68,
	types68,
	attr_flags68,
	gtypes68,
	(uint16) 4096,
	cn_attr68,
	48,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"FLOATING_POINT_FAILURE",
	names69,
	types69,
	attr_flags69,
	gtypes69,
	(uint16) 0,
	cn_attr69,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"OPERATING_SYSTEM_EXCEPTION",
	names70,
	types70,
	attr_flags70,
	gtypes70,
	(uint16) 4096,
	cn_attr70,
	48,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 10,
	(long) 10,
	"COM_FAILURE",
	names71,
	types71,
	attr_flags71,
	gtypes71,
	(uint16) 0,
	cn_attr71,
	64,
	6L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 8,
	(long) 8,
	"OPERATING_SYSTEM_FAILURE",
	names72,
	types72,
	attr_flags72,
	gtypes72,
	(uint16) 0,
	cn_attr72,
	56,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 8,
	(long) 8,
	"OPERATING_SYSTEM_SIGNAL_FAILURE",
	names73,
	types73,
	attr_flags73,
	gtypes73,
	(uint16) 0,
	cn_attr73,
	56,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"OBSOLETE_EXCEPTION",
	names74,
	types74,
	attr_flags74,
	gtypes74,
	(uint16) 4096,
	cn_attr74,
	48,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"RESUMPTION_FAILURE",
	names75,
	types75,
	attr_flags75,
	gtypes75,
	(uint16) 0,
	cn_attr75,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"RESCUE_FAILURE",
	names76,
	types76,
	attr_flags76,
	gtypes76,
	(uint16) 0,
	cn_attr76,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"EXCEPTION_IN_SIGNAL_HANDLER_FAILURE",
	names77,
	types77,
	attr_flags77,
	gtypes77,
	(uint16) 0,
	cn_attr77,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"SYS_EXCEPTION",
	names78,
	types78,
	attr_flags78,
	gtypes78,
	(uint16) 4096,
	cn_attr78,
	48,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 8,
	(long) 8,
	"EIFFEL_RUNTIME_PANIC",
	names79,
	types79,
	attr_flags79,
	gtypes79,
	(uint16) 0,
	cn_attr79,
	56,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"OLD_VIOLATION",
	names80,
	types80,
	attr_flags80,
	gtypes80,
	(uint16) 0,
	cn_attr80,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"EIF_EXCEPTION",
	names81,
	types81,
	attr_flags81,
	gtypes81,
	(uint16) 4096,
	cn_attr81,
	48,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"EIFFEL_RUNTIME_EXCEPTION",
	names82,
	types82,
	attr_flags82,
	gtypes82,
	(uint16) 4096,
	cn_attr82,
	48,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"EXTERNAL_FAILURE",
	names83,
	types83,
	attr_flags83,
	gtypes83,
	(uint16) 0,
	cn_attr83,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 8,
	(long) 8,
	"NO_MORE_MEMORY",
	names84,
	types84,
	attr_flags84,
	gtypes84,
	(uint16) 0,
	cn_attr84,
	56,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"DATA_EXCEPTION",
	names85,
	types85,
	attr_flags85,
	gtypes85,
	(uint16) 4096,
	cn_attr85,
	48,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"SERIALIZATION_FAILURE",
	names86,
	types86,
	attr_flags86,
	gtypes86,
	(uint16) 0,
	cn_attr86,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"MISMATCH_FAILURE",
	names87,
	types87,
	attr_flags87,
	gtypes87,
	(uint16) 0,
	cn_attr87,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 9,
	(long) 9,
	"IO_FAILURE",
	names88,
	types88,
	attr_flags88,
	gtypes88,
	(uint16) 0,
	cn_attr88,
	56,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"LANGUAGE_EXCEPTION",
	names89,
	types89,
	attr_flags89,
	gtypes89,
	(uint16) 4096,
	cn_attr89,
	48,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"VOID_TARGET",
	names90,
	types90,
	attr_flags90,
	gtypes90,
	(uint16) 0,
	cn_attr90,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"VOID_ASSIGNED_TO_EXPANDED",
	names91,
	types91,
	attr_flags91,
	gtypes91,
	(uint16) 0,
	cn_attr91,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 9,
	(long) 9,
	"ROUTINE_FAILURE",
	names92,
	types92,
	attr_flags92,
	gtypes92,
	(uint16) 0,
	cn_attr92,
	64,
	7L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"BAD_INSPECT_VALUE",
	names93,
	types93,
	attr_flags93,
	gtypes93,
	(uint16) 0,
	cn_attr93,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"EIFFELSTUDIO_SPECIFIC_LANGUAGE_EXCEPTION",
	names94,
	types94,
	attr_flags94,
	gtypes94,
	(uint16) 4096,
	cn_attr94,
	48,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"CREATE_ON_DEFERRED",
	names95,
	types95,
	attr_flags95,
	gtypes95,
	(uint16) 0,
	cn_attr95,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"ADDRESS_APPLIED_TO_MELTED_FEATURE",
	names96,
	types96,
	attr_flags96,
	gtypes96,
	(uint16) 0,
	cn_attr96,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"ASSERTION_VIOLATION",
	names97,
	types97,
	attr_flags97,
	gtypes97,
	(uint16) 4096,
	cn_attr97,
	48,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"LOOP_INVARIANT_VIOLATION",
	names98,
	types98,
	attr_flags98,
	gtypes98,
	(uint16) 0,
	cn_attr98,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"PRECONDITION_VIOLATION",
	names99,
	types99,
	attr_flags99,
	gtypes99,
	(uint16) 0,
	cn_attr99,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"POSTCONDITION_VIOLATION",
	names100,
	types100,
	attr_flags100,
	gtypes100,
	(uint16) 0,
	cn_attr100,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"VARIANT_VIOLATION",
	names101,
	types101,
	attr_flags101,
	gtypes101,
	(uint16) 0,
	cn_attr101,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"CHECK_VIOLATION",
	names102,
	types102,
	attr_flags102,
	gtypes102,
	(uint16) 0,
	cn_attr102,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 8,
	(long) 8,
	"INVARIANT_VIOLATION",
	names103,
	types103,
	attr_flags103,
	gtypes103,
	(uint16) 0,
	cn_attr103,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"STRING_SEARCHER",
	names104,
	types104,
	attr_flags104,
	gtypes104,
	(uint16) 4096,
	cn_attr104,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"STRING_8_SEARCHER",
	names105,
	types105,
	attr_flags105,
	gtypes105,
	(uint16) 8192,
	cn_attr105,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"STRING_32_SEARCHER",
	names106,
	types106,
	attr_flags106,
	gtypes106,
	(uint16) 8192,
	cn_attr106,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"PART_COMPARABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"COMPARABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SED_UTILITIES",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 8,
	(long) 8,
	"SED_SESSION_SERIALIZER",
	names110,
	types110,
	attr_flags110,
	gtypes110,
	(uint16) 0,
	cn_attr110,
	48,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 8,
	(long) 8,
	"SED_BASIC_SERIALIZER",
	names111,
	types111,
	attr_flags111,
	gtypes111,
	(uint16) 0,
	cn_attr111,
	48,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 8,
	(long) 8,
	"SED_RECOVERABLE_SERIALIZER",
	names112,
	types112,
	attr_flags112,
	gtypes112,
	(uint16) 0,
	cn_attr112,
	48,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 8,
	(long) 8,
	"SED_INDEPENDENT_SERIALIZER",
	names113,
	types113,
	attr_flags113,
	gtypes113,
	(uint16) 0,
	cn_attr113,
	48,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 9,
	(long) 9,
	"SED_SESSION_DESERIALIZER",
	names114,
	types114,
	attr_flags114,
	gtypes114,
	(uint16) 0,
	cn_attr114,
	64,
	7L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 9,
	(long) 9,
	"SED_BASIC_DESERIALIZER",
	names115,
	types115,
	attr_flags115,
	gtypes115,
	(uint16) 0,
	cn_attr115,
	64,
	7L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 10,
	(long) 10,
	"SED_INDEPENDENT_DESERIALIZER",
	names116,
	types116,
	attr_flags116,
	gtypes116,
	(uint16) 0,
	cn_attr116,
	72,
	8L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"MATH_CONST",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SINGLE_MATH",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"DOUBLE_MATH",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 13,
	(long) 13,
	"FORMAT_DOUBLE",
	names120,
	types120,
	attr_flags120,
	gtypes120,
	(uint16) 0,
	cn_attr120,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"MEM_CONST",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"MEM_INFO",
	names122,
	types122,
	attr_flags122,
	gtypes122,
	(uint16) 0,
	cn_attr122,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"GC_INFO",
	names123,
	types123,
	attr_flags123,
	gtypes123,
	(uint16) 0,
	cn_attr123,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"PLATFORM",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 8,
	(long) 8,
	"SED_MEDIUM_READER_WRITER_1",
	names125,
	types125,
	attr_flags125,
	gtypes125,
	(uint16) 0,
	cn_attr125,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 9,
	(long) 9,
	"SED_MEDIUM_READER_WRITER",
	names126,
	types126,
	attr_flags126,
	gtypes126,
	(uint16) 0,
	cn_attr126,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"STRING_HANDLER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"C_STRING",
	names128,
	types128,
	attr_flags128,
	gtypes128,
	(uint16) 0,
	cn_attr128,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"REFLECTOR_CONSTANTS",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"REFLECTED_OBJECT",
	names130,
	types130,
	attr_flags130,
	gtypes130,
	(uint16) 4096,
	cn_attr130,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"REFLECTOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"INTERNAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ECMA_INTERNAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"RT_DBG_INTERNAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 11,
	(long) 11,
	"RT_DBG_EXECUTION_RECORDER",
	names135,
	types135,
	attr_flags135,
	gtypes135,
	(uint16) 0,
	cn_attr135,
	48,
	4L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"REFLECTED_COPY_SEMANTICS_OBJECT",
	names136,
	types136,
	attr_flags136,
	gtypes136,
	(uint16) 0,
	cn_attr136,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"REFLECTED_REFERENCE_OBJECT",
	names137,
	types137,
	attr_flags137,
	gtypes137,
	(uint16) 0,
	cn_attr137,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"DEBUG_OUTPUT",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 15,
	(long) 15,
	"RT_DBG_CALL_RECORD",
	names139,
	types139,
	attr_flags139,
	gtypes139,
	(uint16) 0,
	cn_attr139,
	80,
	8L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ABSTRACT_SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"RT_DBG_VALUE_RECORD",
	names141,
	types141,
	attr_flags141,
	gtypes141,
	(uint16) 4096,
	cn_attr141,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"NUMERIC",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"CIRCULAR_CURSOR",
	names144,
	types144,
	attr_flags144,
	gtypes144,
	(uint16) 0,
	cn_attr144,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"HASH_TABLE_CURSOR",
	names145,
	types145,
	attr_flags145,
	gtypes145,
	(uint16) 0,
	cn_attr145,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"ARRAYED_LIST_CURSOR",
	names146,
	types146,
	attr_flags146,
	gtypes146,
	(uint16) 0,
	cn_attr146,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ARGUMENTS",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ARGUMENTS_32",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"INTEGER_INTERVAL",
	names149,
	types149,
	attr_flags149,
	gtypes149,
	(uint16) 0,
	cn_attr149,
	16,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ACTIVE_INTEGER_INTERVAL",
	names150,
	types150,
	attr_flags150,
	gtypes150,
	(uint16) 0,
	cn_attr150,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_STRING_HANDLER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"NATIVE_STRING",
	names152,
	types152,
	attr_flags152,
	gtypes152,
	(uint16) 0,
	cn_attr152,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"FILE_COMPARER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"EXECUTION_ENVIRONMENT",
	names154,
	types154,
	attr_flags154,
	gtypes154,
	(uint16) 0,
	cn_attr154,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"FILE_INFO",
	names155,
	types155,
	attr_flags155,
	gtypes155,
	(uint16) 0,
	cn_attr155,
	32,
	3L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"UNIX_FILE_INFO",
	names156,
	types156,
	attr_flags156,
	gtypes156,
	(uint16) 0,
	cn_attr156,
	32,
	3L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"MISMATCH_CORRECTOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 18,
	(long) 18,
	"SED_RECOVERABLE_DESERIALIZER",
	names158,
	types158,
	attr_flags158,
	gtypes158,
	(uint16) 0,
	cn_attr158,
	112,
	12L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 17,
	(long) 17,
	"CLASS_NAME_TRANSLATIONS",
	names159,
	types159,
	attr_flags159,
	gtypes159,
	(uint16) 0,
	cn_attr159,
	88,
	7L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 19,
	(long) 19,
	"MISMATCH_INFORMATION",
	names160,
	types160,
	attr_flags160,
	gtypes160,
	(uint16) 0,
	cn_attr160,
	104,
	9L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 18,
	(long) 18,
	"SED_OBJECTS_TABLE",
	names161,
	types161,
	attr_flags161,
	gtypes161,
	(uint16) 0,
	cn_attr161,
	88,
	4L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"DISPOSABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"READ_WRITE_LOCK",
	names163,
	types163,
	attr_flags163,
	gtypes163,
	(uint16) 1024,
	cn_attr163,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 2,
	"MANAGED_POINTER",
	names164,
	types164,
	attr_flags164,
	gtypes164,
	(uint16) 1024,
	cn_attr164,
	24,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"MEMORY_STREAM",
	names165,
	types165,
	attr_flags165,
	gtypes165,
	(uint16) 1024,
	cn_attr165,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"MEMORY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 1024,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONDITION_VARIABLE",
	names167,
	types167,
	attr_flags167,
	gtypes167,
	(uint16) 1024,
	cn_attr167,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"DIRECTORY",
	names168,
	types168,
	attr_flags168,
	gtypes168,
	(uint16) 1024,
	cn_attr168,
	48,
	3L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEMAPHORE",
	names169,
	types169,
	attr_flags169,
	gtypes169,
	(uint16) 1024,
	cn_attr169,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"MUTEX",
	names170,
	types170,
	attr_flags170,
	gtypes170,
	(uint16) 1024,
	cn_attr170,
	16,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"IDENTIFIED",
	names171,
	types171,
	attr_flags171,
	gtypes171,
	(uint16) 1024,
	cn_attr171,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 13,
	(long) 13,
	"IO_MEDIUM",
	names172,
	types172,
	attr_flags172,
	gtypes172,
	(uint16) 5120,
	cn_attr172,
	56,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 17,
	(long) 17,
	"STREAM",
	names173,
	types173,
	attr_flags173,
	gtypes173,
	(uint16) 1024,
	cn_attr173,
	72,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 20,
	(long) 19,
	"FILE",
	names174,
	types174,
	attr_flags174,
	gtypes174,
	(uint16) 5120,
	cn_attr174,
	88,
	3L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 21,
	(long) 20,
	"RAW_FILE",
	names175,
	types175,
	attr_flags175,
	gtypes175,
	(uint16) 1024,
	cn_attr175,
	96,
	4L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 21,
	(long) 20,
	"PLAIN_TEXT_FILE",
	names176,
	types176,
	attr_flags176,
	gtypes176,
	(uint16) 1024,
	cn_attr176,
	88,
	3L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"FILE_ITERATION_CURSOR",
	names177,
	types177,
	attr_flags177,
	gtypes177,
	(uint16) 1024,
	cn_attr177,
	16,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"RANDOM",
	names178,
	types178,
	attr_flags178,
	gtypes178,
	(uint16) 0,
	cn_attr178,
	24,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"PRIMES",
	names179,
	types179,
	attr_flags179,
	gtypes179,
	(uint16) 0,
	cn_attr179,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"FIBONACCI",
	names180,
	types180,
	attr_flags180,
	gtypes180,
	(uint16) 0,
	cn_attr180,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"REPEATABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"STRING_ITERATION_CURSOR",
	names182,
	types182,
	attr_flags182,
	gtypes182,
	(uint16) 0,
	cn_attr182,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"STRING_32_ITERATION_CURSOR",
	names183,
	types183,
	attr_flags183,
	gtypes183,
	(uint16) 0,
	cn_attr183,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"STRING_8_ITERATION_CURSOR",
	names184,
	types184,
	attr_flags184,
	gtypes184,
	(uint16) 0,
	cn_attr184,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"HASHABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"PATH",
	names186,
	types186,
	attr_flags186,
	gtypes186,
	(uint16) 0,
	cn_attr186,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TUPLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INTEGER_8_REF",
	names188,
	types188,
	attr_flags188,
	gtypes188,
	(uint16) 0,
	cn_attr188,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INTEGER_8",
	names189,
	types189,
	attr_flags189,
	gtypes189,
	(uint16) 8966,
	cn_attr189,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INTEGER_8",
	names190,
	types190,
	attr_flags190,
	gtypes190,
	(uint16) 8448,
	cn_attr190,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"REAL_32_REF",
	names191,
	types191,
	attr_flags191,
	gtypes191,
	(uint16) 0,
	cn_attr191,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"REAL_32",
	names192,
	types192,
	attr_flags192,
	gtypes192,
	(uint16) 8964,
	cn_attr192,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"REAL_32",
	names193,
	types193,
	attr_flags193,
	gtypes193,
	(uint16) 8448,
	cn_attr193,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHARACTER_32_REF",
	names194,
	types194,
	attr_flags194,
	gtypes194,
	(uint16) 0,
	cn_attr194,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHARACTER_32",
	names195,
	types195,
	attr_flags195,
	gtypes195,
	(uint16) 8974,
	cn_attr195,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHARACTER_32",
	names196,
	types196,
	attr_flags196,
	gtypes196,
	(uint16) 8448,
	cn_attr196,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHARACTER_8_REF",
	names197,
	types197,
	attr_flags197,
	gtypes197,
	(uint16) 0,
	cn_attr197,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHARACTER_8",
	names198,
	types198,
	attr_flags198,
	gtypes198,
	(uint16) 8962,
	cn_attr198,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHARACTER_8",
	names199,
	types199,
	attr_flags199,
	gtypes199,
	(uint16) 8448,
	cn_attr199,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INTEGER_64_REF",
	names200,
	types200,
	attr_flags200,
	gtypes200,
	(uint16) 0,
	cn_attr200,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INTEGER_64",
	names201,
	types201,
	attr_flags201,
	gtypes201,
	(uint16) 8969,
	cn_attr201,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INTEGER_64",
	names202,
	types202,
	attr_flags202,
	gtypes202,
	(uint16) 8448,
	cn_attr202,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOOLEAN_REF",
	names203,
	types203,
	attr_flags203,
	gtypes203,
	(uint16) 0,
	cn_attr203,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOOLEAN",
	names204,
	types204,
	attr_flags204,
	gtypes204,
	(uint16) 8961,
	cn_attr204,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOOLEAN",
	names205,
	types205,
	attr_flags205,
	gtypes205,
	(uint16) 8448,
	cn_attr205,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"REAL_64_REF",
	names206,
	types206,
	attr_flags206,
	gtypes206,
	(uint16) 0,
	cn_attr206,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"REAL_64",
	names207,
	types207,
	attr_flags207,
	gtypes207,
	(uint16) 8963,
	cn_attr207,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"REAL_64",
	names208,
	types208,
	attr_flags208,
	gtypes208,
	(uint16) 8448,
	cn_attr208,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"NATURAL_8_REF",
	names209,
	types209,
	attr_flags209,
	gtypes209,
	(uint16) 0,
	cn_attr209,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"NATURAL_8",
	names210,
	types210,
	attr_flags210,
	gtypes210,
	(uint16) 8970,
	cn_attr210,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"NATURAL_8",
	names211,
	types211,
	attr_flags211,
	gtypes211,
	(uint16) 8448,
	cn_attr211,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"NATURAL_32_REF",
	names212,
	types212,
	attr_flags212,
	gtypes212,
	(uint16) 0,
	cn_attr212,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"NATURAL_32",
	names213,
	types213,
	attr_flags213,
	gtypes213,
	(uint16) 8972,
	cn_attr213,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"NATURAL_32",
	names214,
	types214,
	attr_flags214,
	gtypes214,
	(uint16) 8448,
	cn_attr214,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"NATURAL_16_REF",
	names215,
	types215,
	attr_flags215,
	gtypes215,
	(uint16) 0,
	cn_attr215,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"NATURAL_16",
	names216,
	types216,
	attr_flags216,
	gtypes216,
	(uint16) 8971,
	cn_attr216,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"NATURAL_16",
	names217,
	types217,
	attr_flags217,
	gtypes217,
	(uint16) 8448,
	cn_attr217,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INTEGER_32_REF",
	names218,
	types218,
	attr_flags218,
	gtypes218,
	(uint16) 0,
	cn_attr218,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INTEGER_32",
	names219,
	types219,
	attr_flags219,
	gtypes219,
	(uint16) 8968,
	cn_attr219,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INTEGER_32",
	names220,
	types220,
	attr_flags220,
	gtypes220,
	(uint16) 8448,
	cn_attr220,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INTEGER_16_REF",
	names221,
	types221,
	attr_flags221,
	gtypes221,
	(uint16) 0,
	cn_attr221,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INTEGER_16",
	names222,
	types222,
	attr_flags222,
	gtypes222,
	(uint16) 8967,
	cn_attr222,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INTEGER_16",
	names223,
	types223,
	attr_flags223,
	gtypes223,
	(uint16) 8448,
	cn_attr223,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"NATURAL_64_REF",
	names224,
	types224,
	attr_flags224,
	gtypes224,
	(uint16) 0,
	cn_attr224,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"NATURAL_64",
	names225,
	types225,
	attr_flags225,
	gtypes225,
	(uint16) 8973,
	cn_attr225,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"NATURAL_64",
	names226,
	types226,
	attr_flags226,
	gtypes226,
	(uint16) 8448,
	cn_attr226,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"POINTER_REF",
	names227,
	types227,
	attr_flags227,
	gtypes227,
	(uint16) 0,
	cn_attr227,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"POINTER",
	names228,
	types228,
	attr_flags228,
	gtypes228,
	(uint16) 8965,
	cn_attr228,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"POINTER",
	names229,
	types229,
	attr_flags229,
	gtypes229,
	(uint16) 8448,
	cn_attr229,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"READABLE_STRING_GENERAL",
	names230,
	types230,
	attr_flags230,
	gtypes230,
	(uint16) 4096,
	cn_attr230,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"READABLE_STRING_8",
	names231,
	types231,
	attr_flags231,
	gtypes231,
	(uint16) 4096,
	cn_attr231,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"STRING_GENERAL",
	names232,
	types232,
	attr_flags232,
	gtypes232,
	(uint16) 4096,
	cn_attr232,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"STRING_8",
	names233,
	types233,
	attr_flags233,
	gtypes233,
	(uint16) 0,
	cn_attr233,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"SEQ_STRING",
	names234,
	types234,
	attr_flags234,
	gtypes234,
	(uint16) 0,
	cn_attr234,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"PATH_NAME",
	names235,
	types235,
	attr_flags235,
	gtypes235,
	(uint16) 4096,
	cn_attr235,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"DIRECTORY_NAME",
	names236,
	types236,
	attr_flags236,
	gtypes236,
	(uint16) 0,
	cn_attr236,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"FILE_NAME",
	names237,
	types237,
	attr_flags237,
	gtypes237,
	(uint16) 0,
	cn_attr237,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"READABLE_STRING_32",
	names238,
	types238,
	attr_flags238,
	gtypes238,
	(uint16) 4096,
	cn_attr238,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"STRING_32",
	names239,
	types239,
	attr_flags239,
	gtypes239,
	(uint16) 0,
	cn_attr239,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"IMMUTABLE_STRING_GENERAL",
	names240,
	types240,
	attr_flags240,
	gtypes240,
	(uint16) 4096,
	cn_attr240,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"IMMUTABLE_STRING_8",
	names241,
	types241,
	attr_flags241,
	gtypes241,
	(uint16) 8192,
	cn_attr241,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"IMMUTABLE_STRING_32",
	names242,
	types242,
	attr_flags242,
	gtypes242,
	(uint16) 8192,
	cn_attr242,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 21,
	(long) 20,
	"CONSOLE",
	names243,
	types243,
	attr_flags243,
	gtypes243,
	(uint16) 1024,
	cn_attr243,
	88,
	3L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOOL_STRING",
	names244,
	types244,
	attr_flags244,
	gtypes244,
	(uint16) 0,
	cn_attr244,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"SED_MULTI_OBJECT_SERIALIZATION",
	names245,
	types245,
	attr_flags245,
	gtypes245,
	(uint16) 0,
	cn_attr245,
	32,
	3L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names246,
	types246,
	attr_flags246,
	gtypes246,
	(uint16) 8192,
	cn_attr246,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names247,
	types247,
	attr_flags247,
	gtypes247,
	(uint16) 8192,
	cn_attr247,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names248,
	types248,
	attr_flags248,
	gtypes248,
	(uint16) 4096,
	cn_attr248,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names251,
	types251,
	attr_flags251,
	gtypes251,
	(uint16) 4096,
	cn_attr251,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names252,
	types252,
	attr_flags252,
	gtypes252,
	(uint16) 4096,
	cn_attr252,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"READABLE_INDEXABLE_ITERATION_CURSOR",
	names254,
	types254,
	attr_flags254,
	gtypes254,
	(uint16) 0,
	cn_attr254,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TYPED_INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names257,
	types257,
	attr_flags257,
	gtypes257,
	(uint16) 4096,
	cn_attr257,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 13,
	(long) 13,
	"FUNCTION",
	names258,
	types258,
	attr_flags258,
	gtypes258,
	(uint16) 0,
	cn_attr258,
	72,
	4L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names259,
	types259,
	attr_flags259,
	gtypes259,
	(uint16) 8965,
	cn_attr259,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names260,
	types260,
	attr_flags260,
	gtypes260,
	(uint16) 8448,
	cn_attr260,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names261,
	types261,
	attr_flags261,
	gtypes261,
	(uint16) 8192,
	cn_attr261,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 12,
	(long) 12,
	"ROUTINE",
	names262,
	types262,
	attr_flags262,
	gtypes262,
	(uint16) 4096,
	cn_attr262,
	72,
	4L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 12,
	(long) 12,
	"PROCEDURE",
	names263,
	types263,
	attr_flags263,
	gtypes263,
	(uint16) 0,
	cn_attr263,
	72,
	4L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names264,
	types264,
	attr_flags264,
	gtypes264,
	(uint16) 4096,
	cn_attr264,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names265,
	types265,
	attr_flags265,
	gtypes265,
	(uint16) 4096,
	cn_attr265,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names266,
	types266,
	attr_flags266,
	gtypes266,
	(uint16) 4096,
	cn_attr266,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names267,
	types267,
	attr_flags267,
	gtypes267,
	(uint16) 4096,
	cn_attr267,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names268,
	types268,
	attr_flags268,
	gtypes268,
	(uint16) 4096,
	cn_attr268,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names269,
	types269,
	attr_flags269,
	gtypes269,
	(uint16) 0,
	cn_attr269,
	32,
	3L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names270,
	types270,
	attr_flags270,
	gtypes270,
	(uint16) 4096,
	cn_attr270,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names271,
	types271,
	attr_flags271,
	gtypes271,
	(uint16) 4096,
	cn_attr271,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names272,
	types272,
	attr_flags272,
	gtypes272,
	(uint16) 4096,
	cn_attr272,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names273,
	types273,
	attr_flags273,
	gtypes273,
	(uint16) 4096,
	cn_attr273,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names274,
	types274,
	attr_flags274,
	gtypes274,
	(uint16) 4096,
	cn_attr274,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names275,
	types275,
	attr_flags275,
	gtypes275,
	(uint16) 4096,
	cn_attr275,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names276,
	types276,
	attr_flags276,
	gtypes276,
	(uint16) 8192,
	cn_attr276,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names277,
	types277,
	attr_flags277,
	gtypes277,
	(uint16) 8192,
	cn_attr277,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names278,
	types278,
	attr_flags278,
	gtypes278,
	(uint16) 8192,
	cn_attr278,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names279,
	types279,
	attr_flags279,
	gtypes279,
	(uint16) 8192,
	cn_attr279,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names280,
	types280,
	attr_flags280,
	gtypes280,
	(uint16) 8192,
	cn_attr280,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names281,
	types281,
	attr_flags281,
	gtypes281,
	(uint16) 8192,
	cn_attr281,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names282,
	types282,
	attr_flags282,
	gtypes282,
	(uint16) 8192,
	cn_attr282,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names283,
	types283,
	attr_flags283,
	gtypes283,
	(uint16) 8192,
	cn_attr283,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names284,
	types284,
	attr_flags284,
	gtypes284,
	(uint16) 8192,
	cn_attr284,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names285,
	types285,
	attr_flags285,
	gtypes285,
	(uint16) 8192,
	cn_attr285,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names286,
	types286,
	attr_flags286,
	gtypes286,
	(uint16) 8192,
	cn_attr286,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names287,
	types287,
	attr_flags287,
	gtypes287,
	(uint16) 8192,
	cn_attr287,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names288,
	types288,
	attr_flags288,
	gtypes288,
	(uint16) 8192,
	cn_attr288,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names289,
	types289,
	attr_flags289,
	gtypes289,
	(uint16) 4096,
	cn_attr289,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names292,
	types292,
	attr_flags292,
	gtypes292,
	(uint16) 4096,
	cn_attr292,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names293,
	types293,
	attr_flags293,
	gtypes293,
	(uint16) 4096,
	cn_attr293,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names294,
	types294,
	attr_flags294,
	gtypes294,
	(uint16) 4096,
	cn_attr294,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names295,
	types295,
	attr_flags295,
	gtypes295,
	(uint16) 4096,
	cn_attr295,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names296,
	types296,
	attr_flags296,
	gtypes296,
	(uint16) 4096,
	cn_attr296,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names297,
	types297,
	attr_flags297,
	gtypes297,
	(uint16) 4096,
	cn_attr297,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names298,
	types298,
	attr_flags298,
	gtypes298,
	(uint16) 4096,
	cn_attr298,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names299,
	types299,
	attr_flags299,
	gtypes299,
	(uint16) 4096,
	cn_attr299,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"READABLE_INDEXABLE_ITERATION_CURSOR",
	names301,
	types301,
	attr_flags301,
	gtypes301,
	(uint16) 0,
	cn_attr301,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TYPED_INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names304,
	types304,
	attr_flags304,
	gtypes304,
	(uint16) 4096,
	cn_attr304,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names307,
	types307,
	attr_flags307,
	gtypes307,
	(uint16) 4096,
	cn_attr307,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names308,
	types308,
	attr_flags308,
	gtypes308,
	(uint16) 4096,
	cn_attr308,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names309,
	types309,
	attr_flags309,
	gtypes309,
	(uint16) 4096,
	cn_attr309,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names310,
	types310,
	attr_flags310,
	gtypes310,
	(uint16) 4096,
	cn_attr310,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names311,
	types311,
	attr_flags311,
	gtypes311,
	(uint16) 4096,
	cn_attr311,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names312,
	types312,
	attr_flags312,
	gtypes312,
	(uint16) 4096,
	cn_attr312,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names313,
	types313,
	attr_flags313,
	gtypes313,
	(uint16) 4096,
	cn_attr313,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names314,
	types314,
	attr_flags314,
	gtypes314,
	(uint16) 4096,
	cn_attr314,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"READABLE_INDEXABLE_ITERATION_CURSOR",
	names316,
	types316,
	attr_flags316,
	gtypes316,
	(uint16) 0,
	cn_attr316,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TYPED_INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names319,
	types319,
	attr_flags319,
	gtypes319,
	(uint16) 0,
	cn_attr319,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names321,
	types321,
	attr_flags321,
	gtypes321,
	(uint16) 0,
	cn_attr321,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names322,
	types322,
	attr_flags322,
	gtypes322,
	(uint16) 4096,
	cn_attr322,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names323,
	types323,
	attr_flags323,
	gtypes323,
	(uint16) 4096,
	cn_attr323,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"ARRAY_ITERATION_CURSOR",
	names324,
	types324,
	attr_flags324,
	gtypes324,
	(uint16) 0,
	cn_attr324,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"SPECIAL_ITERATION_CURSOR",
	names326,
	types326,
	attr_flags326,
	gtypes326,
	(uint16) 0,
	cn_attr326,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"GENERAL_SPECIAL_ITERATION_CURSOR",
	names327,
	types327,
	attr_flags327,
	gtypes327,
	(uint16) 4096,
	cn_attr327,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names328,
	types328,
	attr_flags328,
	gtypes328,
	(uint16) 0,
	cn_attr328,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAYED_LIST_ITERATION_CURSOR",
	names329,
	types329,
	attr_flags329,
	gtypes329,
	(uint16) 0,
	cn_attr329,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names330,
	types330,
	attr_flags330,
	gtypes330,
	(uint16) 4096,
	cn_attr330,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names331,
	types331,
	attr_flags331,
	gtypes331,
	(uint16) 4096,
	cn_attr331,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names332,
	types332,
	attr_flags332,
	gtypes332,
	(uint16) 4096,
	cn_attr332,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names333,
	types333,
	attr_flags333,
	gtypes333,
	(uint16) 0,
	cn_attr333,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names336,
	types336,
	attr_flags336,
	gtypes336,
	(uint16) 4096,
	cn_attr336,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names337,
	types337,
	attr_flags337,
	gtypes337,
	(uint16) 4096,
	cn_attr337,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names338,
	types338,
	attr_flags338,
	gtypes338,
	(uint16) 4096,
	cn_attr338,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names339,
	types339,
	attr_flags339,
	gtypes339,
	(uint16) 4096,
	cn_attr339,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names340,
	types340,
	attr_flags340,
	gtypes340,
	(uint16) 4096,
	cn_attr340,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names341,
	types341,
	attr_flags341,
	gtypes341,
	(uint16) 4096,
	cn_attr341,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names342,
	types342,
	attr_flags342,
	gtypes342,
	(uint16) 4096,
	cn_attr342,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"READABLE_INDEXABLE_ITERATION_CURSOR",
	names344,
	types344,
	attr_flags344,
	gtypes344,
	(uint16) 0,
	cn_attr344,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TYPED_INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names347,
	types347,
	attr_flags347,
	gtypes347,
	(uint16) 4096,
	cn_attr347,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names348,
	types348,
	attr_flags348,
	gtypes348,
	(uint16) 4096,
	cn_attr348,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names349,
	types349,
	attr_flags349,
	gtypes349,
	(uint16) 4096,
	cn_attr349,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names350,
	types350,
	attr_flags350,
	gtypes350,
	(uint16) 4096,
	cn_attr350,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names352,
	types352,
	attr_flags352,
	gtypes352,
	(uint16) 0,
	cn_attr352,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names353,
	types353,
	attr_flags353,
	gtypes353,
	(uint16) 4096,
	cn_attr353,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names354,
	types354,
	attr_flags354,
	gtypes354,
	(uint16) 4096,
	cn_attr354,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"SPECIAL_ITERATION_CURSOR",
	names356,
	types356,
	attr_flags356,
	gtypes356,
	(uint16) 0,
	cn_attr356,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"GENERAL_SPECIAL_ITERATION_CURSOR",
	names357,
	types357,
	attr_flags357,
	gtypes357,
	(uint16) 4096,
	cn_attr357,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names358,
	types358,
	attr_flags358,
	gtypes358,
	(uint16) 8192,
	cn_attr358,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAYED_LIST_ITERATION_CURSOR",
	names359,
	types359,
	attr_flags359,
	gtypes359,
	(uint16) 0,
	cn_attr359,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names360,
	types360,
	attr_flags360,
	gtypes360,
	(uint16) 4096,
	cn_attr360,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names361,
	types361,
	attr_flags361,
	gtypes361,
	(uint16) 4096,
	cn_attr361,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names362,
	types362,
	attr_flags362,
	gtypes362,
	(uint16) 4096,
	cn_attr362,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names363,
	types363,
	attr_flags363,
	gtypes363,
	(uint16) 4096,
	cn_attr363,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names364,
	types364,
	attr_flags364,
	gtypes364,
	(uint16) 4096,
	cn_attr364,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names365,
	types365,
	attr_flags365,
	gtypes365,
	(uint16) 4096,
	cn_attr365,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names366,
	types366,
	attr_flags366,
	gtypes366,
	(uint16) 4096,
	cn_attr366,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names367,
	types367,
	attr_flags367,
	gtypes367,
	(uint16) 0,
	cn_attr367,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"ARRAY_ITERATION_CURSOR",
	names368,
	types368,
	attr_flags368,
	gtypes368,
	(uint16) 0,
	cn_attr368,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 13,
	(long) 13,
	"FUNCTION",
	names369,
	types369,
	attr_flags369,
	gtypes369,
	(uint16) 0,
	cn_attr369,
	80,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names370,
	types370,
	attr_flags370,
	gtypes370,
	(uint16) 0,
	cn_attr370,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names373,
	types373,
	attr_flags373,
	gtypes373,
	(uint16) 4096,
	cn_attr373,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names374,
	types374,
	attr_flags374,
	gtypes374,
	(uint16) 4096,
	cn_attr374,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names375,
	types375,
	attr_flags375,
	gtypes375,
	(uint16) 4096,
	cn_attr375,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names376,
	types376,
	attr_flags376,
	gtypes376,
	(uint16) 4096,
	cn_attr376,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names377,
	types377,
	attr_flags377,
	gtypes377,
	(uint16) 4096,
	cn_attr377,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names378,
	types378,
	attr_flags378,
	gtypes378,
	(uint16) 4096,
	cn_attr378,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names379,
	types379,
	attr_flags379,
	gtypes379,
	(uint16) 4096,
	cn_attr379,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"READABLE_INDEXABLE_ITERATION_CURSOR",
	names381,
	types381,
	attr_flags381,
	gtypes381,
	(uint16) 0,
	cn_attr381,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TYPED_INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names384,
	types384,
	attr_flags384,
	gtypes384,
	(uint16) 4096,
	cn_attr384,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names385,
	types385,
	attr_flags385,
	gtypes385,
	(uint16) 4096,
	cn_attr385,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names386,
	types386,
	attr_flags386,
	gtypes386,
	(uint16) 4096,
	cn_attr386,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names387,
	types387,
	attr_flags387,
	gtypes387,
	(uint16) 4096,
	cn_attr387,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names389,
	types389,
	attr_flags389,
	gtypes389,
	(uint16) 0,
	cn_attr389,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names390,
	types390,
	attr_flags390,
	gtypes390,
	(uint16) 4096,
	cn_attr390,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names391,
	types391,
	attr_flags391,
	gtypes391,
	(uint16) 4096,
	cn_attr391,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"SPECIAL_ITERATION_CURSOR",
	names393,
	types393,
	attr_flags393,
	gtypes393,
	(uint16) 0,
	cn_attr393,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"GENERAL_SPECIAL_ITERATION_CURSOR",
	names394,
	types394,
	attr_flags394,
	gtypes394,
	(uint16) 4096,
	cn_attr394,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names395,
	types395,
	attr_flags395,
	gtypes395,
	(uint16) 8192,
	cn_attr395,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAYED_LIST_ITERATION_CURSOR",
	names396,
	types396,
	attr_flags396,
	gtypes396,
	(uint16) 0,
	cn_attr396,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names397,
	types397,
	attr_flags397,
	gtypes397,
	(uint16) 4096,
	cn_attr397,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names398,
	types398,
	attr_flags398,
	gtypes398,
	(uint16) 4096,
	cn_attr398,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names399,
	types399,
	attr_flags399,
	gtypes399,
	(uint16) 4096,
	cn_attr399,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names400,
	types400,
	attr_flags400,
	gtypes400,
	(uint16) 4096,
	cn_attr400,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names401,
	types401,
	attr_flags401,
	gtypes401,
	(uint16) 4096,
	cn_attr401,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names402,
	types402,
	attr_flags402,
	gtypes402,
	(uint16) 4096,
	cn_attr402,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names403,
	types403,
	attr_flags403,
	gtypes403,
	(uint16) 4096,
	cn_attr403,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names404,
	types404,
	attr_flags404,
	gtypes404,
	(uint16) 0,
	cn_attr404,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"ARRAY_ITERATION_CURSOR",
	names405,
	types405,
	attr_flags405,
	gtypes405,
	(uint16) 0,
	cn_attr405,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names406,
	types406,
	attr_flags406,
	gtypes406,
	(uint16) 0,
	cn_attr406,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names409,
	types409,
	attr_flags409,
	gtypes409,
	(uint16) 4096,
	cn_attr409,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names410,
	types410,
	attr_flags410,
	gtypes410,
	(uint16) 4096,
	cn_attr410,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names411,
	types411,
	attr_flags411,
	gtypes411,
	(uint16) 4096,
	cn_attr411,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names412,
	types412,
	attr_flags412,
	gtypes412,
	(uint16) 4096,
	cn_attr412,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names413,
	types413,
	attr_flags413,
	gtypes413,
	(uint16) 4096,
	cn_attr413,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names414,
	types414,
	attr_flags414,
	gtypes414,
	(uint16) 4096,
	cn_attr414,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names415,
	types415,
	attr_flags415,
	gtypes415,
	(uint16) 4096,
	cn_attr415,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"READABLE_INDEXABLE_ITERATION_CURSOR",
	names417,
	types417,
	attr_flags417,
	gtypes417,
	(uint16) 0,
	cn_attr417,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TYPED_INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names420,
	types420,
	attr_flags420,
	gtypes420,
	(uint16) 4096,
	cn_attr420,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names421,
	types421,
	attr_flags421,
	gtypes421,
	(uint16) 4096,
	cn_attr421,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names422,
	types422,
	attr_flags422,
	gtypes422,
	(uint16) 4096,
	cn_attr422,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names423,
	types423,
	attr_flags423,
	gtypes423,
	(uint16) 4096,
	cn_attr423,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names425,
	types425,
	attr_flags425,
	gtypes425,
	(uint16) 0,
	cn_attr425,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names426,
	types426,
	attr_flags426,
	gtypes426,
	(uint16) 4096,
	cn_attr426,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names427,
	types427,
	attr_flags427,
	gtypes427,
	(uint16) 4096,
	cn_attr427,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"SPECIAL_ITERATION_CURSOR",
	names429,
	types429,
	attr_flags429,
	gtypes429,
	(uint16) 0,
	cn_attr429,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"GENERAL_SPECIAL_ITERATION_CURSOR",
	names430,
	types430,
	attr_flags430,
	gtypes430,
	(uint16) 4096,
	cn_attr430,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names431,
	types431,
	attr_flags431,
	gtypes431,
	(uint16) 8192,
	cn_attr431,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAYED_LIST_ITERATION_CURSOR",
	names432,
	types432,
	attr_flags432,
	gtypes432,
	(uint16) 0,
	cn_attr432,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names433,
	types433,
	attr_flags433,
	gtypes433,
	(uint16) 4096,
	cn_attr433,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names434,
	types434,
	attr_flags434,
	gtypes434,
	(uint16) 4096,
	cn_attr434,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names435,
	types435,
	attr_flags435,
	gtypes435,
	(uint16) 4096,
	cn_attr435,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names436,
	types436,
	attr_flags436,
	gtypes436,
	(uint16) 4096,
	cn_attr436,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names437,
	types437,
	attr_flags437,
	gtypes437,
	(uint16) 4096,
	cn_attr437,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names438,
	types438,
	attr_flags438,
	gtypes438,
	(uint16) 4096,
	cn_attr438,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names439,
	types439,
	attr_flags439,
	gtypes439,
	(uint16) 4096,
	cn_attr439,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names440,
	types440,
	attr_flags440,
	gtypes440,
	(uint16) 0,
	cn_attr440,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"ARRAY_ITERATION_CURSOR",
	names441,
	types441,
	attr_flags441,
	gtypes441,
	(uint16) 0,
	cn_attr441,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 17,
	(long) 17,
	"HASH_TABLE",
	names442,
	types442,
	attr_flags442,
	gtypes442,
	(uint16) 0,
	cn_attr442,
	88,
	6L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"HASH_TABLE_ITERATION_CURSOR",
	names443,
	types443,
	attr_flags443,
	gtypes443,
	(uint16) 0,
	cn_attr443,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"READABLE_INDEXABLE_ITERATION_CURSOR",
	names448,
	types448,
	attr_flags448,
	gtypes448,
	(uint16) 0,
	cn_attr448,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TYPED_INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"SPECIAL_ITERATION_CURSOR",
	names453,
	types453,
	attr_flags453,
	gtypes453,
	(uint16) 0,
	cn_attr453,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"GENERAL_SPECIAL_ITERATION_CURSOR",
	names454,
	types454,
	attr_flags454,
	gtypes454,
	(uint16) 4096,
	cn_attr454,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names455,
	types455,
	attr_flags455,
	gtypes455,
	(uint16) 8192,
	cn_attr455,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names456,
	types456,
	attr_flags456,
	gtypes456,
	(uint16) 0,
	cn_attr456,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names457,
	types457,
	attr_flags457,
	gtypes457,
	(uint16) 4096,
	cn_attr457,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names458,
	types458,
	attr_flags458,
	gtypes458,
	(uint16) 4096,
	cn_attr458,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names459,
	types459,
	attr_flags459,
	gtypes459,
	(uint16) 4096,
	cn_attr459,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names460,
	types460,
	attr_flags460,
	gtypes460,
	(uint16) 4096,
	cn_attr460,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names461,
	types461,
	attr_flags461,
	gtypes461,
	(uint16) 4096,
	cn_attr461,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names462,
	types462,
	attr_flags462,
	gtypes462,
	(uint16) 4096,
	cn_attr462,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names463,
	types463,
	attr_flags463,
	gtypes463,
	(uint16) 4096,
	cn_attr463,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names464,
	types464,
	attr_flags464,
	gtypes464,
	(uint16) 4096,
	cn_attr464,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names465,
	types465,
	attr_flags465,
	gtypes465,
	(uint16) 4096,
	cn_attr465,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names466,
	types466,
	attr_flags466,
	gtypes466,
	(uint16) 4096,
	cn_attr466,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names467,
	types467,
	attr_flags467,
	gtypes467,
	(uint16) 4096,
	cn_attr467,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names468,
	types468,
	attr_flags468,
	gtypes468,
	(uint16) 0,
	cn_attr468,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names469,
	types469,
	attr_flags469,
	gtypes469,
	(uint16) 4096,
	cn_attr469,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names470,
	types470,
	attr_flags470,
	gtypes470,
	(uint16) 4096,
	cn_attr470,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAYED_LIST_ITERATION_CURSOR",
	names471,
	types471,
	attr_flags471,
	gtypes471,
	(uint16) 0,
	cn_attr471,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names472,
	types472,
	attr_flags472,
	gtypes472,
	(uint16) 4096,
	cn_attr472,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names473,
	types473,
	attr_flags473,
	gtypes473,
	(uint16) 4096,
	cn_attr473,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names474,
	types474,
	attr_flags474,
	gtypes474,
	(uint16) 4096,
	cn_attr474,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names475,
	types475,
	attr_flags475,
	gtypes475,
	(uint16) 4096,
	cn_attr475,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names476,
	types476,
	attr_flags476,
	gtypes476,
	(uint16) 4096,
	cn_attr476,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names477,
	types477,
	attr_flags477,
	gtypes477,
	(uint16) 4096,
	cn_attr477,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names478,
	types478,
	attr_flags478,
	gtypes478,
	(uint16) 4096,
	cn_attr478,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names479,
	types479,
	attr_flags479,
	gtypes479,
	(uint16) 0,
	cn_attr479,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"ARRAY_ITERATION_CURSOR",
	names480,
	types480,
	attr_flags480,
	gtypes480,
	(uint16) 0,
	cn_attr480,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TABLE_ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names482,
	types482,
	attr_flags482,
	gtypes482,
	(uint16) 0,
	cn_attr482,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names483,
	types483,
	attr_flags483,
	gtypes483,
	(uint16) 8965,
	cn_attr483,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names484,
	types484,
	attr_flags484,
	gtypes484,
	(uint16) 8448,
	cn_attr484,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names485,
	types485,
	attr_flags485,
	gtypes485,
	(uint16) 8192,
	cn_attr485,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names486,
	types486,
	attr_flags486,
	gtypes486,
	(uint16) 8192,
	cn_attr486,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names487,
	types487,
	attr_flags487,
	gtypes487,
	(uint16) 0,
	cn_attr487,
	40,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names488,
	types488,
	attr_flags488,
	gtypes488,
	(uint16) 8965,
	cn_attr488,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names489,
	types489,
	attr_flags489,
	gtypes489,
	(uint16) 8448,
	cn_attr489,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names490,
	types490,
	attr_flags490,
	gtypes490,
	(uint16) 8192,
	cn_attr490,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names491,
	types491,
	attr_flags491,
	gtypes491,
	(uint16) 0,
	cn_attr491,
	40,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names492,
	types492,
	attr_flags492,
	gtypes492,
	(uint16) 8965,
	cn_attr492,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names493,
	types493,
	attr_flags493,
	gtypes493,
	(uint16) 8448,
	cn_attr493,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names494,
	types494,
	attr_flags494,
	gtypes494,
	(uint16) 8192,
	cn_attr494,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names495,
	types495,
	attr_flags495,
	gtypes495,
	(uint16) 0,
	cn_attr495,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"SPECIAL_ITERATION_CURSOR",
	names498,
	types498,
	attr_flags498,
	gtypes498,
	(uint16) 0,
	cn_attr498,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"GENERAL_SPECIAL_ITERATION_CURSOR",
	names499,
	types499,
	attr_flags499,
	gtypes499,
	(uint16) 4096,
	cn_attr499,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names500,
	types500,
	attr_flags500,
	gtypes500,
	(uint16) 8192,
	cn_attr500,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names501,
	types501,
	attr_flags501,
	gtypes501,
	(uint16) 0,
	cn_attr501,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names502,
	types502,
	attr_flags502,
	gtypes502,
	(uint16) 4096,
	cn_attr502,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names503,
	types503,
	attr_flags503,
	gtypes503,
	(uint16) 4096,
	cn_attr503,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names504,
	types504,
	attr_flags504,
	gtypes504,
	(uint16) 0,
	cn_attr504,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names505,
	types505,
	attr_flags505,
	gtypes505,
	(uint16) 4096,
	cn_attr505,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names506,
	types506,
	attr_flags506,
	gtypes506,
	(uint16) 4096,
	cn_attr506,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAYED_LIST_ITERATION_CURSOR",
	names507,
	types507,
	attr_flags507,
	gtypes507,
	(uint16) 0,
	cn_attr507,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names508,
	types508,
	attr_flags508,
	gtypes508,
	(uint16) 4096,
	cn_attr508,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names509,
	types509,
	attr_flags509,
	gtypes509,
	(uint16) 4096,
	cn_attr509,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names510,
	types510,
	attr_flags510,
	gtypes510,
	(uint16) 4096,
	cn_attr510,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names511,
	types511,
	attr_flags511,
	gtypes511,
	(uint16) 4096,
	cn_attr511,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names512,
	types512,
	attr_flags512,
	gtypes512,
	(uint16) 4096,
	cn_attr512,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names513,
	types513,
	attr_flags513,
	gtypes513,
	(uint16) 4096,
	cn_attr513,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names514,
	types514,
	attr_flags514,
	gtypes514,
	(uint16) 4096,
	cn_attr514,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"ARRAY_ITERATION_CURSOR",
	names515,
	types515,
	attr_flags515,
	gtypes515,
	(uint16) 0,
	cn_attr515,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 13,
	(long) 13,
	"ACTION_SEQUENCE",
	names516,
	types516,
	attr_flags516,
	gtypes516,
	(uint16) 0,
	cn_attr516,
	88,
	9L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"LINKED_QUEUE",
	names517,
	types517,
	attr_flags517,
	gtypes517,
	(uint16) 0,
	cn_attr517,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 8,
	(long) 7,
	"LINKED_LIST_ITERATION_CURSOR",
	names518,
	types518,
	attr_flags518,
	gtypes518,
	(uint16) 0,
	cn_attr518,
	40,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"LINKABLE",
	names519,
	types519,
	attr_flags519,
	gtypes519,
	(uint16) 0,
	cn_attr519,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CELL",
	names520,
	types520,
	attr_flags520,
	gtypes520,
	(uint16) 0,
	cn_attr520,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"LINKED_LIST",
	names521,
	types521,
	attr_flags521,
	gtypes521,
	(uint16) 0,
	cn_attr521,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"LINKED_LIST_CURSOR",
	names522,
	types522,
	attr_flags522,
	gtypes522,
	(uint16) 0,
	cn_attr522,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"QUEUE",
	names523,
	types523,
	attr_flags523,
	gtypes523,
	(uint16) 4096,
	cn_attr523,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DISPENSER",
	names524,
	types524,
	attr_flags524,
	gtypes524,
	(uint16) 4096,
	cn_attr524,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"INTERACTIVE_LIST",
	names525,
	types525,
	attr_flags525,
	gtypes525,
	(uint16) 4096,
	cn_attr525,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 17,
	(long) 17,
	"HASH_TABLE",
	names526,
	types526,
	attr_flags526,
	gtypes526,
	(uint16) 0,
	cn_attr526,
	80,
	4L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"HASH_TABLE_ITERATION_CURSOR",
	names527,
	types527,
	attr_flags527,
	gtypes527,
	(uint16) 0,
	cn_attr527,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TABLE_ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names530,
	types530,
	attr_flags530,
	gtypes530,
	(uint16) 4096,
	cn_attr530,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names531,
	types531,
	attr_flags531,
	gtypes531,
	(uint16) 4096,
	cn_attr531,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names532,
	types532,
	attr_flags532,
	gtypes532,
	(uint16) 0,
	cn_attr532,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names533,
	types533,
	attr_flags533,
	gtypes533,
	(uint16) 8965,
	cn_attr533,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names534,
	types534,
	attr_flags534,
	gtypes534,
	(uint16) 8448,
	cn_attr534,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names535,
	types535,
	attr_flags535,
	gtypes535,
	(uint16) 8192,
	cn_attr535,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names536,
	types536,
	attr_flags536,
	gtypes536,
	(uint16) 0,
	cn_attr536,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names537,
	types537,
	attr_flags537,
	gtypes537,
	(uint16) 8965,
	cn_attr537,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names538,
	types538,
	attr_flags538,
	gtypes538,
	(uint16) 8448,
	cn_attr538,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names539,
	types539,
	attr_flags539,
	gtypes539,
	(uint16) 8192,
	cn_attr539,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"LINKED_LIST",
	names540,
	types540,
	attr_flags540,
	gtypes540,
	(uint16) 0,
	cn_attr540,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 8,
	(long) 7,
	"LINKED_LIST_ITERATION_CURSOR",
	names541,
	types541,
	attr_flags541,
	gtypes541,
	(uint16) 0,
	cn_attr541,
	40,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"LINKABLE",
	names542,
	types542,
	attr_flags542,
	gtypes542,
	(uint16) 0,
	cn_attr542,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CELL",
	names543,
	types543,
	attr_flags543,
	gtypes543,
	(uint16) 0,
	cn_attr543,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"LINKED_LIST_CURSOR",
	names544,
	types544,
	attr_flags544,
	gtypes544,
	(uint16) 0,
	cn_attr544,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names545,
	types545,
	attr_flags545,
	gtypes545,
	(uint16) 0,
	cn_attr545,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names546,
	types546,
	attr_flags546,
	gtypes546,
	(uint16) 8965,
	cn_attr546,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names547,
	types547,
	attr_flags547,
	gtypes547,
	(uint16) 8448,
	cn_attr547,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names548,
	types548,
	attr_flags548,
	gtypes548,
	(uint16) 8192,
	cn_attr548,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names549,
	types549,
	attr_flags549,
	gtypes549,
	(uint16) 0,
	cn_attr549,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names550,
	types550,
	attr_flags550,
	gtypes550,
	(uint16) 0,
	cn_attr550,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names551,
	types551,
	attr_flags551,
	gtypes551,
	(uint16) 0,
	cn_attr551,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SET",
	names552,
	types552,
	attr_flags552,
	gtypes552,
	(uint16) 4096,
	cn_attr552,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names553,
	types553,
	attr_flags553,
	gtypes553,
	(uint16) 0,
	cn_attr553,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"READABLE_INDEXABLE_ITERATION_CURSOR",
	names557,
	types557,
	attr_flags557,
	gtypes557,
	(uint16) 0,
	cn_attr557,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TYPED_INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"SPECIAL_ITERATION_CURSOR",
	names562,
	types562,
	attr_flags562,
	gtypes562,
	(uint16) 0,
	cn_attr562,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"GENERAL_SPECIAL_ITERATION_CURSOR",
	names563,
	types563,
	attr_flags563,
	gtypes563,
	(uint16) 4096,
	cn_attr563,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names564,
	types564,
	attr_flags564,
	gtypes564,
	(uint16) 8192,
	cn_attr564,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names565,
	types565,
	attr_flags565,
	gtypes565,
	(uint16) 0,
	cn_attr565,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names566,
	types566,
	attr_flags566,
	gtypes566,
	(uint16) 4096,
	cn_attr566,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names567,
	types567,
	attr_flags567,
	gtypes567,
	(uint16) 4096,
	cn_attr567,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names568,
	types568,
	attr_flags568,
	gtypes568,
	(uint16) 4096,
	cn_attr568,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names569,
	types569,
	attr_flags569,
	gtypes569,
	(uint16) 4096,
	cn_attr569,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names570,
	types570,
	attr_flags570,
	gtypes570,
	(uint16) 4096,
	cn_attr570,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names571,
	types571,
	attr_flags571,
	gtypes571,
	(uint16) 4096,
	cn_attr571,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names572,
	types572,
	attr_flags572,
	gtypes572,
	(uint16) 4096,
	cn_attr572,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names573,
	types573,
	attr_flags573,
	gtypes573,
	(uint16) 4096,
	cn_attr573,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names574,
	types574,
	attr_flags574,
	gtypes574,
	(uint16) 4096,
	cn_attr574,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names575,
	types575,
	attr_flags575,
	gtypes575,
	(uint16) 4096,
	cn_attr575,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names576,
	types576,
	attr_flags576,
	gtypes576,
	(uint16) 4096,
	cn_attr576,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names577,
	types577,
	attr_flags577,
	gtypes577,
	(uint16) 0,
	cn_attr577,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names578,
	types578,
	attr_flags578,
	gtypes578,
	(uint16) 4096,
	cn_attr578,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names579,
	types579,
	attr_flags579,
	gtypes579,
	(uint16) 4096,
	cn_attr579,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAYED_LIST_ITERATION_CURSOR",
	names580,
	types580,
	attr_flags580,
	gtypes580,
	(uint16) 0,
	cn_attr580,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names581,
	types581,
	attr_flags581,
	gtypes581,
	(uint16) 4096,
	cn_attr581,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names582,
	types582,
	attr_flags582,
	gtypes582,
	(uint16) 4096,
	cn_attr582,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names583,
	types583,
	attr_flags583,
	gtypes583,
	(uint16) 4096,
	cn_attr583,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names584,
	types584,
	attr_flags584,
	gtypes584,
	(uint16) 4096,
	cn_attr584,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names585,
	types585,
	attr_flags585,
	gtypes585,
	(uint16) 4096,
	cn_attr585,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names586,
	types586,
	attr_flags586,
	gtypes586,
	(uint16) 4096,
	cn_attr586,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names587,
	types587,
	attr_flags587,
	gtypes587,
	(uint16) 4096,
	cn_attr587,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names588,
	types588,
	attr_flags588,
	gtypes588,
	(uint16) 0,
	cn_attr588,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"ARRAY_ITERATION_CURSOR",
	names589,
	types589,
	attr_flags589,
	gtypes589,
	(uint16) 0,
	cn_attr589,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names590,
	types590,
	attr_flags590,
	gtypes590,
	(uint16) 0,
	cn_attr590,
	40,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names591,
	types591,
	attr_flags591,
	gtypes591,
	(uint16) 8965,
	cn_attr591,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names592,
	types592,
	attr_flags592,
	gtypes592,
	(uint16) 8448,
	cn_attr592,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names593,
	types593,
	attr_flags593,
	gtypes593,
	(uint16) 8192,
	cn_attr593,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names594,
	types594,
	attr_flags594,
	gtypes594,
	(uint16) 0,
	cn_attr594,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names595,
	types595,
	attr_flags595,
	gtypes595,
	(uint16) 8965,
	cn_attr595,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names596,
	types596,
	attr_flags596,
	gtypes596,
	(uint16) 8448,
	cn_attr596,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names597,
	types597,
	attr_flags597,
	gtypes597,
	(uint16) 8192,
	cn_attr597,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 17,
	(long) 17,
	"HASH_TABLE",
	names598,
	types598,
	attr_flags598,
	gtypes598,
	(uint16) 0,
	cn_attr598,
	88,
	7L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"HASH_TABLE_ITERATION_CURSOR",
	names599,
	types599,
	attr_flags599,
	gtypes599,
	(uint16) 0,
	cn_attr599,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names601,
	types601,
	attr_flags601,
	gtypes601,
	(uint16) 4096,
	cn_attr601,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TABLE_ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names603,
	types603,
	attr_flags603,
	gtypes603,
	(uint16) 0,
	cn_attr603,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names604,
	types604,
	attr_flags604,
	gtypes604,
	(uint16) 4096,
	cn_attr604,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names605,
	types605,
	attr_flags605,
	gtypes605,
	(uint16) 0,
	cn_attr605,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names606,
	types606,
	attr_flags606,
	gtypes606,
	(uint16) 8965,
	cn_attr606,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names607,
	types607,
	attr_flags607,
	gtypes607,
	(uint16) 8448,
	cn_attr607,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names608,
	types608,
	attr_flags608,
	gtypes608,
	(uint16) 8192,
	cn_attr608,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"READABLE_INDEXABLE_ITERATION_CURSOR",
	names612,
	types612,
	attr_flags612,
	gtypes612,
	(uint16) 0,
	cn_attr612,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TYPED_INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"SPECIAL_ITERATION_CURSOR",
	names617,
	types617,
	attr_flags617,
	gtypes617,
	(uint16) 0,
	cn_attr617,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"GENERAL_SPECIAL_ITERATION_CURSOR",
	names618,
	types618,
	attr_flags618,
	gtypes618,
	(uint16) 4096,
	cn_attr618,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names619,
	types619,
	attr_flags619,
	gtypes619,
	(uint16) 8192,
	cn_attr619,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names620,
	types620,
	attr_flags620,
	gtypes620,
	(uint16) 0,
	cn_attr620,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names621,
	types621,
	attr_flags621,
	gtypes621,
	(uint16) 4096,
	cn_attr621,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names622,
	types622,
	attr_flags622,
	gtypes622,
	(uint16) 4096,
	cn_attr622,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names623,
	types623,
	attr_flags623,
	gtypes623,
	(uint16) 4096,
	cn_attr623,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names624,
	types624,
	attr_flags624,
	gtypes624,
	(uint16) 4096,
	cn_attr624,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names625,
	types625,
	attr_flags625,
	gtypes625,
	(uint16) 4096,
	cn_attr625,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names626,
	types626,
	attr_flags626,
	gtypes626,
	(uint16) 4096,
	cn_attr626,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names627,
	types627,
	attr_flags627,
	gtypes627,
	(uint16) 4096,
	cn_attr627,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names628,
	types628,
	attr_flags628,
	gtypes628,
	(uint16) 4096,
	cn_attr628,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names629,
	types629,
	attr_flags629,
	gtypes629,
	(uint16) 4096,
	cn_attr629,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names630,
	types630,
	attr_flags630,
	gtypes630,
	(uint16) 4096,
	cn_attr630,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names631,
	types631,
	attr_flags631,
	gtypes631,
	(uint16) 4096,
	cn_attr631,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names632,
	types632,
	attr_flags632,
	gtypes632,
	(uint16) 0,
	cn_attr632,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names633,
	types633,
	attr_flags633,
	gtypes633,
	(uint16) 4096,
	cn_attr633,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names634,
	types634,
	attr_flags634,
	gtypes634,
	(uint16) 4096,
	cn_attr634,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAYED_LIST_ITERATION_CURSOR",
	names635,
	types635,
	attr_flags635,
	gtypes635,
	(uint16) 0,
	cn_attr635,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names636,
	types636,
	attr_flags636,
	gtypes636,
	(uint16) 4096,
	cn_attr636,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names637,
	types637,
	attr_flags637,
	gtypes637,
	(uint16) 4096,
	cn_attr637,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names638,
	types638,
	attr_flags638,
	gtypes638,
	(uint16) 4096,
	cn_attr638,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names639,
	types639,
	attr_flags639,
	gtypes639,
	(uint16) 4096,
	cn_attr639,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names640,
	types640,
	attr_flags640,
	gtypes640,
	(uint16) 4096,
	cn_attr640,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names641,
	types641,
	attr_flags641,
	gtypes641,
	(uint16) 4096,
	cn_attr641,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names642,
	types642,
	attr_flags642,
	gtypes642,
	(uint16) 4096,
	cn_attr642,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names643,
	types643,
	attr_flags643,
	gtypes643,
	(uint16) 0,
	cn_attr643,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"ARRAY_ITERATION_CURSOR",
	names644,
	types644,
	attr_flags644,
	gtypes644,
	(uint16) 0,
	cn_attr644,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"READABLE_INDEXABLE_ITERATION_CURSOR",
	names648,
	types648,
	attr_flags648,
	gtypes648,
	(uint16) 0,
	cn_attr648,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TYPED_INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"SPECIAL_ITERATION_CURSOR",
	names653,
	types653,
	attr_flags653,
	gtypes653,
	(uint16) 0,
	cn_attr653,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"GENERAL_SPECIAL_ITERATION_CURSOR",
	names654,
	types654,
	attr_flags654,
	gtypes654,
	(uint16) 4096,
	cn_attr654,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names655,
	types655,
	attr_flags655,
	gtypes655,
	(uint16) 8192,
	cn_attr655,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names656,
	types656,
	attr_flags656,
	gtypes656,
	(uint16) 0,
	cn_attr656,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names657,
	types657,
	attr_flags657,
	gtypes657,
	(uint16) 4096,
	cn_attr657,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names658,
	types658,
	attr_flags658,
	gtypes658,
	(uint16) 4096,
	cn_attr658,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names659,
	types659,
	attr_flags659,
	gtypes659,
	(uint16) 4096,
	cn_attr659,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names660,
	types660,
	attr_flags660,
	gtypes660,
	(uint16) 4096,
	cn_attr660,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names661,
	types661,
	attr_flags661,
	gtypes661,
	(uint16) 4096,
	cn_attr661,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names662,
	types662,
	attr_flags662,
	gtypes662,
	(uint16) 4096,
	cn_attr662,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names663,
	types663,
	attr_flags663,
	gtypes663,
	(uint16) 4096,
	cn_attr663,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names664,
	types664,
	attr_flags664,
	gtypes664,
	(uint16) 4096,
	cn_attr664,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names665,
	types665,
	attr_flags665,
	gtypes665,
	(uint16) 4096,
	cn_attr665,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names666,
	types666,
	attr_flags666,
	gtypes666,
	(uint16) 4096,
	cn_attr666,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names667,
	types667,
	attr_flags667,
	gtypes667,
	(uint16) 4096,
	cn_attr667,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names668,
	types668,
	attr_flags668,
	gtypes668,
	(uint16) 0,
	cn_attr668,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names669,
	types669,
	attr_flags669,
	gtypes669,
	(uint16) 4096,
	cn_attr669,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names670,
	types670,
	attr_flags670,
	gtypes670,
	(uint16) 4096,
	cn_attr670,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAYED_LIST_ITERATION_CURSOR",
	names671,
	types671,
	attr_flags671,
	gtypes671,
	(uint16) 0,
	cn_attr671,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names672,
	types672,
	attr_flags672,
	gtypes672,
	(uint16) 4096,
	cn_attr672,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names673,
	types673,
	attr_flags673,
	gtypes673,
	(uint16) 4096,
	cn_attr673,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names674,
	types674,
	attr_flags674,
	gtypes674,
	(uint16) 4096,
	cn_attr674,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names675,
	types675,
	attr_flags675,
	gtypes675,
	(uint16) 4096,
	cn_attr675,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names676,
	types676,
	attr_flags676,
	gtypes676,
	(uint16) 4096,
	cn_attr676,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names677,
	types677,
	attr_flags677,
	gtypes677,
	(uint16) 4096,
	cn_attr677,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names678,
	types678,
	attr_flags678,
	gtypes678,
	(uint16) 4096,
	cn_attr678,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names679,
	types679,
	attr_flags679,
	gtypes679,
	(uint16) 0,
	cn_attr679,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"ARRAY_ITERATION_CURSOR",
	names680,
	types680,
	attr_flags680,
	gtypes680,
	(uint16) 0,
	cn_attr680,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"READABLE_INDEXABLE_ITERATION_CURSOR",
	names684,
	types684,
	attr_flags684,
	gtypes684,
	(uint16) 0,
	cn_attr684,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TYPED_INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"SPECIAL_ITERATION_CURSOR",
	names689,
	types689,
	attr_flags689,
	gtypes689,
	(uint16) 0,
	cn_attr689,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"GENERAL_SPECIAL_ITERATION_CURSOR",
	names690,
	types690,
	attr_flags690,
	gtypes690,
	(uint16) 4096,
	cn_attr690,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names691,
	types691,
	attr_flags691,
	gtypes691,
	(uint16) 8192,
	cn_attr691,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names692,
	types692,
	attr_flags692,
	gtypes692,
	(uint16) 0,
	cn_attr692,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names693,
	types693,
	attr_flags693,
	gtypes693,
	(uint16) 4096,
	cn_attr693,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names694,
	types694,
	attr_flags694,
	gtypes694,
	(uint16) 4096,
	cn_attr694,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names695,
	types695,
	attr_flags695,
	gtypes695,
	(uint16) 4096,
	cn_attr695,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names696,
	types696,
	attr_flags696,
	gtypes696,
	(uint16) 4096,
	cn_attr696,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names697,
	types697,
	attr_flags697,
	gtypes697,
	(uint16) 4096,
	cn_attr697,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names698,
	types698,
	attr_flags698,
	gtypes698,
	(uint16) 4096,
	cn_attr698,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names699,
	types699,
	attr_flags699,
	gtypes699,
	(uint16) 4096,
	cn_attr699,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names700,
	types700,
	attr_flags700,
	gtypes700,
	(uint16) 4096,
	cn_attr700,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names701,
	types701,
	attr_flags701,
	gtypes701,
	(uint16) 4096,
	cn_attr701,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names702,
	types702,
	attr_flags702,
	gtypes702,
	(uint16) 4096,
	cn_attr702,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names703,
	types703,
	attr_flags703,
	gtypes703,
	(uint16) 4096,
	cn_attr703,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names704,
	types704,
	attr_flags704,
	gtypes704,
	(uint16) 0,
	cn_attr704,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names705,
	types705,
	attr_flags705,
	gtypes705,
	(uint16) 4096,
	cn_attr705,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names706,
	types706,
	attr_flags706,
	gtypes706,
	(uint16) 4096,
	cn_attr706,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAYED_LIST_ITERATION_CURSOR",
	names707,
	types707,
	attr_flags707,
	gtypes707,
	(uint16) 0,
	cn_attr707,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names708,
	types708,
	attr_flags708,
	gtypes708,
	(uint16) 4096,
	cn_attr708,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names709,
	types709,
	attr_flags709,
	gtypes709,
	(uint16) 4096,
	cn_attr709,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names710,
	types710,
	attr_flags710,
	gtypes710,
	(uint16) 4096,
	cn_attr710,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names711,
	types711,
	attr_flags711,
	gtypes711,
	(uint16) 4096,
	cn_attr711,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names712,
	types712,
	attr_flags712,
	gtypes712,
	(uint16) 4096,
	cn_attr712,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names713,
	types713,
	attr_flags713,
	gtypes713,
	(uint16) 4096,
	cn_attr713,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names714,
	types714,
	attr_flags714,
	gtypes714,
	(uint16) 4096,
	cn_attr714,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names715,
	types715,
	attr_flags715,
	gtypes715,
	(uint16) 0,
	cn_attr715,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"ARRAY_ITERATION_CURSOR",
	names716,
	types716,
	attr_flags716,
	gtypes716,
	(uint16) 0,
	cn_attr716,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"READABLE_INDEXABLE_ITERATION_CURSOR",
	names720,
	types720,
	attr_flags720,
	gtypes720,
	(uint16) 0,
	cn_attr720,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TYPED_INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"SPECIAL_ITERATION_CURSOR",
	names725,
	types725,
	attr_flags725,
	gtypes725,
	(uint16) 0,
	cn_attr725,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"GENERAL_SPECIAL_ITERATION_CURSOR",
	names726,
	types726,
	attr_flags726,
	gtypes726,
	(uint16) 4096,
	cn_attr726,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names727,
	types727,
	attr_flags727,
	gtypes727,
	(uint16) 8192,
	cn_attr727,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names728,
	types728,
	attr_flags728,
	gtypes728,
	(uint16) 0,
	cn_attr728,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names729,
	types729,
	attr_flags729,
	gtypes729,
	(uint16) 4096,
	cn_attr729,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names730,
	types730,
	attr_flags730,
	gtypes730,
	(uint16) 4096,
	cn_attr730,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names731,
	types731,
	attr_flags731,
	gtypes731,
	(uint16) 4096,
	cn_attr731,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names732,
	types732,
	attr_flags732,
	gtypes732,
	(uint16) 4096,
	cn_attr732,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names733,
	types733,
	attr_flags733,
	gtypes733,
	(uint16) 4096,
	cn_attr733,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names734,
	types734,
	attr_flags734,
	gtypes734,
	(uint16) 4096,
	cn_attr734,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names735,
	types735,
	attr_flags735,
	gtypes735,
	(uint16) 4096,
	cn_attr735,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names736,
	types736,
	attr_flags736,
	gtypes736,
	(uint16) 4096,
	cn_attr736,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names737,
	types737,
	attr_flags737,
	gtypes737,
	(uint16) 4096,
	cn_attr737,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names738,
	types738,
	attr_flags738,
	gtypes738,
	(uint16) 4096,
	cn_attr738,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names739,
	types739,
	attr_flags739,
	gtypes739,
	(uint16) 4096,
	cn_attr739,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names740,
	types740,
	attr_flags740,
	gtypes740,
	(uint16) 0,
	cn_attr740,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names741,
	types741,
	attr_flags741,
	gtypes741,
	(uint16) 4096,
	cn_attr741,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names742,
	types742,
	attr_flags742,
	gtypes742,
	(uint16) 4096,
	cn_attr742,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAYED_LIST_ITERATION_CURSOR",
	names743,
	types743,
	attr_flags743,
	gtypes743,
	(uint16) 0,
	cn_attr743,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names744,
	types744,
	attr_flags744,
	gtypes744,
	(uint16) 4096,
	cn_attr744,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names745,
	types745,
	attr_flags745,
	gtypes745,
	(uint16) 4096,
	cn_attr745,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names746,
	types746,
	attr_flags746,
	gtypes746,
	(uint16) 4096,
	cn_attr746,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names747,
	types747,
	attr_flags747,
	gtypes747,
	(uint16) 4096,
	cn_attr747,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names748,
	types748,
	attr_flags748,
	gtypes748,
	(uint16) 4096,
	cn_attr748,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names749,
	types749,
	attr_flags749,
	gtypes749,
	(uint16) 4096,
	cn_attr749,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names750,
	types750,
	attr_flags750,
	gtypes750,
	(uint16) 4096,
	cn_attr750,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names751,
	types751,
	attr_flags751,
	gtypes751,
	(uint16) 0,
	cn_attr751,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"ARRAY_ITERATION_CURSOR",
	names752,
	types752,
	attr_flags752,
	gtypes752,
	(uint16) 0,
	cn_attr752,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"SPECIAL_ITERATION_CURSOR",
	names755,
	types755,
	attr_flags755,
	gtypes755,
	(uint16) 0,
	cn_attr755,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"GENERAL_SPECIAL_ITERATION_CURSOR",
	names756,
	types756,
	attr_flags756,
	gtypes756,
	(uint16) 4096,
	cn_attr756,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names757,
	types757,
	attr_flags757,
	gtypes757,
	(uint16) 8192,
	cn_attr757,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names758,
	types758,
	attr_flags758,
	gtypes758,
	(uint16) 0,
	cn_attr758,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names759,
	types759,
	attr_flags759,
	gtypes759,
	(uint16) 4096,
	cn_attr759,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names760,
	types760,
	attr_flags760,
	gtypes760,
	(uint16) 4096,
	cn_attr760,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names761,
	types761,
	attr_flags761,
	gtypes761,
	(uint16) 0,
	cn_attr761,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAYED_LIST_ITERATION_CURSOR",
	names762,
	types762,
	attr_flags762,
	gtypes762,
	(uint16) 0,
	cn_attr762,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names763,
	types763,
	attr_flags763,
	gtypes763,
	(uint16) 4096,
	cn_attr763,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names764,
	types764,
	attr_flags764,
	gtypes764,
	(uint16) 4096,
	cn_attr764,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names765,
	types765,
	attr_flags765,
	gtypes765,
	(uint16) 4096,
	cn_attr765,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names766,
	types766,
	attr_flags766,
	gtypes766,
	(uint16) 4096,
	cn_attr766,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names767,
	types767,
	attr_flags767,
	gtypes767,
	(uint16) 4096,
	cn_attr767,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names768,
	types768,
	attr_flags768,
	gtypes768,
	(uint16) 4096,
	cn_attr768,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names769,
	types769,
	attr_flags769,
	gtypes769,
	(uint16) 0,
	cn_attr769,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"ARRAY_ITERATION_CURSOR",
	names770,
	types770,
	attr_flags770,
	gtypes770,
	(uint16) 0,
	cn_attr770,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names771,
	types771,
	attr_flags771,
	gtypes771,
	(uint16) 8192,
	cn_attr771,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"READABLE_INDEXABLE_ITERATION_CURSOR",
	names775,
	types775,
	attr_flags775,
	gtypes775,
	(uint16) 0,
	cn_attr775,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TYPED_INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"SPECIAL_ITERATION_CURSOR",
	names780,
	types780,
	attr_flags780,
	gtypes780,
	(uint16) 0,
	cn_attr780,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"GENERAL_SPECIAL_ITERATION_CURSOR",
	names781,
	types781,
	attr_flags781,
	gtypes781,
	(uint16) 4096,
	cn_attr781,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names782,
	types782,
	attr_flags782,
	gtypes782,
	(uint16) 8192,
	cn_attr782,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names783,
	types783,
	attr_flags783,
	gtypes783,
	(uint16) 0,
	cn_attr783,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names784,
	types784,
	attr_flags784,
	gtypes784,
	(uint16) 4096,
	cn_attr784,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names785,
	types785,
	attr_flags785,
	gtypes785,
	(uint16) 4096,
	cn_attr785,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names786,
	types786,
	attr_flags786,
	gtypes786,
	(uint16) 4096,
	cn_attr786,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names787,
	types787,
	attr_flags787,
	gtypes787,
	(uint16) 4096,
	cn_attr787,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names788,
	types788,
	attr_flags788,
	gtypes788,
	(uint16) 4096,
	cn_attr788,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names789,
	types789,
	attr_flags789,
	gtypes789,
	(uint16) 4096,
	cn_attr789,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names790,
	types790,
	attr_flags790,
	gtypes790,
	(uint16) 4096,
	cn_attr790,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names791,
	types791,
	attr_flags791,
	gtypes791,
	(uint16) 4096,
	cn_attr791,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names792,
	types792,
	attr_flags792,
	gtypes792,
	(uint16) 4096,
	cn_attr792,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names793,
	types793,
	attr_flags793,
	gtypes793,
	(uint16) 4096,
	cn_attr793,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names794,
	types794,
	attr_flags794,
	gtypes794,
	(uint16) 4096,
	cn_attr794,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names795,
	types795,
	attr_flags795,
	gtypes795,
	(uint16) 0,
	cn_attr795,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names796,
	types796,
	attr_flags796,
	gtypes796,
	(uint16) 4096,
	cn_attr796,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names797,
	types797,
	attr_flags797,
	gtypes797,
	(uint16) 4096,
	cn_attr797,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAYED_LIST_ITERATION_CURSOR",
	names798,
	types798,
	attr_flags798,
	gtypes798,
	(uint16) 0,
	cn_attr798,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names799,
	types799,
	attr_flags799,
	gtypes799,
	(uint16) 4096,
	cn_attr799,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names800,
	types800,
	attr_flags800,
	gtypes800,
	(uint16) 4096,
	cn_attr800,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names801,
	types801,
	attr_flags801,
	gtypes801,
	(uint16) 4096,
	cn_attr801,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names802,
	types802,
	attr_flags802,
	gtypes802,
	(uint16) 4096,
	cn_attr802,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names803,
	types803,
	attr_flags803,
	gtypes803,
	(uint16) 4096,
	cn_attr803,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names804,
	types804,
	attr_flags804,
	gtypes804,
	(uint16) 4096,
	cn_attr804,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names805,
	types805,
	attr_flags805,
	gtypes805,
	(uint16) 4096,
	cn_attr805,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names806,
	types806,
	attr_flags806,
	gtypes806,
	(uint16) 0,
	cn_attr806,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"ARRAY_ITERATION_CURSOR",
	names807,
	types807,
	attr_flags807,
	gtypes807,
	(uint16) 0,
	cn_attr807,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"READABLE_INDEXABLE_ITERATION_CURSOR",
	names811,
	types811,
	attr_flags811,
	gtypes811,
	(uint16) 0,
	cn_attr811,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TYPED_INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"SPECIAL_ITERATION_CURSOR",
	names816,
	types816,
	attr_flags816,
	gtypes816,
	(uint16) 0,
	cn_attr816,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"GENERAL_SPECIAL_ITERATION_CURSOR",
	names817,
	types817,
	attr_flags817,
	gtypes817,
	(uint16) 4096,
	cn_attr817,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names818,
	types818,
	attr_flags818,
	gtypes818,
	(uint16) 0,
	cn_attr818,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names819,
	types819,
	attr_flags819,
	gtypes819,
	(uint16) 4096,
	cn_attr819,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names820,
	types820,
	attr_flags820,
	gtypes820,
	(uint16) 4096,
	cn_attr820,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names821,
	types821,
	attr_flags821,
	gtypes821,
	(uint16) 4096,
	cn_attr821,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names822,
	types822,
	attr_flags822,
	gtypes822,
	(uint16) 4096,
	cn_attr822,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names823,
	types823,
	attr_flags823,
	gtypes823,
	(uint16) 4096,
	cn_attr823,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names824,
	types824,
	attr_flags824,
	gtypes824,
	(uint16) 4096,
	cn_attr824,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names825,
	types825,
	attr_flags825,
	gtypes825,
	(uint16) 4096,
	cn_attr825,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names826,
	types826,
	attr_flags826,
	gtypes826,
	(uint16) 4096,
	cn_attr826,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names827,
	types827,
	attr_flags827,
	gtypes827,
	(uint16) 4096,
	cn_attr827,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names828,
	types828,
	attr_flags828,
	gtypes828,
	(uint16) 4096,
	cn_attr828,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names829,
	types829,
	attr_flags829,
	gtypes829,
	(uint16) 4096,
	cn_attr829,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names830,
	types830,
	attr_flags830,
	gtypes830,
	(uint16) 0,
	cn_attr830,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names831,
	types831,
	attr_flags831,
	gtypes831,
	(uint16) 4096,
	cn_attr831,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names832,
	types832,
	attr_flags832,
	gtypes832,
	(uint16) 4096,
	cn_attr832,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAYED_LIST_ITERATION_CURSOR",
	names833,
	types833,
	attr_flags833,
	gtypes833,
	(uint16) 0,
	cn_attr833,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names834,
	types834,
	attr_flags834,
	gtypes834,
	(uint16) 4096,
	cn_attr834,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names835,
	types835,
	attr_flags835,
	gtypes835,
	(uint16) 4096,
	cn_attr835,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names836,
	types836,
	attr_flags836,
	gtypes836,
	(uint16) 4096,
	cn_attr836,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names837,
	types837,
	attr_flags837,
	gtypes837,
	(uint16) 4096,
	cn_attr837,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names838,
	types838,
	attr_flags838,
	gtypes838,
	(uint16) 4096,
	cn_attr838,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names839,
	types839,
	attr_flags839,
	gtypes839,
	(uint16) 4096,
	cn_attr839,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names840,
	types840,
	attr_flags840,
	gtypes840,
	(uint16) 4096,
	cn_attr840,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names841,
	types841,
	attr_flags841,
	gtypes841,
	(uint16) 0,
	cn_attr841,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"ARRAY_ITERATION_CURSOR",
	names842,
	types842,
	attr_flags842,
	gtypes842,
	(uint16) 0,
	cn_attr842,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names843,
	types843,
	attr_flags843,
	gtypes843,
	(uint16) 0,
	cn_attr843,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names844,
	types844,
	attr_flags844,
	gtypes844,
	(uint16) 8965,
	cn_attr844,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names845,
	types845,
	attr_flags845,
	gtypes845,
	(uint16) 8448,
	cn_attr845,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names846,
	types846,
	attr_flags846,
	gtypes846,
	(uint16) 8192,
	cn_attr846,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"COUNTABLE_SEQUENCE",
	names847,
	types847,
	attr_flags847,
	gtypes847,
	(uint16) 4096,
	cn_attr847,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"COUNTABLE",
	names848,
	types848,
	attr_flags848,
	gtypes848,
	(uint16) 4096,
	cn_attr848,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INFINITE",
	names849,
	types849,
	attr_flags849,
	gtypes849,
	(uint16) 4096,
	cn_attr849,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names850,
	types850,
	attr_flags850,
	gtypes850,
	(uint16) 0,
	cn_attr850,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names851,
	types851,
	attr_flags851,
	gtypes851,
	(uint16) 8965,
	cn_attr851,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names852,
	types852,
	attr_flags852,
	gtypes852,
	(uint16) 8448,
	cn_attr852,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names853,
	types853,
	attr_flags853,
	gtypes853,
	(uint16) 8192,
	cn_attr853,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names854,
	types854,
	attr_flags854,
	gtypes854,
	(uint16) 0,
	cn_attr854,
	40,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names855,
	types855,
	attr_flags855,
	gtypes855,
	(uint16) 0,
	cn_attr855,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names856,
	types856,
	attr_flags856,
	gtypes856,
	(uint16) 8965,
	cn_attr856,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names857,
	types857,
	attr_flags857,
	gtypes857,
	(uint16) 8448,
	cn_attr857,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names858,
	types858,
	attr_flags858,
	gtypes858,
	(uint16) 8192,
	cn_attr858,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names859,
	types859,
	attr_flags859,
	gtypes859,
	(uint16) 0,
	cn_attr859,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names860,
	types860,
	attr_flags860,
	gtypes860,
	(uint16) 0,
	cn_attr860,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names861,
	types861,
	attr_flags861,
	gtypes861,
	(uint16) 8965,
	cn_attr861,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names862,
	types862,
	attr_flags862,
	gtypes862,
	(uint16) 8448,
	cn_attr862,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names863,
	types863,
	attr_flags863,
	gtypes863,
	(uint16) 8192,
	cn_attr863,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names864,
	types864,
	attr_flags864,
	gtypes864,
	(uint16) 0,
	cn_attr864,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names865,
	types865,
	attr_flags865,
	gtypes865,
	(uint16) 8965,
	cn_attr865,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names866,
	types866,
	attr_flags866,
	gtypes866,
	(uint16) 8448,
	cn_attr866,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names867,
	types867,
	attr_flags867,
	gtypes867,
	(uint16) 8192,
	cn_attr867,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"READABLE_INDEXABLE_ITERATION_CURSOR",
	names871,
	types871,
	attr_flags871,
	gtypes871,
	(uint16) 0,
	cn_attr871,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TYPED_INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"SPECIAL_ITERATION_CURSOR",
	names876,
	types876,
	attr_flags876,
	gtypes876,
	(uint16) 0,
	cn_attr876,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"GENERAL_SPECIAL_ITERATION_CURSOR",
	names877,
	types877,
	attr_flags877,
	gtypes877,
	(uint16) 4096,
	cn_attr877,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names878,
	types878,
	attr_flags878,
	gtypes878,
	(uint16) 8192,
	cn_attr878,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names879,
	types879,
	attr_flags879,
	gtypes879,
	(uint16) 0,
	cn_attr879,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names880,
	types880,
	attr_flags880,
	gtypes880,
	(uint16) 4096,
	cn_attr880,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names881,
	types881,
	attr_flags881,
	gtypes881,
	(uint16) 4096,
	cn_attr881,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names882,
	types882,
	attr_flags882,
	gtypes882,
	(uint16) 4096,
	cn_attr882,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names883,
	types883,
	attr_flags883,
	gtypes883,
	(uint16) 4096,
	cn_attr883,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names884,
	types884,
	attr_flags884,
	gtypes884,
	(uint16) 4096,
	cn_attr884,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names885,
	types885,
	attr_flags885,
	gtypes885,
	(uint16) 4096,
	cn_attr885,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names886,
	types886,
	attr_flags886,
	gtypes886,
	(uint16) 4096,
	cn_attr886,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names887,
	types887,
	attr_flags887,
	gtypes887,
	(uint16) 4096,
	cn_attr887,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names888,
	types888,
	attr_flags888,
	gtypes888,
	(uint16) 4096,
	cn_attr888,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names889,
	types889,
	attr_flags889,
	gtypes889,
	(uint16) 4096,
	cn_attr889,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names890,
	types890,
	attr_flags890,
	gtypes890,
	(uint16) 4096,
	cn_attr890,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names891,
	types891,
	attr_flags891,
	gtypes891,
	(uint16) 0,
	cn_attr891,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names892,
	types892,
	attr_flags892,
	gtypes892,
	(uint16) 4096,
	cn_attr892,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names893,
	types893,
	attr_flags893,
	gtypes893,
	(uint16) 4096,
	cn_attr893,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAYED_LIST_ITERATION_CURSOR",
	names894,
	types894,
	attr_flags894,
	gtypes894,
	(uint16) 0,
	cn_attr894,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names895,
	types895,
	attr_flags895,
	gtypes895,
	(uint16) 4096,
	cn_attr895,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names896,
	types896,
	attr_flags896,
	gtypes896,
	(uint16) 4096,
	cn_attr896,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names897,
	types897,
	attr_flags897,
	gtypes897,
	(uint16) 4096,
	cn_attr897,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names898,
	types898,
	attr_flags898,
	gtypes898,
	(uint16) 4096,
	cn_attr898,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names899,
	types899,
	attr_flags899,
	gtypes899,
	(uint16) 4096,
	cn_attr899,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names900,
	types900,
	attr_flags900,
	gtypes900,
	(uint16) 4096,
	cn_attr900,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names901,
	types901,
	attr_flags901,
	gtypes901,
	(uint16) 4096,
	cn_attr901,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names902,
	types902,
	attr_flags902,
	gtypes902,
	(uint16) 0,
	cn_attr902,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"ARRAY_ITERATION_CURSOR",
	names903,
	types903,
	attr_flags903,
	gtypes903,
	(uint16) 0,
	cn_attr903,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names904,
	types904,
	attr_flags904,
	gtypes904,
	(uint16) 0,
	cn_attr904,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"LINKED_STACK",
	names905,
	types905,
	attr_flags905,
	gtypes905,
	(uint16) 0,
	cn_attr905,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 8,
	(long) 7,
	"LINKED_LIST_ITERATION_CURSOR",
	names906,
	types906,
	attr_flags906,
	gtypes906,
	(uint16) 0,
	cn_attr906,
	40,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"LINKABLE",
	names907,
	types907,
	attr_flags907,
	gtypes907,
	(uint16) 0,
	cn_attr907,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CELL",
	names908,
	types908,
	attr_flags908,
	gtypes908,
	(uint16) 0,
	cn_attr908,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"LINKED_LIST",
	names909,
	types909,
	attr_flags909,
	gtypes909,
	(uint16) 0,
	cn_attr909,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"LINKED_LIST_CURSOR",
	names910,
	types910,
	attr_flags910,
	gtypes910,
	(uint16) 0,
	cn_attr910,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"STACK",
	names911,
	types911,
	attr_flags911,
	gtypes911,
	(uint16) 4096,
	cn_attr911,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_STACK",
	names912,
	types912,
	attr_flags912,
	gtypes912,
	(uint16) 0,
	cn_attr912,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DISPENSER",
	names913,
	types913,
	attr_flags913,
	gtypes913,
	(uint16) 4096,
	cn_attr913,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"LINKED_STACK",
	names914,
	types914,
	attr_flags914,
	gtypes914,
	(uint16) 0,
	cn_attr914,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"STACK",
	names915,
	types915,
	attr_flags915,
	gtypes915,
	(uint16) 4096,
	cn_attr915,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_STACK",
	names916,
	types916,
	attr_flags916,
	gtypes916,
	(uint16) 0,
	cn_attr916,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DISPENSER",
	names917,
	types917,
	attr_flags917,
	gtypes917,
	(uint16) 4096,
	cn_attr917,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 17,
	(long) 17,
	"HASH_TABLE",
	names918,
	types918,
	attr_flags918,
	gtypes918,
	(uint16) 0,
	cn_attr918,
	80,
	4L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"HASH_TABLE_ITERATION_CURSOR",
	names919,
	types919,
	attr_flags919,
	gtypes919,
	(uint16) 0,
	cn_attr919,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names921,
	types921,
	attr_flags921,
	gtypes921,
	(uint16) 4096,
	cn_attr921,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TABLE_ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names923,
	types923,
	attr_flags923,
	gtypes923,
	(uint16) 0,
	cn_attr923,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 13,
	(long) 13,
	"PREDICATE",
	names924,
	types924,
	attr_flags924,
	gtypes924,
	(uint16) 0,
	cn_attr924,
	72,
	4L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 18,
	(long) 18,
	"STRING_TABLE",
	names925,
	types925,
	attr_flags925,
	gtypes925,
	(uint16) 0,
	cn_attr925,
	88,
	7L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CELL",
	names926,
	types926,
	attr_flags926,
	gtypes926,
	(uint16) 0,
	cn_attr926,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names927,
	types927,
	attr_flags927,
	gtypes927,
	(uint16) 0,
	cn_attr927,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names928,
	types928,
	attr_flags928,
	gtypes928,
	(uint16) 0,
	cn_attr928,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names929,
	types929,
	attr_flags929,
	gtypes929,
	(uint16) 0,
	cn_attr929,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names930,
	types930,
	attr_flags930,
	gtypes930,
	(uint16) 0,
	cn_attr930,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names931,
	types931,
	attr_flags931,
	gtypes931,
	(uint16) 0,
	cn_attr931,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names932,
	types932,
	attr_flags932,
	gtypes932,
	(uint16) 0,
	cn_attr932,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAYED_QUEUE",
	names933,
	types933,
	attr_flags933,
	gtypes933,
	(uint16) 0,
	cn_attr933,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_QUEUE_ITERATION_CURSOR",
	names934,
	types934,
	attr_flags934,
	gtypes934,
	(uint16) 0,
	cn_attr934,
	24,
	3L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names935,
	types935,
	attr_flags935,
	gtypes935,
	(uint16) 0,
	cn_attr935,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names936,
	types936,
	attr_flags936,
	gtypes936,
	(uint16) 0,
	cn_attr936,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names937,
	types937,
	attr_flags937,
	gtypes937,
	(uint16) 0,
	cn_attr937,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names938,
	types938,
	attr_flags938,
	gtypes938,
	(uint16) 0,
	cn_attr938,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names939,
	types939,
	attr_flags939,
	gtypes939,
	(uint16) 0,
	cn_attr939,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names940,
	types940,
	attr_flags940,
	gtypes940,
	(uint16) 0,
	cn_attr940,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names941,
	types941,
	attr_flags941,
	gtypes941,
	(uint16) 0,
	cn_attr941,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CELL",
	names942,
	types942,
	attr_flags942,
	gtypes942,
	(uint16) 0,
	cn_attr942,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names943,
	types943,
	attr_flags943,
	gtypes943,
	(uint16) 0,
	cn_attr943,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_STACK",
	names944,
	types944,
	attr_flags944,
	gtypes944,
	(uint16) 0,
	cn_attr944,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"STACK",
	names945,
	types945,
	attr_flags945,
	gtypes945,
	(uint16) 4096,
	cn_attr945,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names946,
	types946,
	attr_flags946,
	gtypes946,
	(uint16) 0,
	cn_attr946,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names947,
	types947,
	attr_flags947,
	gtypes947,
	(uint16) 0,
	cn_attr947,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names948,
	types948,
	attr_flags948,
	gtypes948,
	(uint16) 0,
	cn_attr948,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 18,
	(long) 18,
	"STRING_TABLE",
	names949,
	types949,
	attr_flags949,
	gtypes949,
	(uint16) 0,
	cn_attr949,
	80,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"HASH_TABLE_ITERATION_CURSOR",
	names950,
	types950,
	attr_flags950,
	gtypes950,
	(uint16) 0,
	cn_attr950,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 17,
	(long) 17,
	"HASH_TABLE",
	names952,
	types952,
	attr_flags952,
	gtypes952,
	(uint16) 0,
	cn_attr952,
	80,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names953,
	types953,
	attr_flags953,
	gtypes953,
	(uint16) 4096,
	cn_attr953,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TABLE_ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names955,
	types955,
	attr_flags955,
	gtypes955,
	(uint16) 8192,
	cn_attr955,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names956,
	types956,
	attr_flags956,
	gtypes956,
	(uint16) 0,
	cn_attr956,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names957,
	types957,
	attr_flags957,
	gtypes957,
	(uint16) 0,
	cn_attr957,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names958,
	types958,
	attr_flags958,
	gtypes958,
	(uint16) 0,
	cn_attr958,
	40,
	3L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names959,
	types959,
	attr_flags959,
	gtypes959,
	(uint16) 0,
	cn_attr959,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},};


#ifdef __cplusplus
}
#endif
