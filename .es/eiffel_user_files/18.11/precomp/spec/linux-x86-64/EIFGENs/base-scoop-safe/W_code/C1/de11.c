/*
 * Code for class DECLARATOR
 */

#include "eif_eiffel.h"
#include "../E1/estructure.h"


#ifdef __cplusplus
extern "C" {
#endif

extern EIF_TYPED_VALUE F11_242(EIF_REFERENCE);
extern EIF_TYPED_VALUE F11_243(EIF_REFERENCE);
extern EIF_TYPED_VALUE F11_244(EIF_REFERENCE);
extern EIF_TYPED_VALUE F11_245(EIF_REFERENCE);
extern EIF_TYPED_VALUE F11_246(EIF_REFERENCE);
extern EIF_TYPED_VALUE F11_247(EIF_REFERENCE);
extern EIF_TYPED_VALUE F11_248(EIF_REFERENCE);
extern EIF_TYPED_VALUE F11_249(EIF_REFERENCE);
extern EIF_TYPED_VALUE F11_250(EIF_REFERENCE);
extern EIF_TYPED_VALUE F11_251(EIF_REFERENCE);
extern EIF_TYPED_VALUE F11_252(EIF_REFERENCE);
extern EIF_TYPED_VALUE F11_253(EIF_REFERENCE);
extern EIF_TYPED_VALUE F11_254(EIF_REFERENCE);
extern EIF_TYPED_VALUE F11_255(EIF_REFERENCE);
extern void EIF_Minit11(void);

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* {DECLARATOR}.s1 */
EIF_TYPED_VALUE F11_242 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_REF;
	r.it_r = *(EIF_REFERENCE *)(Current + RTWA(246,Dtype(Current)));
	return r;
}


/* {DECLARATOR}.s1_2 */
EIF_TYPED_VALUE F11_243 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_REF;
	r.it_r = *(EIF_REFERENCE *)(Current + RTWA(247,Dtype(Current)));
	return r;
}


/* {DECLARATOR}.s1_3 */
EIF_TYPED_VALUE F11_244 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_REF;
	r.it_r = *(EIF_REFERENCE *)(Current + RTWA(248,Dtype(Current)));
	return r;
}


/* {DECLARATOR}.s1_4 */
EIF_TYPED_VALUE F11_245 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_REF;
	r.it_r = *(EIF_REFERENCE *)(Current + RTWA(249,Dtype(Current)));
	return r;
}


/* {DECLARATOR}.s2 */
EIF_TYPED_VALUE F11_246 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_REF;
	r.it_r = *(EIF_REFERENCE *)(Current + RTWA(250,Dtype(Current)));
	return r;
}


/* {DECLARATOR}.s3 */
EIF_TYPED_VALUE F11_247 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_REF;
	r.it_r = *(EIF_REFERENCE *)(Current + RTWA(251,Dtype(Current)));
	return r;
}


/* {DECLARATOR}.s4 */
EIF_TYPED_VALUE F11_248 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_REF;
	r.it_r = *(EIF_REFERENCE *)(Current + RTWA(252,Dtype(Current)));
	return r;
}


/* {DECLARATOR}.s5 */
EIF_TYPED_VALUE F11_249 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_REF;
	r.it_r = *(EIF_REFERENCE *)(Current + RTWA(253,Dtype(Current)));
	return r;
}


/* {DECLARATOR}.s6 */
EIF_TYPED_VALUE F11_250 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_REF;
	r.it_r = *(EIF_REFERENCE *)(Current + RTWA(254,Dtype(Current)));
	return r;
}


/* {DECLARATOR}.s7 */
EIF_TYPED_VALUE F11_251 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_REF;
	r.it_r = *(EIF_REFERENCE *)(Current + RTWA(255,Dtype(Current)));
	return r;
}


/* {DECLARATOR}.s8 */
EIF_TYPED_VALUE F11_252 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_REF;
	r.it_r = *(EIF_REFERENCE *)(Current + RTWA(256,Dtype(Current)));
	return r;
}


/* {DECLARATOR}.s8_2 */
EIF_TYPED_VALUE F11_253 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_REF;
	r.it_r = *(EIF_REFERENCE *)(Current + RTWA(257,Dtype(Current)));
	return r;
}


/* {DECLARATOR}.s8_3 */
EIF_TYPED_VALUE F11_254 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_REF;
	r.it_r = *(EIF_REFERENCE *)(Current + RTWA(258,Dtype(Current)));
	return r;
}


/* {DECLARATOR}.s8_4 */
EIF_TYPED_VALUE F11_255 (EIF_REFERENCE Current)
{
	EIF_TYPED_VALUE r;
	r.type = SK_REF;
	r.it_r = *(EIF_REFERENCE *)(Current + RTWA(259,Dtype(Current)));
	return r;
}


void EIF_Minit11 (void)
{
	GTCX
}


#ifdef __cplusplus
}
#endif
